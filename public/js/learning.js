var html = "";
var inc  = 1;

function getNew(id)
{
    html = '<div class="block block_'+id+'">';
        html += '<div class="block-content" style="padding: 18px;">';
            html += '<div class="row">';
                html += '<div class="col-sm-8">';
                    html += '<div class="form-group">';
                        html += '<div class="form-material">';
                            html += '<input type="text" class="form-control" placeholder="Learning code" autofocus="" name="learning_code[]" required>';
                            html += '<label for="" class="control-label">Code</label>';
                        html += '</div>';
                    html += '</div>';
                    html += '<div class="form-group">';
                        html += '<div class="form-material">';
                            html += '<textarea name="learning_question[]" required cols="30" rows="3" class="form-control" placeholder="Question"></textarea>';
                            html += '<label for="" class="control-label">Question</label>';
                        html += '</div>';
                    html += '</div>';
                    html += '<div class="block-answer_'+ id +'" data-id="'+ id +'">';
                        html += '<div class="row mt-3">';
                            html += '<div class="col-sm-5 form-material">';
                                html += '<select name="learning_rating_start[]" class="form-control">';
                                    html += '<option value="0">0</option>';
                                    html += '<option value="1" selected>1</option>';
                                html += '</select>'; 
                            html += '</div>';
                            html += '<div class="col-sm-2 form-material text-center">';
                                html += 'To';
                            html += '</div>';
                            html += '<div class="col-sm-5 form-material">';
                                html += '<select name="learning_rating_finish[]" class="form-control">';
                                    html += '<option value="2">2</option>';
                                    html += '<option value="3">3</option>';
                                    html += '<option value="4">4</option>';
                                    html += '<option value="5">5</option>';
                                    html += '<option value="6">6</option>';
                                    html += '<option value="7">7</option>';
                                    html += '<option value="8">8</option>';
                                    html += '<option value="9">9</option>';
                                    html += '<option value="10">10</option>';
                                html += '</select>'; 
                            html += '</div>';
                        html += '</div>';
                        html += '<div class="form-group row mt-3">';
                            html += '<div class="col-sm-5">';
                                html += '<div class="input-group form-material">';
                                    html += '<span class="input-group-addon">1</span>';
                                    html += '<input type="text" class="form-control learning_answer_'+ id +'" name="learning_answer_'+ id +'[]" placeholder="Label (optional)">';
                                html += '</div>';
                                html += '<div class="input-group mt-3 form-material">';
                                    html += '<span class="input-group-addon">5</span>';
                                    html += '<input type="text" class="form-control learning_answer_'+ id +'" name="learning_answer_'+ id +'[]" placeholder="Label (optional)">';
                                html += '</div>';
                            html += '</div>';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
            html += '<div class="col-sm-4">';
                html += '<div class="form-group">';
                    html += '<select name="type_id[]" class="form-control" onchange="getAnswer(this);" data-id="'+ id +'" required>';
                        html += '<option value="">Type Answer</option>';
                        html += type;
                    html += '</select>';
                html += '</div>';
                html += '<div class="form-group">';
                    html += '<select name="group_id[]" class="form-control" required>';
                        html += '<option value="">Question Group</option>';
                        html += group;
                    html += '</select>';
               html += '</div>';
               html += '<div class="form-group hide" id="trainning_id_'+id+'">';
                    html += '<select name="trainning_id[]" class="form-control" required>';
                        html += '<option value="">Trainning</option>';
                        html += trainning;
                    html += '</select>';
                html += '</div>';
            html += '</div>';
            html += '<div class="col-sm-12 text-right">';
                html += '<hr style="height: 10px">';
                html += '<button type="button" class="btn btn-alt-primary" onclick="duplicate(this);" data-id="'+ id +'"><i class="fa fa-copy"></i></button> ';
                html += '<button type="button" class="btn btn-alt-danger" onclick="removeBlock(this);" data-id="'+ id +'"><i class="fa fa-trash"></i></button> ';
                html += ' | ';
                html += '<label class="css-control css-control-sm css-control-primary css-switch">';
                    html += '<input type="checkbox" name="learning_is_required[]" class="css-control-input" checked>';
                    html += '<span class="css-control-indicator"></span> Required';
                html += '</label>';
                html += ' | ';
                html += '<label class="css-control css-control-sm css-control-primary css-switch">';
                    html += '<input type="checkbox" name="learning_is_default[]" class="css-control-input" data-id="'+id+'" checked onchange="getTrainning(this)">';
                    html += '<span class="css-control-indicator"></span> Default';
                html += '</label>';
            html += '</div>';
        html += '</div>';

    return html;
}

function getNewField(id){
  html = '<div class="row block_field_'+id+'">';    
    html += '<div class="col-sm-10">';
        html += '<div class="form-material" style="padding-top:0px">';
            html += '<input type="text" class="form-control learning_answer_'+ id +'" name="learning_answer[]" placeholder="Answer option">';                            
        html += '</div>';
    html += '</div>';
      html += '<input type="radio" name="learning_correct_answer" value="'+ id +'">';
    html += '<div class="col-sm-1"><button type="button" class="btn btn-sm btn-danger" onclick="removeField(this);" data-id="'+ id +'"><i class="fa fa-minus"></i></button></div>';
    html += '<br>';
  html += '</div>';
  return html;
}

function getMultipleChoice(inc){
  $html = '<div class="row">';
    $html += '<div class="col-sm-2">';
      $html += '<button type="button" class="btn btn-sm btn-success" onclick="addField();"><i class="fa fa-plus"></i> Add Answer</button>';
    $html += "</div>"
    $html += "<div class='clearfix'></div>"
    $html += '<div id="block-question-field" class="col-lg-8">';        
    $html += '</div>';      
  $html += '</div>';  
  $html += '<div class="row mt-3">';
      $html += '<div class="col-sm-12">';
          $html += '<div class="form-material" style="padding-top:5px">';
              // $html += '<input type="text" class="form-control learning_correct_answer_'+ inc +'" name="learning_correct_answer_'+inc+'[]" placeholder="Kunci jawaban (Tulis salah satu jawaban yang ada di list)">';              
          $html += '</div>';
      $html += '</div>';
  $html += '</div>';
  return $html;  
}

function getRating(id)
{
    html = '<div class="row mt-3">';
            html += '<div class="col-sm-5 form-material">';
                html += '<select name="learning_rating_start[]" class="form-control">';
                    html += '<option value="0">0</option>';
                    html += '<option value="1" selected>1</option>';
                html += '</select>'; 
            html += '</div>';
            html += '<div class="col-sm-2 form-material text-center">';
                html += 'To';
            html += '</div>';
            html += '<div class="col-sm-5 form-material">';
                html += '<select name="learning_rating_finish[]" class="form-control">';
                    html += '<option value="2">2</option>';
                    html += '<option value="3">3</option>';
                    html += '<option value="4">4</option>';
                    html += '<option value="5">5</option>';
                    html += '<option value="6">6</option>';
                    html += '<option value="7">7</option>';
                    html += '<option value="8">8</option>';
                    html += '<option value="9">9</option>';
                    html += '<option value="10">10</option>';
                html += '</select>'; 
            html += '</div>';
        html += '</div>';
        html += '<div class="form-group row mt-3">';
            html += '<div class="col-sm-5">';
                html += '<div class="input-group form-material">';
                    html += '<span class="input-group-addon">1</span>';
                    html += '<input type="text" class="form-control learning_answer_'+ id +'" name="learning_answer_'+ id +'[]" placeholder="Label (optional)">';
                html += '</div>';
                html += '<div class="input-group mt-3 form-material">';
                    html += '<span class="input-group-addon">5</span>';
                    html += '<input type="text" class="form-control learning_answer_'+ id +'" name="learning_answer_'+ id +'[]" placeholder="Label (optional)">';
                html += '</div>';
            html += '</div>';
        html += '</div>';

    return html;
}

function getTrainning(item) {
    var _this   = $(item);
    var id      = _this.data('id');

    if(!_this.is(":checked")){
        $('#trainning_id_'+id).removeClass('hide');
        $('#session_id_'+id).removeClass('hide');
        $('#module_id_'+id).removeClass('hide');
    } else {
        $('#trainning_id_'+id).addClass('hide');
        $('#session_id_'+id).addClass('hide');
        $('#module_id_'+id).addClass('hide');
    }
}

function getShort(id) {

    $html = '<div class="row mt-3">';
        $html += '<div class="col-sm-12">';
            $html += '<div class="form-material">';
                $html += '<input type="text" class="form-control learning_answer_'+ inc +'" name="learning_answer_'+ inc +'[]" placeholder="Short-answer text">';
                $html += '<label class="control-label">Short Answer</label>';
            $html += '</div>';
        $html += '</div>';
    $html += '</div>';

    return $html;
}


function getAnswer(item) {
    var _this   = $(item);
    var id      = _this.data('id');
    var answer  = "";

    if (_this.val() == "rating") {
        answer = getRating(id);
    } else if(_this.val() == "short_answer") {
        answer = getShort(id);
    }else if(_this.val() == "multiple_choice"){
      answer = getMultipleChoice(id);
    }

    $('.block-answer_' + id).html(answer);
}

function add() {
    
    inc = inc + 1;
    var html = getNew(inc);

    $("#block-question").append(html);
}

function addField(){
  inc = inc + 1;
  var html = getNewField(inc);

  $("#block-question-field").append(html);
}

function removeBlock(item) {
    var _this   = $(item);
    var id      = _this.data('id');

    inc = inc - 1;

    $('.block_' + id).remove();
}

function removeField(item){
  var _this   = $(item);
  var id      = _this.data('id');

  inc = inc - 1;

  $('.block_field_' + id).remove(); 
}

function duplicate(item) {
    var _this   = $(item);
    var id      = _this.data('id');

    inc = inc + 1;

    var block = $(".block_" + id).clone().attr('class', 'block block_'+inc ).insertAfter(".block_" + id);

    block.find('.block-answer_'+id).attr('class', 'block-answer_'+inc );
    block.find('input, button, select').attr('data-id', inc);
    block.find('.learning_answer_'+id).attr('name', 'learning_answer_'+inc+'[]').attr('class', 'form-control learning_answer_'+inc);
}