<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use File;
use PhpOffice\PhpWord\Shared\ZipArchive;

use App\Model\Behaviour\Story;
use App\Model\Behaviour\StoryDetail;
use App\Model\Behaviour\StoryResult;
use App\Model\Behaviour\StoryResultWord;
use App\Model\Behaviour\StoryResultUnword;
use App\Model\Master\Word;
use App\Model\Master\WordDetail;
use App\Model\Master\Formulation;
use App\Model\Master\WordCategory;

class ProcessStory implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $storyId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($storyId)
    {
        $this->storyId = $storyId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $story = Story::find($this->storyId);

        foreach ($story->details as $detail) {
            switch ($detail->filename) {
                case 'default':
                    $content = strtolower($this->modifyContent(trim($detail->story)));

                    // 90%
                    $result = $this->result($story, trim($content));

                    // 10%
                    $this->insertUndefinedWords($detail->id, $content);

                    $updateProgress = StoryDetail::find($detail->id);
                    $updateProgress->progress += 10;

                    if ($result['status'] == 200)
                        $updateProgress->story_after = trim($content);

                    $updateProgress->save();
                    break;
                default:
                    // Read the files
                    $ext = explode('.', $detail->filename);
                    $ext = end($ext);

                    $content = $this->readByExtension($detail->story, $detail->filename, $ext);
                    $detail->story = $content;

                    $content = strtolower($this->modifyContent(trim($content)));

                    // 90%
                    $result = $this->result($story, trim($content));

                    // 10%
                    $this->insertUndefinedWords($detail->id, $content);

                    $updateProgress = StoryDetail::find($detail->id);
                    $updateProgress->story = $detail->story;
                    $updateProgress->progress += 10;

                    if ($result['status'] == 200)
                        $updateProgress->story_after = trim($content);

                    $updateProgress->save();
                    break;
            }
        }
    }

    /**
     * Identify Story
     * @param $story
     * @return string
     */
    private function result($story, $content)
    {
        $countedCategories = [];
        $allCats = [];

        $storyTitle = $story->title;
        $storyDetail = $story->detail;

        $storyDetailId = $storyDetail->id;

        $moduleFormulations = $story->module->behaviour_formulations;


        // Split Story to Words
        $storyWords = explode(' ', $content);
        $totalStoryWord = count($storyWords);
        $formulas = count($moduleFormulations) ? $moduleFormulations : Formulation::where('status', 1)->get();

        $countFormula = 1;

        // Get Formulas that active
        foreach ($formulas as $_formula) {
            if (count($moduleFormulations))
                $_formula = $_formula->formulation;

            $story = ' ' . $content . ' ';
            $countedWords = [];
            $totalStoryWordPerFormula = $totalStoryWord;
            $_storyWords = $storyWords;
            $bobots = [];
            $konstantas = [];

            $cats = $bobot = $konstanta = [];
            foreach ($_formula->detail as $formulaDetail) {
                $cats[] = $formulaDetail->cat_id;
                $bobot[] = $formulaDetail->bobot;
                $konstanta[] = $formulaDetail->konstanta;
            }

            $a = 0;
            foreach ($cats as $cat) {
                $bobots[$cat] = $bobot[$a] * $konstanta[$a];
                $a++;
            }

            $allCats = array_merge($allCats, $cats);

            // Get Words by categories that registered in formula
            $words = Word::whereHas('detail', function ($query) use ($cats) {
                $query->whereIn('cat_id', $cats);
            })->get();

            $totalWordScore = 0;
            if ($words) {
                // Get Word
                foreach ($words as $_word) {

                    if (in_array($_word->name, $countedWords))
                        continue;

                    $individualCountWord = 0;
                    $_storyWords = array_values($_storyWords);

                    $individualCountWord = preg_match_all("/\s" . preg_quote($_word->name, '/*') . "\s/i", $story);

                    if ($individualCountWord != 0 && !in_array($_word->name, $countedWords)) {
                        
                        $newCat = [];
                        foreach ($_word->detail as $wordDetail)
                            $newCat[] = $wordDetail->cat_id;

                        $newBobot = 0;
                        foreach ($newCat as $cat)
                            if (!empty($bobots[$cat]))
                                $newBobot += $bobots[$cat];

                        $totalWordScore += ($newBobot / 100 ) * $individualCountWord;
                    
                        $countedWords[] = $_word->name;

                        // Check the word is combined words
                        if ($_word->is_combined_words == 1) {
                            $combinedWord = explode(' ', $_word->name);
                            $story = preg_replace("/" . preg_quote($_word->name, '/*') . "\s/i", ' ', $story);
                            $totalStoryWordPerFormula -= (count($combinedWord) - 1);
                        }
                    }
                }

                $countedCategories = array_merge($countedCategories, $cats);
            } else {
                // Tidak ada kata dalam kategori
            }

            if ($storyTitle) {
                // Update Counter
                $storyDetail->progress = $countFormula / count($formulas) * 90;
                $storyDetail->save();
            }

            $storyResults[] = [
                'story_detail_id' => $storyDetailId,
                'formula_id' => $_formula->id,
                'formula_name' => $_formula->name,
                'score' => ($totalWordScore / $totalStoryWordPerFormula * 100)
            ];

            $countFormula++;
        }

        $this->countWords(array_unique($allCats), $storyWords, $storyDetailId);

        // Count category
        if ($countedCategories) {
            $countedCategories = array_unique($countedCategories);
            foreach ($countedCategories as $_cat) {
                $countCategory = WordCategory::find($_cat);
                $countCategory->count += 1;
                $countCategory->save();
            }
        }

        StoryResult::insert($storyResults);
        return [
            'status' => 200,
            'message' => '',
            'data' => $storyResults
        ];
    }

    /**
     * Modify story, split every text types using asterik symbol.
     * @param $story
     * @return string
     */
    private function modifyContent($story)
    {
        $undefinedWords = [];
        $definedWords = [];

        // Remove Special Character
        $story = preg_replace('/[^A-Za-z0-9\- ]/', ' ', $story);


        // Split Story to Words
        $storyWords = explode(' ', preg_replace('!\s+!', ' ', trim($story)));
        $storyWords = array_map('strtolower', $storyWords);
        $countStoryWords = array_count_values($storyWords);

        $unWords = Word::whereIn('name', $storyWords)->get();

        if ($unWords)
            foreach ($unWords as $unWord)
                $definedWords[] = $unWord->name;

        $storyWords = array_unique($storyWords);
        $definedWords = array_unique($definedWords);
        $undefinedWords = array_values(array_diff($storyWords, $definedWords));

        $imbuhan = Word::select('name')->where('name', 'like', '%*%')->get();

        $newStory = preg_replace('!\s+!', ' ', $story);

        // foreach ($imbuhan as $_i) {
        //  // Ambil Depan
        //  echo $_i->name . ' ' . substr($_i->name, 0, 1) . '<br/>';

        //  // Ambil Belakang
        //  echo $_i->name . ' ' . substr($_i->name, -1) . '<br/>';
        // }

        if ($undefinedWords) {
            foreach ($undefinedWords as $_word) {
                $originalWord = $_word;
                if (!empty($_word)) {
                    $should_insert = true;
                    $splitWord = $_word;

                    // Split Imbuhan
                    foreach ($imbuhan as $_i) {
                        if (!preg_match("/[a-z]/i", $_i->name)) continue;

                        $_i->name = trim($_i->name);

                        $_i2 = str_replace('*', '', $_i->name);

                        if (substr($_i->name, 0, 1) == '*' && substr($_i->name, -1) == '*') { // Both
                            $condition1 = substr($_word, 0, strlen($_i2));
                            $condition2 = substr($_word, -strlen($_i2));
                            $type = 'both';
                        } else if (substr($_i->name, 0, 1) == '*') { // Last Word
                            $condition = substr($_word, -strlen($_i2));
                            $type = 'last';
                        } else if (substr($_i->name, -1) == '*') { // First Word
                            $condition = substr($_word, 0, strlen($_i2));
                            $type = 'first';
                        } else continue;

                        if ($type == 'both') {
                            if ($condition1 == $_i2) {
                                $newSentence = ' *' . $_i2 . '* ' . substr($_word, strlen($_i2), strlen($_word));
                                $halfSentence = substr($_word, strlen($_i2), strlen($_word));
                                $splitWord = $this->findSpecificWord($splitWord, $_word, $newSentence);
                                $_word = $halfSentence;
                            }

                            if ($condition2 == $_i2) {
                                $newSentence = substr($_word, 0, strlen($_word) - strlen($_i2)) . ' *' . $_i2 . '* ';
                                $halfSentence = substr($_word, 0, strlen($_word) - strlen($_i2));
                                $splitWord = $this->findSpecificWord($splitWord, $_word, $newSentence);
                                $_word = $halfSentence;
                            }
                        } else {
                            if ($condition == $_i2) {
                                if ($type == 'first') {
                                    $newSentence = $_i2 . '* ' . substr($_word, strlen($_i2), strlen($_word));
                                    $halfSentence = substr($_word, strlen($_i2), strlen($_word));
                                    $splitWord = $this->findSpecificWord($splitWord, $_word, $newSentence);
                                    $_word = $halfSentence;
                                } else if ($type == 'last') {   
                                    $newSentence = substr($_word, 0, strlen($_word) - strlen($_i2)) . ' *' . $_i2;
                                    $halfSentence = substr($_word, 0, strlen($_word) - strlen($_i2));
                                    $splitWord = $this->findSpecificWord($splitWord, $_word, $newSentence);
                                    $_word = $halfSentence;
                                } else continue;
                            }
                        }
                    }

                    $newStory = $this->findSpecificWord($newStory, $originalWord, $splitWord);
                }
            }
        }

        return $newStory;
    }

    private function findSpecificWord($newStory, $_word, $newSentence)
    {
        $words = [];
        foreach (explode(' ', $newStory) as $word) {
            if ($word == $_word) {
                $words[] = $newSentence;
            } else $words[] = $word;
        }

        return implode(' ', $words);
    }

    private function countWords($cats, $storyWords, $storyDetailId)
    {
        $words = Word::whereHas('detail', function ($query) {
            $query->where('cat_id', '!=', 1);
        })->get();

        if ($words) {
            // Get Word
            foreach ($words as $_word) {
                $individualCountWord = 0;
                $_storyWords = array_values($storyWords);

                // If the word is combined words
                if ($_word->is_combined_words == 1) {
                    $combinedWord = explode(' ', $_word->name);

                    // Get word from story
                    $countStoryWords = count($_storyWords);
                    for ($i = 0; $i < $countStoryWords; $i++) {
                        // If the first letter of combined word matches with word from story, check the word next it
                        if (strtolower($_storyWords[$i]) == $combinedWord[0]) {
                            $match = true;
                            for ($j = 1; $j < count($combinedWord); $j++) {
                                // Check the word next it
                                if (!empty($_storyWords[$i + $j]) && strtolower($_storyWords[$i + $j]) == $combinedWord[$j]) {
                                    continue;
                                } else {
                                    $match = false;
                                    break;
                                }
                            }

                            // If combine word matches, unset from array
                            if ($match) {
                                $first = true;
                                for ($j = 0; $j < count($combinedWord); $j++) {
                                    unset($_storyWords[$i]);
                                    if ($first) $first = false;
                                    $i++;
                                }

                                $i--;
                                $individualCountWord++;
                            }
                        }
                    }
                } else {
                    // Get word from story
                    $countStoryWords = count($_storyWords);
                    for ($i = 0; $i < $countStoryWords; $i++) {
                        if (strtolower($_storyWords[$i]) == $_word->name) {
                            unset($_storyWords[$i]);
                            $individualCountWord++;
                        }
                    }
                }

                if ($individualCountWord != 0) {
                    // Update word score
                    $wordUpdate = Word::find($_word->id);
                    $wordUpdate->count += $individualCountWord;
                    $wordUpdate->save();

                    $resultWord = StoryResultWord::where(['story_detail_id' => $storyDetailId, 'word_id' => $_word->id])->first();

                    if ($resultWord) {
                        $resultWord->score += $individualCountWord;
                        $resultWord->save();
                    } else {
                        StoryResultWord::create([
                            'story_detail_id' => $storyDetailId,
                            'word_id' => $_word->id,
                            'score' => $individualCountWord
                        ]);
                    }
                }
            }
        }
    }

    private function insertUndefinedWords($story_detail_id, $story)
    {
        $undefinedWords = [];
        $definedWords = [];

        $insert = [];

        // Split Story to Words
        $storyWords = explode(' ', preg_replace('!\s+!', ' ', $story));
        $storyWords = array_map('strtolower', $storyWords);
        $countStoryWords = array_count_values($storyWords);

        $unWords = Word::whereIn('name', $storyWords)->get();
        if ($unWords) {
            $i = 0;
            foreach ($unWords as $unWord) {
                // Unidentified Word
                if ($unWord->cat_id == 1) {
                    $unwordUpdate = Word::find($unWord->id);
                    $unwordUpdate->count += $countStoryWords[$unWord->name];
                    $unwordUpdate->save();
                    $insert[] = [
                        'story_detail_id' => $storyDetailId,
                        'word_id' => $unWord->id,
                        'score' => $countStoryWords[$unWord->name]
                    ];
                }
                $definedWords[] = $unWord->name;
            }
        }

        $undefinedWords = array_values(array_diff($storyWords, $definedWords));
        if (count($undefinedWords) != 0) {
            foreach ($undefinedWords as $_word) {
                if (!empty($_word)) {
                    $data = [
                        'name' => $_word,
                        'is_combined_word' => 0,
                        'count' => (!empty($countStoryWords[$_word]) ? $countStoryWords[$_word] : 0)
                    ];

                    $inputWord = Word::create($data);
                    $inputWordId = $inputWord->id;
                    WordDetail::create([
                        'word_id' => $inputWordId,
                        'cat_id' => 1
                    ]);

                    $insert[] = [
                        'story_detail_id' => $story_detail_id,
                        'word_id' => $inputWordId,
                        'score' => $countStoryWords[$_word]
                    ];
                }
            }
        }

        if (!empty($insert))
            StoryResultUnword::insert($insert);
    }

    private function readByExtension($path, $filename, $ext)
    {
        switch ($ext) {
            case 'txt':
                $file = File::get($path);
                $string = preg_split('/\s+/', $file);
                return strip_tags(implode(' ', $string));
                break;
            case 'doc':
                if (($fh = fopen($path, 'rb')) !== false) {
                    $headers = fread($fh, 0xA00);

                    // read doc from 0 to 255 characters
                    $n1 = (ord($headers[0x21C]) - 1);

                    // read doc from 256 to 63743 characters
                    $n2 = ((ord($headers[0x21D]) - 8) * 256);

                    // read doc from 63744 to 16775423 characters
                    $n3 = ((ord($headers[0x21E]) * 256) * 256);

                    //read doc from 16775424 to 4294965504 characters
                    $n4 = (((ord($headers[0x21F]) * 256) * 256) * 256);

                    // Total length of text in the document
                    $textLength = ($n1 + $n2 + $n3 + $n4);
                    ini_set('memory_limit', '-1');
                    $extractedPlaintext = fread($fh, $textLength);
                    return strip_tags($extractedPlaintext);
                }
                break;
            case 'docx':
                $zip = new ZipArchive;
                // | ZIPARCHIVE::OVERWRITE
                if (true === $zip->open($path)) {
                    // If successful, search for the data file in the archive
                    if (($index = $zip->locateName("word/document.xml")) !== false) {
                        // Index found! Now read it to a string
                        $text = $zip->getFromIndex($index);

                        // Load XML from a string
                        // Ignore errors and warnings
                        $xml = new \DOMDocument();
                        $xml->loadXML($text, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);
                        
                        // Remove XML formatting tags and return the text
                        $content = $xml->saveXML();

                        $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
                        $content = str_replace('</w:r></w:p>', " ", $content);
                        // $content = str_replace('</w:r></w:p>', "\r\n", $content);
                        return strip_tags($content);
                    }
                    $zip->close();
                }
                break;
            default:
                return 'undefined';
                break;
        }
    }
}
