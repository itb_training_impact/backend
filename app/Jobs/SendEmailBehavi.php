<?php

namespace App\Jobs;

use App\User;
use App\Model\Master\Group;
use App\Model\Master\Boss;
use App\Model\Master\Partner;
use App\Model\Master\Subordinate;
use App\Model\Trainning\Module;
use App\Model\Trainning\Session;
use App\Model\Trainning\Trainning;
use Illuminate\Support\Facades\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmailBehavi implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $id = null;
    private $field = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $field)
    {
        $this->id = $id;
        $this->field = $field;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $id = $this->id;
        $field = $this->field;
  

        $model      = Module::find($id);
        $session    = Session::where('session_id',$model->session_id)->first();
        $training   = Trainning::where('trainning_id',$session->trainning_id)->first();

        $participant    = explode(',',$training['participant_id']);
        $users          = User::whereIn('id',$participant)->get();
        $boss           = Boss::whereIn('user_id',$participant)->get();
        $partners       = Partner::whereIn('user_id',$participant)->get();
        $subordinates   = Subordinate::whereIn('user_id',$participant)->get();

        foreach ($users as $user){
            Mail::send('email.notifyTraining', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'field'=>$field], function($message) use ($user)
            {    
                $message->to($user->email)->subject('NOTIFICATION START TRAINING');    
            });
        }
        foreach($boss as $boss){
            $user = User::where('id',$boss->user_id)->get();
            foreach ($user as $user){
                Mail::send('email.notifyTrainingBoss', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'boss'=>$boss,'field'=>$field], function($message) use ($boss)
                {    
                    $message->to($boss->boss_email)->subject('NOTIFICATION START TRAINING');    
                });
            }
        }
        foreach($partners as $partner){
            $user = User::where('id',$partner->user_id)->get();
            foreach ($user as $user){
                Mail::send('email.notifyTrainingPartner', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'partner'=>$partner,'field'=>$field], function($message) use ($partner)
                {    
                    $message->to($partner->partner_email)->subject('NOTIFICATION START TRAINING');    
                });
            }
        }
        foreach($subordinates as $subordinate){
            $user = User::where('id',$subordinate->user_id)->get();
            foreach ($user as $user){
                Mail::send('email.notifyTrainingSubordinate', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'subordinate'=>$subordinate,'field'=>$field], function($message) use ($subordinate)
                {    
                    $message->to($subordinate->subordinate_email)->subject('NOTIFICATION START TRAINING');    
                });
            }
        }
    }
}
