<?php

namespace App\Jobs;

use App\User;
use App\Model\Master\Group;
use App\Model\Master\Boss;
use App\Model\Master\Partner;
use App\Model\Master\Subordinate;
use App\Model\Trainning\Module;
use App\Model\Trainning\ModuleQuestion as Questions;
use App\Model\Trainning\Session;
use App\Model\Trainning\Trainning;
use Illuminate\Support\Facades\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmailPerfo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $id = null;
    private $field = null;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $field)
    {
        $this->id = $id;
        $this->field = $field;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $id = $this->id;
        $field = $this->field;

        $model      = Module::find($id);
        $session    = Session::where('session_id',$model->session_id)->first();
        $training   = Trainning::where('trainning_id',$session->trainning_id)->first();
        $questions  = Questions::WhereIn('module_id',$model)->get();

        $participant    = explode(',',$training['participant_id']);
        $users          = User::whereIn('id',$participant)->get();
        $boss           = Boss::whereIn('user_id',$participant)->get();
        $partners       = Partner::whereIn('user_id',$participant)->get();
        $subordinates   = Subordinate::whereIn('user_id',$participant)->get();

            foreach($boss as $boss){
                $user = User::where('id',$boss->user_id)->get();
                foreach ($user as $user){
                    Mail::send('email.notifyTrainingPerfo', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'boss'=>$boss,'field'=>$field], function($message) use ($boss)
                    {    
                        $message->to($boss->boss_email)->subject('NOTIFICATION START TRAINING');    
                    });
                }
            }

           
    }
}
