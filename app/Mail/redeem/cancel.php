<?php

namespace App\Mail\Redeem;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class cancel extends Mailable
{
    use Queueable, SerializesModels;

    public $main;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($main)
    {
        $this->main = $main;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Permohonan redeem ditolak untuk produk ' . $this->main->product_name)
        ->view('mails.redeem.cancel');
        // ->with([
        //     'testVarOne' => '1',
        //     'testVarTwo' => '2',
        // ]);
        // ->attach(public_path('/images').'/demo.jpg', [
        //       'as' => 'demo.jpg',
        //       'mime' => 'image/jpeg',
        // ]);
    }
}
