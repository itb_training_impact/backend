<?php

namespace App\Mail\order;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class confirm extends Mailable
{
    use Queueable, SerializesModels;

    public $main;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($main)
    {
        $this->main = $main;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Pembayaran telah diterima untuk pesanan ' . $this->main->id)
        ->view('mails.order.confirm');
        // ->with([
        //     'testVarOne' => '1',
        //     'testVarTwo' => '2',
        // ]);
        // ->attach(public_path('/images').'/demo.jpg', [
        //       'as' => 'demo.jpg',
        //       'mime' => 'image/jpeg',
        // ]);
    }
}
