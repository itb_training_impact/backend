<?php

namespace App\Imports;
use Illuminate\Support\Collection;
use App\Model\Trainning\ModuleQuestion;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Model\Trainning\Company;
use App\Model\Master\Boss;
use App\Model\Master\Partner;
use App\Model\Master\Subordinate;
use App\User;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\Importable;


class ParticipantImport implements ToCollection, WithStartRow, WithValidation
{
     use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
     public function rules(): array
    {
        return [
           '0' => 'required',
           '1' => 'required',   
           '2' => 'required|email|unique:users,email',  
        ];
    }

    public function customValidationAttributes()
    {
    return [
        '0' => 'name',
        '1' => 'username',
        '2' => 'email'
        ];
    }
   public function customValidationMessages()
    {
    return [
        '0.required' => 'colom :attribute tidak boleh kosong',
        '1.required' => 'colom :attribute tidak boleh kosong',
        '2.required' => 'colom :attribute tidak boleh sama',
    ];
    }
  public function collection(Collection $rows)
    {
        foreach ($rows as $row) {  
   
            
            //check participant not null
             
                $explode  = explode(",", $row[3]);
                $company     = Company::whereIn('name', $explode)->pluck('company_id')->toArray();
               
                            
            $users = User::updateOrCreate(
            [
            'name' => $row[0],
            'username' => $row[1],
            'password' => Hash::make(12345678),
            'email' => $row[2],
            'company_id' => implode(",", $company),
            'dob'=>Carbon::parse($row[4])->format('Y-m-d'),
            'age'=>$row[5],
            'marriage'=>$row[6],
            'religion'=>$row[7],
            'etnis'=>$row[8],
            'job'=>$row[9],
            'years_of_service'=>$row[10],
            'gender'=>$row[11],
            'level_id'=>4,  
            'verified'=>1             
            ]);   

           $userID = $users->id;

            if ($row[12] ? : []) {
               Boss::updateOrCreate(['user_id' => $userID],[
                'boss_name' => $row[12],
                'boss_email' => $row[13],
                'code_boss'  => sha1(time())
            ]);
           }
            if ($row[14] ? : []) {
            Subordinate::create([
                'user_id' => $userID,
                'subordinate_name' => $row[14],
                'subordinate_email' => $row[15],
                'code_subordinate' => sha1(time())
            ]);
            }
            if ($row[16] ? : []) {
            Partner::create([
                'user_id' => $userID,
                'partner_name' => $row[16],
                'partner_email' => $row[17],
                'code_partner' => sha1(time())
            ]);
            }
            
        }
    }
     /**
     * @return int
     */
    public function startRow(): int
    {
        return 3;
    }
   
}
