<?php

namespace App\Imports;

use App\User;
use App\Model\Master\Word;
use App\Model\Master\WordDetail;
use App\Model\Master\WordCategory;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;

use DB;
use Auth;

class WordImport implements ToCollection, WithStartRow
{
    protected $errors = [];

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        $wordCategories = WordCategory::orderBy('id', 'desc')->get();

        foreach ($rows as $key => $row) {
            DB::beginTransaction();
                $hasError = false;
                $details = [];
                $i = 2;
                while (!empty($row[$i])) {
                    if ($hasError)
                        break;

                    $_detail = [];
                    foreach ($wordCategories as $wordCategory) {
                        if ($row[$i] == $wordCategory->code) {
                            $_detail = $wordCategory->id;
                            break;
                        }
                    }

                    if ($_detail)
                        $details[] = $_detail;
                    else {
                        $hasError = true;
                        $this->errors[] = 'Some Categories is not found on line ' . ($key + 1);
                        break;
                    }
                    $i++;
                }

                if (!$hasError) {
                    $word = new Word;
                    $word->name = strtolower($row[0]);
                    $word->is_combined_words = $row[1] == 'y' ? 1 : 0;
                    $word->save();

                    $wordId = $word->id;

                    foreach ($details as $detail) {
                        WordDetail::create([
                            'word_id' => $wordId,
                            'cat_id' => $detail
                        ]);
                    }
                }
            DB::commit();
        }

        if (count($this->errors) > 0) {
            throw \Illuminate\Validation\ValidationException::withMessages($this->errors);
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
}