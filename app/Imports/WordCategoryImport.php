<?php

namespace App\Imports;

use App\User;
use App\Model\Master\WordCategory;
use App\Model\Master\WordCategoryDetail;
use App\Model\Master\Dictionary;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;

use DB;
use Auth;

class WordCategoryImport implements ToCollection, WithStartRow
{
    protected $errors = [];

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        $dictionaries = Dictionary::orderBy('id', 'desc')->get();

        foreach ($rows as $row) {
            $count = WordCategory::where('code', $row[0])->count();

            if (!$count) {
                $wordCategory = new WordCategory;
                $wordCategory->code = $row[0];
                $wordCategory->name = $row[1];
                $wordCategory->description = $row[2];
                $wordCategory->save();

                $wordCategoryId = $wordCategory->id;


                $i = 3;
                while (!empty($row[$i])) {
                    foreach ($dictionaries as $dictionary) {
                        if ($row[$i] == $dictionary->code) {
                            WordCategoryDetail::create([
                                'cat_id' => $wordCategoryId,
                                'dict_id' => $dictionary->id
                            ]);
                            break;
                        }
                    }
                    $i++;
                }
            } else {
                $this->errors[] = 'Word Category with code "' . $row[0] . '" already exists';
            }
        }

        if (count($this->errors) > 0) {
            throw \Illuminate\Validation\ValidationException::withMessages($this->errors);
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
}
