<?php

namespace App\Imports;

use App\User;
use App\Model\Master\Formulation;
use App\Model\Master\FormulationGroup;
use App\Model\Master\FormulationDetail;
use App\Model\Master\WordCategory;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

use DB;
use Auth;

class FormulationImport implements ToCollection, WithStartRow, WithCalculatedFormulas
{
    protected $errors = [];

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        $wordCategories = WordCategory::orderBy('id', 'desc')->get();
        $groups = FormulationGroup::orderBy('id', 'desc')->get();

        foreach ($rows as $key => $row) {
            if (!$row[0])
                break;

            $count = Formulation::where('code', $row[0])->count();

            if ($count) {
                $this->errors[] = 'Formulation with code "' . $row[0] . '" already exists';
                break;
            }

            // Check Formulation Group
            $_group = false;
            foreach ($groups as $group) {
                if ($row[1] == $group->code) {
                    $row[1] = $group->id;
                    $_group = true;
                    break;
                }
            }

            if (!$_group) {
                $this->errors[] = 'Formulation Group is not found on line ' . ($key + 1);
                break;
            }

            DB::beginTransaction();
                $hasError = false;
                $details = [];
                $i = 9;


                while (!empty($row[$i])) {
                    if ($hasError)
                        break;

                    $_detail = [];

                    // Check Word Category
                    foreach ($wordCategories as $wordCategory) {
                        if ($row[$i] == $wordCategory->code) {
                            $_detail = [
                                'cat_id' => $wordCategory->id,
                                'weight' => $row[8]
                            ];
                            break;
                        }
                    }

                    if ($_detail)
                        $details[] = $_detail;
                    else {
                        $hasError = true;
                        $this->errors[] = 'Some Categories is not found on line ' . ($key + 1);
                        break;
                    }
                    $i++;
                }

                if (!$hasError) {
                    $formulation = new Formulation;
                    $formulation->formulation_group_id = $row[1];
                    $formulation->code = $row[2];
                    $formulation->name = strtolower($row[3]);
                    $formulation->status = $row[4] == 'inactive' ? 0 : 1;
                    $formulation->description = $row[7];
                    $formulation->aspek_tertinggi = $row[5];
                    $formulation->aspek_terendah = $row[6];
                    $formulation->save();

                    $formulationId = $formulation->id;

                    foreach ($details as $detail) {
                        FormulationDetail::create([
                            'formulation_id' => $formulationId,
                            'cat_id' => $detail['cat_id'],
                            'bobot' => $detail['weight'] * 100,
                            'konstanta' => 1
                        ]);
                    }
                }
            DB::commit();
        }

        if (count($this->errors) > 0) {
            throw \Illuminate\Validation\ValidationException::withMessages($this->errors);
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 3;
    }
}