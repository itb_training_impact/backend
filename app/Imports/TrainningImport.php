<?php

namespace App\Imports;

use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\Company;
use App\Model\Trainning\Speaker;
use App\Model\Trainning\TrainningDetail;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Carbon\Carbon;
use DB;
use Auth;

class TrainningImport implements ToCollection, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $participant    = [];
            $checkTrainning = Trainning::where('trainning_title', $row[0])->where('trainning_description', $row[1]);
            
            //check participant not null

                $explode        = explode(",", $row[4]);
                $company    = Company::whereIn('name', $explode)->pluck('company_id')->toArray();
          
                $explode        = explode(",", $row[5]);
                $participant    = User::whereIn('name', $explode)->pluck('id')->toArray();
            
                $explode        = explode(",", $row[6]);
                $speaker    = Speaker::whereIn('name', $explode)->pluck('speaker_id')->toArray();

            $trainning                              = $checkTrainning->count() > 0 ? $checkTrainning->first() : new Trainning;
            $trainning->trainning_title             = $row[0];
            $trainning->trainning_description       = $row[1];
             $trainning->start_date                 = Carbon::parse($row[2])->format('Y-m-d');
             $trainning->end_date                   = Carbon::parse($row[2])->format('Y-m-d');
            $trainning->user_id                     = Auth::user()->id;
            $trainning->company_id                  = implode(",", $company);
            $trainning->participant_id              = implode(",", $participant);  
            $trainning->speaker_id                  = implode(",", $speaker);
            $trainning->code                        = str_random(7);
            $trainning->save();

           
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
}
