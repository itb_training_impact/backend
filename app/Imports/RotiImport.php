<?php

namespace App\Imports;



use App\Model\Roti\Roti;
use App\Model\Master\Dimension;
use App\Model\Master\Criteria;
use App\Model\Master\Type;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\ModuleQuestion;
use App\Model\Trainning\Session;
use App\Model\Trainning\Module;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\Importable;

use DB;
class RotiImport implements ToCollection, WithStartRow, WithValidation
{
     use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
     public function rules(): array
    {
        return [
           '0' => 'required',
           '1' => 'required',     
        ];
    }

    public function customValidationAttributes()
    {
    return [
        '0' => 'code',
        '1' => 'type'
        ];
    }
     public function customValidationMessages()
    {
    return [
        '0.required' => 'colom :attribute tidak boleh kosong',
        '1.required' => 'colom :attribute tidak boleh kosong',
    ];
    }
   
  public function collection(Collection $rows)
    {
         
        foreach ($rows as $row) {  

            $checkRoti = Roti::where('roti_code', $row[0]);
            
            //check participant not null
                
                $explode  = explode(",", $row[1]);
                $type     = Type::whereIn('type_label', $explode)->pluck('type_id')->toArray();
               
               
               
                $explode  = explode(",", $row[7]);
                $group    = Dimension::whereIn('group', $explode)->pluck('group_id')->toArray();
               

               
                $explode  = explode(",", $row[8]);
                $criteria  = Criteria::whereIn('criteria', $explode)->pluck('criteria_id')->toArray();
               
                
                
                $explode  = explode(",", $row[11]);
                $trainning    = Trainning::whereIn('trainning_title', $explode)->pluck('trainning_id')->toArray();
               

                
                $explode  = explode(",", $row[12]);
                $session    = Session::whereIn('name', $explode)->pluck('session_id')->toArray();
              

               
                $explode  = explode(",", $row[13]);
                $module    = Module::whereIn('name', $explode)->pluck('module_id')->toArray();
                

           $roti = Roti::updateOrCreate(
            [
            'roti_code' => $row[0],
            'type_id' => implode(",", $type),
            'roti_rating_start' => $row[2],
            'roti_rating_finish' => $row[3],
            'roti_question' => $row[4],
            'roti_answer' => $row[5],
            'roti_correct_answer' => $row[6],
            'group_id' => implode(",", $group),
            'criteria_id' =>implode(",", $criteria),
            'roti_is_required' => $row[9],
            'roti_is_default' => $row[10],
            'trainning_id' => implode(",", $trainning) ? : null,
            'session_id' => implode(",", $session) ? : null,
            'module_id' => implode(",", $module) ? : null,     
            ]);      

            $moduleId = $roti->module_id;
            $merge = [
                'module_id' =>$moduleId
            ];
            $rotis = Roti::where('roti_is_default',0)->where('module_id',$moduleId)->get();
             foreach ($rotis as $value) {
                        ModuleQuestion::updateOrCreate(
                             array_merge(
                                $merge,
                                [
                                     'roti_id' => $value->roti_id
                                ]
                            )
                        );
                    }



         $trainings = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->whereNotIn('module_questions.roti_id',[''])
              ->groupBy('trainnings.trainning_id')->get();
             
              foreach ($trainings as $key => $value) {
                $session = Session::find($value->session_id);
                $periodeArray = array();
                if($session->before == "1"){
                  array_push($periodeArray, 0);
                }elseif ($session->during == "1"){
                  array_push($periodeArray, 1);
                }elseif($session->after == "1"){
                  array_push($periodeArray, 2);
                }
                  foreach ($periodeArray as $value_periode) {          
                    switch ($value_periode) {
                      case 0:
                          $module = "before";
                          break;
                      case 1:
                          $module = "during";
                          break;
                      case 2:
                          $module = "after";
                          break;
                      default:
                          $module = "";
                          break;
                    } 
                    
                    $data = [
                      'module_id' => $value->module_id,
                      'roti_id' => $roti->roti_id,
                      'training_detial_periode' => $module
                    ];          
                    DB::beginTransaction();
                    ModuleQuestion::create($data); 
                    DB::commit();   
                  }
              }              

           
        }
    }
    
    
     /**
     * @return int
     */
    public function startRow(): int
    {
        return 3;
    }
    
}
