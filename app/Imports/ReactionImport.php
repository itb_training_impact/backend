<?php

namespace App\Imports;

use App\Model\Reaction\Reaction;
use App\Model\Master\Dimension;
use App\Model\Master\Type;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\Session;
use App\Model\Trainning\Module;
use Illuminate\Support\Collection;
use App\Model\Trainning\ModuleQuestion;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;


use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\Importable;
use DB;

class ReactionImport implements ToCollection, WithStartRow, WithValidation
{
     use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
     public function rules(): array
    {
        return [
           '0' => 'required',
           '1' => 'required',     
        ];
    }

    public function customValidationAttributes()
    {
    return [
        '0' => 'code',
        '1' => 'type'
        ];
    }
   public function customValidationMessages()
    {
    return [
        '0.required' => 'colom :attribute tidak boleh kosong',
        '1.required' => 'colom :attribute tidak boleh kosong',
    ];
    }
  public function collection(Collection $rows)
    {
        foreach ($rows as $row) {  
   
            
            //check participant not null
             
                $explode  = explode(",", $row[1]);
                $type     = Type::whereIn('type_label', $explode)->pluck('type_id')->toArray();
               
               
               
                $explode  = explode(",", $row[7]);
                $group    = Dimension::whereIn('group', $explode)->pluck('group_id')->toArray();
               
                
                
                $explode  = explode(",", $row[10]);
                $trainning    = Trainning::whereIn('trainning_title', $explode)->pluck('trainning_id')->toArray();
               

                
                $explode  = explode(",", $row[11]);
                $session    = Session::whereIn('name', $explode)->pluck('session_id')->toArray();
              

               
                $explode  = explode(",", $row[12]);
                $module    = Module::whereIn('name', $explode)->pluck('module_id')->toArray();
                            
            $reaction = Reaction::updateOrCreate(
            [
            'reaction_code' => $row[0],
            'type_id' => implode(",", $type),
            'reaction_rating_start' => $row[2],
            'reaction_rating_finish' => $row[3],
            'reaction_question' => $row[4],
            'reaction_answer' => $row[5],
            'reaction_correct_answer' => $row[6],
            'group_id' => implode(",", $group),
            'reaction_is_required' => $row[8],
            'reaction_is_default' => $row[9],
            'trainning_id' => implode(",", $trainning) ? : null,
            'session_id' => implode(",", $session) ? : null,
            'module_id' => implode(",", $module) ? : null,     
            ]);      

            $moduleId = $reaction->module_id;
            $merge = [
                'module_id' =>$moduleId
            ];
            $reactions = Reaction::where('reaction_is_default',0)->where('module_id',$moduleId)->get();
             foreach ($reactions as $value) {
                        ModuleQuestion::updateOrCreate(
                             array_merge(
                                $merge,
                                [
                                     'reaction_id' => $value->reaction_id
                                ]
                            )
                        );
                    }


             $trainings = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->whereNotIn('module_questions.reaction_id',[''])
              ->groupBy('trainnings.trainning_id')->get();
             
              foreach ($trainings as $key => $value) {
                $session = Session::find($value->session_id);
                $periodeArray = array();
                if($session->before == "1"){
                  array_push($periodeArray, 0);
                }elseif ($session->during == "1"){
                  array_push($periodeArray, 1);
                }elseif($session->after == "1"){
                  array_push($periodeArray, 2);
                }
                  foreach ($periodeArray as $value_periode) {          
                    switch ($value_periode) {
                      case 0:
                          $module = "before";
                          break;
                      case 1:
                          $module = "during";
                          break;
                      case 2:
                          $module = "after";
                          break;
                      default:
                          $module = "";
                          break;
                    } 
                    
                    $data = [
                      'module_id' => $value->module_id,
                      'reaction_id' => $reaction->reaction_id,
                      'training_detial_periode' => $module
                    ];          
                    DB::beginTransaction();
                    ModuleQuestion::create($data); 
                    DB::commit();   
                  }
              }              

           
        }
    }
     /**
     * @return int
     */
    public function startRow(): int
    {
        return 3;
    }
   
}
