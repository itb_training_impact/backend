<?php

namespace App\Imports;

use App\User;
use App\Model\Master\Dictionary;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;

use DB;
use Auth;

class DictionaryImport implements ToCollection, WithStartRow
{
    protected $errors = [];

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $count = Dictionary::where('code', $row[0])->count();

            if (!$count) {
                $dictionary = new Dictionary;
                $dictionary->code = $row[0];
                $dictionary->name = $row[1];
                $dictionary->status = $row[2] == 'inactive' ? 0 : 1;
                $dictionary->description = $row[3];
                $dictionary->save();
            } else {
                $this->errors[] = 'Dictionary with code "' . $row[0] . '" already exists';
            }
        }

        if (count($this->errors) > 0) {
            throw \Illuminate\Validation\ValidationException::withMessages($this->errors);
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
}
