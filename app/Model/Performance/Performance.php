<?php

namespace App\Model\Performance;

use Illuminate\Database\Eloquent\Model;
use App\Model\Trainning\Trainning;

class Performance extends Model
{
    protected $table = "performances";

    protected $primaryKey = "performance_id";

    protected $fillable = [
    	"performance_code",
    	"performance_question",
    	"performance_answer",
    	"performance_correct_answer",
    	"group_id",
		"trainning_id",
		"session_id",
		"module_id",
    	"type_id",
    	"performance_is_required",
      	"performance_is_default",
      	"performance_rating_start",
    	"performance_rating_finish",
		"user_id",
		"criteria_id"
    ];

    public function type(){
   		return $this->belongsTo('App\Model\Master\Type','type_id');
   	}

    public function group(){
   		return $this->belongsTo('App\Model\Master\Dimension','group_id');
	}

	public function criteria(){
		return $this->belongsTo('App\Model\Master\Criteria','criteria_id');
	}
	public function ModuleQuestions(){
        return $this->hasMany('App\Model\Trainning\ModuleQuestion','performance_id');
    }
	public function totalTrainning($id)
	{
        $query = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->where('module_questions.performance_id', $id)
              ->groupBy('trainnings.trainning_id')
              ->get();
  
		return count($query);
	}
}
