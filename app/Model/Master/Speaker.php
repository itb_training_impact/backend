<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Speaker extends Model
{
    protected $table = "speaker";

    protected $primaryKey = "id";

    protected $fillable = ["name","description","images"];
}
