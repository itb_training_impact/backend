<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Criteria extends Model
{
    protected $table = 'criterias';
    protected $primaryKey = 'criteria_id';
    protected $fillable = ['criteria', 'criteria_description', 'level'];   
}