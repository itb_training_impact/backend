<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

use App\Model\Behaviour\StoryResult;

class Formulation extends Model
{
	protected $table = 'formulation';
	protected $primaryKey = 'id';
	protected $fillable = ['formulation_group_id', 'code','name', 'status', 'description', 'aspek_tertinggi', 'aspek_terendah'];

	public function detail(){
		return $this->hasMany(FormulationDetail::class, 'formulation_id');
	}

	public function group(){
		return $this->belongsTo(FormulationGroup::class, 'formulation_group_id');
	}

	public function result($storyDetailId){
		return StoryResult::where('story_detail_id', $storyDetailId)->where('formula_id', $this->id)->get();
	}
}