<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Devision extends Model
{
    protected $table = "devision";

    protected $primaryKey = "devision_id";

    protected $fillable = ["name","description"];
}
