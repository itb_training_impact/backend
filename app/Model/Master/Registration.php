<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $table = "registration_form";

    protected $primaryKey = "id";

    protected $fillable = [
    	"status",
    ];
}
