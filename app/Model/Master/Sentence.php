<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

use App\Model\Master\WordCategory;

class Sentence extends Model
{
	protected $table = 'sentence';
	protected $primaryKey = 'id';
	protected $fillable = ['cat_id', 'cat_child_id'];

	public function detail(){
		return $this->hasOne(WordCategory::class, 'id', 'cat_child_id');
	}
}