<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Boss extends Model
{
    protected $table = "user_bosses";

    protected $primaryKey = "user_boss_id";

    protected $fillable = ["user_id","boss_name","boss_email","code_boss"];
}
