<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Formula extends Model
{
    protected $table = 'formulas';
    protected $primaryKey = 'formula_id';
    protected $fillable = ['formula','formula_description'];

   	public function groups(){
   		return $this->hasOne('App\Dimension');
   	}
}
