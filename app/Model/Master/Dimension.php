<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Dimension extends Model
{
    protected $table = 'groups';
    protected $primaryKey = 'group_id';
    protected $fillable = ['group','formula_id', 'is_reaction'];

    public function formulas(){
   		return $this->belongsTo('App\Model\Master\Formula','formula_id');
   	}
   
}