<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class FormulationGroup extends Model
{
	protected $table = 'formulation_group';
	protected $primaryKey = 'id';
	protected $fillable = ['code','name', 'description'];

	public function formulation(){
		return $this->hasMany(Formulation::class, 'formulation_group_id');
	}
}