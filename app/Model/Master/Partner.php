<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $table = "user_partners";

    protected $primaryKey = "user_partner_id";

    protected $fillable = ["user_id","partner_name","partner_email","code_partner"];
}
