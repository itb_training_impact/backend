<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

use App\Model\Master\WordCategoryDetail;
use App\Model\Master\Sentence;

class WordCategory extends Model
{
	protected $table = 'word_category';
	protected $primaryKey = 'id';
	protected $fillable = ['code', 'name', 'description', 'count'];

	public function detail(){
		return $this->hasMany(WordCategoryDetail::class, 'cat_id');
	}

	public function sentence(){
		return $this->hasMany(Sentence::class, 'cat_id', 'code');
	}	
}