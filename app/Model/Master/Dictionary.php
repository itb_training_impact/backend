<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    protected $table = 'dictionary';
    protected $primaryKey = 'id';
    protected $fillable = ['code', 'name', 'status', 'description'];
}