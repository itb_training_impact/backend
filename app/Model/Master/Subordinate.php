<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Subordinate extends Model
{
    protected $table = "user_subordinates";

    protected $primaryKey = "user_subordinate_id";

    protected $fillable = ["user_id","subordinate_name","subordinate_email","code_subordinate"];
}
