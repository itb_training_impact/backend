<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class FormulationDetail extends Model
{
    protected $table = 'formulation_detail';
    protected $primaryKey = 'id';
    protected $fillable = ['formulation_id', 'cat_id', 'bobot', 'konstanta'];

    public function wordCategory()
    {
        return $this->belongsTo(WordCategory::class, 'cat_id', 'id');
    }
}