<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

use App\Model\Master\WordDetail;

class Word extends Model
{
	protected $table = 'word';
	protected $primaryKey = 'id';
	protected $fillable = ['name', 'is_combined_words', 'count'];

	public function detail(){
		return $this->hasMany(WordDetail::class, 'word_id');
	}
}