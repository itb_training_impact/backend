<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

use App\Model\Master\WordCategory;

class WordDetail extends Model
{
    protected $table = 'word_detail';
    protected $primaryKey = 'id';
    protected $fillable = ['word_id', 'cat_id'];

    public function wordCategory()
    {
        return $this->belongsTo(WordCategory::class, 'cat_id', 'id');
    }

    public function word()
    {
        return $this->belongsTo(Word::class, 'word_id', 'id');
    }
}