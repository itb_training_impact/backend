<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

use App\Model\Master\Dictionary;

class WordCategoryDetail extends Model
{
    protected $table = 'word_category_detail';
    protected $primaryKey = 'id';
    protected $fillable = ['cat_id', 'dict_id'];

    public function dictionary()
    {
        return $this->belongsTo(Dictionary::class, 'dict_id', 'id');
    }
}