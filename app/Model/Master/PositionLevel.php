<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class PositionLevel extends Model
{
    protected $table = "position_level";

    protected $primaryKey = "position_level_id";

    protected $fillable = ["name","description"];
}
