<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = "types";

    protected $primaryKey = "type_id";

    protected $fillable = [
    	"type",
    	"type_label"
    ];
}
