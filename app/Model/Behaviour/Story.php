<?php

namespace App\Model\Behaviour;

use Illuminate\Database\Eloquent\Model;

use App\Model\Trainning\Session;
use App\Model\Trainning\Module;

use DB;

class Story extends Model
{
	protected $table = "story";

	protected $primaryKey = "id";

	protected $fillable = ["title", "user_id", "trainning_id", "session_id", "module_id", "submit_periode"];

	public function detail()
	{
		return $this->hasOne(StoryDetail::class, 'story_id');
	}
	
	public function session()
	{
		return $this->hasOne(Session::class, 'session_id', 'session_id');
	}

	public function module()
	{
		return $this->hasOne(Module::class, 'module_id', 'module_id');
	}

	public function details()
	{
		return $this->hasMany(StoryDetail::class, 'story_id');
	}
	
	public function countCategoryPercentage()
	{
		$content = preg_replace('/[^A-Za-z0-9\- ]\*/', ' ', strtolower($this->detail->story_after));

		// Split Story to Words
		$content = preg_replace('!\s+!', ' ', $content);
		$storyWords = explode(' ', trim($content));
		$totalStoryWord = count($storyWords);
		$wordPerStory = array_count_values($storyWords);

		$query = DB::table('story_result_word as srw')
			->join('word as w', 'w.id', '=', 'srw.word_id')
			->join('word_detail as wd', 'wd.word_id', '=', 'w.id')
			->select('wd.cat_id', 'w.id', 'w.name')
			->where('srw.story_detail_id', $this->detail->id)
			->groupBy('wd.cat_id')
			->get();

		$response = [];
		$response['total_unidentified_words'] = 0;

		if (count($query)) {
			foreach ($query as $result) {
				if (!empty($wordPerStory[strtolower($result->name)])) {
					$response[$result->cat_id] = $wordPerStory[strtolower($result->name)] / $totalStoryWord * 100;
					unset($wordPerStory[$result->name]);
				}
			}
		}

		$response['total_unidentified_words'] = array_sum(array_values($wordPerStory));
		return $response;
	}
}