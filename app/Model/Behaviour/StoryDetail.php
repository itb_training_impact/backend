<?php

namespace App\Model\Behaviour;

use Illuminate\Database\Eloquent\Model;

use DB;
use App\Model\Master\WordDetail;

class StoryDetail extends Model
{
	public $timestamps = false;

	protected $table = "story_detail";

	protected $primaryKey = "id";

	protected $fillable = ["story_id", "story", "filename", "story_after"];

	public function results()
	{
		return $this->hasMany(StoryResult::class, 'story_detail_id');
	}

	public function countCharPerWord()
	{
		$story = preg_replace('/[^A-Za-z0-9\- ]/', ' ', $this->story);
		$story = preg_replace('!\s+!', ' ', trim($story));
		$totalChars = strlen(str_replace(' ', '', $story));
		$totalWords = count(explode(' ', $story));
		return ['char_per_word' => round($totalChars / ($totalWords == 0 ? 1 : $totalWords), 2), 'total_words' => $totalWords];
	}

	public function countConjuction()
	{
		$count = 0;
		$story = trim(preg_replace('/[^A-Za-z0-9\- ]/', ' ', $this->story));

		$query = DB::table('word_detail as wd')
			->join('word as w', 'w.id', '=', 'wd.word_id')
			->join('word_category as wc', 'wc.id', '=', 'wd.cat_id')
			->select('w.name')
			->where('wc.name', 'conjunc')
			->get();

		foreach ($query as $word) {
			$count += preg_match_all('/\b' . $word->name . '\b/', $story);
		}

		return $count;
	}

	public function countSentence()
	{	

		$formulaSentence = DB::table('word_category as c')
			->leftJoin('sentence as s', 's.cat_id', '=', 'c.code')
			->select('c.code', DB::raw('GROUP_CONCAT(cat_child_id) as cat'))
			->where('code', 'SC01')
			->get()
			->first();

		if ($formulaSentence->cat) $cats = explode(',', $formulaSentence->cat);
		else $cats = [];

		$arrWords = [];
		$count = 0;
		if (count($cats)) {
			foreach($cats as $cat) {
				$words = WordDetail::where('cat_id' , $cat)->get();
				$wordTemp = [];
				foreach ($words as $word) {
					$wordTemp[] = $word->word->name;
				}
				$arrWords[$cat] = $wordTemp;
			}

			// echo json_encode($arrWords);
			$sentences = preg_replace('/[;:!?]/', '.', $this->story);
			$sentences = explode('.', trim($this->story));
			foreach ($sentences as $sentence) {
				$exist = 0;
				$sentence = preg_replace('!\s+!', ' ', $sentence);
				foreach ($cats as $cat) {
					foreach ($arrWords[$cat] as $word) {
						if (strpos(strtolower($sentence), $word) !== false) {
							$exist++;
							break;
						}
					}
				}

				if ($exist == count($cats)) {
					$count += 1;
				}
			}
		} else {
			$sentences = preg_replace('/[;:!?]/', '.', $this->story);
			$count = count(array_filter(explode('.', trim($sentences))));
		}
		
		return $count;
	}

	function countCoherenceOfSentences($countSentence, $type = 'fix')
	{
		$coherence = 0;
		$arrSentences = [];

		$formulaSentence = DB::table('word_category as c')
			->leftJoin('sentence as s', 's.cat_id', '=', 'c.code')
			->select('c.code', DB::raw('GROUP_CONCAT(cat_child_id) as cat'))
			->where('code', 'SC01')
			->get()
			->first();

		if ($formulaSentence->cat) $cats = explode(',', $formulaSentence->cat);
		else $cats = [];

		foreach($cats as $cat) {
			$words = WordDetail::where('cat_id' , $cat)->get();
			$wordTemp = [];
			foreach ($words as $word) {
				$wordTemp[] = $word->word->name;
			}
			$arrWords[$cat] = $wordTemp;
		}

		$sentences = preg_replace('/[;:!?]/', '.', $this->story);
		$sentences = explode('.', $this->story);

		foreach ($sentences as $sentence) {
			$exist = 0;
			$sentence = preg_replace('!\s+!', ' ', $sentence);
			foreach ($cats as $cat) {
				foreach ($arrWords[$cat] as $word) {
					if (strpos(strtolower($sentence), $word) !== false) {
						$exist++;
						break;
					}
				}
			}

			if ($exist == count($cats)) $arrSentences[] = true;
			else $arrSentences[] = false;
		}

		// Story should has two or more sentence
		if (count($sentences) > 1) {
			for ($i = 0; $i < count($sentences); $i++) {
				// Check if the next sentence is exists
				if ($arrSentences[$i] && !empty($sentences[$i + 1]) && $arrSentences[$i + 1]) {
					$firstSentence = preg_replace('/[^A-Za-z0-9\- ]/', ' ', $sentences[$i]);
					$firstWords = explode(' ', preg_replace('!\s+!', ' ', $firstSentence));
					$firstWords = array_filter(array_map('strtolower', $firstWords));

					$secodeSentence = preg_replace('/[^A-Za-z0-9\- ]/', ' ', $sentences[$i + 1]);
					$secondWords = explode(' ', preg_replace('!\s+!', ' ', $secodeSentence));
					$secondWords = array_filter(array_map('strtolower', $secondWords));

					foreach ($firstWords as $word) {
						if (in_array($word, $secondWords)) {
							$coherence++;
							break;
						}
					}
				}
			}
		}

		if ($type == 'fix') return $coherence;
		else return ($coherence / (empty($countSentence) ? 1 : $countSentence) * 100) . '%';
	}
}