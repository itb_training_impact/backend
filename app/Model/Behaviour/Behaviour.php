<?php

namespace App\Model\Behaviour;

use Illuminate\Database\Eloquent\Model;
use App\Model\Trainning\Trainning;

class Behaviour extends Model
{
    protected $table = "behaviours";

    protected $primaryKey = "behaviour_id";

    protected $fillable = [
    	"behaviour_code",
    	"behaviour_question",
    	"behaviour_answer",
    	"behaviour_correct_answer",
    	"group_id",
        "formulation_id",
    	"type_id",
        "user_id",
        "trainning_id",
        "session_id",
		"module_id",
    	"behaviour_is_required",
    	"behaviour_is_default",
		"behaviour_rating_start",
		"behaviour_rating_finish"
    ];

    public function type(){
   		return $this->belongsTo('App\Model\Master\Type','type_id');
   	}

    public function group(){
   		return $this->belongsTo('App\Model\Master\Dimension','group_id');
   	}
	public function ModuleQuestions(){
        return $this->hasMany('App\Model\Trainning\ModuleQuestion','behaviour_id');
    }
    public function totalTrainning($id)
    {
        // $query = Trainning::join('trainning_details','trainnings.trainning_id','=','trainning_details.trainning_id')
                //    ->where('trainning_details.learning_id', $id)
                //    ->groupBy('trainnings.trainning_id')
                //    ->get();
        $query = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
                ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
                ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
                ->where('module_questions.behaviour_id', $id)
                ->groupBy('trainnings.trainning_id')
                ->get();

        return count($query);
    }
}
