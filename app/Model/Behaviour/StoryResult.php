<?php

namespace App\Model\Behaviour;

use Illuminate\Database\Eloquent\Model;

class StoryResult extends Model
{
	public $timestamps = false;

	protected $table = "story_result";

	protected $primaryKey = "id";

	protected $fillable = ["story_detail_id", "formula_id", "formula_name", "score"];
}