<?php

namespace App\Model\Behaviour;

use Illuminate\Database\Eloquent\Model;

class StoryResultUnword extends Model
{
	public $timestamps = false;

	protected $table = "story_result_unword";

	protected $primaryKey = "id";

	protected $fillable = ["story_detail_id", "word_id", "score"];
}