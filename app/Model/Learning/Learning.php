<?php

namespace App\Model\Learning;

use Illuminate\Database\Eloquent\Model;
use App\Model\Trainning\Trainning;

class Learning extends Model
{
    protected $table = "learnings";

    protected $primaryKey = "learning_id";

    protected $fillable = [
    	"learning_code",
    	"learning_question",
    	"learning_answer",
    	"learning_correct_answer",
    	"group_id",
    	"type_id",
		"trainning_id",
		"session_id",
		"module_id",
    	"learning_is_required",
    	"learning_is_default",
		"learning_rating_start",
		"learning_rating_finish",
    "user_id"
    ];

    public function type(){
   		return $this->belongsTo('App\Model\Master\Type','type_id');
   	}

    public function group(){
   		return $this->belongsTo('App\Model\Master\Dimension','group_id');
   	}
	   public function ModuleQuestions(){
        return $this->hasMany('App\Model\Trainning\ModuleQuestion','learning_id');
    }
	   public function totalTrainning($id)
	   {
		   // $query = Trainning::join('trainning_details','trainnings.trainning_id','=','trainning_details.trainning_id')
					//    ->where('trainning_details.learning_id', $id)
					//    ->groupBy('trainnings.trainning_id')
					//    ->get();
      $query = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->where('module_questions.learning_id', $id)
              ->groupBy('trainnings.trainning_id')
              ->get();
   
		   return count($query);
	   }
}
