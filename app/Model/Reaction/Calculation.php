<?php

namespace App\Model\Reaction;

use Illuminate\Database\Eloquent\Model;
use DB;

class Calculation extends Model
{
    protected $table = "v_reaction_calculate";

    public function scopeFilter($model, $trainningID = null, $participantID = null, $periode = null)
    {
        $query = $model->addSelect(
                    DB::raw('SUM(sum_response) as response'),
                    DB::raw('SUM(count_response) as count')
                )->groupBy('trainning_id');

        if($participantID != "" && !empty($participantID)) {
            $query->where('participant_id', $participantID);
        }

        if($trainningID != "" && !empty($trainningID)) {
            $query->where('trainning_id', $trainningID);
        }

        if($periode != "" && !empty($periode)) {
            $query->where('periode', $periode);
        }

        return $query;
    }
}
