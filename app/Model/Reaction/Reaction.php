<?php

namespace App\Model\Reaction;

use Illuminate\Database\Eloquent\Model;
use App\Model\Trainning\Trainning;

class Reaction extends Model
{
    protected $table = "reactions";

    protected $primaryKey = "reaction_id";

    protected $fillable = [
    	"reaction_code",
    	"reaction_question",
    	"reaction_answer",
    	"reaction_correct_answer",
    	"group_id",
		"trainning_id",
		"session_id",
		"module_id",
    	"type_id",
    	"reaction_is_required",
      	"reaction_is_default",
      	"reaction_rating_start",
    	"reaction_rating_finish",
      "user_id"
    ];

    public function type(){
   		return $this->belongsTo('App\Model\Master\Type','type_id');
   	}

    public function group(){
   		return $this->belongsTo('App\Model\Master\Dimension','group_id');
	}
  public function ModuleQuestions(){
        return $this->hasMany('App\Model\Trainning\ModuleQuestion','reaction_id');
    }
	
	public function totalTrainning($id)
	{
		// $query = Trainning::join('trainning_details','trainnings.trainning_id','=','trainning_details.trainning_id')
		// 			->where('trainning_details.reaction_id', $id)
		// 			->groupBy('trainnings.trainning_id')
		// 			->get();

    $query = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->where('module_questions.reaction_id', $id)
              ->groupBy('trainnings.trainning_id')
              ->get();
  
		return count($query);
	}
}
