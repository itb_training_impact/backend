<?php

namespace App\Model\Trainning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Model\Trainning\TrainningDetail as Detail;
use App\Model\Trainning\Company;
use App\User;

class Trainning extends Model
{
	use SoftDeletes;

    protected $table = "trainnings";

    protected $primaryKey = "trainning_id";

    protected $fillable = [
    	"trainning_title",
    	"trainning_description",
    	"user_id",
        "participant_id",
         "speaker_id",
         "images",
          "speaker_descriptions",
          "company_id",
          "start_date",
          "end_date",
          "code"
    ];
    public function company()
  {
    return $this->belongsTo(Company::class, 'company_id');
  }
    public function user(){
   		return $this->belongsTo(User::class,'user_id', 'id');
    }
	   
	public function detail()
	{
		return $this->hasMany(Detail::class, 'trainning_id');
	}
	   
}
