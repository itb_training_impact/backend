<?php

namespace App\Model\Trainning;

use Illuminate\Database\Eloquent\Model;

use App\Model\Master\Formulation;

class ModuleBehaviourFormulations extends Model
{

	protected $table = "module_behaviour_formulations";

	protected $primaryKey = "id";

	protected $fillable = [
		"trainning_id",
		"session_id",
		"module_id",
		"formulation_id"
	];

	public function formulation()
	{
		return $this->hasOne(Formulation::class, 'id', 'formulation_id');
	}
}
