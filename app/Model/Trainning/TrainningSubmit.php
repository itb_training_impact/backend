<?php

namespace App\Model\Trainning;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Model\Reaction\Reaction;
use App\Model\Learning\Learning;
use App\Model\Behaviour\Behaviour;
use App\Model\Trainning\Trainning;
use App\Model\Performance\Performance;
use App\Model\Roti\Roti;
class TrainningSubmit extends Model
{
    protected $table = "trainning_submits";

    protected $primaryKey = "submit_id";

    protected $fillable = [
    	"trainning_id",
        "session_id",
        "module_id",
    	"reaction_id",
    	"learning_id",
    	"user_id",
    	"submit_periode",
    	"submit_response",
    ];

    public function reaction(){
        return $this->belongsTo(Reaction::class,'reaction_id');
    }

    public function learning(){
        return $this->belongsTo(Learning::class,'learning_id');
    }

    public function behaviour(){
        return $this->belongsTo(Behaviour::class,'behaviour_id');
    }
     public function performance(){
        return $this->belongsTo(Performance::class,'performance_id');
    }

    public function roti(){
        return $this->belongsTo(Roti::class,'roti_id');
    }
    public function trainning(){
        return $this->belongsTo(Trainning::class,'trainning_id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    
    public function scopeFilterCalculation($model, $trainningID = null, $sessionId = null, $moduleId = null, $participantID = null,  $periode = null, $type = null)
    {
        $query = $model->addSelect(
            //\DB::raw('SUM(submit_response) / (COUNT(*)*5) * 100 as response')
            \DB::raw('SUM(CASE 
                WHEN submit_response IS NOT NULL THEN submit_response
                WHEN submit_response IS NULL THEN 0
                END) / (COUNT(*)*5) * 100 as response')
            );

        if ($participantID)
            $query->where('user_id', $participantID);

        if($type == "reaction"){
            $query->whereNull('learning_id')->whereNull('behaviour_id')->whereNotNull('reaction_id');
        }
        elseif($type == "behaviour") {
            $query->whereNull('learning_id')->whereNull('reaction_id')->whereNotNull('behaviour_id');
        }
        else{
            $query->whereNull('reaction_id')->whereNull('behaviour_id')->whereNotNull('learning_id');
        }

        if ((!empty($periode) && $periode != '') || $periode == '0')
            $query->where('submit_periode', $periode);
     
        
        if (!empty($trainningID) && $trainningID != '')
            $query->where('trainning_id', $trainningID);

        if(!empty($sessionId) && $sessionId != '')
            $query->where('session_id', $sessionId);

        if (!empty($moduleId) && $moduleId != '')
            $query->where('module_id', $moduleId);

        return $query;
    }

    public function scopeFilterDimension($model, $trainningID = null, $sessionId = null, $moduleId = null, $participantID = null, $periode = null, $type = null, $level_intermediary=null, $primaryGroup=null)
    {
        $query = $model->addSelect(
                    'groups.group_id',
                    'groups.group',
                    'submit_periode as periode',
                    'trainning_submits.user_id as participant_id',
                    'trainning_submits.trainning_id',
                    'trainning_submits.session_id',
                    'trainning_submits.module_id',
                    \DB::raw('MAX(trainning_submits.created_at) AS created_at'),
                    //\DB::raw('SUM(submit_response) as sum_response'),
                    \DB::raw('SUM(CASE 
                WHEN submit_response IS NOT NULL THEN submit_response
                WHEN submit_response IS NULL THEN 0
                END) AS sum_response')
                    //\DB::raw('COUNT(*) as count_response')
                )        
                //->where('trainning_submits.user_id', $participantID)
                ->where('trainning_submits.trainning_id', $trainningID);
        if ($participantID){
            $query->where('trainning_submits.user_id', $participantID);
        }
        if($type == "reaction") {
            $query->whereNull('trainning_submits.learning_id')
                ->whereNull('trainning_submits.behaviour_id')
                ->whereNull('trainning_submits.performance_id')
                ->whereNotNull('trainning_submits.reaction_id')            
                    ->join('reactions', 'trainning_submits.reaction_id', '=', 'reactions.reaction_id')
                    ->join('groups', 'reactions.group_id', '=', 'groups.group_id')
                    ->where('is_reaction', 0);                    
                    if(empty($primaryGroup)){
                        $query->groupBy('reactions.group_id','trainning_submits.user_id','submit_periode');
                    }  
        } else if($type == "behaviour"){
            $query->addSelect('formulation.name AS formulation_name')
                ->addSelect('formulation.id AS formulation_id');
            $query->whereNull('trainning_submits.learning_id')
                ->whereNull('trainning_submits.reaction_id')
                ->whereNull('trainning_submits.performance_id')
                ->whereNotNull('trainning_submits.behaviour_id')              
                ->join('behaviours', 'trainning_submits.behaviour_id', '=', 'behaviours.behaviour_id')
                ->join('formulation', 'behaviours.formulation_id', '=', 'formulation.id')
                ->join('groups', 'behaviours.group_id', '=', 'groups.group_id')
                ->where('is_reaction', 2)
                ->where('trainning_submits.level_intermediary', $level_intermediary);                
                if(empty($primaryGroup)){
                    $query->groupBy('formulation.id','trainning_submits.user_id','submit_periode');
                }  
        } else if($type == "performance"){
          $query->whereNull('trainning_submits.learning_id')
                ->whereNull('trainning_submits.behaviour_id')
                ->whereNull('trainning_submits.reaction_id')
                ->whereNotNull('trainning_submits.performance_id')            
                ->join('performances', 'trainning_submits.performance_id', '=', 'performances.performance_id')
                ->join('groups', 'performances.group_id', '=', 'groups.group_id')
                ->where('is_reaction', 2)
                ->where('trainning_submits.level_intermediary', 1);               
                if(empty($primaryGroup)){
                    $query->groupBy('performances.group_id','trainning_submits.user_id','submit_periode');
                }  
        }else {
            //$query->addSelect(\DB::raw('COUNT(learnings.learning_id) AS count_response'));
            $query->whereNull('trainning_submits.reaction_id')
                ->whereNull('trainning_submits.behaviour_id')
                ->whereNull('trainning_submits.performance_id')
                ->whereNotNull('trainning_submits.learning_id')
                //->addSelect(\DB::raw('(learnings.learning_is_default = 1 OR learnings.trainning_id = '.$trainningID.')'))
                ->join('learnings', 'trainning_submits.learning_id', '=', 'learnings.learning_id')
                ->join('groups', 'learnings.group_id', '=', 'groups.group_id')
                ->where('is_reaction', 1);
                if(empty($primaryGroup)){
                    $query->groupBy('learnings.group_id','trainning_submits.user_id','submit_periode');
                }                        
        }        
        if((!empty($periode) && $periode != '') || $periode == '0') {
            $query->where('submit_periode', $periode);
        }

        if(!empty($sessionId) && $sessionId != '') {            
            $sessionId = $sessionId;
            $query->where('trainning_submits.session_id', $sessionId);
        }
        elseif(!empty($moduleId) && $moduleId != '') {
            $moduleId = $moduleId;
            $query->where('trainning_submits.module_id', $moduleId);
        }
        if(!empty($primaryGroup)){
            $query->groupBy('trainning_submits.user_id');
        }        
        return $query;
    }

    public function scopeFilterBossesPartners($model, $trainningID = null , $sessionId = null, $moduleId = null, $participantID = null, $periode = null, $type){

      $query = $model->addSelect(
                    'groups.group_id',
                    'groups.group',
                    'submit_periode as periode',
                    'trainning_submits.user_id as participant_id',
                    'trainning_submits.trainning_id',
                    'trainning_submits.session_id',
                    'trainning_submits.module_id',
                    //\DB::raw('SUM(submit_response) as sum_response'),
                    \DB::raw('SUM(CASE 
                WHEN submit_response IS NOT NULL THEN submit_response
                WHEN submit_response IS NULL THEN 0
                END) AS sum_response')
                    //\DB::raw('COUNT(*) as count_response')
                )
                ->where('trainning_submits.user_id', $participantID)
                ->where('trainning_submits.trainning_id', $trainningID);
        
            $query->addSelect('formulation.name AS formulation_name')
                    ->addSelect('formulation.id AS formulation_id');
            $query->whereNull('trainning_submits.learning_id')
                ->whereNull('trainning_submits.reaction_id')
                ->whereNotNull('trainning_submits.behaviour_id')              
                ->join('behaviours', 'trainning_submits.behaviour_id', '=', 'behaviours.behaviour_id')
                ->join('formulation', 'behaviours.formulation_id', '=', 'formulation.id')
                ->join('groups', 'behaviours.group_id', '=', 'groups.group_id')
                ->where('is_reaction', 2)
                ->groupBy('formulation.id','trainning_submits.user_id','submit_periode');      

        if((!empty($periode) && $periode != '') || $periode == '0') {
            $query->where('submit_periode', $periode);
        }

        if(!empty($sessionId) && $sessionId != '') {            
            $sessionId = $sessionId;
            $query->where('trainning_submits.session_id', $sessionId);
        }
        elseif(!empty($moduleId) && $moduleId != '') {
            $moduleId = $moduleId;
            $query->where('trainning_submits.module_id', $moduleId);
        }        

      if($type == "bosses") {
        $query->join('user_bosses', 'user_bosses.user_id', '=', 'trainning_submits.user_id');
        $query->addSelect(
          \DB::raw('user_bosses.boss_name AS boss_name')
        );
        $query->where('level_intermediary','=','1');
      }elseif($type == "partners"){
        $query->join('user_partners', 'user_partners.user_id', '=', 'trainning_submits.user_id');
        $query->addSelect(          
          \DB::raw('user_partners.partner_name AS partner_name')
        );
        $query->where('level_intermediary','=','2');
      }else{
        $query->join('user_subordinates', 'user_subordinates.user_id', '=', 'trainning_submits.user_id');
        $query->addSelect(          
          \DB::raw('user_subordinates.subordinate_name AS subordinate_name')
        );
        $query->where('level_intermediary','=','3');
      }      

      return $query;
    }
}
