<?php

namespace App\Model\Trainning;

use Illuminate\Database\Eloquent\Model;
use App\Model\Trainning\Session;
use App\Model\Trainning\ModuleQuestion as Question;
use DB;
class ModuleQuestion extends Model
{
    protected $table = "module_questions";

    protected $primaryKey = "module_question_id";

    protected $fillable = [
    	"module_id",
        "reaction_id",
        "learning_id",
        "behaviour_id",
        "performance_id",
        "roti_id",
        "trainning_detail_periode"
       
    ];

    public function behaviourTrue($moduleID,$periode)
  {
    #$query = \DB::table('module_questions')->whereNotNull('behaviour_id')->where('module_id',$moduleID)->where('trainning_detail_periode',$periode)->get();
    $query = \DB::table('module_questions')->whereNotNull('behaviour_id')->where('module_id',$moduleID)->get();
    return $query;
  }
   public function learningTrue($moduleID,$periode)
  {
    #$query = \DB::table('module_questions')->whereNotNull('learning_id')->where('module_id',$moduleID)->where('trainning_detail_periode',$periode)->get();
    $query = \DB::table('module_questions')->whereNotNull('learning_id')->where('module_id',$moduleID)->get();
    return $query;
  }
   public function reactionTrue($moduleID,$periode)
  {
    #$query = \DB::table('module_questions')->whereNotNull('reaction_id')->where('module_id',$moduleID)->where('trainning_detail_periode',$periode)->get();
    $query = \DB::table('module_questions')->whereNotNull('reaction_id')->where('module_id',$moduleID)->get();
    return $query;
  }
    public function performanceTrue($moduleID)
  {
    #$query = \DB::table('module_questions')->whereNotNull('performance_id')->where('module_id',$moduleID)->where('trainning_detail_periode',$periode)->get();
    $query = \DB::table('module_questions')->whereNotNull('performance_id')->where('module_id',$moduleID)->get();
    return $query;
  }
   public function rotiTrue($moduleID)
  {
    #$query = \DB::table('module_questions')->whereNotNull('roti_id')->where('module_id',$moduleID)->where('trainning_detail_periode',$periode)->get();
    $query = \DB::table('module_questions')->whereNotNull('roti_id')->where('module_id',$moduleID)->get();
    return $query;
  }
    public function module()
  {
    return $this->belongsTo(Module::class, 'module_id');
  }
  public function reaction()
  {
    return $this->belongsTo(Reaction::class, 'reaction_id');
  }
   public function learning()
  {
    return $this->belongsTo(Reaction::class, 'reaction_id');
  }
  public function behaviour()
  {
    return $this->hasMany(Behaviour::class, 'behaviour_id');
  }
  public function performance()
  {
    return $this->hasMany(Performance::class, 'performance_id');
  }
  public function roti()
  {
    return $this->hasMany(Roti::class, 'roti_id');
  }
  public function question()
  {
    return $this->hasMany(Question::class, 'module_id');
  }
  public function periodeHtml($periode)
  {
    $html = "";

    switch ($periode) {
      case 0:
        $html = "BEFORE SECTION";
        break;

      case 1:
        $html = "DURING SECTION";
        break;
      
      case 2:
        $html = "AFTER SECTION";
        break;
      default:
        $html = "";
        break;
    }

    return $html;
  }

}
