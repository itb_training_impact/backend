<?php

namespace App\Model\Trainning;

use Illuminate\Database\Eloquent\Model;

use App\Model\Trainning\Trainning;
use App\Model\Reaction\Reaction;
use App\Model\Learning\Learning;

class TrainningDetail extends Model
{
    protected $table = "trainning_details";

    protected $primaryKey = "trainning_detail_id";

    protected $fillable = [
    	"trainning_detail_periode",
    	"reaction_id",
    	"learning_id",
    	"trainning_id"
    ];

    public function trainning(){
   		return $this->belongsTo(Trainning::class,'trainning_id');
   	}

    public function reaction(){
   		return $this->belongsTo(Reaction::class,'reaction_id');
   	}

    public function learning(){
   		return $this->belongsTo(Learning::class,'learning_id');
   	}

	public function periodeHtml($periode)
	{
		$html = "";

		switch ($periode) {
			case 0:
				$html = "BEFORE SECTION";
				break;

			case 1:
				$html = "DURING SECTION";
				break;
			
			case 2:
				$html = "AFTER SECTION";
				break;
			default:
				$html = "";
				break;
		}

		return $html;
	}
}
