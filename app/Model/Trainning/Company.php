<?php

namespace App\Model\Trainning;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Company extends Model
{
    protected $table = "company";

    protected $primaryKey = "company_id";

    protected $fillable = [
    	"name",
    	"description",
    	"images"
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'company_id');
    }
}
