<?php

namespace App\Model\Trainning;

use Illuminate\Database\Eloquent\Model;
use App\Model\Trainning\Speaker;
class Session extends Model
{
    protected $table = "sessions";

    protected $primaryKey = "session_id";

    protected $fillable = [
    	"name",
    	"speaker_id",
    	"description",
      "trainning_id",
      "before",
      "during",
      "after"
    ];
   public function speaker()
  {
    return $this->belongsTo(Speaker::class, 'speaker_id');
  }
   public function trainning()
  {
    return $this->belongsTo(Trainning::class, 'trainning_id');
  }
}
