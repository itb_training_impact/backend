<?php

namespace App\Model\Trainning;

use Illuminate\Database\Eloquent\Model;
use App\Model\Trainning\Session;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\ModuleQuestion as Question;

class Module extends Model
{
	protected $table = "modules";

	protected $primaryKey = "module_id";

	protected $fillable = [
		"name",
		"session_id",
		"description",
		"code_module",
		"start_date",
		"status",
		"email",
	];

	public function session()
	{
		return $this->belongsTo(Session::class, 'session_id');
	}
   
	public function trainning()
	{
		return $this->belongsTo(Trainning::class, 'trainning_id');
	}

	public function question()
	{
		return $this->hasMany(Question::class, 'module_id');
	}
	public function questions()
	{
		return $this->belongsTo(Question::class, 'module_id');
	}
	public function behaviour_formulations()
	{
		return $this->hasMany(ModuleBehaviourFormulations::class, 'module_id', 'module_id');
	}
}