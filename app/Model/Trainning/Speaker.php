<?php

namespace App\Model\Trainning;

use Illuminate\Database\Eloquent\Model;

class Speaker extends Model
{
    protected $table = "speaker";

    protected $primaryKey = "speaker_id";

    protected $fillable = ["name","description","images"];
}
