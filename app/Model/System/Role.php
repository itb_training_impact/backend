<?php

namespace App\Model\System;

use Illuminate\Database\Eloquent\Model;
use App\Model\Level;

class Role extends Model
{
    protected $table        = 'sys_roles';
    protected $primaryKey   = 'role_id';

    /**
     * Get the user that owns the task.
     */
    public function tasks()
    {
        return $this->belongsTo('App\Model\System\Task', 'task_id', 'task_id');
    }

    /**
     * Get the user that owns the user.
     */
    public function level()
    {
        return $this->belongsTo('App\Model\Level', 'level_id', 'level_id');
    }

    public function getDataLevel($request = null, $limit = null)
    {
        $result = Level::orderBy('level','ASC');

        if($request->has('key') && $request->has('value'))
        {
            $key    = $request->key;
            $value  = $request->value;

            $result->where($key,'like','%'. $value .'%');
        }

        return $result->paginate($this->limit);
    }

    public function checkRole($level = null, $task = null)
    {
        return Role::where('level_id', $level)->where('task_id', $task)->first();
    }
}
