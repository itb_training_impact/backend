<?php

namespace App\Model\System;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'sys_modules';
}
