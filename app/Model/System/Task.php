<?php

namespace App\Model\System;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table        = 'sys_tasks';
    protected $primaryKey   = 'task_id';

    /**
     * Get the user that owns the module.
     */
    public function module()
    {
        return $this->belongsTo('App\Model\System\Module', 'module_id','modules_id');
    }
}
