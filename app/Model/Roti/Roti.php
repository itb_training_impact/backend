<?php

namespace App\Model\Roti;

use Illuminate\Database\Eloquent\Model;
use App\Model\Trainning\Trainning;

class Roti extends Model
{
    protected $table = "roti";

    protected $primaryKey = "roti_id";

    protected $fillable = [
    	"roti_code",
    	"roti_question",
    	"roti_answer",
    	"roti_correct_answer",
    	"group_id",
    	"type_id",
		"trainning_id",
		"session_id",
		"module_id",
    	"roti_is_required",
    	"roti_is_default",
		"roti_rating_start",
		"roti_rating_finish",
        "user_id",
        "criteria_id"
    ];

    public function type(){
   		return $this->belongsTo('App\Model\Master\Type','type_id');
   	}

    public function group(){
   		return $this->belongsTo('App\Model\Master\Dimension','group_id');
    }
       
    public function criteria(){
        return $this->belongsTo('App\Model\Master\Criteria','criteria_id');
    }
	public function ModuleQuestions(){
        return $this->hasMany('App\Model\Trainning\ModuleQuestion','roti_id');
    }
    public function totalTrainning($id)
    {
      $query = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->where('module_questions.roti_id', $id)
              ->groupBy('trainnings.trainning_id')
              ->get();
   
		   return count($query);
    }
}
