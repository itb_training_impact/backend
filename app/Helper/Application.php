<?php

namespace App\Helper;
use Illuminate\Support\Facades\DB;
use App\Model\Reaction\Reaction;
use App\Model\Learning\Learning;
use App\Model\Behaviour\Behaviour;
use App\Model\Performance\Performance;
use App\Model\Trainning\Session;
use App\Model\Trainning\Module;
use App\Model\Trainning\TrainningSubmit;
use App\Model\Roti\Roti;
use App\Model\Master\Boss;
use App\Model\Master\Partner;
use App\Model\Master\Subordinate;
use App\Model\Master\Criteria;
use App\User;

class Application
{
	public static function get_calculate($type='learning',$group_id, $training_id=null, $session_id=null, $module_id=null, $formulation_id=null, $user_id=null){
		if($type=='learning'){
			$result = Learning::select(\DB::raw("SUM(CASE 
                WHEN learning_rating_finish IS NOT NULL THEN learning_rating_finish
                WHEN learning_rating_finish IS NULL THEN 1
                END)AS result"));
			if(!empty($module_id)){
				$result = $result->join('module_questions', 'module_questions.learning_id', '=', 'learnings.learning_id')->where("group_id","=",$group_id);
				$result = $result->where("module_questions.module_id","=",$module_id);	
			}else if(!empty($training_id)){
				$result = $result->whereRaw("(learnings.trainning_id =".$training_id." OR learnings.learning_is_default = 1)");	
			}
		}else if($type=='behaviour'){
			$result = Behaviour::select(\DB::raw("SUM(CASE 
                WHEN behaviour_rating_finish IS NOT NULL THEN behaviour_rating_finish
                WHEN behaviour_rating_finish IS NULL THEN 1
                END)AS result"))
				->join('module_questions', 'module_questions.behaviour_id', '=', 'behaviours.behaviour_id')
				->where("group_id","=",$group_id)
				->where("module_questions.module_id","=",$module_id)
				->where('formulation_id','=',$formulation_id);
		}else if($type=='performance'){
				$result = Performance::select(\DB::raw("SUM(CASE 
                WHEN performance_rating_finish IS NOT NULL THEN performance_rating_finish
                WHEN performance_rating_finish IS NULL THEN 1
                END)AS result"))
				->join('module_questions', 'module_questions.performance_id', '=', 'performances.performance_id')
				->where("group_id","=",$group_id)
				->where("module_questions.module_id","=",$module_id);		
		}else if ($type == 'roti') {
			$session = Session::whereRaw("trainning_id = ".$training_id." AND LOWER(name) LIKE '%roti%'")->first();
			$module = Module::whereRaw("session_id = ".$session->session_id." AND LOWER(name) LIKE '%roti%'")->first();
				$result = TrainningSubmit::select(\DB::raw("submit_response AS result, submit_response_real_answer AS result_real"))
				->join("roti", "roti.roti_id", "=", "trainning_submits.roti_id")
				->where('trainning_submits.session_id',"=",$session->session_id)
				->where("trainning_submits.module_id","=",$module->module_id)
				->whereRaw("criteria_id IN (".$group_id.")")
				->where("trainning_submits.user_id","=",$user_id);
		}else{
			$result = Reaction::select(\DB::raw("SUM(CASE 
                WHEN reaction_rating_finish IS NOT NULL THEN reaction_rating_finish
                WHEN reaction_rating_finish IS NULL THEN 1
                END)AS result"));
			if(!empty($module_id)){
				$result = $result->join('module_questions', 'module_questions.reaction_id', '=', 'reactions.reaction_id')->where("group_id","=",$group_id);
				$result = $result->where("module_questions.module_id","=",$module_id);	
			}else if(!empty($training_id)){
				$result = $result->whereRaw("(reactions.trainning_id =".$training_id." OR reactions.reaction_is_default = 1)");	
			}			
		}				
		return $result->get();
	}

	public static function get_all_learning($group_id, $training_id=null, $formulation_id=null){
		
		$result = Learning::select(\DB::raw("SUM(CASE 
              WHEN learning_rating_finish IS NOT NULL THEN learning_rating_finish
              WHEN learning_rating_finish IS NULL THEN 1
              END)AS result"));
	 	if(!empty($training_id)){
			$result = $result->whereRaw("(learnings.trainning_id =".$training_id." OR learnings.learning_is_default = 1)");	
		}				
	return $result->get();
	}

	public static function get_criteria($type, $level){
		$result = Criteria::whereRaw("LOWER(criteria) = '".$type."' AND level = ".$level);
		return $result->first();
	}

	public static function get_all_reaction($group_id, $training_id=null, $formulation_id=null){

		$result = Reaction::select(\DB::raw("SUM(CASE 
              WHEN reaction_rating_finish IS NOT NULL THEN reaction_rating_finish
              WHEN reaction_rating_finish IS NULL THEN 1
              END)AS result"));
		 if(!empty($training_id)){
			$result = $result->whereRaw("(reactions.trainning_id =".$training_id." OR reactions.reaction_is_default = 1)");	
		}						
	return $result->get();
	}

	public static function get_all_behaviour($type=null,$group_id, $training_id=null, $session_id=null, $module_id=null, $formulation_id=null){
		$result = Behaviour::select(\DB::raw("SUM(CASE 
              WHEN behaviour_rating_finish IS NOT NULL THEN behaviour_rating_finish
              WHEN behaviour_rating_finish IS NULL THEN 1
              END)AS result"))
			->join('module_questions', 'module_questions.behaviour_id', '=', 'behaviours.behaviour_id')
			->where("group_id","=",$group_id)
			->where('formulation_id','=',$formulation_id);	
			return $result->get();
	}	
	public static function get_user($user_id){
		$result = User::where("id","=",$user_id);			
		return $result->get();
	}
	public static function get_relations($group, $user_id){
		if ($group == 'Boss Position') {
			$result = Boss::select(\DB::raw("boss_name AS name"))->where("user_id","=",$user_id);			
			return $result->first();
		}else if($group == 'Partner Position'){
			$result = Partner::select(\DB::raw("partner_name AS name"))->where("user_id","=",$user_id);			
			return $result->first();
		}else if($group == 'Subordinate Position'){
			$result = Subordinate::select(\DB::raw("subordinate_name AS name"))->where("user_id","=",$user_id);			
			return $result->first();
		}else if($group == 'Self Report'){
			$result = User::select(\DB::raw("name AS name"))->where("id","=",$user_id);			
			return $result->get();
		}
	}

}