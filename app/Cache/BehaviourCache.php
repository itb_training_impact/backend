<?php
namespace App\Cache;
/**
 * 
 */
use App\Model\Behaviour\Behaviour;
use Carbon\Carbon;
class BehaviourCache
{
	CONST CACHE_KEY = 'behaviours';
	public function all($orderBy)
	{
		$key = "all.{$orderBy}";
		$cacheKey = $this->getCacheKey($key);
		dd($cacheKey);
		return cache()->remember($cacheKey, 5, function() use($orderBy){
			return Behaviour::orderBy($orderBy)->get();
		});
	}
	public function get($id)
	{

	}
	public function getCacheKey($key)
	{
		$key = strtoupper($key);
		return self::CACHE_KEY .".$key";
	}
}