<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Model\Trainning\TrainningSubmit as Submit;
use App\Model\Behaviour\Story;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password','verified', 'level_id', 'dob', 'age', 'gender', 'etnis', 'religion', 'job', 'years_of_service', 'marriage', 'company_id', 'position_level_id', 'devision_id', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the phone record associated with the user.
     */
    public function level()
    {
        return $this->belongsTo('App\Model\Level','level_id','level_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Model\Trainning\Company','company_id');
    }

    public function position_level()
    {
        return $this->belongsTo('App\Model\Master\PositionLevel','position_level_id');
    }

    public function devision()
    {
        return $this->belongsTo('App\Model\Master\Devision','devision_id');
    }

    public function storyByTrainning($trainningID, $sessionID = null, $moduleID = null, $period = null, $sort = [])
    {
        $story = Story::where('user_id', $this->id)->where('trainning_id', $trainningID);

        if ($sessionID)
            $story->where('session_id', $sessionID);

        if ($moduleID)
            $story->where('module_id', $moduleID);

        if (isset($period))
            $story->where('submit_periode', $period);

        if ($sort)
            $story->orderBy($sort[0], $sort[1]);

        return $story->get();
    }

    public function boss()
    {
        return $this->belongsTo('App\Model\Master\Boss','user_id');
    }

    public function partner()
    {
        return $this->belongsTo('App\Model\Master\Partner','user_id');
    }

    public function subordinate()
    {
        return $this->belongsTo('App\Model\Master\Subordinate','user_id');
    }
    public function submit(){
        return $this->hasMany(Submit::class, 'user_id','id');
    }
     public function users(){
        return $this->hasMany(Story::class, 'user_id','id');
    }
}
