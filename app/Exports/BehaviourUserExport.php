<?php

namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use App\Model\Behaviour\Story;
use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Master\WordCategory;
use App\Model\Master\Formulation;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class BehaviourUserExport implements FromView
{
	private $trainningID;
	private $sessionID;
	private $moduleID;
	private $userID;

	public function __construct($trainningID, $sessionID = null, $moduleID = null, $userID)
	{
		$this->trainningID = $trainningID;
		$this->sessionID = $sessionID;
		$this->moduleID = $moduleID;
		$this->userID = $userID;
	}

	use Exportable;

	public function view(): View
	{
		$trainningID = $this->trainningID;
		$sessionID = $this->sessionID;
		$moduleID = $this->moduleID;
		$userID = $this->userID;

		$trainning = Trainning::find($trainningID);
		$participantID = explode(",", $trainning->participant_id);
		$user = User::find($userID);

		$wordCategories = WordCategory::where('id', '!=', 1)->where('code', '!=', 'SC01')->get();
		$formulas = Formulation::all();

		return view('export.behaviour.user_report', compact('user', 'wordCategories', 'formulas', 'trainningID', 'sessionID', 'moduleID'));
	}
}
