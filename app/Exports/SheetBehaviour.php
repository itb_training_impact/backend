<?php

namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use App\Model\Behaviour\Story;
use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\TrainningSubmit as Submit;
use App\Model\Master\WordCategory;
use App\Model\Master\Formulation;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;


class SheetBehaviour implements FromView
{
	private $trainningID;
	private $userID;
	public function __construct($userID, $trainningID)
	{
		$this->trainningID = $trainningID;
		$this->userID = $userID;
		//var_dump($trainningID);exit();
	}

	use Exportable;

	public function view(): View
	{
		//return "behahasil";
		$trainningID = $this->trainningID;
		$userID = $this->userID;
		
		$trainning = Trainning::find($trainningID);
		$users = User::find($userID);

		$wordCategories = WordCategory::where('id', '!=', 1)->where('code', '!=', 'SC01')->get();
		$formulas = Formulation::where('status',1)->get();
		$stories = Story::where('user_id',$userID)->where('trainning_id',$trainningID)->get();

   



		return view('export.Submit.sheetBehaviour',compact('trainning','users','trainningID','wordCategories','formulas','stories'));
	}
}
