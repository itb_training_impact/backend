<?php

namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use App\Model\Behaviour\Story;
use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\TrainningSubmit as Submit;
use App\Model\Master\WordCategory;
use App\Model\Master\Formulation;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
class ALL_Performance implements WithMultipleSheets
{
	  use Exportable;
    private $trainningID;
   
    public function __construct($trainningID)
    {
        $this->trainningID = $trainningID;
      
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $trainningID = $this->trainningID;
        $sheets = [];
        $sheets[] = new Performance_Question($trainningID);   
        $sheets[] = new Performance_Answer($trainningID);
        
        return $sheets;
    }
}
