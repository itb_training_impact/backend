<?php

namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\Session;
use App\Model\Trainning\Module;
use App\Model\Trainning\TrainningSubmit as Submit;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class QuestionAnswerExport implements FromView, ShouldAutoSize, WithStyles
{
    private $id;
    private $trainningID;
	private $sessionID;
    private $moduleID;
    private $active;
    private $level;
    
    public function __construct($id, $trainningID, $sessionID, $moduleID, $active, $level)
	{
        $this->id = $id;
		$this->trainningID = $trainningID;
		$this->sessionID = $sessionID;
        $this->moduleID = $moduleID;
        $this->active   = $active;
        $this->level    = $level;
	}

    use Exportable;

    public function styles(Worksheet $sheet)
    {
        $styleArray = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $sheet->getStyle('A1:C3')->applyFromArray($styleArray);
    }

    public function view(): View
	{
		$trainningID = $this->trainningID;
		$sessionID = $this->sessionID;
        $moduleID = $this->moduleID;
        $id = $this->id;
        $active = $this->active;
        $level  = $this->level;

        
        $trainning = Trainning::find($trainningID);
        $sessions  = Session::find($sessionID);
        $modules   = Module::find($moduleID);
        $participant = User::find($id);

        switch ($active) {
            case 'before':
            default:
                $submit_periode = 0;
                break;
            case 'during':
                $submit_periode = 1;
                break;
            case 'after':
                $submit_periode = 2;
                break;
        }

        $submit = Submit::where('trainning_id', '=', $trainningID)->where('user_id', '=', $id)->where('session_id', '=', $sessionID)->where('module_id','=',$moduleID)->where(['submit_periode' => $submit_periode])->get();

		return view('export.Submit.question_answer', compact('id', 'participant', 'sessions', 'modules', 'trainningID', 'sessionID', 'moduleID', 'trainning', 'submit', 'level'));
	}
}