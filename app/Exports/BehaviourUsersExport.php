<?php

namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use App\Model\Behaviour\Story;
use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Master\WordCategory;
use App\Model\Master\Formulation;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class BehaviourUsersExport implements FromView, WithTitle
{
	private $trainningID;
	private $sessionID;
	private $moduleID;
	private $userID;
	private $periode;
	public function __construct($trainningID, $sessionID, $moduleID, $userID, $periode)
	{
		$this->trainningID = $trainningID;
		$this->sessionID = $sessionID;
		$this->moduleID = $moduleID;
		$this->userID = $userID;
		$this->periode = $periode;
	}

	use Exportable;

	public function view(): View
	{
		$trainningID = $this->trainningID;
		$sessionID = $this->sessionID;
		$moduleID = $this->moduleID;
		$userID = $this->userID;
		$periode = $this->periode;

		$trainning = Trainning::find($trainningID);
		if(empty($userID)){
			$users = User::whereIn('id',explode(",",$trainning->participant_id))->get();
		}else{
			$users = User::where('id',$userID)->get();
		}
		
		$wordCategories = WordCategory::where('id', '!=', 1)->where('code', '!=', 'SC01')->get();
		$formulas = Formulation::all();

		return view('export.behaviour.users_report', compact('users', 'wordCategories', 'formulas', 'trainningID', 'sessionID', 'moduleID', 'periode'));
	}

	public function title(): string
    {
        return 'Behaviour Analysis';
    }
}
