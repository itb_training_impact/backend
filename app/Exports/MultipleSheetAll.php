<?php
namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use App\Model\Behaviour\Story;
use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\TrainningSubmit as Submit;
use App\Model\Master\WordCategory;
use App\Model\Master\Formulation;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Illuminate\Contracts\Queue\ShouldQueue;

class MultipleSheetAll implements WithMultipleSheets, ShouldQueue
{
    use Exportable;
    private $trainingID;
    private $sessionID;
    private $moduleID;
    private $periode;
    public function __construct($trainingID, $sessionID, $moduleID, $periode)
    {
        $this->trainingID = $trainingID;
        $this->sessionID = $sessionID;
        $this->moduleID = $moduleID;
        $this->periode = $periode;
       // var_dump($trainingID);exit();
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $trainingID = $this->trainingID;
        $sessionID = $this->sessionID;
        $moduleID = $this->moduleID;
        $periode = $this->periode;
        $sheets = [];
        $sheets[] = new All_Question($trainingID, $sessionID, $moduleID, $periode);
        $sheets[] = new All_Reaction($trainingID, $sessionID, $moduleID, $periode);
        $sheets[] = new All_Learning($trainingID, $sessionID, $moduleID, $periode);  
        $sheets[] = new All_Behaviour360($trainingID, $sessionID, $moduleID, $periode);   
        $sheets[] = new BehaviourUsersExport($trainingID, $sessionID, $moduleID, null, $periode);
        
        return $sheets;
    }
}