<?php

namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use App\Model\Behaviour\Story;
use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\TrainningSubmit as Submit;
use App\Model\Master\WordCategory;
use App\Model\Master\Formulation;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;


class SheetReaction implements FromView, ShouldAutoSize, WithStyles
{
	private $trainningID;
	private $userID;
	public function __construct($userID, $trainningID)
	{
		$this->trainningID = $trainningID;
		$this->userID = $userID;
	}

	use Exportable;
	 public function styles(Worksheet $sheet)
    {
        $styleArray = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $sheet->getStyle('A1:C3')->applyFromArray($styleArray);
    }

	public function view(): View
	{
		//return "behahasil";
		$trainningID = $this->trainningID;
		$userID = $this->userID;
		
		$trainning = Trainning::find($trainningID);
		$users = User::find($userID);

		$wordCategories = WordCategory::where('id', '!=', 1)->where('code', '!=', 'SC01')->get();
		$formulas = Formulation::where('status',1)->get();
		$submit = Submit::where('user_id',$userID)->whereNotNull('reaction_id')->groupBy('reaction_id')->get();
		//var_dump($submit);exit();
        $reaction           = Submit::filterCalculation($trainningID, null, null, $userID,  null, 'reaction')->first();
        $training_submit_reaction = Submit::where('trainning_id',$trainningID)->where('user_id',$userID)->take(1)->get();
       // print_r($behaviour_group);exit();
        $data = [
            'reaction' => $reaction,
            'training_submit_reaction' => $training_submit_reaction,
        ];


		return view('export.Submit.sheetReaction',compact('trainning','users','data','trainningID','submit','userID'));
	}
}
