<?php

namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\Session;
use App\Model\Trainning\Module;
use App\Model\Trainning\TrainningSubmit as Submit;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class QuestionAnswerExportAll implements FromView, ShouldAutoSize, WithStyles
{
    private $userID;
    private $trainningID;
	
    
    public function __construct($userID, $trainningID)
	{
        $this->userID = $userID;
		$this->trainningID = $trainningID;
		//var_dump($trainningID);exit();
	}

    use Exportable;

    public function styles(Worksheet $sheet)
    {
        $styleArray = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $sheet->getStyle('A1:C3')->applyFromArray($styleArray);
    }

    public function view(): View
	{
		$trainningID = $this->trainningID;
		$userID = $this->userID;

        
        $trainning = Trainning::find($trainningID);
        $participant = User::find($userID);

        $submit = Submit::where('trainning_id', '=', $trainningID)->where('user_id', '=', $userID)->get();

		return view('export.Submit.question_answer_participant', compact('participant','trainning','submit'));
	}
}