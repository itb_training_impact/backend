<?php

namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use App\Model\Behaviour\Story;
use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\TrainningSubmit as Submit;
use App\Model\Master\WordCategory;
use App\Model\Master\Formulation;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;

class Performance_Answer implements FromView, WithTitle
{
	private $trainningID;
    
    public function __construct($trainningID)
	{
    
		$this->trainningID = $trainningID;
	}

	use Exportable;

	public function view(): View
	{
		$trainningID = $this->trainningID;
        $performances = Submit::filterDimension($trainningID, null, null, null, 0, 'performance')->get();
        //var_dump($data['learning_group']);exit();
		return view('export.all_performance',compact('trainningID','performances'));
	}

    public function title(): string
    {
        return 'Performance Result';
    }
}
