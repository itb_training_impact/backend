<?php
namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use App\Model\Behaviour\Story;
use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\TrainningSubmit as Submit;
use App\Model\Master\WordCategory;
use App\Model\Master\Formulation;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class MultipleSheet implements WithMultipleSheets
{
    use Exportable;
    private $trainningID;
    private $userID;
    public function __construct($userID, $trainningID)
    {
        $this->trainningID = $trainningID;
        $this->userID = $userID;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $trainningID = $this->trainningID;
        $userID = $this->userID;
        return [
            'QuestionAnswerExportAll' => new QuestionAnswerExportAll ($userID, $trainningID),
            'Reaction' => new SheetReaction($userID, $trainningID),
            'Learning' => new SheetLearning($userID, $trainningID),
            'Behaviour' => new SheetBehaviour($userID, $trainningID),
            
        ];
    }
}