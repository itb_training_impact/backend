<?php

namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\Session;
use App\Model\Trainning\Module;
use App\Model\Trainning\TrainningSubmit as Submit;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SubmitUsersExport implements FromView, ShouldAutoSize, WithStyles
{
    private $id;
    private $trainningID;
	private $sessionID;
    private $moduleID;
    private $active;
    private $section;
    
    public function __construct($id, $trainningID, $sessionID, $moduleID, $active, $section)
	{
        $this->id = $id;
		$this->trainningID = $trainningID;
		$this->sessionID = $sessionID;
        $this->moduleID = $moduleID;
        $this->active   = $active;
        $this->section  = $section;
	}

    use Exportable;

    public function styles(Worksheet $sheet)
    {
        $styleArray = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $sheet->getStyle('A1:C3')->applyFromArray($styleArray);
    }

    public function view(): View
	{
		$trainningID = $this->trainningID;
		$sessionID = $this->sessionID;
        $moduleID = $this->moduleID;
        $id = $this->id;
        $active = $this->active;
        $section = $this->section;

        
        $trainning = Trainning::find($trainningID);
        $sessions  = Session::find($sessionID);
        $modules   = Module::find($moduleID);

        switch ($active) {
            case 'before':
            default:
                $submit_periode = 0;
                break;
            case 'during':
                $submit_periode = 1;
                break;
            case 'after':
                $submit_periode = 2;
                break;
        }

        $reaction           = Submit::filterCalculation($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'reaction')->first();
        $learning           = Submit::filterCalculation($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'learning')->first();
        $behaviour          = Submit::filterCalculation($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'behaviour')->first();
        $reaction_group     = Submit::filterDimension($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'reaction')->get();
        $learning_group     = Submit::filterDimension($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'learning')->get();
        $behaviour_group    = Submit::filterDimension($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'behaviour')->get();
        
        $data = [
            'reaction' => $reaction,
            'learning' => $learning,
            'behaviour' => $behaviour,
            'reaction_group' => $reaction_group,
            'learning_group' => $learning_group,
            'behaviour_group' => $behaviour_group
        ];

		return view('export.Submit.user_submit', compact('id', 'data', 'sessions', 'modules', 'trainningID', 'sessionID', 'moduleID', 'trainning', 'section'));
	}
}