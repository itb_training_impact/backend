<?php

namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use App\Model\Behaviour\Story;
use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\TrainningSubmit as Submit;
use App\Model\Master\WordCategory;
use App\Model\Master\Formulation;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class All_Reaction implements FromView, WithTitle
{
	private $trainingID;
	private $sessionID;
    private $moduleID;
    private $periode;
	

	public function __construct($trainingID, $sessionID, $moduleID, $periode)
	{
		$this->trainingID = $trainingID;
		$this->sessionID = $sessionID;
        $this->moduleID = $moduleID;
        $this->periode = $periode;
	}

	use Exportable;

	public function view(): View
	{
		$trainingID = $this->trainingID;
		$sessionID = $this->sessionID;
        $moduleID = $this->moduleID;
        $periode = $this->periode;
        
        //var_dump($data['learning_group']);exit();
		return view('export.all_reaction',compact('trainingID', 'sessionID', 'moduleID', 'periode'));
	}

	public function title(): string
    {
        return 'Reaction';
    }
}
