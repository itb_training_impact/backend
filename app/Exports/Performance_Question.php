<?php

namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\Session;
use App\Model\Trainning\Module;
use App\Model\Trainning\TrainningSubmit as Submit;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class Performance_Question implements FromView, ShouldAutoSize, WithStyles, WithTitle
{

    private $trainningID;

    
    public function __construct($trainningID)
	{
    
		$this->trainningID = $trainningID;
    
	}

    use Exportable;

    public function styles(Worksheet $sheet)
    {
        $styleArray = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $sheet->getStyle('A1:C3')->applyFromArray($styleArray);
    }

    public function view(): View
	{
		$trainningID = $this->trainningID;
		$trainningId = Trainning::find($trainningID);
        $submit = Submit::where('trainning_id', '=', $trainningID);
		return view('export.performance_question', compact('submit','trainningId'));
    }

    public function title(): string
    {
        return 'Question Performance and Roti';
    }
}