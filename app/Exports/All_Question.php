<?php

namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\Session;
use App\Model\Trainning\Module;
use App\Model\Trainning\TrainningSubmit as Submit;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class All_Question implements FromView, ShouldAutoSize, WithStyles, WithTitle
{

    private $trainningID;
	private $sessionID;
    private $moduleID;
    private $periode;
    
    public function __construct($trainningID, $sessionID, $moduleID, $periode)
	{
    
		$this->trainningID = $trainningID;
        $this->sessionID = $sessionID;
        $this->moduleID = $moduleID;
        $this->periode = $periode;
		//var_dump($trainningID);exit();
	}

    use Exportable;

    public function styles(Worksheet $sheet)
    {
        $styleArray = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ];
        $sheet->getStyle('A1:C3')->applyFromArray($styleArray);
    }

    public function view(): View
	{
		$trainningID = $this->trainningID;
		$sessionID = $this->sessionID;
        $moduleID = $this->moduleID;
        $periode = $this->periode;

        $submit = Submit::where('trainning_id', '=', $trainningID)->where("session_id", "=", $sessionID)->where("module_id", "=", $moduleID)->where('submit_periode',"=", $periode);#->where("level_intermediary","=",null);

		return view('export.all_question_answer', compact('submit'));
    }

    public function title(): string
    {
        return 'Question And Answer';
    }
}