<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Jobs\ActiveModule;
use DB;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
         Commands\status::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $time = Closed::first();
        //  $schedule->job(new ActiveModule)
        //  ->everyMinute()
        // ->appendOutputTo(storage_path('logs/inspire.log'));
       $schedule->command('status:active')->everyMinute();

    //       $schedule->call(function () {

    //     DB::table('logs')->insert([
    //         'name' => "http://www.example.com/demo-scheduler",
    //         'description' => "This is a message from laravel scheduler with every 1 minute...",
       
    //     ]);

    // })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
