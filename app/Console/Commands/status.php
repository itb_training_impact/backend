<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Model\Master\Group;
use App\Model\Master\Boss;
use App\Model\Master\Partner;
use App\Model\Master\Subordinate;
use App\Model\Trainning\Module;
use App\Model\Trainning\ModuleQuestion as Questions;
use App\Model\Trainning\Session;
use App\Model\Trainning\Trainning;
class status extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'status:active';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
 
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $dt = Carbon::now('Asia/Jakarta')->toDateTimeString();
             DB::table('modules')->where('start_date', '=', $dt)->update(['status' => 1]);

 
    $modulesBefore = Module::join("sessions", "sessions.session_id", "=", "modules.session_id")->where('modules.status',1)->where('sessions.before',1)->where('modules.start_date','=',$dt)->where('email',null)->get();
    $modulesDuring = Module::join("sessions", "sessions.session_id", "=", "modules.session_id")->where('modules.status',1)->where('sessions.during',1)->where('modules.start_date','=',$dt)->where('email',null)->get();
    $modulesAfter = Module::join("sessions", "sessions.session_id", "=", "modules.session_id")->where('modules.status',1)->where('sessions.after',1)->where('modules.start_date','=',$dt)->where('email',null)->get();

              
   

       
        if (count($modulesBefore)>0) {
        foreach ($modulesBefore as $key => $value) {
                $moduleID = $value->module_id;
               
          }
        $model      = Module::find($moduleID);
        $session    = Session::where('session_id',$model->session_id)->first();
        $training   = Trainning::where('trainning_id',$session->trainning_id)->first();
        $questions  = Questions::WhereIn('module_id',$model)->get();
        $behavi     = false;

        $participant    = explode(',',$training['participant_id']);
        $users          = User::whereIn('id',$participant)->get();
        $boss           = Boss::whereIn('user_id',$participant)->get();
        $partners       = Partner::whereIn('user_id',$participant)->get();
        $subordinates   = Subordinate::whereIn('user_id',$participant)->get();
        $field = 'before';
        foreach($questions as $question){
           
        }
        if($question->behaviour_id != null){
            foreach($boss as $boss){
                $user = User::where('id',$boss->user_id)->get();
                foreach ($user as $user){
                    Mail::send('email.notifyTrainingBoss', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'boss'=>$boss,'field'=>$field], function($message) use ($boss)
                    {    
                        $message->to($boss->boss_email)->subject('NOTIFICATION START TRAINING');    
                    });
                }
            }
            foreach($partners as $partner){
                $user = User::where('id',$partner->user_id)->get();
                foreach ($user as $user){
                    Mail::send('email.notifyTrainingPartner', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'partner'=>$partner,'field'=>$field], function($message) use ($partner)
                    {    
                        $message->to($partner->partner_email)->subject('NOTIFICATION START TRAINING');    
                    });
                }
            }
            foreach($subordinates as $subordinate){
                $user = User::where('id',$subordinate->user_id)->get();
                foreach ($user as $user){
                    Mail::send('email.notifyTrainingSubordinate', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'subordinate'=>$subordinate,'field'=>$field], function($message) use ($subordinate)
                    {    
                        $message->to($subordinate->subordinate_email)->subject('NOTIFICATION START TRAINING');    
                    });
                }
            }
        }
         if($question->roti_id != null){
                        foreach($boss as $boss){
                        $user = User::where('id',$boss->user_id)->get();
                        foreach ($user as $user){
                        Mail::send('email.notifyTrainingPerfo', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'boss'=>$boss,'field'=>$field], function($message) use ($boss)
                        {    
                            $message->to($boss->boss_email)->subject('NOTIFICATION START TRAINING');    
                        });
                    }
                }
            }else{
                    foreach ($users as $user){
                    Mail::send('email.notifyTraining', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'field'=>$field], function($message) use ($user)
                    {    
                    $message->to($user->email)->subject('NOTIFICATION START TRAINING');    
                    });
                }

            }
        
         $modulesBefore = Module::where('start_date', '=', $dt)->update(['email'=>1]);
        }


       if (count($modulesDuring)>0) {
        foreach ($modulesDuring as $key => $value) {
                $moduleID = $value->module_id;
             
               
          }
        $model      = Module::find($moduleID);
        $session    = Session::where('session_id',$model->session_id)->first();
        $training   = Trainning::where('trainning_id',$session->trainning_id)->first();
        $questions  = Questions::WhereIn('module_id',$model)->get();
        $behavi     = false;

        $participant    = explode(',',$training['participant_id']);
        $users          = User::whereIn('id',$participant)->get();
        $boss           = Boss::whereIn('user_id',$participant)->get();
        $partners       = Partner::whereIn('user_id',$participant)->get();
        $subordinates   = Subordinate::whereIn('user_id',$participant)->get();
        $field = 'during';
        foreach($questions as $question){
    

        }
       if($question->behaviour_id != null){
            foreach($boss as $boss){
                $user = User::where('id',$boss->user_id)->get();
                foreach ($user as $user){
                    Mail::send('email.notifyTrainingBoss', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'boss'=>$boss,'field'=>$field], function($message) use ($boss)
                    {    
                        $message->to($boss->boss_email)->subject('NOTIFICATION START TRAINING');    
                    });
                }
            }
            foreach($partners as $partner){
                $user = User::where('id',$partner->user_id)->get();
                foreach ($user as $user){
                    Mail::send('email.notifyTrainingPartner', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'partner'=>$partner,'field'=>$field], function($message) use ($partner)
                    {    
                        $message->to($partner->partner_email)->subject('NOTIFICATION START TRAINING');    
                    });
                }
            }
            foreach($subordinates as $subordinate){
                $user = User::where('id',$subordinate->user_id)->get();
                foreach ($user as $user){
                    Mail::send('email.notifyTrainingSubordinate', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'subordinate'=>$subordinate,'field'=>$field], function($message) use ($subordinate)
                    {    
                        $message->to($subordinate->subordinate_email)->subject('NOTIFICATION START TRAINING');    
                    });
                }
            }
        }
        foreach ($users as $user){
            Mail::send('email.notifyTraining', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'field'=>$field], function($message) use ($user)
            {    
                $message->to($user->email)->subject('NOTIFICATION START TRAINING');    
            });
        }
         $modulesDuring = Module::where('start_date', '=', $dt)->update(['email'=>1]);
        }


               if (count($modulesAfter)>0) {
        foreach ($modulesAfter as $key => $value) {
                $moduleID = $value->module_id;
               
          }
        $model      = Module::find($moduleID);
        $session    = Session::where('session_id',$model->session_id)->first();
        $training   = Trainning::where('trainning_id',$session->trainning_id)->first();
        $questions  = Questions::WhereIn('module_id',$model)->get();
        $behavi     = false;

        $participant    = explode(',',$training['participant_id']);
        $users          = User::whereIn('id',$participant)->get();
        $boss           = Boss::whereIn('user_id',$participant)->get();
        $partners       = Partner::whereIn('user_id',$participant)->get();
        $subordinates   = Subordinate::whereIn('user_id',$participant)->get();
        $field = 'after';
        foreach($questions as $question){
           
        }
        if($question->behaviour_id != null){
            foreach($boss as $boss){
                $user = User::where('id',$boss->user_id)->get();
                foreach ($user as $user){
                    Mail::send('email.notifyTrainingBoss', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'boss'=>$boss,'field'=>$field], function($message) use ($boss)
                    {    
                        $message->to($boss->boss_email)->subject('NOTIFICATION START TRAINING');    
                    });
                }
            }
            foreach($partners as $partner){
                $user = User::where('id',$partner->user_id)->get();
                foreach ($user as $user){
                    Mail::send('email.notifyTrainingPartner', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'partner'=>$partner,'field'=>$field], function($message) use ($partner)
                    {    
                        $message->to($partner->partner_email)->subject('NOTIFICATION START TRAINING');    
                    });
                }
            }
            foreach($subordinates as $subordinate){
                $user = User::where('id',$subordinate->user_id)->get();
                foreach ($user as $user){
                    Mail::send('email.notifyTrainingSubordinate', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'subordinate'=>$subordinate,'field'=>$field], function($message) use ($subordinate)
                    {    
                        $message->to($subordinate->subordinate_email)->subject('NOTIFICATION START TRAINING');    
                    });
                }
            }
        }
        foreach ($users as $user){
            Mail::send('email.notifyTraining', ['user'=>$user,'trainning'=>$training,'session'=>$session,'module'=>$model,'field'=>$field], function($message) use ($user)
            {    
                $message->to($user->email)->subject('NOTIFICATION START TRAINING');    
            });
        }
         $modulesAfter = Module::where('start_date', '=', $dt)->update(['email'=>1]);
        }


    }
    
}
