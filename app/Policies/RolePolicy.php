<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the role.
     *
     * @param  \App\User  $user
     * @param  \App\Model\System\Role  $role
     * @return mixed
     */
    public function index(User $user, $modules)
    {
        $permission = "list";

        $module     = \App\Model\System\Module::where('module', $modules)->first();


        if ($module != NULL) {

            $task       = \App\Model\System\Task::where('task', $permission)
                        ->where('module_id', $module->modules_id)
                        ->first();

            if ($task != NULL) {

                $role       = \App\Model\System\Role::where('task_id', $task->task_id)->pluck('level_id');

                if ($role != NULL) {
                    if (in_array($user->level_id, $role->toArray())) {
                        return true;
                    }
                }
            }
        }
    }

    /**
     * Determine whether the user can view the role.
     *
     * @param  \App\User  $user
     * @param  \App\Model\System\Role  $role
     * @return mixed
     */
    public function view(User $user, $modules)
    {
        $permission = "view";

        $module     = \App\Model\System\Module::where('module', $modules)->first();

        if ($module != NULL) {

            $task       = \App\Model\System\Task::where('task', $permission)
                        ->where('module_id', $module->modules_id)
                        ->first();

            if ($task != NULL) {

                $role       = \App\Model\System\Role::where('task_id', $task->task_id)->pluck('level_id');

                if ($role != NULL) {
                    if (in_array($user->level_id, $role->toArray())) {
                        return true;
                    }
                }
            }
        }
    }

    /**
     * Determine whether the user can create roles.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user, $modules)
    {
        $permission = "create";

        $module     = \App\Model\System\Module::where('module', $modules)->first();

        if ($module != NULL) {

            $task       = \App\Model\System\Task::where('task', $permission)
                        ->where('module_id', $module->modules_id)
                        ->first();

            if ($task != NULL) {

                $role       = \App\Model\System\Role::where('task_id', $task->task_id)->pluck('level_id');

                if ($role != NULL) {
                    if (in_array($user->level_id, $role->toArray())) {
                        return true;
                    }
                }
            }
        }
    }

    /**
     * Determine whether the user can edit roles.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function edit(User $user, $modules)
    {
        $permission = "edit";

        $module     = \App\Model\System\Module::where('module', $modules)->first();

        if ($module != NULL) {

            $task       = \App\Model\System\Task::where('task', $permission)
                        ->where('module_id', $module->modules_id)
                        ->first();

            if ($task != NULL) {

                $role       = \App\Model\System\Role::where('task_id', $task->task_id)->pluck('level_id');

                if ($role != NULL) {
                    if (in_array($user->level_id, $role->toArray())) {
                        return true;
                    }
                }
            }
        }
    }

    /**
     * Determine whether the user can update the role.
     *
     * @param  \App\User  $user
     * @param  \App\Model\System\Role  $role
     * @return mixed
     */
    public function update(User $user, $modules)
    {
        $permission = "update";

        $module     = \App\Model\System\Module::where('module', $modules)->first();

        if ($module != NULL) {

            $task       = \App\Model\System\Task::where('task', $permission)
                        ->where('module_id', $module->modules_id)
                        ->first();

            if ($task != NULL) {

                $role       = \App\Model\System\Role::where('task_id', $task->task_id)->pluck('level_id');

                if ($role != NULL) {
                    if (in_array($user->level_id, $role->toArray())) {
                        return true;
                    }
                }
            }
        }
    }

    /**
     * Determine whether the user can delete the role.
     *
     * @param  \App\User  $user
     * @param  \App\Model\System\Role  $role
     * @return mixed
     */
    public function delete(User $user, $modules)
    {
        $permission = "delete";

        $module     = \App\Model\System\Module::where('module', $modules)->first();

        if ($module != NULL) {

            $task       = \App\Model\System\Task::where('task', $permission)
                        ->where('module_id', $module->modules_id)
                        ->first();

            if ($task != NULL) {

                $role       = \App\Model\System\Role::where('task_id', $task->task_id)->pluck('level_id');

                if ($role != NULL) {
                    if (in_array($user->level_id, $role->toArray())) {
                        return true;
                    }
                }
            }
        }
    }

    public function super(User $user)
    {
        if ($user->level_id == 1 || $user->Level->level == "Super Admin") {
            return true;
        }
    }

    public function admin(User $user)
    {
        if ($user->level_id == 1 || $user->Level->level == "Super Admin" || $user->level_id == 2 || $user->Level->level == "Admin") {
            return true;
        }
    }

    public function sales(User $user)
    {
        if ($user->level_id == 1 || $user->Level->level == "Super Admin" || $user->level_id == 2 || $user->Level->level == "Admin" || $user->level_id == 3 || $user->Level->level == "Sales" ) {
            return true;
        }
    }
}
