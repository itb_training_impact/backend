<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|string|min:3',
            'username'              => 'required|string|min:3',
            'email'             => 'required|string|email|unique:users',
            'password'          => 'required|string|min:5',
            'confirm-password'  => 'required|same:password',
            'level_id'          => 'required',
        ];
    }
}
