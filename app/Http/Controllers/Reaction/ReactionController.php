<?php

namespace App\Http\Controllers\Reaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\Type;
use App\Model\Master\Dimension as Group;
use App\Model\Reaction\Reaction;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\TrainningDetail;
use App\Model\Trainning\ModuleQuestion;
use App\Model\Trainning\Session;
use App\Model\Trainning\Module;
use App\Imports\ReactionImport;
use Maatwebsite\Excel\Facades\Excel;

use Auth;
use DB;

class ReactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application reaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $reaction   = Reaction::orderBy('created_at', 'DESC');

        if($request->has('group_id') && !empty($request->group_id))
            $reaction   = $reaction->where('group_id', $request->group_id);

        if($request->has('type_id') && !empty($request->type_id))
            $reaction   = $reaction->where('type_id', $request->type_id);

        if($request->has('reaction_code') && !empty($request->reaction_code))
            $reaction   = $reaction->where('reaction_code', $request->reaction_code);
        // if($request->has('reaction_is_default') && !empty($request->reaction_is_default))
        //     $reaction   = $reaction->where('reaction_is_default', $request->reaction_is_default);
        if($request->has('reaction_question') && !empty($request->reaction_question))
            $reaction   = $reaction->where('reaction_question', $request->reaction_question);
        // if( Auth::user()->level_id != 1){
        //   $reaction = $reaction->where('reaction_is_default', 1)->whereOr('user_id', Auth::user()->id);
        // }        
        $reaction   = $reaction->paginate(20);
    	   $type 		= Type::all();
        $group      = Group::where('is_reaction', 0)->get();

        return view('reaction.index', compact('reaction','type', 'group'));
    }

    public function create()
    {
        $type           = Type::all();
        $group          = Group::where('is_reaction', 0)->get();
        $trainning      = Trainning::where('user_id', Auth::User()->id)->get();

        $trainningId = null;
        $sessionId = null;
        $moduleId = null;

        $optGroup       = "";
        $optType        = "";
        $optTrainning   = "";

        foreach ($type as $key => $value) {
            $selected = $value->type_label == "rating" ? "selected" : "";
            $optType .= "<option value='". $value->type_label ."' ". $selected .">". $value->type ."</option>";
        }

        foreach ($group as $key => $value) {
            $optGroup .= "<option value='". $value->group_id ."'>". $value->group ."</option>";
        }

        foreach ($trainning as $key => $value) {
            $optTrainning .= "<option value='". $value->trainning_id ."'>". $value->trainning_title ."</option>";
        }
        
    	return view('reaction.create', compact('type', 'group', 'trainning', 'optGroup', 'optType', 'optTrainning','trainningId','sessionId','moduleId'));
    }

    public function createCustom($trainningID,$sessionID,$moduleID)
    {
        $trainningId      = Trainning::where('trainning_id',$trainningID)->get();
        $sessionId       = Session::where('session_id',$sessionID)->get();
        $moduleId       = Module::where('module_id',$moduleID)->get();
        $type           = Type::all();
        $group          = Group::where('is_reaction', 0)->get();
        $trainning      = Trainning::where('user_id', Auth::User()->id)->get();

        $optGroup       = "";
        $optType        = "";
        $optTrainning   = "";

        foreach ($type as $key => $value) {
            $selected = $value->type_label == "rating" ? "selected" : "";
            $optType .= "<option value='". $value->type_label ."' ". $selected .">". $value->type ."</option>";
        }

        foreach ($group as $key => $value) {
            $optGroup .= "<option value='". $value->group_id ."'>". $value->group ."</option>";
        }

        foreach ($trainning as $key => $value) {
            $optTrainning .= "<option value='". $value->trainning_id ."'>". $value->trainning_title ."</option>";
        }
        
      return view('reaction.create', compact('type', 'group', 'trainning','trainningId', 'optGroup', 'optType', 'optTrainning','sessionId','moduleId'));
    }

    public function get_session($id)
    {
        $sessions = Session::where('trainning_id', $id)->pluck('name','session_id');
        return response()->json($sessions);
    }

    public function get_module($id)
    {
        $modules = Module::where('session_id', $id)->pluck('name','module_id');
        return response()->json($modules);
    }

    public function store(Request $request)
    {
    //  return $request;
    	$params = $request->except('_token');
    	$merge  = array();

    	// return $params; exit;
        $code           = $request->reaction_code;
        $question       = $request->reaction_question;
        $correct_answer = $request->reaction_correct_answer;
        $typeId         = $request->type_id;
        $groupId        = $request->group_id;
        $trainningId    = $request->trainning_id;
        $sessionId      = $request->sessions;
        $moduleId       = $request->modules;
        $isRequired     = $request->reaction_is_required;
        $isDefault      = $request->reaction_is_default;
        $ratingStart    = $request->reaction_rating_start;
        $ratingFinish   = $request->reaction_rating_finish;        
          
            foreach ($code as $key => $value) {
              DB::beginTransaction();
              $answer = $request['reaction_answer_'.($key+1)];    
              if($answer == ""){
                $answer = $request->reaction_answer;
              }
              $reaction_correct_answer = $request->reaction_correct_answer;

              if(!empty($answer) || isset($answer))                  
                  $merge['reaction_answer'] = implode(",", $answer);  

              if($reaction_correct_answer ==2)
                  $merge['reaction_correct_answer'] = $answer[$key+0];
              if($reaction_correct_answer ==3)
                  $merge['reaction_correct_answer'] = $answer[$key+1];
              if($reaction_correct_answer ==4)
                  $merge['reaction_correct_answer'] = $answer[$key+2];
              if($reaction_correct_answer ==5)
                  $merge['reaction_correct_answer'] = $answer[$key+3];
              if($reaction_correct_answer ==6)
                  $merge['reaction_correct_answer'] = $answer[$key+4];
              if($reaction_correct_answer ==7)
                  $merge['reaction_correct_answer'] = $answer[$key+5];

              if(!empty($ratingStart) || isset($ratingStart))
                  $merge['reaction_rating_start'] = !empty($ratingStart[$key]) ? $ratingStart[$key] : null;

              if(!empty($ratingFinish) || isset($ratingFinish))
                  $merge['reaction_rating_finish'] = !empty($ratingFinish[$key]) ? $ratingFinish[$key] : null;

              $type = Type::where('type_label', $typeId[$key])->first();

              if($type)
                  $merge['type_id'] = $type->type_id;

              $data = [
                  'user_id' => Auth::user()->id,
                  'reaction_code' => $code[$key],
                  'reaction_question' => $question[$key],
                  'reaction_answer' => null,
                  'reaction_correct_answer' =>null,
                  'group_id' => $groupId[$key],
                  'type_id' => $typeId[$key],
                  'trainning_id' => !empty($trainningId[$key]) ? $trainningId[$key] : null,
                  'session_id' => !empty($sessionId[$key]) ? $sessionId[$key] : null,
                  'module_id' => !empty($moduleId[$key]) ? $moduleId[$key] : null,
                  'reaction_is_required' => $isRequired[$key] == "on" ? 1 : 0,
                  'reaction_is_default' => $isDefault[$key] == "on" ? 1 : 0                  
              ];

              $reaction = Reaction::create(
                  array_merge(
                      $data,
                      $merge
                  )
              );              
              DB::commit();              
              $trainings = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->whereNotIn('module_questions.reaction_id',[''])
              ->groupBy('trainnings.trainning_id');
              if(!empty($data['trainning_id'])){
                $trainings = $trainings->where('trainnings.trainning_id', $data['trainning_id'])->get();
              }else{
                $trainings = $trainings->get();
              }              

              foreach ($trainings as $key => $value) {
                $session = Session::find($value->session_id);
                $periodeArray = array();
                if($session->before == "1"){
                  array_push($periodeArray, 0);
                }elseif ($session->during == "1"){
                  array_push($periodeArray, 1);
                }elseif($session->after == "1"){
                  array_push($periodeArray, 2);
                }
                  foreach ($periodeArray as $value_periode) {          
                    switch ($value_periode) {
                      case 0:
                          $module = "before";
                          break;
                      case 1:
                          $module = "during";
                          break;
                      case 2:
                          $module = "after";
                          break;
                      default:
                          $module = "";
                          break;
                    } 
                    
                    $data = [
                      'module_id' => $value->module_id,
                      'reaction_id' => $reaction->reaction_id,
                      'training_detial_periode' => $module
                    ];                                  
                    DB::beginTransaction();
                    ModuleQuestion::create($data); 
                    DB::commit(); 
                  }
              }              
            }            
        


    	$request->session()->flash('status', '200');
      $request->session()->flash('msg', 'Reaction has been added');
      if(!empty($request->save_and_new)){
        return redirect('reaction/create');
      }else{
        return redirect('reaction');
      }      
    }

    public function edit($id)
    {
        $reaction       = Reaction::find($id);
        $type           = Type::all();
        $group          = Group::where('is_reaction', 0)->get();
        $trainning      = Trainning::where('user_id', Auth::User()->id)->get();
        $session        = Session::where('session_id', $reaction['session_id'])->first();
        $module         = Module::where('module_id', $reaction['module_id'])->first();

        return view('reaction.edit', compact('reaction','type','group','trainning','session','module'));
    }

    public function show($id)
    {
        $reaction = Reaction::find($id);
        return view('reaction.detail', compact('reaction'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->except(['_token', '_method']); 

        $reaction = Reaction::findOrFail($id);

        if($reaction) {
          
            $merge = array(
                'reaction_is_default' => $request->reaction_is_default == "on" ? 1 : 0,
                'reaction_is_required' => $request->reaction_is_required == "on" ? 1 : 0,
                'reaction_answer' => implode(",", (array) $request->reaction_answer),
                'trainning_id' => $request->reaction_is_default == "on" ? null : $request->trainning_id,
                'session_id' => $request->reaction_is_default == "on" ? null : $request->sessions,
                'module_id' => $request->reaction_is_default == "on" ? null : $request->modules,
                'reaction_correct_answer' => implode(",",(array) $request->reaction_correct_answer)
            );
            
            $reaction->update(
                array_merge($data, $merge)
            );

            $module_questions = ModuleQuestion::join('modules', 'modules.module_id', '=', 'module_questions.module_id')
              ->join('sessions','modules.session_id','=','sessions.session_id')              
              ->join('trainnings', 'trainnings.trainning_id', '=', 'sessions.trainning_id');              
            $training_datas_two = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')              
              ->groupBy('trainnings.trainning_id');
              if(!empty($merge['trainning_id'])){
                $module_question_deletes = $module_questions->where('module_questions.reaction_id',$reaction->reaction_id)->whereNotIn('trainnings.trainning_id', [$merge['trainning_id']])->get();
                $trainings = $training_datas_two->whereNotIn('module_questions.reaction_id',[''])->where('trainnings.trainning_id', $merge['trainning_id'])->get();
                
                foreach ($module_question_deletes as $key => $value) {                
                  //DB::beginTransaction();
                  $module_question = ModuleQuestion::findOrFail($value->module_question_id);
                  $module_question->delete();  
                  // var_dump($module_question);
                  // exit(); 
                  //DB::commit();                            
                }
                // var_dump($trainings->count());
                // exit();
                foreach ($trainings as $key => $value) {
                  $session = Session::find($value->session_id);
                  $periodeArray = array();
                  if($session->before == "1"){
                    array_push($periodeArray, 0);
                  }elseif ($session->during == "1"){
                    array_push($periodeArray, 1);
                  }elseif($session->after == "1"){
                    array_push($periodeArray, 2);
                  }
                  foreach ($periodeArray as $value_periode) {          
                    switch ($value_periode) {
                      case 0:
                          $module = "before";
                          break;
                      case 1:
                          $module = "during";
                          break;
                      case 2:
                          $module = "after";
                          break;
                      default:
                          $module = "";
                          break;
                    } 
                    
                    $data = [
                      'module_id' => $value->module_id,
                      'reaction_id' => $reaction->reaction_id,
                      'training_detial_periode' => $module
                    ];
                    //DB::beginTransaction();
                    ModuleQuestion::create($data);  
                    //DB::commit();                 
                  }
                }
              }else{
                $trainings = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
                ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
                ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
                ->whereNotIn('module_questions.reaction_id',[''])
                ->groupBy('trainnings.trainning_id')
                ->get();                

                foreach ($trainings as $key => $value) {
                  $session = Session::find($value->session_id);
                  $periodeArray = array();
                  if($session->before == "1"){
                    array_push($periodeArray, 0);
                  }elseif ($session->during == "1"){
                    array_push($periodeArray, 1);
                  }elseif($session->after == "1"){
                    array_push($periodeArray, 2);
                  }
                    foreach ($periodeArray as $value_periode) {          
                      switch ($value_periode) {
                        case 0:
                            $module = "before";
                            break;
                        case 1:
                            $module = "during";
                            break;
                        case 2:
                            $module = "after";
                            break;
                        default:
                            $module = "";
                            break;
                      } 
                      
                      $data = [
                        'module_id' => $value->module_id,
                        'reaction_id' => $reaction->reaction_id,
                        'training_detial_periode' => $module
                      ];              
                      DB::beginTransaction();
                      ModuleQuestion::create($data); 
                      DB::commit();  
                    }
                }                 
              }                                        

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Reaction has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Reaction');
        }

        if(!empty($request->save_and_new)){
          return redirect('reaction/create');
        }else{
          return redirect('reaction');
        }      
    }

    public function delete(Request $request, $id)
    {
        $model  = Reaction::findOrFail($id);

        if ($model) {
            $module_question = ModuleQuestion::where('reaction_id', $model->reaction_id)->delete();
            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Reaction has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Reaction');
        }

        return redirect()->back();
    }
    public function delete_questions(Request $request, $id, $answer){
      $reaction = Reaction::findOrFail($id);
      $answers = explode(",",$reaction->reaction_answer);
      if($reaction){        
        foreach(explode(",",$reaction->reaction_answer) as $key => $value_answer){          

          if ($answer == $value_answer) {
            unset($answers[$key]);      
          }
        }                
        
        $reaction->update(array("reaction_answer" => implode(",",$answers)));
      }
    }

    public function trainning(Request $request, $id)
    {
        $html  = "
            <div class='table-responsive'>
                <table class='table table-striped'>
                    <thead>
                        <th>No</th>
                        <th>Training Title</th>
                    </thead>
                    <tbody>
        ";

        // $model = Trainning::where('reaction_id', $id)
        //             ->join('trainning_details', 'trainnings.trainning_id', '=', 'trainning_details.trainning_id')
        //             ->groupBy('trainnings.trainning_id')
        //             ->get();
        $model = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->where('module_questions.reaction_id', $id)
              ->groupBy('trainnings.trainning_id')
              ->get();

        foreach ($model as $key => $value) {
            $html .= "
                <tr>
                    <td>". ($key+1) ."</td>
                    <td>". $value->trainning_title ."</td>
                </tr>
            ";
        }

        $html .= "
                    </tbody>
                </table>
            </div>
        ";

        return $html;
    }

    public function import(Request $request) 
    {
        try{$import =     Excel::Import(new ReactionImport, $request->file('import'));
      
        
        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Reaction has been imported');

        return redirect('/reaction');
         }catch(\Illuminate\Database\QueryException $e){
        return redirect()->back()->with('error','type atau group keliru');
      }
      
    }
}
