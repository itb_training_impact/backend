<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Behaviour\Story;
use App\Model\Behaviour\StoryDetail;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Insert Story
        $story = Story::create([
            'title' => 'Identifikasi Cerita 3',
            'user_id' => 3,
            'trainning_id' => 15,
            'session_id' => 1,
            'module_id' => 1,
            'submit_periode' => 2
        ]);

        $storyId = $story->id;

        // Isian filename untuk data dari textarea adalah default. Untuk data bukan dari textfield, field story diisi path file
        StoryDetail::create([
            'story_id' => $storyId,
            'filename' => 'default',
            'story' => 'aku pergi ke pasar bersama dirinya',
        ]);

        // File
        // StoryDetail::create([
        //     'story_id' => $storyId,
        //     'filename' => 'story_sample.txt',
        //     'story' => 'imports/story_sample.txt',
        // ]);


        // $storyId = 20;

        dispatch(new \App\Jobs\ProcessStory($storyId));
    }
}
