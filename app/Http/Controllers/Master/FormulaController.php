<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\Formula;

class FormulaController extends Controller
{
    public function __construct()
    {
        $this->limit = 25;
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formulas = Formula::all();
       
        return view('formula.index', compact('formulas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('formula.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules());

        $model = Formula::create($request->except('_token'));
        
        if ($model) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Formula has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add formula');
        }

        return redirect('/formula');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    public function edit($id)
    {
    	$formulas = Formula::find($id);

        return view('/formula/edit', compact('formulas'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules());
        
    	$model = Formula::find($id);

        if ($model) {

            $model->update($request->except('_token'));

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Formula has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit formula');
        }

    	return redirect('/formula');
    }

    public function delete(Request $request, $id)
    {
        $model	= Formula::findOrFail($id);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Formula has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete formula');
        }

    	return redirect('/formula');
    }

    private function rules()
    {
        return [
            "formula" => ['required'],
            "formula_description" => ['required']
        ];
    }
    public function show($id)
    {
        $formulas = Formula::find($id);

        return view('/formula/detail', compact('formulas'));
    }


   
}

