<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

use App\Model\Master\WordCategory;
use App\Model\Master\WordCategoryDetail;
use App\Model\Master\Sentence;
use App\Model\Master\Dictionary;

use App\Imports\WordCategoryImport;
use Maatwebsite\Excel\Facades\Excel;

use DB;
use Validator;

class WordCategoryController extends Controller
{    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->limit = 25;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $wordCategories = WordCategory::orderBy('id', 'desc');

        $wordCategories->where('code', '!=', 'C002');
        $wordCategories->where('code', '!=', 'SC01');

        if($request->has('name') && !empty($request->name))
            $wordCategories   = $wordCategories->where('name', 'like', '%' . $request->name . '%');

        if($request->has('dictionary') && !empty($request->dictionary)) {
            $value = $request->dictionary;
            $wordCategories->whereHas('detail', function ($query) use ($value) {
                        $query->where('dict_id', $value);
                    });
        }

        $total = $wordCategories->count();

        $wordCategories = $wordCategories->paginate($this->limit);
        $wordCategories->appends($request->all());


        $sentence = WordCategory::where('code', 'SC01')->first();

        $dictionaries = Dictionary::orderBy('name', 'asc')->get();
       
        return view('word_category.index', compact('wordCategories', 'total', 'sentence', 'dictionaries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $dictionaries = Dictionary::orderBy('name', 'asc')->get();
        return view('word_category.create', compact('dictionaries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules());

        DB::beginTransaction();

            $dataWordCategory = $request->only(['code', 'name', 'description']);
            
            $wordCategory = WordCategory::create($dataWordCategory);

            $wordCategoryId = $wordCategory->id;

            foreach ($request->dictionaries as $dictionary) {
                WordCategoryDetail::create([
                    'cat_id' => $wordCategoryId,
                    'dict_id' => $dictionary
                ]);
            }

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Word Category has been added');

        return redirect('/word_category');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function edit($id)
    {
    	$wordCategory = WordCategory::find($id);
        $dictionaries = Dictionary::orderBy('name', 'asc')->get();

        return view('/word_category/edit', compact('wordCategory', 'dictionaries'));
    }

    /**
     * Show the form for editing sentence.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function edit_sentence($id)
    {
        if ($id == 'SC01') {
            $sentence = WordCategory::where('code', $id)->first();
            $wordCategories = WordCategory::where('code', '!=', 'C002')->where('code', '!=', 'SC01')->orderBy('id', 'desc')->get();

            return view('/word_category/edit_sentence', compact('sentence', 'wordCategories'));
        } else
            return redirect('/word_category');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$model = WordCategory::find($id);

        $this->validate($request, $this->rules(true, $model->code));        

        if ($model) {
            if ($model->code == 'SC01' || $model->code == 'C002') {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Failed to edit word category');
                return redirect('/word_category');
            }

            DB::beginTransaction();

                $dataWordCategory = $request->only(['code', 'name', 'description']);
                
                $wordCategory = $model->update($dataWordCategory);

                WordCategoryDetail::where('cat_id', $id)->delete();
                foreach ($request->dictionaries as $dictionary) {
                    WordCategoryDetail::create([
                        'cat_id' => $model->id,
                        'dict_id' => $dictionary
                    ]);
                }

            DB::commit();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Word Category has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit word category');
        }

    	return redirect('/word_category');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_sentence(Request $request, $id)
    {
        $model = WordCategory::where('code', $id)->first();

        $this->validate($request, $this->sentence_rules());

        if ($model) {
            DB::beginTransaction();
                Sentence::where('cat_id', $id)->delete();
                foreach ($request->word_categories as $word_category) {
                    Sentence::create([
                        'cat_id' => $id,
                        'cat_child_id' => $word_category
                    ]);
                }

            DB::commit();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Word Category has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit word category');
        }

        return redirect('/word_category');      
    }    

    public function delete(Request $request, $id)
    {
        $model	= wordCategory::findOrFail($id);

        if ($model) {
            if ($model->code == 'SC01' || $model->code == 'C002') {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Failed to delete word category');
                return redirect('/word_category');
            }

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Word Category has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete word category');
        }

    	return redirect('/word_category');
    }

    public function multiple_delete(Request $request)
    {
        if (!empty($request->check)) {
            $wordCategories = [];
            foreach ($request->check as $key => $value)
                if ($value == 'on' && $key != 'C002' && $key != 'SC01')
                    $wordCategories[] = $key;
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete word_category');
            return redirect('/word_category');
        }

        $model  = WordCategory::whereIn('id', $wordCategories);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Word Category has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Word Category');
        }

        return redirect('/word_category');
    }

    private function rules($update = false, $id = null)
    {
        $code = $update ? ['required', Rule::unique('word_category')->ignore($id, 'code')] : ['required|unique:word_category,code'];
        return [
            "code" => $code,
            "name" => ['required'],
            "dictionaries" => ['required'],
            "description" => ['required'],
        ];
    }

    private function sentence_rules()
    {
        return [
            "word_categories" => ['required'],
        ];
    }

    public function show($id)
    {
        $wordCategory = WordCategory::find($id);

        return view('/word_category/detail', compact('wordCategory'));
    }

    public function show_sentence($id)
    {
        if ($id != 'SC01')
            return redirect('/word_category');

        $wordCategory = WordCategory::where('code', $id)->first();

        return view('/word_category/detail_sentence', compact('wordCategory'));
    }

    public function import(Request $request) 
    {
        Excel::import(new WordCategoryImport, $request->file('import'));
        
        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Word Category has been imported');

        return redirect('/word_category');
    }
}