<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\Word;
use App\Model\Master\WordDetail;
use App\Model\Master\WordCategory;

use Auth;
use DB;

class UnidentifiedWordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->limit = 25;
    }

    /**
     * Show the application reaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $words = Word::orderBy('count', 'desc');
        $words->whereHas('detail', function ($query) {
            $query->where('cat_id', '=', 1);
            $query->whereOr('cat_id', '=', NULL);
        });

        $total = $words->count();

        $words = $words->paginate($this->limit);
        $words->appends($request->all());

        $wordCategories = WordCategory::orderBy('id', 'desc')->get();
       
        return view('unidentified_word.index', compact('words', 'total', 'wordCategories'));
    }

    public function update(Request $request, $id)
    {
    	$model = Word::find($id);

        $this->validate($request, $this->rules());

        if ($model) {

            DB::beginTransaction();
                WordDetail::where('word_id', $id)->delete();
                foreach ($request->wordCategories as $wordCategory) {
                    WordDetail::create([
                        'word_id' => $id,
                        'cat_id' => $wordCategory
                    ]);
                }

            DB::commit();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Unidentified Word has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit unidentified word');
        }

    	return redirect('/unidentified_word');
    }

    public function multiple_update(Request $request)
    {
    	if (!empty($request->check)) {
            $words = [];
            foreach ($request->check as $key => $value)
                $words[] = $key;
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to save unidentified word');
            return redirect('/unidentified_word');
        }

        $model  = Word::whereIn('id', $words)->get();

        if ($model) {
        	foreach ($model as $data) {
        		$cats = explode(',', $request->select[$data->id]);
        		if (!$request->select[$data->id])
        			continue;

        		DB::beginTransaction();
	                WordDetail::where('word_id', $data->id)->delete();
	                foreach ($cats as $wordCategory) {
	                    WordDetail::create([
	                        'word_id' => $data->id,
	                        'cat_id' => $wordCategory
	                    ]);
	                }

	            DB::commit();
        	}

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Unidentified Word has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete unidentified word');
        }

        return redirect('/unidentified_word');
    }

    public function delete(Request $request, $id)
    {
        $model  = Word::findOrFail($id);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Unidentified Word has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete unidentified word');
        }

        return redirect('/unidentified_word');
    }

    public function multiple_delete(Request $request)
    {
        if (!empty($request->check)) {
            $words = [];
            foreach ($request->check as $key => $value)
                $words[] = $key;
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete unidentified word');
            return redirect('/word');
        }

        $model  = Word::whereIn('id', $words);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Unidentified Word has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete unidentified word');
        }

        return redirect('/unidentified_word');
    }

    private function rules()
    {
        return [
            "wordCategories" => ['required'],
        ];
    }
}
