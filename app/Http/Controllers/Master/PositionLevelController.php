<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

use App\Model\Master\PositionLevel;

class PositionLevelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->limit = 25;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $positionLevels = PositionLevel::orderBy('updated_at', 'desc');

        if($request->has('name') && !empty($request->name))
            $positionLevels   = $positionLevels->where('name', 'like', '%' . $request->name . '%');

        $total = $positionLevels->count();

        $positionLevels = $positionLevels->paginate($this->limit);
        $positionLevels->appends($request->all());
       
        return view('position_level.index', compact('positionLevels', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $positionLevels = PositionLevel::all();
        return view('position_level.create', compact('positionLevels')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // $this->validate($request, $this->rules());

        $model = PositionLevel::create($request->except('_token'));
        
        if ($model) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Speaker has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Speaker');
        }

        return redirect('/position_level');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    public function edit($id)
    {
    	$positionLevels = PositionLevel::find($id);

        return view('/position_level/edit', compact('positionLevels'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$this->validate($request, $this->rules());
        
    	$model = PositionLevel::find($id);

        if ($model) {

            $model->update($request->except('_token'));

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Speaker has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Speaker');
        }

    	return redirect('/position_level');
    }

    public function delete(Request $request, $id)
    {
        $model	= PositionLevel::findOrFail($id);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Speaker has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Speaker');
        }

    	return redirect('/position_level');
    }

    

    private function rules()
    {
        return [
            "name" => ['required'],
            "description" => ['required'],
        ];
    }

    public function show($id)
    {
        $positionLevels = PositionLevel::find($id);

        return view('/position_level/detail', compact('positionLevels'));
    }  

   
}