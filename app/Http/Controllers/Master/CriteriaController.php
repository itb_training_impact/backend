<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

use App\Model\Master\Criteria;

class CriteriaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->limit = 25;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $criterias = Criteria::orderBy('updated_at', 'desc');

        if($request->has('name') && !empty($request->name))
            $criterias   = $criterias->where('name', 'like', '%' . $request->name . '%');

        $total = $criterias->count();

        $criterias = $criterias->paginate($this->limit);
        $criterias->appends($request->all());
       
        return view('criteria.index', compact('criterias', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $criterias = Criteria::all();
        return view('criteria.create', compact('criterias')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // $this->validate($request, $this->rules());

        $model = Criteria::create($request->except('_token'));
        
        if ($model) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Speaker has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Speaker');
        }

        return redirect('/criteria');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    public function edit($id)
    {
    	$criterias = Criteria::find($id);

        return view('/criteria/edit', compact('criterias'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$this->validate($request, $this->rules());
        
    	$model = Criteria::find($id);

        if ($model) {

            $model->update($request->except('_token'));

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Speaker has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Speaker');
        }

    	return redirect('/criteria');
    }

    public function delete(Request $request, $id)
    {
        $model	= Criteria::findOrFail($id);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Speaker has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Speaker');
        }

    	return redirect('/criteria');
    }

    

    private function rules()
    {
        return [
            "name" => ['required'],
            "description" => ['required'],
        ];
    }

    public function show($id)
    {
        $criterias = Criteria::find($id);

        return view('/criteria/detail', compact('criterias'));
    }  

   
}