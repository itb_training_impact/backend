<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\FormulationGroup;

class FormulationGroupController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->limit = 25;
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$groups = FormulationGroup::orderBy('updated_at', 'desc');

		if($request->has('name') && !empty($request->name))
			$groups->where('name', 'like', '%' . $request->name . '%');

		$total = $groups->count();

		$groups = $groups->paginate($this->limit);
		$groups->appends($request->all());

		return view('formulation_group.index', compact('groups', 'total'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Request $request)
	{
		return view('formulation_group.create'); 
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, $this->rules());

		$model = FormulationGroup::create($request->except('_token'));

		if ($model) {
			$request->session()->flash('status', '200');
			$request->session()->flash('msg', 'Formulation Group has been added');
		} else {
			$request->session()->flash('status', 'err');
			$request->session()->flash('msg', 'Failed to add formulation group');
		}

		return redirect('/formulation-group');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */

	public function edit($id)
	{
		$group     = FormulationGroup::find($id);
		return view('formulation_group.edit', compact('group'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, $this->rules());

		$model = FormulationGroup::find($id);

		if ($model) {

			$model->update($request->except('_token'));

			$request->session()->flash('status', '200');
			$request->session()->flash('msg', 'Formulation Group has been edited');
		} else {
			$request->session()->flash('status', 'err');
			$request->session()->flash('msg', 'Failed to edit formulation group');
		}

		return redirect('/formulation-group');
	}

	public function delete(Request $request, $id)
	{
		$model	= FormulationGroup::findOrFail($id);

		if ($model) {
			$model->delete();

			$request->session()->flash('status', '200');
			$request->session()->flash('msg', 'Formulation Group has been deleted');
		} else {
			$request->session()->flash('status', 'err');
			$request->session()->flash('msg', 'Failed to delete formulation group');
		}

		return redirect('/formulation-group');
	}

	public function show($id)
	{
		$group = FormulationGroup::find($id);
		return view('/formulation_group/detail', compact('group'));
	}

	private function rules()
	{
		return [
			"code" => ['required'],
			"name" => ['required'],
			"description" => ['required']
		];
	}
}

