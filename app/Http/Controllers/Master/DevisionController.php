<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

use App\Model\Master\Devision;

class DevisionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->limit = 25;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $devisions = Devision::orderBy('updated_at', 'desc');

        if($request->has('name') && !empty($request->name))
            $devisions   = $devisions->where('name', 'like', '%' . $request->name . '%');

        $total = $devisions->count();

        $devisions = $devisions->paginate($this->limit);
        $devisions->appends($request->all());
       
        return view('devision.index', compact('devisions', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $devisions = Devision::all();
        return view('devision.create', compact('devisions')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // $this->validate($request, $this->rules());

        $model = Devision::create($request->except('_token'));
        
        if ($model) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Speaker has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Speaker');
        }

        return redirect('/devision');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    public function edit($id)
    {
    	$devisions = Devision::find($id);

        return view('/devision/edit', compact('devisions'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$this->validate($request, $this->rules());
        
    	$model = Devision::find($id);

        if ($model) {

            $model->update($request->except('_token'));

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Speaker has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Speaker');
        }

    	return redirect('/devision');
    }

    public function delete(Request $request, $id)
    {
        $model	= Devision::findOrFail($id);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Speaker has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Speaker');
        }

    	return redirect('/devision');
    }

    

    private function rules()
    {
        return [
            "name" => ['required'],
            "description" => ['required'],
        ];
    }

    public function show($id)
    {
        $devisions = Devision::find($id);

        return view('/devision/detail', compact('devisions'));
    }  

   
}