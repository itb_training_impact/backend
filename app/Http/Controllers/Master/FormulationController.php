<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

use App\Model\Master\Formulation;
use App\Model\Master\FormulationGroup;
use App\Model\Master\WordCategory;
use App\Model\Master\FormulationDetail;

use App\Imports\FormulationImport;
use Maatwebsite\Excel\Facades\Excel;

use DB;
use Validator;

class FormulationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->limit = 25;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formulations = Formulation::orderBy('id', 'desc');

        if($request->has('name') && !empty($request->name)) {
            $formulations   = $formulations->where('code', 'like', '%' . $request->name . '%');
            $formulations   = $formulations->orWhere('name', 'like', '%' . $request->name . '%');
        }

        $total = $formulations->count();

        $formulations = $formulations->paginate($this->limit);
        $formulations->appends($request->all());
       
        return view('formulation.index', compact('formulations', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $groups = FormulationGroup::orderBy('name', 'asc')->get();
        $wordCategories = WordCategory::orderBy('name', 'asc')->get();
        return view('formulation.create', compact('groups', 'wordCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules());

        DB::beginTransaction();
            $formulation = Formulation::create([
                'formulation_group_id' => $request->group,
                'code' => $request->code,
                'name' => $request->name,
                'status' => $request->status ? 1 : 0,
                'description' => $request->description,
                'aspek_tertinggi' => $request->aspek_tertinggi,
                'aspek_terendah' => $request->aspek_terendah,
            ]);

            $formulationId = $formulation->id;

            foreach ($request->cat as $key => $value) {
                FormulationDetail::create([
                    'formulation_id' => $formulationId,
                    'cat_id' => $value,
                    'konstanta' => $request->konstanta[$key],
                    'bobot' => $request->bobot[$key]
                ]);
            }

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Formulation has been added');

        return redirect('/formulation');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function edit($id)
    {
        $formulation = Formulation::find($id);
    	$groups = FormulationGroup::orderBy('name', 'asc')->get();
        $wordCategories = WordCategory::orderBy('name', 'asc')->get();
        return view('/formulation/edit', compact('formulation', 'groups', 'wordCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$model = Formulation::find($id);

        $this->validate($request, $this->rules());

        if ($model) {

            DB::beginTransaction();
                $formulation = $model->update([
                    'formulation_group_id' => $request->group,
                    'code' => $request->code,
                    'name' => $request->name,
                    'status' => $request->status ? 1 : 0,
                    'description' => $request->description,
                    'aspek_tertinggi' => $request->aspek_tertinggi,
                    'aspek_terendah' => $request->aspek_terendah,
                ]);
                FormulationDetail::where('formulation_id', $id)->delete();
                foreach ($request->cat as $key => $value) {
                    FormulationDetail::create([
                        'formulation_id' => $id,
                        'cat_id' => $value,
                        'konstanta' => $request->konstanta[$key],
                        'bobot' => $request->bobot[$key]
                    ]);
                }

            DB::commit();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Formulation has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit formulation');
        }

    	return redirect('/formulation');
    }  

    public function delete(Request $request, $id)
    {
        $model	= Formulation::findOrFail($id);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Formulation has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete formulation');
        }

    	return redirect('/formulation');
    }

    public function multiple_delete(Request $request)
    {
        if (!empty($request->check)) {
            $formulations = [];
            foreach ($request->check as $key => $value)
                $formulations[] = $key;
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete formulation');
            return redirect('/formulation');
        }

        $model  = Formulation::whereIn('id', $formulations);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Formulation has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete formulation');
        }

        return redirect('/formulation');
    }

    private function rules()
    {
        return [
            "group" => ['required'],
            "code" => ['required'],
            "name" => ['required'],
            "status" => ['required'],
            "cat" => ['required|array'],
            "aspek_tertinggi" => ['required'],
            "aspek_terendah" => ['required'],
            "cat" => ['required', 'array'],
            "cat.*" => ['required', 'numeric'],
            "konstanta" => ['required', 'array'],
            "konstanta.*" => ['required', 'numeric'],
            "bobot" => ['required', 'array'],
            "bobot.*" => ['required', 'numeric'],
        ];
    }

    public function show($id)
    {
        $formulation = Formulation::find($id);

        return view('/formulation/detail', compact('formulation'));
    }

    public function switch_formulation(Request $request)
    {
        $id = $request->id;
        
        $model = Formulation::find($id);

        if ($model) {

            $model->update([
                'status' => $request->status == 'on' ? 1 : 0
            ]);

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Formulation has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit formulation');
        }

        return redirect('/formulation');
    }

    public function import(Request $request) 
    {
        Excel::import(new FormulationImport, $request->file('import'));
        
        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Formulation has been imported');

        return redirect('/formulation');
    }
}