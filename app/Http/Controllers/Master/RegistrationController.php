<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\Registration;

class RegistrationController extends Controller
{
    public function __construct()
    {
        $this->limit = 25;
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $registrationForms = Registration::orderBy('created_at', 'asc');

        if($request->has('name') && !empty($request->name))
            $registrationForms   = $registrationForms->where('name', 'like', '%' . $request->name . '%');

        if($request->has('label') && !empty($request->label))
            $registrationForms   = $registrationForms->where('label', 'like', '%' . $request->label . '%');

        $registrationForms = $registrationForms->get();

        return view('registration.index', compact('registrationForms'));
    }

    public function switch_status(Request $request)
    {
        $id = $request->id;
        
        $model = Registration::find($id);

        if ($model) {

            $model->update([
                'status' => $request->status == 'on' ? 1 : 0
            ]);

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Form has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit form');
        }

        return redirect('/registration');
    }
}