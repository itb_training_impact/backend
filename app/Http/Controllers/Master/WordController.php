<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

use App\Model\Master\Word;
use App\Model\Master\WordCategory;
use App\Model\Master\WordDetail;

use App\Imports\WordImport;
use Maatwebsite\Excel\Facades\Excel;

use DB;
use Validator;

class WordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->limit = 25;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $words = Word::orderBy('id', 'desc');
        $words->whereHas('detail', function ($query) {
            $query->where('cat_id', '!=', 1);
            $query->whereOr('cat_id', '=', NULL);
        });

        if($request->has('name') && !empty($request->name))
            $words   = $words->where('name', 'like', '%' . $request->name . '%');

        if($request->has('wordCategory') && !empty($request->wordCategory)) {
            $value = $request->wordCategory;
            $words->whereHas('detail', function ($query) use ($value) {
                        $query->where('cat_id', $value);
                    });
        }

        $total = $words->count();

        $words = $words->paginate($this->limit);
        $words->appends($request->all());

        $wordCategories = WordCategory::orderBy('id', 'desc')->get();
       
        return view('word.index', compact('words', 'total', 'wordCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $wordCategories = WordCategory::orderBy('id', 'desc')->get();
        return view('word.create', compact('wordCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules());

        DB::beginTransaction();
            $word = Word::create([
                'name' => $request->name,
                'is_combined_words' => $request->is_combined_words ? 1 : 0
            ]);

            $wordId = $word->id;

            foreach ($request->wordCategories as $wordCategory) {
                WordDetail::create([
                    'word_id' => $wordId,
                    'cat_id' => $wordCategory
                ]);
            }

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Word has been added');

        return redirect('/word');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function edit($id)
    {
    	$word = Word::find($id);
        $wordCategories = WordCategory::orderBy('id', 'desc')->get();

        return view('/word/edit', compact('word', 'wordCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$model = Word::find($id);

        $this->validate($request, $this->rules());

        if ($model) {

            DB::beginTransaction();
                $word = $model->update([
                    'name' => $request->name,
                    'is_combined_words' => $request->is_combined_words ? 1 : 0
                ]);

                WordDetail::where('word_id', $id)->delete();
                foreach ($request->wordCategories as $wordCategory) {
                    WordDetail::create([
                        'word_id' => $id,
                        'cat_id' => $wordCategory
                    ]);
                }

            DB::commit();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Word has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit word');
        }

    	return redirect('/word');
    }  

    public function delete(Request $request, $id)
    {
        $model	= Word::findOrFail($id);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Word has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete word');
        }

    	return redirect('/word');
    }

    public function multiple_delete(Request $request)
    {
        if (!empty($request->check)) {
            $words = [];
            foreach ($request->check as $key => $value)
                $words[] = $key;
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete word');
            return redirect('/word');
        }

        $model  = Word::whereIn('id', $words);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Word has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Word');
        }

        return redirect('/word');
    }

    private function rules()
    {
        return [
            "name" => ['required'],
            "wordCategories" => ['required'],
        ];
    }

    public function show($id)
    {
        $word = Word::find($id);

        return view('/word/detail', compact('word'));
    }

    public function import(Request $request) 
    {
        Excel::import(new WordImport, $request->file('import'));
        
        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Word has been imported');

        return redirect('/word');
    }
}