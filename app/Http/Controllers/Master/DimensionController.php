<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\Formula;
use App\Model\Master\Dimension;

class DimensionController extends Controller
{
    public function __construct()
    {
        $this->limit = 25;
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $groups = Dimension::all();
       
        return view('dimension.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $formulas = Formula::all();
        return view('dimension.create', compact('formulas')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules());
        
        $model = Dimension::create($request->except('_token'));

        if ($model) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Dimension has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Dimension');
        }

        return redirect('/dimension');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    public function edit($id)
    {
        $groups     = Dimension::find($id);
    	$formulas   = Formula::all();

        return view('dimension.edit', compact('groups', 'formulas'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules());

    	$model = Dimension::find($id);

        if ($model) {

            $model->update($request->except('_token'));

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Dimension has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Dimension');
        }

    	return redirect('/dimension');
    }
    public function delete(Request $request, $id)
    {
        $model	= Dimension::findOrFail($id);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Dimension has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Dimension');
        }

    	return redirect('/dimension');
    }

    private function rules()
    {
        return [
            "group" => ['required'],
            "formula_id" => ['required'],
            "is_reaction" => ['required']
        ];
    }
   
}

