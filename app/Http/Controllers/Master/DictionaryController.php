<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

use App\Model\Master\Dictionary;

use App\Imports\DictionaryImport;
use Maatwebsite\Excel\Facades\Excel;

class DictionaryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->limit = 25;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dictionaries = Dictionary::orderBy('updated_at', 'desc');

        if($request->has('name') && !empty($request->name))
            $dictionaries   = $dictionaries->where('name', 'like', '%' . $request->name . '%');

        $total = $dictionaries->count();

        $dictionaries = $dictionaries->paginate($this->limit);
        $dictionaries->appends($request->all());
       
        return view('dictionary.index', compact('dictionaries', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('dictionary.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules());

        $model = Dictionary::create($request->except('_token'));
        
        if ($model) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Dictionary has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add dictionary');
        }

        return redirect('/dictionary');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    public function edit($id)
    {
    	$dictionary = Dictionary::find($id);

        return view('/dictionary/edit', compact('dictionary'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules());
        
    	$model = Dictionary::find($id);

        if ($model) {

            $model->update($request->except('_token'));

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Dictionary has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit dictionary');
        }

    	return redirect('/dictionary');
    }

    public function delete(Request $request, $id)
    {
        $model	= Dictionary::findOrFail($id);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Dictionary has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete dictionary');
        }

    	return redirect('/dictionary');
    }

    public function multiple_delete(Request $request)
    {
        if (!empty($request->check)) {
            $dictionaries = [];
            foreach ($request->check as $key => $value)
                if ($value == 'on')
                    $dictionaries[] = $key;
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete dictionary');
            return redirect('/dictionary');
        }

        $model  = Dictionary::whereIn('id', $dictionaries);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Dictionary has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete dictionary');
        }

        return redirect('/dictionary');
    }

    private function rules()
    {
        return [
            "code" => ['required'],
            "name" => ['required'],
            "status" => ['required', Rule::in([0, 1])],
            "description" => ['required'],
        ];
    }

    public function show($id)
    {
        $dictionary = Dictionary::find($id);

        return view('/dictionary/detail', compact('dictionary'));
    }  

    public function switch_dict(Request $request)
    {
        $id = $request->id;
        
        $model = Dictionary::find($id);

        if ($model) {

            $model->update([
                'status' => $request->status == 'on' ? 1 : 0
            ]);

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Dictionary has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit dictionary');
        }

        return redirect('/dictionary');
    }

    public function import(Request $request) 
    {
        Excel::import(new DictionaryImport, $request->file('import'));
        
        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Dictionary has been imported');

        return redirect('/dictionary');
    }
}