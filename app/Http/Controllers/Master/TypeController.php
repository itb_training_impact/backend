<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\Type;

use Auth;

class TypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application Type.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = Type::all();

        return view('master.type.index', compact('type'));
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->rules());

        $merge = [
            'type_label' => str_replace(" ", "_", strtolower($request->type))
        ];

        $model = Type::create(
        	array_merge(
                $request->except('_token'),
                $merge
            )
        );

        if ($model) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Type question has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Type question failed to added');
        }

    	return redirect('type');
    }

    public function create()
    {
        return view('master.type.create');
    }

    public function edit($id)
    {
    	$type = Type::find($id);

        return view('master.type.edit', compact('type'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules());

    	$model = Type::find($id);

        if ($model) {

            $merge = [
                'type_label' => str_replace(" ", "_", strtolower($request->type))
            ];

            $model->update(
                array_merge(
                    $request->except('_token'),
                    $merge
                )
            );

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Type question has been updated');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Type question failed to update');
        }

    	return redirect('type');
    }

    public function delete(Request $request, $id)
    {
    	$model	= Type::findOrFail($id);

        if ($model) {

        	$model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Type question has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Type question failed to delete');
        }

    	return redirect('type');
    }

    private function rules()
    {
        return [
            'type' => ['required']
        ];
    }
}
