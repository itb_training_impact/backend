<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\System\StoreRole;

use App\Model\System\Module;
use App\Model\System\Role;
use App\Model\Level;
use DB;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->modelRoles   = New Role;
        $this->module       = "Role";
        $this->limit        = 50;
    }

    /**
     * Show the application task.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $this->authorize('index', [ \App\User::class, $this->module ]);

        $role   	= $this->modelRoles->getDataLevel($request, $this->limit);
    
    	return view('system.role.index', compact('role'));
    }

    public function edit($id)
    {
        // $this->authorize('edit', [ \App\User::class, $this->module ]);

    	$level  = Level::where('level_id',$id)->first();
        $module = Module::orderBy('modules_id', 'ASC')->get();

        return view('system.role.create', compact('module', 'level'));
    }

    public function update(Request $request)
    {
        // $this->authorize('edit', [ \App\User::class, $this->module ]);

        DB::beginTransaction();

            $task   = $request->task_id;
            $level  = $request->level_id;

            //add new role
            foreach($task as $key) {
                $checkRole = $this->modelRoles->checkRole($level, $key);

                if (!$checkRole) {                
                    $role           = new Role;
                    $role->level_id = $request->level_id;
                    $role->task_id = $key;
                    $role->save();
                }
            }	

            //delete role with where not in task
            Role::where('level_id', $level)->whereNotIn('task_id', $task)->delete();

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Role berhasil diubah');

    	return redirect('system/role');
    }
}
