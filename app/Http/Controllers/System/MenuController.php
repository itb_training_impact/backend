<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\System\Menu;
use App\Model\System\Module;
use DB;

class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->modelMenus = new Menu;
        $this->limit = 25;
    }

    public function index(Request $request)
    {
        $menu = $this->modelMenus->getDataWithPaginate($request, $this->limit);

        return view('system.menu.index', compact('menu'));
    }
    
    public function create()
    {
        try {
            $data['module'] = Module::orderBy('module','ASC')->get();
            $data['menu']   = Menu::orderBy('menu','ASC')->where('menu_is_sub',0)->get();

            return view('system.menu.create', $data);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function store(Request $request)
    {
        $menu = new Menu;

        foreach($request->except('_token') as $key => $value)
        {
            if($value != "") {
                $menu->{$key} = $value;
            }
        }

        if($menu->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Menu berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Menu gagal ditambahkan');
        }

        return redirect('system/menu');
    }

    public function edit($id)
    {
        $menu     = Menu::find($id);
        $module   = Module::orderBy('module','ASC')->get();
        $parent   = Menu::orderBy('menu','ASC')->where('menu_is_sub',0)->get();

        return view('system.menu.edit', compact('module','menu','parent'));
    }

    public function update(Request $request, $id)
    {
        $menu = Menu::find($id);

        foreach($request->except('_token','_method') as $key => $value)
        {
            if($value != "") {
                $menu->{$key} = $value;
            }
        }

        if($menu->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Menu berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Menu gagal diubah');
        }

        return redirect('system/menu');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Menu::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Menu berhasil di hapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Menu gagal di hapus');
        }

    	return redirect()->back();
    }
}
