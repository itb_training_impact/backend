<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\System\StoreModule;

use App\Model\System\Module;

class ModuleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no = 1;
        $module = Module::all();
        return view('system.module.index', compact('no','module'));
    }

    public function store(StoreModule $request)
    {
    	$module 			= new Module;
    	$module->module 	= $request->module;

        $insert = $module->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Modul berhasil di tambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Modul gagal di tambahkan');
        }

    	return redirect('system/module');
    }

    public function create()
    {
        return view('system.module.create');
    }

    public function edit($id)
    {
    	$module = Module::where('modules_id',$id)->first();
        return view('system.module.edit', compact('module'));
    }

    public function update(Request $request, $id)
    {
    	$update = Module::where('modules_id',$id)->update([
                'module' => $request->module
            ]);

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Modul berhasil di ubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Modul gagal di ubah');
        }

    	return redirect('system/module');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Module::where('modules_id',$id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Modul berhasil di hapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Modul gagal di hapus');
        }

    	return redirect('system/module');
    }
}
