<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\System\StoreTask;

use App\Model\System\Task;
use App\Model\System\Module;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no       = 1;
        $task     = Task::all();
        $module   = Module::all();

    	return view('system.task.index', compact('no', 'task', 'module'));
    }

    public function create()
    {
       $modules    = Module::all();

       return view('system.task.create', compact('modules'));
    }

    public function store(StoreTask $request)
    {
    	$nTask 				= $request->tasks;

    	for ($i=0; $i < sizeof($nTask); $i++) {
    		$task 				= new Task;
	    	$task->module_id 	= $request->modules_id;
	    	$task->task 	    = $nTask[$i];

	        $insert = $task->save();
    	}

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Task baru berhasil di tambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Task baru gagal di tambahkan');
        }

    	return redirect('system/task');
    }

    public function edit($id)
    {
    	$data['task'] = Task::where('tasks_id',$id)->first();
    	return $data;
    }

    public function update(StoreTask $request, $id)
    {
        $nTask              = $request->tasks;

        for ($i=0; $i < sizeof($nTask); $i++) {
        	$update = Task::where('tasks_id',$id)->update([
                    'task' => $nTask[$i],
                    'module_id' => $request->modules_id
                ]);
        }

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Task berhasil di ubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Task gagal di ubah');
        }

    	return redirect('system/task');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Task::where('task_id',$id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Task berhasil di hapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Task gagal di hapus');
        }

    	return redirect('system/task');
    }
}
