<?php

namespace App\Http\Controllers\Performance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\Type;
use App\Model\Master\Dimension as Group;
use App\Model\Master\Criteria;
use App\Imports\PerformanceImport;
use App\Model\Performance\Performance;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\TrainningDetail;
use App\Model\Trainning\ModuleQuestion;
use App\Model\Trainning\Session;
use App\Model\Trainning\Module;
use Maatwebsite\Excel\Facades\Excel;

use Auth;
use DB;

class PerformanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application reaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $performance   = Performance::orderBy('created_at', 'DESC');

        if($request->has('criteria_id') && !empty($request->criteria_id))
            $performance   = $performance->where('criteria_id', $request->criteria_id);

        if($request->has('type_id') && !empty($request->type_id))
            $performance   = $performance->where('type_id', $request->type_id);

        if($request->has('performance_code') && !empty($request->performance_code))
            $performance   = $performance->where('performance_code', $request->performance_code);

        if($request->has('performance_question') && !empty($request->performance_question))
            $performance   = $performance->where('performance_question', $request->performance_question);

        $performance   = $performance->paginate(20);
      	$type 		     = Type::all();
        $criteria      = Criteria::where('level', 0)->get();

        return view('performance.index', compact('performance','type', 'criteria'));
    }

    public function create()
    {
        $type           = Type::all();
        $group          = Group::where('is_reaction', 2)->where("group", "Boss Position")->get();
        $criteria       = Criteria::where('level', 0)->get();
        $trainningID = null;
        $trainning      = Trainning::where('user_id', Auth::User()->id)->get();

        $optGroup       = "";
        $optCriteria    = "";
        $optType        = "";
        $optTrainning   = "";
        
        foreach ($type as $key => $value) {
            $selected = $value->type_label == "rating" ? "selected" : "";
            $optType .= "<option value='". $value->type_label ."' ". $selected .">". $value->type ."</option>";
        }

        foreach ($group as $key => $value) {
            $optGroup .= "<option value='". $value->group_id ."'>". $value->group ."</option>";
        }

        foreach ($criteria as $key => $value) {
          $optCriteria .= "<option value='". $value->criteria_id ."'>". $value->criteria ."</option>";
        }

        foreach ($trainning as $key => $value) {
            $optTrainning .= "<option value='". $value->trainning_id ."'>". $value->trainning_title ."</option>";
        }
        
    	  return view('performance.create', compact('type', 'group', 'criteria', 'trainning', 'optGroup', 'optType', 'optTrainning', 'optCriteria','trainningID'));
    }

     public function createCustom($trainningID,$sessionID,$moduleID)
    {
        $type           = Type::all();
        $group          = Group::where('is_reaction', 2)->get();
        $criteria       = Criteria::where('level', 0)->get();

        $trainningID      = Trainning::where('trainning_id',$trainningID)->get();


        $optGroup       = "";
        $optCriteria    = "";
        $optType        = "";
        $optTrainning   = "";
        
        foreach ($type as $key => $value) {
            $selected = $value->type_label == "rating" ? "selected" : "";
            $optType .= "<option value='". $value->type_label ."' ". $selected .">". $value->type ."</option>";
        }

        foreach ($group as $key => $value) {
            $optGroup .= "<option value='". $value->group_id ."'>". $value->group ."</option>";
        }

        foreach ($criteria as $key => $value) {
          $optCriteria .= "<option value='". $value->criteria_id ."'>". $value->criteria ."</option>";
        }

        $sessions = Session::where('session_id',$sessionID)->get();
        $modules = Module::where('module_id',$moduleID)->get();
        
        return view('performance.create', compact('type', 'group', 'criteria', 'trainningID', 'optGroup', 'optType', 'optTrainning', 'optCriteria','sessions','modules'));
    }
    public function get_session($id)
    {
        $sessions = Session::where('trainning_id', $id)->pluck('name','session_id');
        return response()->json($sessions);
    }

    public function get_module($id)
    {
        $modules = Module::where('session_id', $id)->pluck('name','module_id');
        return response()->json($modules);
    }

    public function store(Request $request)
    {
    	$params = $request->except('_token');
    	$merge  = array();

    	// return $params; exit;
        $code           = $request->performance_code;
        $question       = $request->performance_question;
        $correct_answer = $request->performance_correct_answer;
        $typeId         = $request->type_id;
        $groupId        = $request->group_id;
        $criteriaId     = $request->criteria_id;
        $trainningId    = $request->trainning_id;
        $sessionId      = $request->sessions;
        $moduleId       = $request->modules;
        $isRequired     = $request->performance_is_required;
        $isDefault      = $request->performance_is_default;
        $ratingStart    = $request->performance_rating_start;
        $ratingFinish   = $request->performance_rating_finish;        
          
            foreach ($code as $key => $value) {
              DB::beginTransaction();
              $answer = $request['performance_answer_'.($key+1)];    
              if($answer == ""){
                $answer = $request->performance_answer;
              }
               $performance_correct_answer = $request->performance_correct_answer;
              if(!empty($answer) || isset($answer))                  
                  $merge['performance_answer'] = implode(",", $answer);
             if($performance_correct_answer ==2)
                  $merge['performance_correct_answer'] = $answer[$key+0];
              if($performance_correct_answer ==3)
                  $merge['performance_correct_answer'] = $answer[$key+1];
              if($performance_correct_answer ==4)
                  $merge['performance_correct_answer'] = $answer[$key+2];
              if($performance_correct_answer ==5)
                  $merge['performance_correct_answer'] = $answer[$key+3];
              if($performance_correct_answer ==6)
                  $merge['performance_correct_answer'] = $answer[$key+4];
              if($performance_correct_answer ==7)
                  $merge['performance_correct_answer'] = $answer[$key+5];

              if(!empty($ratingStart) || isset($ratingStart))
                  $merge['performance_rating_start'] = !empty($ratingStart[$key]) ? $ratingStart[$key] : null;

              if(!empty($ratingFinish) || isset($ratingFinish))
                  $merge['performance_rating_finish'] = !empty($ratingFinish[$key]) ? $ratingFinish[$key] : null;

              $type = Type::where('type_label', $typeId[$key])->first();

              if($type)
                  $merge['type_id'] = $type->type_id;

              $data = [
                  'user_id' => Auth::user()->id,
                  'performance_code' => $code[$key],
                  'performance_question' => $question[$key],
                  'performance_answer' => null,
                  'performance_correct_answer' =>null,
                  'group_id' => $groupId[$key],
                  'criteria_id' => $criteriaId[$key],
                  'type_id' => $typeId[$key],
                  'trainning_id' => !empty($trainningId[$key]) ? $trainningId[$key] : null,
                  'session_id' => !empty($sessionId[$key]) ? $sessionId[$key] : null,
                  'module_id' => !empty($moduleId[$key]) ? $moduleId[$key] : null,
                  'performance_is_required' => $isRequired[$key] == "on" ? 1 : 0,
                  'performance_is_default' => $isDefault[$key] == "on" ? 1 : 0                  
              ];

              $performance = Performance::create(
                  array_merge(
                      $data,
                      $merge
                  )
              );              
              DB::commit();              
              $trainings = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->whereNotIn('module_questions.performance_id',[''])
              ->groupBy('trainnings.trainning_id');
              if(!empty($data['trainning_id'])){
                $trainings = $trainings->where('trainnings.trainning_id', $data['trainning_id'])->get();
              }else{
                $trainings = $trainings->get();
              }
              
              foreach ($trainings as $key => $value) {
                //$session = Session::find($value->session_id);
                //$periodeArray = array();
                // if($session->before == "1"){
                //   array_push($periodeArray, 0);
                // }elseif ($session->during == "1"){
                //   array_push($periodeArray, 1);
                // }elseif($session->after == "1"){
                //   array_push($periodeArray, 2);
                // }
                // foreach ($periodeArray as $value_periode) {          
                //   switch ($value_periode) {
                //     case 0:
                //         $module = "before";
                //         break;
                //     case 1:
                //         $module = "during";
                //         break;
                //     case 2:
                //         $module = "after";
                //         break;
                //     default:
                //         $module = "";
                //         break;
                //   } 
                  
                  $data = [
                    'module_id' => $value->module_id,
                    'performance_id' => $performance->performance_id,
                    'training_detial_periode' => 0
                  ];                                    
                //}
                DB::beginTransaction();
                ModuleQuestion::create($data);               
                DB::commit();                                        
              }              
            }            
        


    	$request->session()->flash('status', '200');
      $request->session()->flash('msg', 'Performance has been added');
      if(!empty($request->save_and_new)){
        return redirect('performance/create');
      }else{
        return redirect('performance');
      }      
    }

    public function edit($id)
    {
        $performance    = Performance::find($id);
        $type           = Type::all();
        $group          = Group::where('is_reaction', 2)->where("group", "Boss Position")->get();
        $criteria       = Criteria::where('level', 0)->get();
        $trainning      = Trainning::where('user_id', Auth::User()->id)->get();
        $session        = Session::where('session_id', $performance['session_id'])->first();
        $module         = Module::where('module_id', $performance['module_id'])->first();

        return view('performance.edit', compact('performance','type','group','trainning','session','module','criteria'));
    }

    public function show($id)
    {
        $performance = Performance::find($id);
        return view('performance.detail', compact('performance'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->except(['_token', '_method']); 

        $performance = Performance::findOrFail($id);

        if($performance) {
          
            $merge = array(
                'performance_is_default' => $request->performance_is_default == "on" ? 1 : 0,
                'performance_is_required' => $request->performance_is_required == "on" ? 1 : 0,
                'performance_answer' => implode(",", (array) $request->performance_answer),
                'trainning_id' => $request->performance_is_default == "on" ? null : $request->trainning_id,
                'session_id' => $request->performance_is_default == "on" ? null : $request->sessions,
                'module_id' => $request->performance_is_default == "on" ? null : $request->modules,
                'performance_correct_answer' => implode(",",(array) $request->performance_correct_answer)
            );
            
            $performance->update(
                array_merge($data, $merge)
            );

            $module_questions = ModuleQuestion::join('modules', 'modules.module_id', '=', 'module_questions.module_id')
              ->join('sessions','modules.session_id','=','sessions.session_id')              
              ->join('trainnings', 'trainnings.trainning_id', '=', 'sessions.trainning_id');              
            $training_datas_two = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')              
              ->groupBy('trainnings.trainning_id');
              if(!empty($merge['trainning_id'])){
                $module_question_deletes = $module_questions->where('module_questions.performance_id',$performance->performance_id)->whereNotIn('trainnings.trainning_id', [$merge['trainning_id']])->get();
                $trainings = $training_datas_two->where('trainnings.trainning_id', $merge['trainning_id'])->get();
                
                foreach ($module_question_deletes as $key => $value) {                
                  //DB::beginTransaction();
                  $module_question = ModuleQuestion::findOrFail($value->module_question_id);
                  $module_question->delete();  
                  // var_dump($module_question);
                  // exit(); 
                  //DB::commit();                            
                }
                // var_dump($trainings->count());
                // exit();
                foreach ($trainings as $key => $value) {
                  //$session = Session::find($value->session_id);
                  // $periodeArray = array();
                  // if($session->before == "1"){
                  //   array_push($periodeArray, 0);
                  // }elseif ($session->during == "1"){
                  //   array_push($periodeArray, 1);
                  // }elseif($session->after == "1"){
                  //   array_push($periodeArray, 2);
                  // }
                  // foreach ($periodeArray as $value_periode) {          
                  //   switch ($value_periode) {
                  //     case 0:
                  //         $module = "before";
                  //         break;
                  //     case 1:
                  //         $module = "during";
                  //         break;
                  //     case 2:
                  //         $module = "after";
                  //         break;
                  //     default:
                  //         $module = "";
                  //         break;
                  //   } 
                    
                    $data = [
                      'module_id' => $value->module_id,
                      'performance_id' => $performance->performance_id,
                      'training_detial_periode' => 0
                    ];
                    DB::beginTransaction();
                    ModuleQuestion::create($data);  
                    DB::commit();                 
                  //}
                }
              }else{
                $trainings = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
                ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
                ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
                ->whereNotIn('module_questions.performance_id',[''])
                ->groupBy('trainnings.trainning_id')
                ->get();                

                foreach ($trainings as $key => $value) {
                  // $session = Session::find($value->session_id);
                  // $periodeArray = array();
                  // if($session->before == "1"){
                  //   array_push($periodeArray, 0);
                  // }elseif ($session->during == "1"){
                  //   array_push($periodeArray, 1);
                  // }elseif($session->after == "1"){
                  //   array_push($periodeArray, 2);
                  // }
                  // foreach ($periodeArray as $value_periode) {          
                  //   switch ($value_periode) {
                  //     case 0:
                  //         $module = "before";
                  //         break;
                  //     case 1:
                  //         $module = "during";
                  //         break;
                  //     case 2:
                  //         $module = "after";
                  //         break;
                  //     default:
                  //         $module = "";
                  //         break;
                  //   } 
                    
                    $data = [
                      'module_id' => $value->module_id,
                      'performance_id' => $performance->performance_id,
                      'training_detial_periode' => 0
                    ];               
                    DB::beginTransaction();
                    ModuleQuestion::create($data); 
                    DB::commit(); 
                  //}
                }                 
              }                                        

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Performance has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Performance');
        }

        if(!empty($request->save_and_new)){
          return redirect('performance/create');
        }else{
          return redirect('performance');
        }      
    }

    public function delete(Request $request, $id)
    {
        $model  = Performance::findOrFail($id);

        if ($model) {
            $module_question = ModuleQuestion::where('performance_id', $model->performance_id)->delete();
            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Performance has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Performance');
        }

        return redirect()->back();
    }

    public function delete_questions(Request $request, $id, $answer)
    {
      $performance = Performance::findOrFail($id);
        $answers = explode(",",$performance->performance_answer);
        if($performance){        
          foreach(explode(",",$performance->performance_answer) as $key => $value_answer){          

            if ($answer == $value_answer) {
              unset($answers[$key]);      
            }
          }                
          
          $performance->update(array("performance_answer" => implode(",",$answers)));
        }
    }

    public function trainning(Request $request, $id)
    {
        $html  = "
            <div class='table-responsive'>
                <table class='table table-striped'>
                    <thead>
                        <th>No</th>
                        <th>Training Title</th>
                    </thead>
                    <tbody>
        ";

        // $model = Trainning::where('reaction_id', $id)
        //             ->join('trainning_details', 'trainnings.trainning_id', '=', 'trainning_details.trainning_id')
        //             ->groupBy('trainnings.trainning_id')
        //             ->get();
        $model = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->where('module_questions.performance_id', $id)
              ->groupBy('trainnings.trainning_id')
              ->get();

        foreach ($model as $key => $value) {
            $html .= "
                <tr>
                    <td>". ($key+1) ."</td>
                    <td>". $value->trainning_title ."</td>
                </tr>
            ";
        }

        $html .= "
                    </tbody>
                </table>
            </div>
        ";

        return $html;
    }

    public function import(Request $request) 
    {
        try{$import =  Excel::Import(new PerformanceImport, $request->file('import'));
        
        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Performance has been imported');

        return redirect('/performance');
      }catch(\Illuminate\Database\QueryException $e){
        return redirect()->back()->with('error','type atau group keliru');
      }

    }
}
