<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Trainning\Trainning;
use App\User;
use Auth;
use App\Model\Trainning\Company;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::user()->level_id != 1){
       $trainning = Trainning::where('user_id', Auth::user()->id); 
       if(Auth::user()->company_id != 0){
          $usertrainner   = \App\User::whereNotIn('level_id', [1,2,4,5])->where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->count();
          $in_company = Auth::user()->company->name;
          $participant    = \App\User::whereNotIn('level_id', [1,2,3,5])->where('company_id', Auth::user()->company_id)->orderBy('id', 'desc');
          $my_companies = Company::where('company_id', Auth::user()->company->company_id)->count();
        }else{
          $usertrainner   = 0;
          $in_company = "";
          $participant    = \App\User::whereNotIn('level_id', [1,2,3,5])->orderBy('id', 'desc');
          $my_companies = 0;
        }
        $companies = Company::all()->count();   
      }else{
        $trainning = Trainning::all();
        $companies = Company::all()->count();   
        $usertrainner   = \App\User::whereNotIn('level_id', [1,2,4,5])->orderBy('id', 'desc')->count();
        // if(Auth::user()->company_id != 0){
        //   $in_company = Auth::user()->company->name;        
        // }else{
          $in_company = "";        
        //}
        if(Auth::user()->level_id != 1){
        $participant    = \App\User::where('user_id', Auth::user()->id)->whereNotIn('level_id', [1,2,3,5])->orderBy('id', 'desc');
        }else{
            $participant    = \App\User::whereNotIn('level_id', [1,2,3,5])->orderBy('id', 'desc');
        }
        $my_companies = $companies;#Company::where('company_id', Auth::user()->company->company_id);
      }                     
    $trainee = \App\User::whereNotIn('level_id', [1,2,3,5])->orderBy('id', 'desc');      
    return view('page/home',compact('trainning','trainee','participant', 'companies', 'my_companies','usertrainner', 'in_company'));
    }
}
