<?php

namespace App\Http\Controllers\Roti;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
 use App\Imports\RotiImport;
use App\Model\Master\Type;
use App\Model\Master\Dimension as Group;
use App\Model\Master\Criteria;
use App\Model\Roti\Roti;
use App\Model\Trainning\ModuleQuestion;
use App\Model\Trainning\Session;
use App\Model\Trainning\Module;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\TrainningDetail;

// use App\Imports\LearningImport;
use Maatwebsite\Excel\Facades\Excel;

use Auth;
use DB;

class RotiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $roti   = Roti::orderBy('created_at', 'DESC');

        if($request->has('group_id') && !empty($request->group_id))
            $roti   = $roti->where('group_id', $request->group_id);

        if($request->has('type_id') && !empty($request->type_id))
            $roti   = $roti->where('type_id', $request->type_id);

        if($request->has('roti_code') && !empty($request->roti_code))
            $roti   = $roti->where('roti_code', $request->roti_code);

        if($request->has('roti_question') && !empty($request->roti_question))
            $roti   = $roti->where('roti_question', $request->roti_question);

        $roti       = $roti->paginate(20);
        $type 		= Type::all();
        $criteria   = Criteria::where('level', 1)->get();

        return view('roti.index', compact('roti', 'type', 'criteria'));
    }

    public function create()
    {
        $type           = Type::all();
        $group          = Group::where('is_reaction', 2)->where("group", "Boss Position")->get();
        $criteria       = Criteria::where('level', 1)->get();
        $trainningID = null;
        $trainning      = Trainning::where('user_id', Auth::User()->id)->get();

        $optGroup       = "";
        $optCriteria    = "";
        $optType        = "";
        $optTrainning   = "";

        foreach ($type as $key => $value) {
            $selected = $value->type_label == "rating" ? "selected" : "";
            $optType .= "<option value='". $value->type_label ."' ". $selected .">". $value->type ."</option>";
        }

        foreach ($group as $key => $value) {
            $optGroup .= "<option value='". $value->group_id ."'>". $value->group ."</option>";
        }

        foreach ($criteria as $key => $value) {
            $optCriteria .= "<option value='". $value->criteria_id ."'>". $value->criteria ."</option>";
        }

        foreach ($trainning as $key => $value) {
            $optTrainning .= "<option value='". $value->trainning_id ."'>". $value->trainning_title ."</option>";
        }
       
    	return view('roti.create', compact('type','group','criteria','trainning','optGroup','optCriteria','optType','optTrainning','trainningID'));
    }
     public function createCustom($trainningID,$sessionID,$moduleID)
    {
        $type           = Type::all();
        $group          = Group::where('is_reaction', 2)->get();
        $criteria       = Criteria::where('level', 0)->get();

        $trainningID      = Trainning::where('trainning_id',$trainningID)->get();


        $optGroup       = "";
        $optCriteria    = "";
        $optType        = "";
        $optTrainning   = "";
        
        foreach ($type as $key => $value) {
            $selected = $value->type_label == "rating" ? "selected" : "";
            $optType .= "<option value='". $value->type_label ."' ". $selected .">". $value->type ."</option>";
        }

        foreach ($group as $key => $value) {
            $optGroup .= "<option value='". $value->group_id ."'>". $value->group ."</option>";
        }

        foreach ($criteria as $key => $value) {
          $optCriteria .= "<option value='". $value->criteria_id ."'>". $value->criteria ."</option>";
        }

        $sessions = Session::where('session_id',$sessionID)->get();
        $modules = Module::where('module_id',$moduleID)->get();
        
      return view('roti.create', compact('type','group','criteria','optGroup','optCriteria','optType','optTrainning','sessions','modules','trainningID'));
    }

    public function get_session($id)
    {
        $sessions = Session::where('trainning_id', $id)->pluck('name','session_id');
        return response()->json($sessions);
    }

    public function get_module($id)
    {
        $modules = Module::where('session_id', $id)->pluck('name','module_id');
        return response()->json($modules);
    }

    public function store(Request $request)
    {
    	$params = $request->except('_token');
    	$merge  = array();

    	// return $params; exit;
        $code           = $request->roti_code;
        $question       = $request->roti_question;
        $correct_answer = $request->roti_correct_answer;
        $typeId         = $request->type_id;
        $groupId        = $request->group_id;
        $criteriaId     = $request->criteria_id;
        $trainningId    = $request->trainning_id;
        $sessionId      = $request->sessions;
        $moduleId       = $request->modules;
        $isRequired     = $request->roti_is_required;
        $isDefault      = $request->roti_is_default;
        $ratingStart    = $request->roti_rating_start;
        $ratingFinish   = $request->roti_rating_finish;
        
            foreach ($code as $key => $value) {
              DB::beginTransaction();
              $answer = $request['roti_answer_'.($key+1)];    
              if($answer == ""){
                $answer = $request->roti_answer;
              }
               $roti_correct_answer = $request->roti_correct_answer;
              if(!empty($answer) || isset($answer))
                  $merge['roti_answer'] = implode(",", $answer);
              if($roti_correct_answer ==2)
                  $merge['roti_correct_answer'] = $answer[$key+0];
              if($roti_correct_answer ==3)
                  $merge['roti_correct_answer'] = $answer[$key+1];
              if($roti_correct_answer ==4)
                  $merge['roti_correct_answer'] = $answer[$key+2];
              if($roti_correct_answer ==5)
                  $merge['roti_correct_answer'] = $answer[$key+3];
              if($roti_correct_answer ==6)
                  $merge['roti_correct_answer'] = $answer[$key+4];
              if($roti_correct_answer ==7)
                  $merge['roti_correct_answer'] = $answer[$key+5];
              

              if(!empty($ratingStart) || isset($ratingStart))
                  $merge['roti_rating_start'] = !empty($ratingStart[$key]) ? $ratingStart[$key] : null;

              if(!empty($ratingFinish) || isset($ratingFinish))
                  $merge['roti_rating_finish'] = !empty($ratingFinish[$key]) ? $ratingFinish[$key] : null;

              $type = Type::where('type_label', $typeId[$key])->first();

              if($type)
                  $merge['type_id'] = $type->type_id;                
              $data = [
                  'roti_code' => $code[$key],
                  'roti_question' => $question[$key],
                  'roti_answer' => null,
                  'group_id' => $groupId[$key],
                  'criteria_id' => $criteriaId[$key],
                  'type_id' => $typeId[$key],
                  'trainning_id' => !empty($trainningId[$key]) ? $trainningId[$key] : null,
                  'session_id' => !empty($sessionId[$key]) ? $sessionId[$key] : null,
                  'module_id' => !empty($moduleId[$key]) ? $moduleId[$key] : null,
                  'roti_is_required' => $isRequired[$key] == "on" ? 1 : 0,
                  'roti_is_default' => $isDefault[$key] == "on" ? 1 : 0
              ];

              $roti = Roti::create(
                  array_merge(
                      $data,
                      $merge
                  )
              );
              
              DB::commit(); 
              $trainings = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->whereNotIn('module_questions.roti_id',[''])
              ->groupBy('trainnings.trainning_id');
              if(!empty($data['trainning_id'])){
                $trainings = $trainings->where('trainnings.trainning_id', $data['trainning_id'])->get();
              }else{
                $trainings = $trainings->get();
              }              

              foreach ($trainings as $key => $value) {
                // $session = Session::find($value->session_id);
                // $periodeArray = array();
                // if($session->before == "1"){
                //   array_push($periodeArray, 0);
                // }elseif ($session->during == "1"){
                //   array_push($periodeArray, 1);
                // }elseif($session->after == "1"){
                //   array_push($periodeArray, 2);
                // }
                //   foreach ($periodeArray as $value_periode) {          
                //     switch ($value_periode) {
                //       case 0:
                //           $module = "before";
                //           break;
                //       case 1:
                //           $module = "during";
                //           break;
                //       case 2:
                //           $module = "after";
                //           break;
                //       default:
                //           $module = "";
                //           break;
                //     } 
                    
                    $data = [
                      'module_id' => $value->module_id,
                      'roti_id' => $roti->roti_id,
                      'training_detial_periode' => 0
                    ];
                    DB::beginTransaction();
                    ModuleQuestion::create($data); 
                    DB::commit(); 
                  //}
              }                
            }         


    	$request->session()->flash('status', '200');
        $request->session()->flash('msg', 'R.O.T.I has been added');

        if(!empty($request->save_and_new)){
            return redirect('roti/create');
        }else{
            return redirect('roti');
        } 
    }

    public function show($id)
    {
        $roti = Roti::find($id);
        return view('roti.detail', compact('roti'));
    }

    public function edit($id)
    {
        $roti       = Roti::find($id);
        $type       = Type::all();
        $group          = Group::where('is_reaction', 2)->where("group", "Boss Position")->get();
        $criteria   = Criteria::where('level', 1)->get();
        $trainning  = Trainning::where('user_id', Auth::User()->id)->get();
        $session        = Session::where('session_id', $roti['session_id'])->first();
        $module         = Module::where('module_id', $roti['module_id'])->first();

        return view('roti.edit', compact('roti','type','group','criteria','trainning','session','module'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->except(['_token', '_method']);    

        $roti = Roti::findOrFail($id);

        if($roti) {
            $merge = array(
                'roti_is_default' => $request->roti_is_default == "on" ? 1 : 0,
                'roti_is_required' => $request->roti_is_required == "on" ? 1 : 0,
                'roti_answer' => implode(",", (array) $request->roti_answer),
                'trainning_id' => $request->roti_is_default == "on" ? null : $request->trainning_id,
                'session_id' => $request->roti_is_default == "on" ? null : $request->sessions,
                'module_id' => $request->roti_is_default == "on" ? null : $request->modules,
                'roti_correct_answer' => implode(",",(array) $request->roti_correct_answer)
            );            

            $roti->update(
                array_merge($data, $merge)
            );

            $module_questions = ModuleQuestion::join('modules', 'modules.module_id', '=', 'module_questions.module_id')
            ->join('sessions','modules.session_id','=','sessions.session_id')              
            ->join('trainnings', 'trainnings.trainning_id', '=', 'sessions.trainning_id');
                  
            $training_datas_two = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
            ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
            ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')        
            ->groupBy('trainnings.trainning_id');
            // Custom
            if(!empty($merge['trainning_id'])){
              $module_question_deletes = $module_questions->where('module_questions.roti_id',$roti->roti_id)->whereNotIn('trainnings.trainning_id', [$merge['trainning_id']])->get();
              $trainings = $training_datas_two->where('trainnings.trainning_id', $merge['trainning_id'])->get();

              foreach ($module_question_deletes as $key => $value) {
                $module_question = ModuleQuestion::findOrFail($value->module_question_id);
                $module_question->delete();                        
              }

              foreach ($trainings as $key => $value) {
                // $session = Session::find($value->session_id);
                // $periodeArray = array();
                // if($session->before == "1"){
                //   array_push($periodeArray, 0);
                // }elseif ($session->during == "1"){
                //   array_push($periodeArray, 1);
                // }elseif($session->after == "1"){
                //   array_push($periodeArray, 2);
                // }
                // foreach ($periodeArray as $value_periode) {          
                //   switch ($value_periode) {
                //     case 0:
                //         $module = "before";
                //         break;
                //     case 1:
                //         $module = "during";
                //         break;
                //     case 2:
                //         $module = "after";
                //         break;
                //     default:
                //         $module = "";
                //         break;
                //   } 
                  
                  $data = [
                    'module_id' => $value->module_id,
                    'roti_id' => $roti->roti_id,
                    'training_detial_periode' => 0
                  ];
                  ModuleQuestion::create($data);     
                //}
              }
            }
            // Default
            else{
              $trainings = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->whereNotIn('module_questions.roti_id',[''])
              ->groupBy('trainnings.trainning_id')
              ->get();

              foreach ($trainings as $key => $value) {
                // $session = Session::find($value->session_id);
                // $periodeArray = array();
                // if($session->before == "1"){
                //   array_push($periodeArray, 0);
                // }elseif ($session->during == "1"){
                //   array_push($periodeArray, 1);
                // }elseif($session->after == "1"){
                //   array_push($periodeArray, 2);
                // }
                //   foreach ($periodeArray as $value_periode) {          
                //     switch ($value_periode) {
                //       case 0:
                //           $module = "before";
                //           break;
                //       case 1:
                //           $module = "during";
                //           break;
                //       case 2:
                //           $module = "after";
                //           break;
                //       default:
                //           $module = "";
                //           break;
                //     } 
                    
                    $data = [
                      'module_id' => $value->module_id,
                      'roti_id' => $roti->roti_id,
                      'training_detial_periode' => 0
                    ];
                    DB::beginTransaction();
                    ModuleQuestion::create($data); 
                    DB::commit(); 
                //}
              }                  
            }   
            

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'R.O.T.I has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit R.O.T.I');
        }

        if(!empty($request->save_and_new)){
            return redirect('roti/create');
        }else{
            return redirect('roti');
        }      
    }

    public function delete(Request $request, $id)
    {
        $model  = Roti::findOrFail($id);

        if ($model) {
            $module_question = ModuleQuestion::where('roti_id', $model->roti_id)->delete();
            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'R.O.T.I has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete R.O.T.I');
        }

        return redirect()->back();
    }

    public function delete_questions(Request $request, $id, $answer){
      $roti = Roti::findOrFail($id);
      $answers = explode(",",$roti->roti_answer);
      if($roti){        
        foreach(explode(",",$roti->roti_answer) as $key => $value_answer){          

          if ($answer == $value_answer) {
            unset($answers[$key]);      
          }
        }                
        
        $roti->update(array("roti_answer" => implode(",",$answers)));
      }
    }

    public function trainning(Request $request, $id)
    {
        $html  = "
            <div class='table-responsive'>
                <table class='table table-striped'>
                    <thead>
                        <th>No</th>
                        <th>Title</th>
                    </thead>
                    <tbody>
        ";

        $model = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->where('module_questions.roti_id', $id)
              ->groupBy('trainnings.trainning_id')
              ->get();

        foreach ($model as $key => $value) {
            $html .= "
                <tr>
                    <td>". ($key+1) ."</td>
                    <td>". $value->trainning_title ."</td>
                </tr>
            ";
        }

        $html .= "
                    </tbody>
                </table>
            </div>
        ";

        return $html;
    }

    public function import(Request $request) 
    {
       try{$import = Excel::Import(new RotiImport, $request->file('import'));

  
        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'roti has been imported');

        return redirect('/roti');
        } catch (\Illuminate\Database\QueryException $e) {
          return redirect()->back()->with('error','type atau group keliru');
        
      }
    }
}
