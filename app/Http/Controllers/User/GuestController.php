<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUser;
use App\Http\Requests\UpdateUser;

class GuestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->module = "User";
        $this->limit = 25;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$this->authorize('index', [ \App\User::class, $this->module ]);

        $guests              = \App\User::whereNotIn('level_id', [1,2,3,4])->orderBy('id', 'desc');

        if (\Auth::user()->level_id == 2)
            $guests->where('level_id','!=',5);

        if( isset($request->key) && isset($request->value) )
        {
            $key    = $request->key;
            $value  = $request->value;

            $guests->where($key,'like','%'. $value .'%');
        }

        $guests    = $guests->paginate($this->limit);
        $guests->appends($request->all());

    	return view('user.guest.index', compact('guests'));
    }

    public function store(StoreUser $request)
    {
    	$user 			    = new \App\User;
    	$user->name 		= $request->name;
    	$user->email 		= $request->email;
    	$user->password	    = bcrypt($request->password);
    	$user->level_id	    = $request->level_id;

        $insert = $user->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Guest has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Guest');
        }

    	return redirect('guest');

    }

    public function create()
    {
        $levels = \App\Model\Level::whereNotIn('level_id',[1,2,3,4])->get(['level_id','level']);
        
    	return view('user.guest.create', compact('levels'));
    }

    public function edit($id)
    {
    	$user = \App\User::find($id);
        $levels = \App\Model\Level::all(['level_id','level']);

    	return view('user.guest.edit', compact('user', 'levels'));
    }

    public function update(Request $request, $id)
    {
    	$this->authorize('edit', [ \App\User::class, $this->module ]);

    	$data = [
            'name' => $request->name,
            'email' => $request->email,
            'level_id' => $request->level_id
        ];

    	if ($request->password != "" || $request->password != NULL) {
    		$data['password'] = bcrypt($request->password);
    	}

        if ($request->pin != "" || $request->pin != NULL) {
            $data['pin'] = bcrypt($request->pin);
        }

    	$update = \App\User::where('id',$id)->update($data);

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Guest has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Guest');
        }

        return redirect('guest');
    }

    public function delete(Request $request, $id)
    {
    	$this->authorize('delete', [ \App\User::class, $this->module ]);

    	$delete	= \App\User::where('id',$id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Guest has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Guest');
        }

    	return redirect('guest');
    }
}
