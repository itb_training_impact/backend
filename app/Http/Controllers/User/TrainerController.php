<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUser;
use App\Http\Requests\UpdateUser;

class TrainerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->module = "User";
        $this->limit = 25;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$this->authorize('index', [ \App\User::class, $this->module ]);

        $trainer              = \App\User::whereNotIn('level_id', [1,2,4,5])->orderBy('id', 'desc');

        if (\Auth::user()->level_id == 2)
            $trainer->where('level_id','!=',1);

        if( isset($request->key) && isset($request->value) )
        {
            $key    = $request->key;
            $value  = $request->value;

            $trainer->where($key,'like','%'. $value .'%');
        }

        $trainer    = $trainer->paginate($this->limit);
        $trainer->appends($request->all());

    	return view('user.trainer.index', compact('trainer'));
    }

    public function store(StoreUser $request)
    {
    	$user 			    = new \App\User;
    	$user->name 		= $request->name;
        $user->username     = $request->username;
    	$user->email 		= $request->email;
    	$user->password	    = bcrypt($request->password);
    	$user->level_id	    = $request->level_id;
        $insert = $user->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Trainer has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Trainer');
        }

    	return redirect('trainer');

    }

    public function create()
    {
        $levels = \App\Model\Level::whereNotIn('level_id',[1,2,4,5])->get(['level_id','level']);
        
    	return view('user.Trainer.create', compact('levels'));
    }

    public function edit($id)
    {
    	$trainer = \App\User::find($id);
        $levels = \App\Model\Level::whereNotIn('level_id',[1,2,4,5])->get(['level_id','level']);

    	return view('user.trainer.edit', compact('trainer', 'levels'));
    }

   public function update(Request $request, $id)
    {
        $this->authorize('edit', [ \App\User::class, $this->module ]);

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'username' =>$request->username,
            'no_telpon' => $request->no_telpon,
            'instansi_perusahaan' => $request->instansi_perusahaan,
            'pendidikan_terakhir' =>$request->pendidikan_terakhir,
            'instansi_pendidikan' => $request->instansi_pendidikan,
            'alamat' => $request->alamat,
            'level_id' => $request->level_id
        ];

        if ($request->password != "" || $request->password != NULL) {
            $data['password'] = bcrypt($request->password);
        }
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('cdn/avatar/',$request->file('avatar')->getClientOriginalName());
            $data->avatar = $request->file('avatar')->getClientOriginalName();
            $data->save();
        }

        $update = \App\User::where('id',$id)->update($data);

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'trainer has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit trainer');
        }

        return redirect('trainer');
    }

    public function delete(Request $request, $id)
    {
    	$this->authorize('delete', [ \App\User::class, $this->module ]);

    	$delete	= \App\User::where('id',$id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Trainer has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Trainer');
        }

    	return redirect('trainer');
    }
}
