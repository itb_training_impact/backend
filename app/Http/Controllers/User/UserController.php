<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUser;
use App\Http\Requests\UpdateUser;
use App\Model\Trainning\Company;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->module = "User";
        $this->limit = 25;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$this->authorize('index', [ \App\User::class, $this->module ]);        

        $user              = \App\User::whereNotIn('level_id', [4,5,6,7,8])->orderBy('id', 'desc');

        if (\Auth::user()->level_id == 2)
            $user->where('level_id','!=',1);

        if( isset($request->key) && isset($request->value) )
        {
            $key    = $request->key;
            $value  = $request->value;

            $user->where($key,'like','%'. $value .'%');
        }

        $user    = $user->paginate($this->limit);
        $user->appends($request->all());

    	return view('user.index', compact('user'));
    }

    public function store(StoreUser $request)
    {
    	$user 			    = new \App\User;
    	$user->name 		= $request->name;
        $user->username     = $request->username;
    	$user->email 		= $request->email;
    	$user->password	    = bcrypt($request->password);
    	$user->level_id	    = $request->level_id;
        $user->company_id   = $request->company_id;
    
        $insert = $user->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

    	return redirect('user');

    }

    public function create()
    {
        $levels = \App\Model\Level::whereNotIn('level_id',[1,2,4,5])->get(['level_id','level']);
        $companies = Company::orderBy('name', 'asc')->get();
        
    	return view('user.create', compact('levels', 'companies'));
    }

    public function edit($id)
    {
    	$user = \App\User::find($id);
        $levels = \App\Model\Level::whereNotIn('level_id',[4,5])->get(['level_id','level']);
        $companies = Company::orderBy('name', 'asc')->get();

    	return view('user.edit', compact('user', 'levels', 'companies'));
    }

    public function update(Request $request, $id)
    {
    	$this->authorize('edit', [ \App\User::class, $this->module ]);

    	$data = [
            'name' => $request->name,
            'email' => $request->email,
            'level_id' => $request->level_id,
            'company_id' => $request->company_id
        ];

    	if ($request->password != "" || $request->password != NULL) {
    		$data['password'] = bcrypt($request->password);
    	}

    	$update = \App\User::where('id',$id)->update($data);

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Admin');
        }

        if ($request->type == "profile") {
            return redirect('profile');
        }

        return redirect('user');
    }

    public function delete(Request $request, $id)
    {
    	$this->authorize('delete', [ \App\User::class, $this->module ]);

    	$delete	= \App\User::where('id',$id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Admin');
        }

    	return redirect('user');
    }

    public function profile()
    {
        $id                 = \Auth::user()->id;
        // $data['path']       = $this->cdn."/".\Auth::user()->photo;
        $profile    = \App\User::where(['id' => $id, 'status' => 1])->first();

        return view('user.profile', compact('profile'));
    }

    public function profileUpdate(Request $request)
    {
        $this->authorize('update', [ \App\User::class, $this->module ]);

        $id = \Auth::user()->id;

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'phone' => $request->phone,
            'address' => $request->address,
        ];

        if ($request->password != "" || $request->password != NULL) {
            $data['password'] = bcrypt($request->password);
        }

        // return $data; exit;

        $update = \App\User::where('id',$id)->update($data);

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'User berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'User gagal diubah');
        }

        return redirect('profile');
    }
    public function approved(Request $request,$id)
    {

        $model  = \App\User::findOrFail($id);
        $model = [
            'verified' => 1,
        ];
        if ($model) {

           $update = \App\User::where('id',$id)->update($model);
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'approved has been edited');
       }
       return redirect()->back();
    }
}
