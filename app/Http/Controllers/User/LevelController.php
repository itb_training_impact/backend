<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreLevelPost;

use App\Model\Level;

class LevelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }
    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $level  = Level::orderBy('level');

        if( isset($request->key) && isset($request->value) )
        {
            $key    = $request->key;
            $value  = $request->value;

            $level->where($key,'like','%'. $value .'%');
        }

        $level    = $level->paginate($this->limit);
        $level->appends($request->all());

    	return view('user.level.index', compact('level'));
    }

    public function create()
    {
    	return view('user.level.create');
    }

    public function store(StoreLevelPost $request)
    {
    	$level 	= new Level;
    	$level->level = $request->level;

        $insert = $level->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Level baru berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Level gagal ditambahkan');
        }

    	return redirect('level');
    }

    public function edit($id)
    {
        $level = Level::where('level_id',$id)->first();

    	return view('user.level.edit', compact(['level']));
    }

    public function update(Request $request, $id)
    {
    	$update = Level::where('level_id',$id)->update([
                'level' => $request->level
            ]);

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Level berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Level gagal diubah');
        }

    	return redirect('level');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Level::where('level_id',$id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Level berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Level gagal dihapus');
        }

    	return redirect('level');
    }
}
