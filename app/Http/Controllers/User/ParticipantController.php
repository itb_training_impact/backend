<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUser;
use App\Http\Requests\UpdateUser;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Model\Trainning\Company;
use App\Model\Master\Boss;
use App\Model\Master\Partner;
use App\Model\Master\Subordinate;
use App\Model\Master\PositionLevel;
use App\Model\Master\Devision;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ParticipantImport;
use Auth;
class ParticipantController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->module = "User";
        $this->limit = 25;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$this->authorize('index', [ \App\User::class, $this->module ]);

         if(Auth::user()->level_id != 1)
             $participant              = \App\User::where('user_id',Auth::user()->id)->whereNotIn('level_id', [1,2,3,5])->orderBy('id', 'desc');
          else{
             $participant              = \App\User::whereNotIn('level_id', [1,2,3,5])->orderBy('id', 'desc');
          }

        if( isset($request->key) && isset($request->value) )
        {
            $key    = $request->key;
            $value  = $request->value;

            $participant->where($key,'like','%'. $value .'%');
        }

        $participant    = $participant->paginate($this->limit);
        $participant->appends($request->all());

    	return view('user.participant.index', compact('participant'));
    }

    public function store(Request $request)
    {
        if(\App\User::where('email', $request['email'])->exists()){
            return back()->with('err', 'Email Telah Digunakan');
        }

        $user = ([
            'name' => $request['name'],
            'username' => $request['username'],
            'email' => $request['email'],
            'level_id' => $request['level_id'],
            'company_id' => $request['company_id'],
            'password' => Hash::make($request['password']),
            'dob'=>Carbon::parse($request->dob)->format('Y-m-d'),
            'age'=>$request['age'],
            'marriage'=>$request['marriage'],
            'religion'=>$request['religion'],
            'etnis'=>$request['etnis'],
            'job'=>$request['job'],
            'years_of_service'=>$request['years_of_service'],
            'gender'=>$request['gender'],            
            'position_level_id'=>$request['position_level'],
            'devision_id'=>$request['devision'],
            'verified'=>"1",
            'user_id'=>Auth::user()->id
        ]);

        if ($request->HasFile('avatar')) {
            $destination_path = "/public/avatar/";
            $image = $request->file('avatar');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('avatar')->storeAs($destination_path,$image_name);
            $user['avatar'] = $image_name;
        }
        
        if ($request->password == "" || $request->password == NULL) {
    		$user['password'] = Hash::make("usernamesiparticipant123");
    	}
        
        $insert = \App\User::insertGetId($user);


        $userID = [
            'user_id'=>$insert
        ];

        
       for($i= 0; $i < count($request->boss_name); $i++){
           if($request->boss_name[$i] != null){
                // Bagian Sesi
                $dataBoss[] = [
                    'boss_name' => $request->boss_name[$i], 
                    'boss_email' => $request->boss_email[$i],
                    'code_boss' => str_random(40)
                ];

                $bos = Boss::updateOrCreate(
                    array_merge(
                        $dataBoss[$i],
                        $userID
                    )
                );
           }                
        }   

       for($i= 0; $i < count($request->partner_name); $i++){
            if($request->partner_name[$i] != null){
                // Bagian Sesi
                $dataPartner[] = [
                    'partner_name' => $request->partner_name[$i], 
                    'partner_email' => $request->partner_email[$i],
                    'code_partner' => str_random(40)
                ];
                
                $partner = Partner::updateOrCreate(
                    array_merge(
                        $dataPartner[$i],
                        $userID
                    )
                );  
            }              
        }   
       for($i= 0; $i < count($request->subordinate_name); $i++){
            if($request->subordinate_name[$i] != null){
                // Bagian Sesi
                $dataBawahan[] = [
                    'subordinate_name' => $request->subordinate_name[$i], 
                    'subordinate_email' => $request->subordinate_email[$i],
                    'code_subordinate' => str_random(40)
                ];
                
                $bawahan = Subordinate::updateOrCreate(
                    array_merge(
                        $dataBawahan[$i],
                        $userID
                    )
                );   
            }             
        }   

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Participant has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Participant');
        }

    	return redirect('participant');

    }

    public function create()
    {
        $levels = \App\Model\Level::whereNotIn('level_id',[1,2,3,5])->get(['level_id','level']);
        if(Auth::user()->level_id != 1){
              $companies = Company::where('user_id', Auth::user()->id)->get();
        }else{
             $companies = Company::orderBy('name', 'asc')->get();
        }
       
        $position_level = PositionLevel::orderBy('name', 'asc')->get();
        $devision = Devision::orderBy('name', 'asc')->get();
    	return view('user.participant.create', compact('levels', 'companies', 'position_level', 'devision'));
    }

    public function edit($id)
    {
    	$participant = \App\User::find($id);
        $levels = \App\Model\Level::whereNotIn('level_id',[1,2,3,5])->get(['level_id','level']);
        
         if(Auth::user()->level_id != 1){
            $companies = Company::where('user_id', Auth::user()->id)->get();
        }else{
            $companies = Company::all();
        }
        $position_level = PositionLevel::orderBy('name', 'asc')->get();
        $devision = Devision::orderBy('name', 'asc')->get();
        $boss = Boss::where('user_id',$id)->get();
        $partner = Partner::where('user_id',$id)->get();
        $subordinate = Subordinate::where('user_id',$id)->get();
    	return view('user.participant.edit', compact('participant', 'levels', 'companies', 'position_level', 'devision', 'boss', 'partner','subordinate'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('edit', [ \App\User::class, $this->module ]);
        
    	$data = [
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'level_id' => $request->level_id,
            'company_id' => $request->company_id,
            'dob' => Carbon::parse($request->dob)->format('Y-m-d'),
            'age' => $request->age,
            'gender' => $request->gender,
            'etnis' => $request->etnis,
            'religion' => $request->religion,
            'job' => $request->job,
            'marriage' => $request->marriage,
            'years_of_service' => $request->years_of_service,        
            'position_level_id' => $request->position_level,
            'devision_id' => $request->devision
        ];

        if ($request->HasFile('avatar')) {
            $destination_path = "/public/avatar/";
            $image = $request->file('avatar');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('avatar')->storeAs($destination_path,$image_name);
            $data['avatar'] = $image_name;
        }

    	if ($request->password ? : []) {
                 $data['password'] = bcrypt($request->password);
        }
        
        if ($request->code_boss == "" || $request->code_boss == NULL) {
            $code_boss = str_random(40);
        }else{ $code_boss = $request->code_boss; }

        if ($request->code_partner == "" || $request->code_partner == NULL) {
            $code_partner = str_random(40);
        }else{ $code_partner = $request->code_partner; }

        if ($request->code_subordinate == "" || $request->code_subordinate == NULL) {
            $code_subordinate = str_random(40);
        }else{ $code_subordinate = $request->code_subordinate; }

        $update = \App\User::where('id',$id)->update($data);

        $userID = [
            'user_id'=>$id
        ];
    if($request->boss_name != null){
       for($i= 0; $i < count($request->boss_name); $i++){
              if($request->boss_name[$i] != null){
                $dataBoss[] = [
                    'boss_name' => $request->boss_name[$i], 
                    'boss_email' => $request->boss_email[$i],
                    'code_boss' => str_random(40)
                ];
                $bos = Boss::updateOrCreate(
                    array_merge(
                        $dataBoss[$i],
                        $userID
                    )
                );
            }               
        }   
    }
     if($request->partner_name != null){
       for($i= 0; $i < count($request->partner_name); $i++){
                if($request->partner_name[$i] != null){
                $dataPartner[] = [
                    'partner_name' => $request->partner_name[$i], 
                    'partner_email' => $request->partner_email[$i],
                    'code_partner' =>  str_random(40)
                ];
                
                $partner = Partner::updateOrCreate(
                    array_merge(
                        $dataPartner[$i],
                        $userID
                        )
                    );
                }                
            }   
        }
    if($request->subordinate_name != null){ 
       for($i= 0; $i < count($request->subordinate_name); $i++){
            if($request->subordinate_name[$i] != null){
                $dataBawahan[] = [
                    'subordinate_name' => $request->subordinate_name[$i], 
                    'subordinate_email' => $request->subordinate_email[$i],
                    'code_subordinate' =>  str_random(40)
                ];
                
                $bawahan = Subordinate::updateOrCreate(
                    array_merge(
                        $dataBawahan[$i],
                        $userID
                    )
                );  
            }              
        }   
    }
        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Participant has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Participant');
        }

        return redirect('participant');
    }

    public function delete(Request $request, $id)
    {
    	$this->authorize('delete', [ \App\User::class, $this->module ]);

    	$delete	= ([
            \App\User::where('id',$id)->delete(),
            Boss::where('user_id',$id)->delete(),
            Partner::where('user_id',$id)->delete(),
            Subordinate::where('user_id',$id)->delete()
        ]);

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Participant has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Participant');
        }

    	return redirect('participant');
    }
    public function import(Request $request) 
    {
     try{$import = Excel::Import(new ParticipantImport, $request->file('import'));

  
        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Participant has been imported');

        return redirect('participant');
        } catch (\Illuminate\Database\QueryException $e) {
          return redirect()->back()->with('error','The import failed because, the company was not found');
        
      }

    }
}
