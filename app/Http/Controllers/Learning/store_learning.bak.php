  public function store(Request $request)
    {
    	$params = $request->except('_token');
    	$merge  = array();

    	// return $params; exit;
        $code           = $request->learning_code;
        $question       = $request->learning_question;
        $correct_answer = $request->learning_correct_answer;
        $typeId         = $request->type_id;
        $groupId        = $request->group_id;
        $trainningId    = $request->trainning_id;
        $sessionId      = $request->sessions;
        $moduleId       = $request->modules;
        $isRequired     = $request->learning_is_required;
        $isDefault      = $request->learning_is_default;
        $ratingStart    = $request->learning_rating_start;
        $ratingFinish   = $request->learning_rating_finish;
        

            foreach ($code as $key => $value) {
              DB::beginTransaction();
              $answer = $request['learning_answer_'.($key+1)];    
              if($answer == ""){
                $answer = $request->learning_answer;
                $learning_correct_answer = !empty($request['learning_correct_answer_'.($key+1)]) ? $request['learning_correct_answer_'.($key+1)] : null;    
              }

              if(!empty($answer) || isset($answer))
                  $merge['learning_answer'] = implode(",", $answer);
                  $merge['learning_correct_answer'] = implode(",", (!empty($learning_correct_answer) ? $learning_correct_answer : []));

              if(!empty($ratingStart) || isset($ratingStart))
                  $merge['learning_rating_start'] = !empty($ratingStart[$key]) ? $ratingStart[$key] : null;

              if(!empty($ratingFinish) || isset($ratingFinish))
                  $merge['learning_rating_finish'] = !empty($ratingFinish[$key]) ? $ratingFinish[$key] : null;

              $type = Type::where('type_label', $typeId[$key])->first();

              if($type)
                  $merge['type_id'] = $type->type_id;                
              $data = [
                  'learning_code' => $code[$key],
                  'learning_question' => $question[$key],
                  'learning_answer' => null,
                  'group_id' => $groupId[$key],
                  'type_id' => $typeId[$key],
                  'trainning_id' => !empty($trainningId[$key]) ? $trainningId[$key] : null,
                  'session_id' => !empty($sessionId[$key]) ? $sessionId[$key] : null,
                  'module_id' => !empty($moduleId[$key]) ? $moduleId[$key] : null,
                  'learning_is_required' => $isRequired[$key] == "on" ? 1 : 0,
                  'learning_is_default' => $isDefault[$key] == "on" ? 1 : 0,
                  //'user_id' => Auth::user()->id
              ];

              $learning = Learning::create(
                  array_merge(
                      $data,
                      $merge
                  )
              );
              DB::commit(); 
              $trainings = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->whereNotIn('module_questions.learning_id',[''])
              ->groupBy('trainnings.trainning_id');
              if(!empty($data['trainning_id'])){
                $trainings = $trainings->where('trainnings.trainning_id', $data['trainning_id'])->get();
              }else{
                $trainings = $trainings->get();
              }              

              foreach ($trainings as $key => $value) {
                $session = Session::find($value->session_id);
                $periodeArray = array();
                if($session->before == "1"){
                  array_push($periodeArray, 0);
                }elseif ($session->during == "1"){
                  array_push($periodeArray, 1);
                }elseif($session->after == "1"){
                  array_push($periodeArray, 2);
                }
                  foreach ($periodeArray as $value_periode) {          
                    switch ($value_periode) {
                      case 0:
                          $module = "before";
                          break;
                      case 1:
                          $module = "during";
                          break;
                      case 2:
                          $module = "after";
                          break;
                      default:
                          $module = "";
                          break;
                    } 
                    
                    $data = [
                      'module_id' => $value->module_id,
                      'learning_id' => $learning->learning_id,
                      'training_detial_periode' => $module
                    ];
                    DB::beginTransaction();
                    ModuleQuestion::create($data); 
                    DB::commit(); 
                  }
              }              
            }         


    	$request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Learning has been added');

        if(!empty($request->save_and_new)){
        return redirect('learning/create');
      }else{
        return redirect('learning');
      } 
    }