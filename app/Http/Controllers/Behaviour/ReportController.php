<?php

namespace App\Http\Controllers\Behaviour;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\BehaviourUsersExport;
use App\Exports\BehaviourUserExport;

use App\User;
use App\Model\Trainning\Trainning;
use App\Model\Behaviour\Story;
use App\Model\Master\FormulationGroup;

use Maatwebsite\Excel\Facades\Excel;

use Auth;
use PDF;
use Response;

class ReportController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function export_story_users_to_txt($trainningID, $sessionID, $moduleID,$userID)
	{
		$trainning = Trainning::find($trainningID);
		$users = User::where('id', $userID)->get();

		$fileName = "story-behaviour-" . $trainning->trainning_title . ".txt";
		$content = '';

		foreach ($users as $user) {
			$stories = $user->storyByTrainning($trainningID, $sessionID, $moduleID);
			foreach ($stories as $story) {
				switch ($story->submit_periode) {
					case 0:
						$periode = 'before';
						break;
					case 1:
						$periode = 'during';
						break;
					case 2:
						$periode = 'after';
						break;
				}
				$content .= "Periode: " . $periode . "\r\n";
				$content .= $story->detail->story;
				$content .= "\r\n\r\n\r\n";
			}
		}

		// use headers in order to generate the download
		$headers = [
			'Content-type' => 'text/plain', 
			'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
			// 'Content-Length' => sizeof($content)
		];

		return Response::make($content, 200, $headers);
	}

	public function export_story_user_to_txt($trainningID, $userID)
	{
		$trainning = Trainning::find($trainningID);
		$user = User::find($userID);

		$fileName = "story-behaviour-" . $trainning->trainning_title . ".txt";
		$content = '';

		$stories = $user->storyByTrainning($trainningID);
		foreach ($stories as $story) {
			switch ($story->submit_periode) {
				case 0:
					$periode = 'before';
					break;
				case 1:
					$periode = 'during';
					break;
				case 2:
					$periode = 'after';
					break;
			}
			$content .= "Session : " . $story->session->name . "\r\n";
			$content .= "Module : " . $story->module->name . "\r\n";
			$content .= "Periode: " . $periode . "\r\n";
			$content .= $story->detail->story;
			$content .= "\r\n\r\n\r\n";
		}

		// use headers in order to generate the download
		$headers = [
			'Content-type' => 'text/plain', 
			'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
			// 'Content-Length' => sizeof($content)
		];

		return Response::make($content, 200, $headers);
	}

	public function export_behaviour_users_to_excel($type, $trainningID, $sessionID, $moduleID, $userID)
	{
		$trainning = Trainning::find($trainningID);
		return Excel::download(new BehaviourUsersExport($trainningID, $sessionID, $moduleID, $userID), 'report-behaviour-' . $trainning->trainning_title . '.xlsx');
	}

	public function export_behaviour_user_to_excel($type, $trainningID, $userID)
	{
		$trainning = Trainning::find($trainningID);
		return Excel::download(new BehaviourUserExport($trainningID, null, null, $userID), 'report-behaviour-' . $trainning->trainning_title . '.xlsx');
	}

	public function export_result($id)
	{
		$story = Story::find($id);

		$formulationGroups = FormulationGroup::orderBy('name', 'asc');
		$formulationGroups->whereHas('formulation', function ($query) {
			$query->where('status', 1);
		});

		$formulationGroups = $formulationGroups->get();

		$pdf = PDF::loadview('export.behaviour.user_result', ['story' => $story, 'formulationGroups' => $formulationGroups]);
		return $pdf->download('behaviour-report-' . time() . '-pdf');
	}
}