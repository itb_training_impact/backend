<?php

namespace App\Http\Controllers\Behaviour;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\Type;
use App\Model\Master\Dimension as Group;
use App\Model\Behaviour\Behaviour;
use App\Model\Trainning\ModuleQuestion;
use App\Model\Trainning\Session;
use App\Model\Trainning\Module;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\TrainningDetail;
use App\Model\Master\Formulation;

use App\Imports\BehaviourImport;
use Maatwebsite\Excel\Facades\Excel;

use Auth;
use DB;

class BehaviourController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application reaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $behaviour   = Behaviour::orderBy('created_at', 'DESC');

        if($request->has('group_id') && !empty($request->group_id))
            $behaviour   = $behaviour->where('group_id', $request->group_id);

        if($request->has('type_id') && !empty($request->type_id))
            $behaviour   = $behaviour->where('type_id', $request->type_id);

        if($request->has('behaviour_code') && !empty($request->behaviour_code))
            $behaviour   = $behaviour->where('behaviour_code', $request->behaviour_code);
        // if($request->has('learning_is_required') && !empty($request->learning_is_required))
        //     $learning   = $learning->where('learning_is_required', $request->learning_is_required);
        if($request->has('behaviour_question') && !empty($request->behaviour_question))
            $behaviour   = $behaviour->where('behaviour_question', $request->behaviour_question);        
        // if( Auth::user()->level_id != 1){
        //   $learning = $learning->where('learning_is_default', 1)->whereOr('user_id', Auth::user()->id);
        // }
        $behaviour   = $behaviour->paginate(15);
        $type 		= Type::all();
        $group      = Group::where('is_reaction', 2)->get();

        return view('behaviour.index', compact('behaviour','type', 'group'));
    }
     public function createCustom($trainningID,$sessionID,$moduleID)
    {
        $trainningId      = Trainning::where('trainning_id',$trainningID)->get();
        $sessionId       = Session::where('session_id',$sessionID)->get();
        $moduleId       = Module::where('module_id',$moduleID)->get();
        $type           = Type::all();
        $group          = Group::where('is_reaction', 0)->get();
        $trainning      = Trainning::where('user_id', Auth::User()->id)->get();
        $formulations   = Formulation::all();

        $optGroup       = "";
        $optType        = "";
        $optTrainning   = "";

        foreach ($type as $key => $value) {
            $selected = $value->type_label == "rating" ? "selected" : "";
            $optType .= "<option value='". $value->type_label ."' ". $selected .">". $value->type ."</option>";
        }

        foreach ($group as $key => $value) {
            $optGroup .= "<option value='". $value->group_id ."'>". $value->group ."</option>";
        }

        foreach ($trainning as $key => $value) {
            $optTrainning .= "<option value='". $value->trainning_id ."'>". $value->trainning_title ."</option>";
        }
        
      return view('behaviour.create', compact('type', 'group', 'trainning','trainningId', 'optGroup', 'optType', 'optTrainning','sessionId','moduleId','formulations'));
    }

    public function create()
    {
        $type           = Type::all();
        $group          = Group::where('is_reaction', 2)->get();
        $trainning      = Trainning::where('user_id', Auth::User()->id)->get();
        $formulations   = Formulation::all();

        $optGroup       = "";
        $optType        = "";
        $optTrainning   = "";

        $trainningId = null;
        $sessionId = null;
        $moduleId = null;
        foreach ($type as $key => $value) {
            $selected = $value->type_label == "rating" ? "selected" : "";
            $optType .= "<option value='". $value->type_label ."' ". $selected .">". $value->type ."</option>";
        }

        foreach ($group as $key => $value) {
            $optGroup .= "<option value='". $value->group_id ."'>". $value->group ."</option>";
        }

        foreach ($trainning as $key => $value) {
            $optTrainning .= "<option value='". $value->trainning_id ."'>". $value->trainning_title ."</option>";
        }
        
    	return view('behaviour.create', compact('type','group','trainning','optGroup','optType','optTrainning', 'formulations','trainningId','sessionId','moduleId'));
    }

    public function get_session($id)
    {
        $sessions = Session::where('trainning_id', $id)->pluck('name','session_id');
        return response()->json($sessions);
    }

    public function get_module($id)
    {
        $modules = Module::where('session_id', $id)->pluck('name','module_id');
        return response()->json($modules);
    }

    public function store(Request $request)
    {
    	$params = $request->except('_token');
    	$merge  = array();

    	// return $params; exit;
        $code           = $request->behaviour_code;
        $question       = $request->behaviour_question;
        $correct_answer = $request->behaviour_correct_answer;
        $typeId         = $request->type_id;
        $groupId        = $request->group_id;
        $formulationId  = $request->formulation_id;
        $trainningId    = $request->trainning_id;
        $sessionId      = $request->sessions;
        $moduleId       = $request->modules;
        $isRequired     = $request->behaviour_is_required;
        $isDefault      = $request->behaviour_is_default;
        $ratingStart    = $request->behaviour_rating_start;
        $ratingFinish   = $request->behaviour_rating_finish;
        

            foreach ($code as $key => $value) {
              DB::beginTransaction();
              $answer = $request['behaviour_answer_'.($key+1)];    
              if($answer == ""){
                $answer = $request->behaviour_answer;
              }
              $behaviour_correct_answer = $request->behaviour_correct_answer;
              if(!empty($answer) || isset($answer))
                  $merge['behaviour_answer'] = implode(",", $answer);
              if($behaviour_correct_answer ==2)
                  $merge['behaviour_correct_answer'] = $answer[$key+0];
              if($behaviour_correct_answer ==3)
                  $merge['behaviour_correct_answer'] = $answer[$key+1];
              if($behaviour_correct_answer ==4)
                  $merge['behaviour_correct_answer'] = $answer[$key+2];
              if($behaviour_correct_answer ==5)
                  $merge['behaviour_correct_answer'] = $answer[$key+3];
              if($behaviour_correct_answer ==6)
                  $merge['behaviour_correct_answer'] = $answer[$key+4];
              if($behaviour_correct_answer ==7)
                  $merge['behaviour_correct_answer'] = $answer[$key+5];

              if(!empty($ratingStart) || isset($ratingStart))
                  $merge['behaviour_rating_start'] = !empty($ratingStart[$key]) ? $ratingStart[$key] : null;

              if(!empty($ratingFinish) || isset($ratingFinish))
                  $merge['behaviour_rating_finish'] = !empty($ratingFinish[$key]) ? $ratingFinish[$key] : null;

              $type = Type::where('type_label', $typeId[$key])->first();

              if($type)
                  $merge['type_id'] = $type->type_id;                
              $data = [
                  'behaviour_code' => $code[$key],
                  'behaviour_question' => $question[$key],
                  'behaviour_answer' => null,
                  'group_id' => $groupId[$key],
                  'formulation_id' => $formulationId[$key],
                  'type_id' => $typeId[$key],
                  'trainning_id' => !empty($trainningId[$key]) ? $trainningId[$key] : null,
                  'session_id' => !empty($sessionId[$key]) ? $sessionId[$key] : null,
                  'module_id' => !empty($moduleId[$key]) ? $moduleId[$key] : null,
                  'behaviour_is_required' => $isRequired[$key] == "on" ? 1 : 0,
                  'behaviour_is_default' => $isDefault[$key] == "on" ? 1 : 0,
                  //'user_id' => Auth::user()->id
              ];

              $behaviour = Behaviour::create(
                  array_merge(
                      $data,
                      $merge
                  )
              );
              DB::commit(); 
              $trainings = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->whereNotIn('module_questions.behaviour_id',[''])
              ->groupBy('trainnings.trainning_id');
              if(!empty($data['trainning_id'])){
                $trainings = $trainings->where('trainnings.trainning_id', $data['trainning_id'])->get();
              }else{
                $trainings = $trainings->get();
              }              

              foreach ($trainings as $key => $value) {
                $session = Session::find($value->session_id);
                $periodeArray = array();
                if($session->before == "1"){
                  array_push($periodeArray, 0);
                }elseif ($session->during == "1"){
                  array_push($periodeArray, 1);
                }elseif($session->after == "1"){
                  array_push($periodeArray, 2);
                }
                  foreach ($periodeArray as $value_periode) {          
                    switch ($value_periode) {
                      case 0:
                          $module = "before";
                          break;
                      case 1:
                          $module = "during";
                          break;
                      case 2:
                          $module = "after";
                          break;
                      default:
                          $module = "";
                          break;
                    } 
                    
                    $data = [
                      'module_id' => $value->module_id,
                      'behaviour_id' => $behaviour->behaviour_id,
                      'training_detial_periode' => $module
                    ];          
                    DB::beginTransaction();
                    ModuleQuestion::create($data); 
                    DB::commit();   
                  }
              }              
            }         


    	$request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Behaviour has been added');

        if(!empty($request->save_and_new)){
        return redirect('behaviour/create');
      }else{
        return redirect('behaviour');
      } 
    }

    public function show($id)
    {
        $behaviour = Behaviour::find($id);

        return view('behaviour.detail', compact('behaviour'));
    }

    public function edit($id)
    {
        $behaviour  = Behaviour::find($id);
        $type       = Type::all();
        $group      = Group::where('is_reaction', 1)->get();
        $trainning  = Trainning::where('user_id', Auth::User()->id)->get();
        $session        = Session::where('session_id', $behaviour['session_id'])->first();
        $module         = Module::where('module_id', $behaviour['module_id'])->first();
        $formulations   = Formulation::all();

        return view('behaviour.edit', compact('behaviour','type','group','trainning','session','module','formulations'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->except(['_token', '_method']);    

        $behaviour = Behaviour::findOrFail($id);

        if($behaviour) {
            $merge = array(
                'formulation_id' => $request->formulation_id[0],
                'behaviour_is_default' => $request->behaviour_is_default == "on" ? 1 : 0,
                'behaviour_is_required' => $request->behaviour_is_required == "on" ? 1 : 0,
                'behaviour_answer' => implode(",", (array) $request->behaviour_answer),
                'trainning_id' => $request->behaviour_is_default == "on" ? null : $request->trainning_id,
                'session_id' => $request->behaviour_is_default == "on" ? null : $request->sessions,
                'module_id' => $request->behaviour_is_default == "on" ? null : $request->modules,
                'behaviour_correct_answer' => implode(",",(array) $request->behaviour_correct_answer)
            );            

            $behaviour->update(
                array_merge($data, $merge)
            );

            $module_questions = ModuleQuestion::join('modules', 'modules.module_id', '=', 'module_questions.module_id')
            ->join('sessions','modules.session_id','=','sessions.session_id')              
            ->join('trainnings', 'trainnings.trainning_id', '=', 'sessions.trainning_id');              
            $training_datas_two = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
            ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
            ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')              
            ->groupBy('trainnings.trainning_id');
            if(!empty($merge['trainning_id'])){
              $module_question_deletes = $module_questions->where('module_questions.behaviour_id',$behaviour->behaviour_id)->whereNotIn('trainnings.trainning_id', [$merge['trainning_id']])->get();
              $trainings = $training_datas_two->whereNotIn('module_questions.behaviour_id',[''])->where('trainnings.trainning_id', $merge['trainning_id'])->get();
              
              foreach ($module_question_deletes as $key => $value) {                
                //DB::beginTransaction();
                $module_question = ModuleQuestion::findOrFail($value->module_question_id);
                $module_question->delete();  
                // var_dump($module_question);
                // exit(); 
                //DB::commit();                            
              }
              // var_dump($trainings->count());
              // exit();
              foreach ($trainings as $key => $value) {
                $session = Session::find($value->session_id);
                $periodeArray = array();
                if($session->before == "1"){
                  array_push($periodeArray, 0);
                }elseif ($session->during == "1"){
                  array_push($periodeArray, 1);
                }elseif($session->after == "1"){
                  array_push($periodeArray, 2);
                }
                foreach ($periodeArray as $value_periode) {          
                  switch ($value_periode) {
                    case 0:
                        $module = "before";
                        break;
                    case 1:
                        $module = "during";
                        break;
                    case 2:
                        $module = "after";
                        break;
                    default:
                        $module = "";
                        break;
                  } 
                  
                  $data = [
                    'module_id' => $value->module_id,
                    'behaviour_id' => $behaviour->behaviour_id,
                    'training_detial_periode' => $module
                  ];
                  //DB::beginTransaction();
                  ModuleQuestion::create($data);  
                  //DB::commit();                 
                }
              }
            }else{
              $trainings = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->whereNotIn('module_questions.behaviour_id',[''])
              ->groupBy('trainnings.trainning_id')
              ->get();                

              foreach ($trainings as $key => $value) {
                $session = Session::find($value->session_id);
                $periodeArray = array();
                if($session->before == "1"){
                  array_push($periodeArray, 0);
                }elseif ($session->during == "1"){
                  array_push($periodeArray, 1);
                }elseif($session->after == "1"){
                  array_push($periodeArray, 2);
                }
                  foreach ($periodeArray as $value_periode) {          
                    switch ($value_periode) {
                      case 0:
                          $module = "before";
                          break;
                      case 1:
                          $module = "during";
                          break;
                      case 2:
                          $module = "after";
                          break;
                      default:
                          $module = "";
                          break;
                    } 
                    
                    $data = [
                      'module_id' => $value->module_id,
                      'behaviour_id' => $behaviour->behaviour_id,
                      'training_detial_periode' => $module
                    ];            
                    DB::beginTransaction();
                    ModuleQuestion::create($data); 
                    DB::commit(); 
                  }
              }               
            }   


            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Behaviour has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Behaviour');
        }

      if(!empty($request->save_and_new)){
        return redirect('behaviour/create');
      }else{
        return redirect('behaviour');
      }      
    }

    public function delete(Request $request, $id)
    {
        $model  = Behaviour::findOrFail($id);

        if ($model) {
            $module_question = ModuleQuestion::where('behaviour_id', $model->behaviour_id)->delete();
            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Behaviour has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Behaviour');
        }

       return redirect()->back();
    }

    public function delete_questions(Request $request, $id, $answer){
      $behaviour = Behaviour::findOrFail($id);
      $answers = explode(",",$behaviour->behaviour_answer);
      if($behaviour){        
        foreach(explode(",",$behaviour->behaviour_answer) as $key => $value_answer){          

          if ($answer == $value_answer) {
            unset($answers[$key]);      
          }
        }                
        
        $behaviour->update(array("behaviour_answer" => implode(",",$answers)));
      }
    }

    public function trainning(Request $request, $id)
    {
        $html  = "
            <div class='table-responsive'>
                <table class='table table-striped'>
                    <thead>
                        <th>No</th>
                        <th>Title</th>
                    </thead>
                    <tbody>
        ";

        $model = Trainning::join('sessions','trainnings.trainning_id','=','sessions.trainning_id')
              ->join('modules', 'modules.session_id', '=', 'sessions.session_id')
              ->join('module_questions', 'module_questions.module_id', '=', 'modules.module_id')
              ->where('module_questions.behaviour_id', $id)
              ->groupBy('trainnings.trainning_id')
              ->get();

        foreach ($model as $key => $value) {
            $html .= "
                <tr>
                    <td>". ($key+1) ."</td>
                    <td>". $value->trainning_title ."</td>
                </tr>
            ";
        }

        $html .= "
                    </tbody>
                </table>
            </div>
        ";

        return $html;
    }

    public function import(Request $request) 
    {
       try{$import =  Excel::Import(new BehaviourImport, $request->file('import'));
       
        
        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Learning has been imported');

        return redirect('/behaviour');
      }catch(\Illuminate\Database\QueryException $e){
        return redirect()->back()->with('error','type atau group keliru');
      }
    }

    private function rules()
    {
        return [
            "wordCategories" => ['required'],
        ];
    }
}