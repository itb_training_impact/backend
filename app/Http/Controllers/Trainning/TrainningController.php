<?php

namespace App\Http\Controllers\Trainning;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\Type;
use App\Model\Master\Group;
use App\Model\Master\Boss;
use App\Model\Master\Partner;
use App\Model\Master\Subordinate;
use App\Model\Trainning\Company;
use App\Model\Trainning\Session;
use App\Model\Trainning\Module;
use App\Model\Trainning\ModuleQuestion as Questions;
use App\Model\Trainning\Speaker;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\TrainningDetail;
use App\Model\Trainning\TrainningSubmit as Submit;
use App\Model\Trainning\ModuleBehaviourFormulations as Formulas;
use App\Model\Reaction\Reaction;
use App\Model\Reaction\Calculation as ReactionCalculation;
use App\Model\Learning\Learning;
use App\Model\Learning\Calculation as LearningCalculation;
use App\Model\Behaviour\Story;
use App\Model\Behaviour\Behaviour;
use App\Model\Master\WordCategory;
use App\Model\Master\Formulation;
use App\Model\Master\FormulationGroup;
use App\Model\Performance\Performance;
use App\Model\Roti\Roti;
use App\User;
use Carbon\Carbon;
use App\Imports\TrainningImport;
use App\Exports\SubmitUsersExport;
use App\Exports\MultipleSheet;
use App\Exports\MultipleSheetAll;
use App\Exports\All_Question as QuestionDownload;
use App\Exports\All_Learning as LearningDownload;
use App\Exports\All_Reaction as ReactionDownload;
use App\Exports\All_Behaviour360 as BehaviourDownload;
use App\Exports\ALL_Performance;
use App\Exports\BehaviourUsersExport;
use App\Exports\SubmitIntermediaryExport;
use App\Exports\QuestionAnswerExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Auth;
use DB;
use PDF;
use Zipper;

class TrainningController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        set_time_limit(300);
    }

    /**
     * Show the application reaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->level_id != 1){
            $trainning  = Trainning::where('user_id', Auth::user()->id)->orderBy('trainning_id', 'DESC')->get();
        }else{
            $trainning  = Trainning::orderBy('trainning_id', 'DESC')->get();
        }
        return view('trainning.index', compact('trainning'));
    }

//delete question trainning
    public function delete_question_reaction(Request $request, $id)
    {
      $model  = Reaction::findOrFail($id);
        if ($model) {
            $module_question = Questions::where('reaction_id', $model->reaction_id)->delete();
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Reaction Question has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Reaction');
        }

        return redirect()->back();
    }
    public function delete_question_learning(Request $request, $id)
    {
      $model  = Learning::findOrFail($id);
        if ($model) {
            $module_question = Questions::where('learning_id', $model->learning_id)->delete();
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Learning Question has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Learning');
        }

        return redirect()->back();
    }
    public function delete_question_behaviour(Request $request, $id)
    {
      $model  = Behaviour::findOrFail($id);
        if ($model) {
            $module_question = Questions::where('behaviour_id', $model->behaviour_id)->delete();
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Behaviour has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Behaviour');
        }

        return redirect()->back();
    }
    public function delete_question_performance(Request $request, $id)
    {
      $model  = Performance::findOrFail($id);
        if ($model) {
            $module_question = Questions::where('performance_id', $model->performance_id)->delete();
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Performance has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Reaction');
        }

        return redirect()->back();
    }
    public function delete_question_roti(Request $request, $id)
    {
      $model  = Roti::findOrFail($id);
        if ($model) {
            $module_question = Questions::where('roti_id', $model->roti_id)->delete();
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'roti has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete roti');
        }

        return redirect()->back();
    }
//end delete
    /**
     * Show the list of config Training.
     *
     * @return \Ilsluminate\Http\Response
     */
    public function checkPerformance(Request $request,$trainningID)
    {
      // $trainning = Trainning::find($trainningID);

      // $modulesPerformanceRoti = Module::join("sessions", "sessions.session_id", "=", "modules.session_id")->join("trainnings", "trainnings.trainning_id", "=", "sessions.trainning_id")->where("trainnings.trainning_id", $trainningID)->where('modules.status',1)->where('sessions.name','=','Session Performance dan Roti-'.$trainning->trainning_title)->get();



      // foreach ($modulesPerformanceRoti as $key => $value) {
      //     $moduleID = $value->module_id;
      //     $field = 'before';
      //     dispatch(new \App\Jobs\SendEmailPerfo($moduleID, $field));      
      //  }
     
    }
    public function checkDefault(Request $request,$trainningID)
    {
   
        $dt = Carbon::now('Asia/Jakarta')->toDateTimeString();
       $modulesDefault = Module::join("sessions", "sessions.session_id", "=", "modules.session_id")->where('modules.status',1)->where('sessions.during',1)->where('modules.start_date','=',$dt)->get();

         if (count($modulesDefault)>0) {
        foreach ($modulesDefault as $key => $value) {
                $moduleID = $value->module_id;
               
          }
        $model      = Module::find($moduleID);
        $session    = Session::where('session_id',$model->session_id)->first();
        $training   = Trainning::where('trainning_id',$session->trainning_id)->first();
        $questions  = Questions::WhereIn('module_id',$model)->get();
        $behavi     = false;

        $participant    = explode(',',$training['participant_id']);
        $users          = User::whereIn('id',$participant)->get();
        $boss           = Boss::whereIn('user_id',$participant)->get();
        $partners       = Partner::whereIn('user_id',$participant)->get();
        $subordinates   = Subordinate::whereIn('user_id',$participant)->get();
        $field = 'during';
        foreach($questions as $question){
          
        }
        if($question->behaviour_id != null){
           return "behaviour true";
        }
        return "not behaviour";
       }
    }
    public function getForm(Request $request,$moduleID)
    {
        $module     =   Module::find($moduleID); 
        $link = url('trainning/jadwal/'.$moduleID);
         $link_token = csrf_token();
        $html  = '
         <form action="'.$link.'" method="POST">
             <input type="hidden" name="_token" id="csrf-token" value="'.$link_token.'" />
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title modal-title">Jadwalkan module</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Jadwalkan</h3>
                        </div>
                     <div class="block-content">
                        <div class="form-group">
                            <div class="form-material">
                                <input class="form-control" type="datetime-local" name="start_date">
                                </div>
                            </div>
                     </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success btn-modal-primary">Submit</button>
                </div>
            </div>
        </form>
        ';
        return $html;
    }
    public function setSchadule(Request $request,$moduleID)
    {
        $moduleSet = Module::find($moduleID);
         $moduleSet->update($request->all());
        if ($moduleSet) {
              $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Trainning has been added');
            return redirect()->back();
        }
      
    }
    public function submitAll()
    {
             $trainningID = \Request::get('trainningID');
       

        $trainning = Trainning::find($trainningID);
        $participant_id = explode(",", $trainning->participant_id);
        $users = User::whereIn('id', $participant_id)->get();

        $isBehaviourExist = false;
        foreach ($users as $user) {
            $stories = $user->storyByTrainning($trainningID);
            if (count($stories)) {
                $isBehaviourExist = true;
                break;
            }
        }
       

        $reaction = Submit::filterCalculation($trainningID, null, null, null, null, 'reaction')->first();
        $learning = Submit::filterCalculation($trainningID, null, null, null, null, 'learning')->first();
        $performances = Submit::filterDimension($trainningID, null, null, null, 0, 'performance')->get();
        
        $wordCategories = WordCategory::where('id', '!=', 1)->where('code', '!=', 'SC01')->get();
        $formulas = Formulation::all();

        echo view('trainning.submit_all_trainning', compact('learning', 'reaction', 'performances', 'users', 'wordCategories', 'formulas', 'isBehaviourExist'));
    }
    public function getPerformance(Request $request,$trainningID,$sessionID,$moduleID)
    {
        $html  = "
          <div class=''>
            <table class='table table-vcenter table-stripped table-bordered ' style='background-color: transparent'>
              <thead>
                <th>No</th>
                <th>Group</th>
                <th>Code</th>
                <th>Question</th>
                <th>Type</th>
                <th width='17%'>Action</th>
              </thead>
              <tbody>";
        $performanceDetails = Performance::whereHas('ModuleQuestions',function($query) use ($moduleID)
        {
          $query->where('module_id', $moduleID)->whereNotNull('performance_id');
        });
        $performanceDetails = $performanceDetails->get();
        foreach ($performanceDetails as $key => $value) {
          $key += 1;
          $html .= "
            <tr>
              <td>". $key ."</td>
              <td>". $value->group->group ."</td>
              <td>". $value->performance_code ."</td>
              <td>". $value->performance_question ."</td>
              <td>". $value->type->type ."</td>";
          $link_detail = url('performance/detail/'. $value->performance_id);
          $link_edit = url('performance/edit/'. $value->performance_id);
          $link_delete = url('trainning/delete_question_performance/'. $value->performance_id);
          $html .='
            <td>
              <a href="'.$link_detail.'" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-search"></i></a>
               <a href="'. $link_delete .'" class="btn btn-sm btn-danger btn-xs btn-delete">
                    <i class="fa fa-trash"></i>
                </a>
              ';
            if($value->performance_is_default == 0){
              $html .='    
                <a href="'. $link_edit .'" class="btn btn-sm btn-warning" target="_blank"><i class="fa fa-pencil"></i></a>
               ';
            }

          $html .='
            </td>';
            $html .="</tr>";
        }

        $html .= "
            </tbody>
          </table>
        </div>";
        $trainning  =   Trainning::find($trainningID);
        $session    =   Session::find($sessionID);
        $module     =   Module::find($moduleID);
        $link_create = url('performance/createCustomPerformance/'.$trainning->trainning_id.'/'.$session->session_id. '/' .$module->module_id);
        $html .= '   
                    <div class="modal-footer">
                        <a href="'.$link_create.'" class="btn btn-primary" target="_blank">Create</a>
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    </div>
        ';
        return $html;
    } 
    
     public function getRoti(Request $request,$trainningID,$sessionID,$moduleID)
    {
        $html  = "
          <div class=''>
            <table class='table table-vcenter table-stripped table-bordered ' style='background-color: transparent'>
              <thead>
                <th>No</th>
                <th>Group</th>
                <th>Code</th>
                <th>Question</th>
                <th>Type</th>
                <th width='17%'>Action</th>
              </thead>
              <tbody>";
        $rotidetails = Roti::whereHas('ModuleQuestions',function($query) use ($moduleID)
        {
          $query->where('module_id', $moduleID)->whereNotNull('roti_id');
        });
        $rotidetails = $rotidetails->get();
        foreach ($rotidetails as $key => $value) {
          $key += 1;
          $html .= "
            <tr>
              <td>". $key ."</td>
              <td>". $value->group->group ."</td>
              <td>". $value->roti_code ."</td>
              <td>". $value->roti_question ."</td>
              <td>". $value->type->type ."</td>";
          $link_detail = url('roti/detail/'. $value->roti_id);
          $link_edit = url('roti/edit/'. $value->roti_id);
          $link_delete = url('trainning/delete_question_roti/'. $value->roti_id);
          $html .='
            <td>
              <a href="'.$link_detail.'" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-search"></i></a>
              <a href="'. $link_delete .'" class="btn btn-sm btn-danger btn-xs btn-delete">
                  <i class="fa fa-trash"></i>
              </a>
              ';
          if($value->roti_is_default == 0){
            $html .='  
              <a href="'. $link_edit .'" class="btn btn-sm btn-warning" target="_blank"><i class="fa fa-pencil"></i></a>
              ';
            }
          $html .='
            </td>';
            $html .="</tr>";
        }

        $html .= "
            </tbody>
          </table>
        </div>";
        $trainning  =   Trainning::find($trainningID);
        $session    =   Session::find($sessionID);
        $module     =   Module::find($moduleID);
        $link_create = url('roti/createCustomRoti/'.$trainning->trainning_id.'/'.$session->session_id. '/' .$module->module_id);
        $html .= '   
                    <div class="modal-footer">
                        <a href="'.$link_create.'" class="btn btn-primary" target="_blank">Create</a>
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    </div>
        ';
        return $html;
    } 
    public function getBehaviour(Request $request,$trainningID,$sessionID,$moduleID){

        $html  = "
          <div class=''>
            <table class='table table-vcenter table-stripped table-bordered ' style='background-color: transparent'>
              <thead>
                <th>No</th>
                <th>Group</th>
                <th>Code</th>
                <th>Question</th>
                <th>Type</th>
                <th width='17%'>Action</th>
              </thead>
              <tbody>";
        $behaviourDetails = Behaviour::whereHas('ModuleQuestions',function($query) use ($moduleID)
        {
          $query->where('module_id', $moduleID)->whereNotNull('behaviour_id');
        });
        $behaviourDetails = $behaviourDetails->get();
        foreach ($behaviourDetails as $key => $value) {
          $key += 1;
          $html .= "
            <tr>
              <td>". $key ."</td>
              <td>". $value->group->group ."</td>
              <td>". $value->behaviour_code ."</td>
              <td>". $value->behaviour_question ."</td>
              <td>". $value->type->type ."</td>";
          $link_detail = url('behaviour/detail/'. $value->behaviour_id);
          $link_edit = url('behaviour/edit/'. $value->behaviour_id);
          $link_delete = url('trainning/delete_question_behaviour/'. $value->behaviour_id);
          $html .='
            <td>
              <a href="'.$link_detail.'" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-search"></i></a>
               <a href="'. $link_delete .'" class="btn btn-sm btn-danger btn-xs btn-delete">
                  <i class="fa fa-trash"></i>
              </a>
              ';
          if($value->behaviour_is_default == 0){
            $html .='
              <a href="'. $link_edit .'" class="btn btn-sm btn-warning" target="_blank"><i class="fa fa-pencil"></i></a>
             ';
          }
          $html .='
            </td>';
            $html .="</tr>";
        }

        $html .= "
            </tbody>
          </table>
        </div>";
        $trainning  =   Trainning::find($trainningID);
        $session    =   Session::find($sessionID);
        $module     =   Module::find($moduleID);
        $link_create = url('behaviour/createCustomBehaviour/'.$trainning->trainning_id.'/'.$session->session_id. '/' .$module->module_id);
        $html .= '   
                    <div class="modal-footer">
                        <a href="'.$link_create.'" class="btn btn-primary" target="_blank">Create</a>
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    </div>
        ';
        return $html;
    }
  
 public function getLearning(Request $request,$trainningID,$sessionID,$moduleID){

        $html  = "
            <div class=''>
                <table class='table table-vcenter table-stripped table-bordered' style='background-color: transparent'>
                    <thead>
                        <th>No</th>
                        <th>Group</th>
                        <th>Code</th>
                        <th>Question</th>
                        <th>Type</th>
                        <th width='17%'>Action</th>
                    </thead>
                    <tbody>
        ";
        $learningDetails = Learning::whereHas('ModuleQuestions',function($query) use ($moduleID)
         {
         $query->where('module_id', $moduleID)->whereNotNull('learning_id');
         });
        $learningDetails = $learningDetails->get();
       foreach ($learningDetails as $key => $value) {
            $key += 1;
            $html .= "
                <tr>
                  <td>". $key ."</td>
                    <td>". $value->group->group ."</td>
                    <td>". $value->learning_code ."</td>
                    <td>". $value->learning_question ."</td>
                    <td>". $value->type->type ."</td>";
            $link_detail = url('learning/detail/'. $value->learning_id);
            $link_edit = url('learning/edit/'. $value->learning_id);
            $link_delete = url('trainning/delete_question_learning/'. $value->learning_id);
            $html .='
              <td>
                <a href="'.$link_detail.'" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-search"></i></a>
                <a href="'. $link_delete .'" class="btn btn-sm btn-danger btn-xs btn-delete">
                    <i class="fa fa-trash"></i>
                </a>
                ';
            if($value->learning_is_default == 0){
              $html .='
                <a href="'. $link_edit .'" class="btn btn-sm btn-warning" target="_blank"><i class="fa fa-pencil"></i></a>
                ';
            }
            $html .='
              </td>';
              $html .="</tr>";
        }

        $html .= "
                    </tbody>
                </table>
            </div>
        ";
        $trainning  =   Trainning::find($trainningID);
        $session    =   Session::find($sessionID);
        $module     =   Module::find($moduleID);
        $link_create = url('learning/createCustomLearning/'.$trainning->trainning_id.'/'.$session->session_id. '/' .$module->module_id);
        $html .= '   
                    <div class="modal-footer">

                        <a href="'.$link_create.'" class="btn btn-primary" target="_blank">Create</a>
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    </div>
        ';
        return $html;
    }
    public function getReaction(Request $request,$trainningID,$sessionID,$moduleID){

        $html  = "
            <div class=''>
                <table class='table table-vcenter table-stripped table-bordered' style='background-color: transparent'>
                    <thead>
                      <th>No</th>
                        <th>Group</th>
                        <th>Code</th>
                        <th>Question</th>
                        <th>Type</th>
                        <th width='17%'>Action</th>
                    </thead>
                    <tbody>
        ";
        $reactionDetails = Reaction::whereHas('ModuleQuestions',function($query) use ($moduleID)
         {
         $query->where('module_id', $moduleID)->whereNotNull('reaction_id');
         });
        $reactionDetails = $reactionDetails->get();
       foreach ($reactionDetails as $key => $value) {
            $key += 1;
            $html .= "
                <tr>
                  <td>". $key ."</td>
                    <td>". $value->group->group ."</td>
                    <td>". $value->reaction_code ."</td>
                    <td>". $value->reaction_question ."</td>
                    <td>". $value->type->type ."</td>";
            $link_detail = url('reaction/detail/'. $value->reaction_id);
            $link_edit = url('reaction/edit/'. $value->reaction_id);
            $link_delete = url('trainning/delete_question_reaction/'. $value->reaction_id);
            $html .='
              <td>
                <a href="'.$link_detail.'" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-search"></i></a>
                <a href="'. $link_delete .'" class="btn btn-sm btn-danger btn-xs btn-delete">
                    <i class="fa fa-trash"></i>
                </a>
                ';
            if($value->reaction_is_default == 0){
              $html .='
                <a href="'. $link_edit .'" class="btn btn-sm btn-warning" target="_blank"><i class="fa fa-pencil"></i></a>
                ';
            }
            $html .='
              </td>';
              $html .="</tr>";
        }

        $html .= "
                    </tbody>
                </table>
            </div>
        ";
        $trainning  =   Trainning::find($trainningID);
        $session    =   Session::find($sessionID);
        $module     =   Module::find($moduleID);
        $link_create = url('reaction/createCustom/'.$trainning->trainning_id.'/'.$session->session_id. '/' .$module->module_id);
        $html .= '   
                    <div class="modal-footer">
                        <a href="'.$link_create.'" class="btn btn-primary" target="_blank">Create</a>
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    </div>
        ';
        return $html;
    }
    public function ActiveBefore(Request $request,$sessionID)
    {
        $moduleBefore = Module::whereHas('session',function($query) use ($sessionID)
        {
        $query->where('session_id', $sessionID);
        });
        $moduleUpdate = [
            'status' => 1,
        ];
        $moduleBefore = $moduleBefore->update($moduleUpdate);
        
        if ($moduleBefore) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Session has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Session');
        }

        return redirect()->back();
    }
    public function ActiveDuring(Request $request,$sessionID)
    {
        $moduleDuring = Module::whereHas('session',function($query) use ($sessionID)
        {
        $query->where('session_id', $sessionID)->where('during',1);
        });
        $moduleUpdate = [
            'status' => 1,
        ];
        $moduleDuring = $moduleDuring->update($moduleUpdate);
        
        if ($moduleDuring) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Session has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Session');
        }

        return redirect()->back();
    }
    public function ActiveAfter(Request $request,$sessionID)
    {
        $moduleAfter = Module::whereHas('session',function($query) use ($sessionID)
        {
        $query->where('session_id', $sessionID)->where('after',1);
        });
        $moduleUpdate = [
            'status' => 1,
        ];
        $moduleAfter = $moduleAfter->update($moduleUpdate);
        
        if ($moduleAfter) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Session has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Session');
        }

        return redirect()->back();
    }

    public function activeModule(Request $request,$id,$field)
    {
        $model = Module::find($id);
        $model = [
            'status' => 1,
        ];

        if ($model) {

            $update = Module::where('module_id',$id)->update($model);       
            dispatch(new \App\Jobs\SendEmailUser($id, $field));
           
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Session has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Session');
        }

        return redirect()->back();
    }
    
    public function activeModuleBehaviour(Request $request,$id,$field)
    {
        $model = Module::find($id);
        $model = [
            'status' => 1,
        ];
        
        if ($model) {
            
            $update = Module::where('module_id',$id)->update($model);
            dispatch(new \App\Jobs\SendEmailBehavi($id, $field));

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Session has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Session');
        }

        return redirect()->back();
    }

    public function activeModulePerformance(Request $request,$id,$field)
    {
        $model = Module::find($id);
        $model = [
            'status' => 1,
        ];
        
        if ($model) {
            
            $update = Module::where('module_id',$id)->update($model);
            dispatch(new \App\Jobs\SendEmailPerfo($id, $field));

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Session has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Session');
        }

        return redirect()->back();
    }

    public function nonActive(Request $request,$id)
    {
        $model = Module::find($id);
        $model = [
            'status' => 0,
        ];
        if ($model) {

           $update = Module::where('module_id',$id)->update($model);

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Session has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Session');
        }

         return redirect()->back();
    }
    public function config(Request $request, $id)
    {

        $trainning = Trainning::find($id);

        if (!$trainning) {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Training not found');
            return redirect('trainning');
        }

        // count before
        $before = TrainningDetail::where(['trainning_id' => $id, 'trainning_detail_periode' => 0])->count();
        $during = TrainningDetail::where(['trainning_id' => $id, 'trainning_detail_periode' => 1])->count();
        $after = TrainningDetail::where(['trainning_id' => $id, 'trainning_detail_periode' => 2])->count();

        $position = [
            [
                'position' => 'before',
                'participants' => $before
            ], [
                'position' => 'during',
                'participants' => $during
            ], [
                'position' => 'after',
                'participants' => $after
            ]
        ];
        
        return view('trainning.config', compact('trainning', 'position'));
    }

    public function update_config(Request $request, $id, $field, $status)
    {
        $trainning      = Trainning::findOrFail($id);        
             
        $session        = Session::where('trainning_id',$trainning->trainning_id)->get(['session_id']);
        $module         = Module::whereIn('session_id',$session)->get(['module_id']);
        $questions      = Questions::WhereIn('module_id',$module)->get();

        $participant    = explode(',',$trainning['participant_id']);
        $users          = User::whereIn('id',$participant)->get();
        $boss           = Boss::whereIn('user_id',$participant)->get();
        $partners       = Partner::whereIn('user_id',$participant)->get();
        $subordinates   = Subordinate::whereIn('user_id',$participant)->get();
        $clear = false;
        
        if ($trainning) {
            if ($trainning->$field == 2) {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Failed to edit trainning config');
            } else {
                $trainning->$field = $status;
                $trainning->update();

                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Trainning config has been edited');
                if($status == 1){
                    foreach($questions as $question){
                        if($question->behaviour_id != null){
                            $clear = true;
                        }
                    }     
                    if($clear == true){
                        foreach($boss as $boss){
                            $user = User::where('id',$boss->user_id)->get();
                            foreach ($user as $user){
                                Mail::send('email.notifyTrainingBoss', ['user'=>$user,'trainning'=>$trainning,'boss'=>$boss,'field'=>$field], function($message) use ($boss)
                                {    
                                    $message->to($boss->boss_email)->subject('NOTIFICATION START TRAINING');    
                                });
                            }
                        }
                        foreach($partners as $partner){
                            $user = User::where('id',$partner->user_id)->get();
                            foreach ($user as $user){
                                Mail::send('email.notifyTrainingPartner', ['user'=>$user,'trainning'=>$trainning,'partner'=>$partner,'field'=>$field], function($message) use ($partner)
                                {    
                                    $message->to($partner->partner_email)->subject('NOTIFICATION START TRAINING');    
                                });
                            }
                        }
                        foreach($subordinates as $subordinate){
                            $user = User::where('id',$subordinate->user_id)->get();
                            foreach ($user as $user){
                                Mail::send('email.notifyTrainingSubordinate', ['user'=>$user,'trainning'=>$trainning,'subordinate'=>$subordinate,'field'=>$field], function($message) use ($subordinate)
                                {    
                                    $message->to($subordinate->subordinate_email)->subject('NOTIFICATION START TRAINING');    
                                });
                            }
                        }
                    }                                                          
                    foreach ($users as $user){
                        Mail::send('email.notifyTraining', ['user'=>$user,'trainning'=>$trainning,'field'=>$field], function($message) use ($user)
                        {    
                            $message->to($user->email)->subject('NOTIFICATION START TRAINING');    
                        });
                    }
                }
            }
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit trainning config');
        }
        return redirect('trainning/config/' . $id);
    }

    public function create()
    {
        $reaction  = Reaction::all();
        $learning   = Learning::all();
       if(Auth::user()->level_id != 1){
            $companys = Company::where('user_id', Auth::user()->id)->get();
        }else{
            $companys = Company::all();
        }
        if(Auth::user()->level_id != 1){
          $speakers = Speaker::where('user_id', Auth::user()->id)->get();
        }else{
          $speakers       = Speaker::all();
        }
        if(Auth::user()->level_id != 1){
          $users       = User::where('user_id',Auth::user()->id)->where('level_id', [4])->get();
        }else{
          $users       = User::where('level_id', [4])->get();
        }
        $formulas = Formulation::where('status',1)->get();
        
        return view('trainning.create', compact('reaction','learning','users','companys','speakers','formulas'));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

            $dataTrainning = $request->only(['trainning_title', 'trainning_description','company_id']);
            if ($request->HasFile('images')) {
                $destination_path = "/public/speaker/";
                $image = $request->file('images');
                $image_name = $image->getClientOriginalName();
                $path = $request->file('images')->storeAs($destination_path,$image_name);
                $dataTrainning['images'] = $image_name;
            }
            $code = [
                'code'=>str_random(7),
            ];
            $date = [
                'start_date'=>Carbon::parse($request->start_date)->format('Y-m-d'),
                'end_date'=>Carbon::parse($request->end_date)->format('Y-m-d'),
            ];
            $speaker = [
                'speaker_id' => Speaker::all()->get('speaker_id'),
                'speaker_id' => (!empty($request->speaker_id) ? implode(",",$request->speaker_id) : null )//implode(",",$request->speaker_id)
            ];
            $merge = [
                'user_id' => Auth::user()->id,
                'participant_id' => (!empty($request->participant_id) ? implode(",",$request->participant_id) : null )
            ];
            
            $trainning = Trainning::insertGetId(
                array_merge(
                    $dataTrainning,
                    $merge,
                    $date,
                    $speaker,
                    $code
                )
            );
            if ($request->module_roti_performance ? : []) {
                $sessionRoti                    = new Session;
                $sessionRoti->trainning_id      = $trainning; 
                $sessionRoti->name              = $request->session_roti_performance.'-'.$request->trainning_title;
                $sessionRoti->speaker_id        = 1;
                $sessionRoti->before            = 1;
                $sessionRoti->save();

                $sessionID = $sessionRoti->session_id;

                $roti                           = new Module;
                $roti->session_id               = $sessionID;
                $roti->name                     = $request->module_roti_performance.'-'.$request->trainning_title;
                $roti->description      = $request->description_roti;
                $roti->code_module              = str_random(7);
                $roti->save();
                
                $moduleId   = $roti->module_id;
               $moduleMerge = [
                        'module_id' => $moduleId,
                ];

                $questionperformance =  Performance::whereRaw('(performance_is_default = 1)')->get();
                 foreach ($questionperformance as $questioner) {
                                Questions::create(
                                    array_merge(
                                        $moduleMerge,
                                        [
                                            'performance_id' => $questioner->performance_id
                                        ]
                                    )
                                );
                            }

                $questionRoti =  Roti::whereRaw('(roti_is_default = 1)')->get();
                 foreach ($questionRoti as $question) {
                                Questions::create(
                                    array_merge(
                                        $moduleMerge,
                                        [
                                            'roti_id' => $question->roti_id
                                        ]
                                    )
                                );
                            }

            }
           // if ($request->module_performance ? : []) {
           //      $sessionperformance                     = new Session;
           //      $sessionperformance->trainning_id       = $trainning; 
           //      $sessionperformance->name               = $request->session_performance.'-'.$request->trainning_title;
           //       $sessionperformance->speaker_id        = 1;
           //      $sessionperformance->before             = 1;
           //      $sessionperformance->save();

           //      $sessionIDS = $sessionperformance->session_id;

           //      $performance                            = new Module;
           //      $performance->session_id                = $sessionIDS;
           //      $performance->name                      = $request->module_performance.'-'.$request->trainning_title;
           //      $performance->description               = $request->description_performance;
           //      $performance->code_module               = str_random(7);
           //      $performance->save();
                
           //      $moduleIds = $performance->module_id;
           //     $moduleMerges = [
           //              'module_id' => $moduleIds,
           //      ]; 
           //      $questionperformance =  Performance::whereRaw('(performance_is_default = 1)')->get();
           //       foreach ($questionperformance as $question) {
           //                      Questions::create(
           //                          array_merge(
           //                              $moduleMerges,
           //                              [
           //                                  'performance_id' => $question->performance_id
           //                              ]
           //                          )
           //                      );
           //                  }

           //  }

            // return $request;
            // Bagian Sesi dan Modul
            for($i= 0; $i < count($request->box); $i++){
                // Bagian Sesi
                $dataSession[] = [
                    'trainning_id' => $trainning,
                    'name' => $request->session_title[$i], 
                    'description' => $request->session_description[$i]
                ];

                if($request->section[$i] == "before"){
                    $section[] = [
                        'before' => "1",
                        'during' => null,
                        'after' => null
                    ];
                }elseif($request->section[$i] == "during"){
                    $section[] = [
                        'before' => null,
                        'during' => "1",
                        'after' => null
                    ];
                }elseif($request->section[$i] == "after"){
                    $section[] = [
                        'before' => null,
                        'during' => null,
                        'after' => "1"
                    ];
                }
                
                $session[] = Session::insertGetId(
                    array_merge(
                        $dataSession[$i],
                        $section[$i],
                        $speaker
                    )
                );                
                
                // // Bagian Modul
                $total = array_sum(array_map("count", $request->module_title));
                for($j= 0; $j < count($request->module_title[$i]); $j++){
                        
                    $dataModule[$i][] = [
                        'session_id' => $session[$i],
                        'name' => $request->module_title[$i][$j], 
                        'description' => $request->module_description[$i][$j]
                    ];                        

                    $sessions[$i] = Session::find($session[$i]);

                    $periodeArray[$i] = array();
                        if($sessions[$i]->before == "1"){
                            array_push($periodeArray[$i], 0);
                        }elseif ($sessions[$i]->during == "1"){
                            array_push($periodeArray[$i], 1);
                        }elseif($sessions[$i]->after == "1"){
                            array_push($periodeArray[$i], 2);
                        }

                    $codemodule[$i][] = [
                        'code_module'=>str_random(7),
                    ];
                    
                    $model[$i][] = Module::insertGetId(
                            array_merge(
                                $dataModule[$i][$j],
                                $codemodule[$i][$j]
                            )
                        );                        
                } 
                
                for($k = 0; $k < count($request->module_title[$i]); $k++){
                    $modulesId[$i][] = [
                        'module_id' => $model[$i][$k]
                    ]; 
                    
                    $formulation_ids[$i][] = (!Empty($request->formulation_id[$i][$k])) ? $request->formulation_id[$i][$k] : [];
                    foreach ($formulation_ids[$i][$k] as $value) {
                        Formulas::create(
                            array_merge(
                                $modulesId[$i][$k],
                                ['formulation_id'=>$value]
                            )
                        );
                    }

                    $modules[] = [
                        'module_id' => $model[$i][$k]
                    ];    
                    foreach ($periodeArray[$i] as $value) { 
                        switch ($value) {
                            case 0:
                                $modules[$i] = 0;
                                break;
                            case 1:
                                $modules[$i] = 1;
                                break;
                            case 2:
                                $modules[$i] = 2;
                                break;
                            default:
                                $modules[$i] = "";
                                break;
                        }
                    
                        $moduleDetail[$i][] = [
                            'module_id' => $model[$i][$k],
                            'trainning_detail_periode' => $modules[$i]
                        ];
                        
                        $isModule[$i][] = $request->module[$i][$k];
                        
                        if (isset($isModule[$i][$k]) || !empty($isModule[$i][$k])) {
                            //if set reaction              
                            if (in_array("is_reaction", $isModule[$i][$k])) {
                                $reactions = Reaction::whereRaw('(reaction_is_default = 1 OR module_id = '.$model[$i][$k].')')->get();
                                foreach ($reactions as $reaction) {
                                    Questions::create(
                                            array_merge(
                                            $moduleDetail[$i][$k],
                                            [
                                                    'reaction_id' => $reaction->reaction_id
                                            ]
                                        )
                                    );
                                }
                                
                            }
                            //if set learning
                            if (in_array("is_learning", $isModule[$i][$k])) {
                                $learnings = Learning::whereRaw('(learning_is_default = 1 OR module_id = '.$model[$i][$k].')')->get();
                                foreach ($learnings as $learning) {
                                    Questions::create(
                                        array_merge(
                                            $moduleDetail[$i][$k],
                                            [
                                                'learning_id' => $learning->learning_id
                                            ]
                                        )
                                    );
                                }
                            }
            
                            // if set behaviour
                            if (in_array("is_behaviour", $isModule[$i][$k])) {
                                $behaviours = Behaviour::whereRaw('(behaviour_is_default = 1 OR module_id = '.$model[$i][$k].')');
                                if(!empty($formulation_ids[$i][$k])){
                                    $behaviours = $behaviours->whereIn("formulation_id", $formulation_ids[$i][$k]);
                                }
                                foreach ($behaviours->get() as $behaviour) {
                                    Questions::create(
                                        array_merge(
                                            $moduleDetail[$i][$k],
                                            [                             
                                                'behaviour_id' => $behaviour->behaviour_id
                                            ]
                                        )
                                    ); 
                                }                  
                            }
                        }
                    }
                }                
            }
            // $trainningId = $trainning->trainning_id;
            // $periode     = $request->trainning_detail_periode;

            // foreach ($periode as $value) {

            //     switch ($value) {
            //         case 0:
            //             $module = "before";
            //             break;

            //         case 1:
            //             $module = "during";
            //             break;

            //         case 2:
            //             $module = "after";
            //             break;
                    
            //         default:
            //             $module = "";
            //             break;
            //     }

            //     $dataDetail  = [
            //         'trainning_id' => $trainningId,
            //         'trainning_detail_periode' => $value
            //     ];

            //     $isModule = $request['module_'.$module];

            //     if(isset($isModule) || !empty($isModule)) {

            //         //if set reaction
            //         if(in_array("is_reaction", $isModule)) {

            //             $reactions = Reaction::all();

            //             foreach ($reactions as $reaction) {
            //                 TrainningDetail::create(
            //                     array_merge(
            //                         $dataDetail,
            //                         [
            //                             'reaction_id' => $reaction->reaction_id
            //                         ]
            //                     )
            //                 );
            //             }
            //         }

            //         //if set learning
            //         if(in_array("is_learning", $isModule)) {

            //             $learnings = Learning::all();

            //             foreach ($learnings as $learning) {
            //                 TrainningDetail::create(
            //                     array_merge(
            //                         $dataDetail,
            //                         [
            //                             'learning_id' => $learning->learning_id
            //                         ]
            //                     )
            //                 );
            //             }
            //         }
            //     }
            // }

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Trainning has been added');

        return redirect('trainning');
    }

    public function edit($id)
    {        
        if(Auth::user()->level_id != 1){
        $users       = User::where('user_id',Auth::user()->id)->where('level_id', [4])->get();
        }else{
          $users       = User::where('level_id', [4])->get();
        }
            $trainning = Trainning::find($id);
            $learnings = Learning::all();
            $reactions = Reaction::all();
          if(Auth::user()->level_id != 1){
            $companys = Company::where('user_id', Auth::user()->id)->get();
        }else{
            $companys = Company::all();
        }
         if(Auth::user()->level_id != 1){
            $speakers = Speaker::where('user_id', Auth::user()->id)->get();
        }else{
        $speakers       = Speaker::all();
        }
            $trainningDetail = TrainningDetail::all();
            $sessions = Session::where('trainning_id', $id)->where('name', '!=', 'Session Performance dan Roti-'.$trainning->trainning_title)->get();
            for($s = 0; $s < count($sessions); $s++){
                $sessionID = $sessions[$s]; 
                $modules[$s]    = Module::where('session_id', $sessionID->session_id)->get();
            }
            for($s = 0; $s < count($sessions); $s++){
                for($m = 0; $m < count($modules[$s]); $m++){
                    $question[$s][$m] = Questions::where('module_id', $modules[$s][$m]->module_id)->get();
                }
            }
            for($s = 0; $s < count($sessions); $s++){
                for($m = 0; $m < count($modules[$s]); $m++){
                    $reaction[$s][$m] = $question[$s][$m]->whereNotIn('reaction_id', '');
                    $learning[$s][$m] = $question[$s][$m]->whereNotIn('learning_id', '');
                    $behaviour[$s][$m] = $question[$s][$m]->whereNotIn('behaviour_id', '');  
                }
            }
            
            $formulas   = Formulation::where('status',1)->get();

        return view('trainning.edit', compact('trainning','users','learnings','reactions','trainningDetail','speakers','companys','sessions','modules','formulas','question','reaction','learning','behaviour'));   
    }

    public function update(Request $request, $id)
    {
        $dataTraining = Trainning::find($id);
        $dataTraining = [
            'trainning_title' => $request->trainning_title,
            'trainning_description' => $request->trainning_description,
            'company_id' => $request->company_id
        ];

        $date = [
            'start_date'=>Carbon::parse($request->start_date)->format('Y-m-d'),
            'end_date'=>Carbon::parse($request->end_date)->format('Y-m-d')
        ];
        $participant = [
            'user_id' => Auth::user()->id,
            'participant_id' => (!empty($request->participant_id) ? implode(",",$request->participant_id) : null )
        ];
        $speaker = [
            'speaker_id' => Speaker::all()->get('speaker_id'),
            'speaker_id' => (!empty($request->speaker_id) ? implode(",",$request->speaker_id) : null )
        ];

        if ($dataTraining) {

            $update = Session::where('name', '=', 'Session Performance dan Roti-'.Trainning::where('trainning_id', $id)->value('trainning_title'))
                    ->update(array(
                        'name' => 'Session Performance dan Roti-'.$request->trainning_title
                    ));

            $update = Module::where('name', '=', 'Module Performance dan Roti-'.Trainning::where('trainning_id', $id)->value('trainning_title'))
                    ->update(array(
                        'name' => 'Module Performance dan Roti-'.$request->trainning_title
                    ));

            $update = Trainning::where('trainning_id', $id)
                        ->update(array_merge(
                            $dataTraining,
                            $date,
                            $speaker,
                            $participant)
                        );

            for($i= 0; $i < count($request->session_id); $i++){

                $dataSession[] = [
                    'name' => $request->session_title[$i], 
                    'description' => $request->session_description[$i]
                ];
                if($request->section[$i] == "before"){
                    $section[] = [
                        'before' => "1",
                        'during' => null,
                        'after' => null
                    ];
                }elseif($request->section[$i] == "during"){
                    $section[] = [
                        'before' => null,
                        'during' => "1",
                        'after' => null
                    ];
                }elseif($request->section[$i] == "after"){
                    $section[] = [
                        'before' => null,
                        'during' => null,
                        'after' => "1"
                    ];
                }
                $sessionID[] = Session::where('session_id',$request->session_id[$i])->get();
                if($sessionID[$i]->isNotEmpty()){
                    $session[] = $request->session_id[$i];
                    Session::where('session_id',$request->session_id[$i])
                    ->update(array_merge(
                        $dataSession[$i],
                        $section[$i],
                        $speaker)
                    );
                }else{
                    $session[] = Session::insertGetId(
                        array_merge(
                            ['trainning_id' => $id],
                            $dataSession[$i],
                            $section[$i],
                            $speaker
                        )
                    ); 
                }

                for($j= 0; $j < count($request->module_title[$i]); $j++){
                    $dataModule[$i][] = [
                        'session_id' => $session[$i],
                        'name' => $request->module_title[$i][$j], 
                        'description' => $request->module_description[$i][$j]
                    ];

                    $sessions[$i] = Session::find($session[$i]);

                    $periodeArray[$i] = array();
                        if($sessions[$i]->before == "1"){
                            array_push($periodeArray[$i], 0);
                        }elseif ($sessions[$i]->during == "1"){
                            array_push($periodeArray[$i], 1);
                        }elseif($sessions[$i]->after == "1"){
                            array_push($periodeArray[$i], 2);
                        }

                    $codemodule[$i][] = [
                        'code_module'=>str_random(7),
                    ];
                    $moduleID[$i][] = Module::where('module_id',$request->module_id[$i][$j])->get();
                    if($moduleID[$i][$j]->isNotEmpty()){
                        $model[$i][] = $request->module_id[$i][$j];
                            Module::where('module_id',$request->module_id[$i])
                            ->update(array_merge(
                                    $dataModule[$i][$j]
                                )
                            );
                    }elseif($moduleID[$i][$j]->isEmpty() || []){
                        $model[$i][] = Module::insertGetId(
                            array_merge(
                                $dataModule[$i][$j],
                                $codemodule[$i][$j]
                            )
                        );
                    }  
                }
                
                for($k = 0; $k < count($request->module_title[$i]); $k++){
                    $modulesId[$i][] = [
                        'module_id' => $model[$i][$k]
                    ]; 
                    
                    $formulation_ids[$i][] = (!Empty($request->formulation_id[$i][$k])) ? $request->formulation_id[$i][$k] : [];
                    foreach ($formulation_ids[$i][$k] as $value) {
                        Formulas::create(
                            array_merge(
                                $modulesId[$i][$k],
                                ['formulation_id'=>$value]
                            )
                        );
                    }

                    $modules[] = [
                        'module_id' => $model[$i][$k]
                    ];    
                    foreach ($periodeArray[$i] as $value) { 
                        switch ($value) {
                            case 0:
                                $modules[$i] = 0;
                                break;
                            case 1:
                                $modules[$i] = 1;
                                break;
                            case 2:
                                $modules[$i] = 2;
                                break;
                            default:
                                $modules[$i] = "";
                                break;
                        }
                    
                        $moduleDetail[$i][] = [
                            'module_id' => $model[$i][$k],
                            'trainning_detail_periode' => $modules[$i]
                        ];
                        
                        $isModule[$i][] = $request->module[$i][$k];
                        // return $isModule[$i][$k];
                        if (isset($isModule[$i][$k]) || !empty($isModule[$i][$k])) {
                            //if set reaction              
                            if (in_array("is_reaction", $isModule[$i][$k])) {
                                $reactions = Reaction::whereRaw('(reaction_is_default = 1 OR module_id = '.$model[$i][$k].')')->get();
                                foreach ($reactions as $reaction) {
                                    Questions::create(
                                            array_merge(
                                            $moduleDetail[$i][$k],
                                            [
                                                    'reaction_id' => $reaction->reaction_id
                                            ]
                                        )
                                    );
                                }
                                
                            }
                            //if set learning
                            if (in_array("is_learning", $isModule[$i][$k])) {
                                $learnings = Learning::whereRaw('(learning_is_default = 1 OR module_id = '.$model[$i][$k].')')->get();
                                foreach ($learnings as $learning) {
                                    Questions::create(
                                        array_merge(
                                            $moduleDetail[$i][$k],
                                            [
                                                'learning_id' => $learning->learning_id
                                            ]
                                        )
                                    );
                                }
                            }
            
                            // if set behaviour
                            if (in_array("is_behaviour", $isModule[$i][$k])) {
                                $behaviours = Behaviour::whereRaw('(behaviour_is_default = 1 OR module_id = '.$model[$i][$k].')');
                                if(!empty($formulation_ids[$i][$k])){
                                    $behaviours = $behaviours->whereIn("formulation_id", $formulation_ids[$i][$k]);
                                }
                                foreach ($behaviours->get() as $behaviour) {
                                    Questions::create(
                                        array_merge(
                                            $moduleDetail[$i][$k],
                                            [                             
                                                'behaviour_id' => $behaviour->behaviour_id
                                            ]
                                        )
                                    ); 
                                }                  
                            }
                        }
                    }
                } 
            }            

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Training has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Training, Training not found');
        }

        return redirect('trainning');
    }

    public function delete_questions(Request $request, $ids, $id, $type){
        $training = Trainning::find($ids);
        $module = Module::findOrFail($id);
        if($module){
            switch ($type) {
            case 'reaction':
                $module_questions = Questions::where('module_id', $module->module_id)->whereNotIn('reaction_id', [''])->delete();
            break;
            case 'learning':
                $module_questions = Questions::where('module_id', $module->module_id)->whereNotIn('learning_id', [''])->delete();
            break;
            case 'behaviour':
                $module_questions = Questions::where('module_id', $module->module_id)->whereNotIn('behaviour_id', [''])->delete();
            break;
            }
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Module question has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Module question');
        }
        return redirect('/trainning/edit/'.$training->trainning_id);
    }

    public function show(Request $request,$id)
    {
        $trainning      = Trainning::find($id);
        $participant_id = explode(",",$trainning->participant_id);
        $users          = User::whereIn('id', $participant_id)->get();
        $sessions       = Session::where('trainning_id', $trainning->trainning_id)->where('name','!=','Session Performance dan Roti-'.$trainning->trainning_title)->get();
        //before
        $moduleBefore = Module::whereHas('session',function($query) use ($id)
         {
         $query->where('trainning_id', $id)->where('before',1);
         });
        $moduleBefore = $moduleBefore->get();

        //during
       $moduleDuring = Module::whereHas('session',function($query) use ($id)
         {
         $query->where('trainning_id', $id)->where('during',1);
         });
        $moduleDuring = $moduleDuring->get();

        //after
         $moduleAfter = Module::whereHas('session',function($query) use ($id)
        {
        $query->where('trainning_id', $id)->where('after',1);
        });
        $moduleAfter = $moduleAfter->get();

        $sessionRotis = Session::where('trainning_id',$id)->where('name','=','Session Performance dan Roti-'.$trainning->trainning_title)->get();
        
       
        $rotis = Module::whereHas('session',function($query) use ($trainning)
         {
         $query->where('name','=','Session Performance dan Roti-'.$trainning->trainning_title);
         });
        $rotis = $rotis->get();
        
      
       
        //return $performances;exit();
           
           
            
            
            $learningDetails = Learning::where('trainning_id',$id)->orWhere('learning_is_default',1)->get();
            $reactionDetails = Reaction::where('trainning_id',$id)->orWhere('reaction_is_default',1)->get();

        

        $speaker_id     = explode(",",$trainning->speaker_id);
        $speakers       = Speaker::whereIn('speaker_id', $speaker_id)->get();


       return view('trainning.detail', compact('trainning', 'users', 'speakers','moduleDuring','moduleAfter','moduleBefore','sessions','learningDetails','reactionDetails','rotis','sessionRotis'));
       
    }

    public function result($id, $trainningID)
    {
        $participant    = User::find($id);
        $trainning      = Trainning::find($trainningID);
        $sessions       = Session::where('trainning_id', $trainningID); 
        
        switch (\Request::get('active')) {
          case 'before':
          default:
            $sessions = $sessions->where('before', 1)->get();
            $data = [];
            break;
          case 'during':
            $sessions = $sessions->where('during', 1)->get();
            $data = [];
            break;
          case 'after':
            $sessions = $sessions->where('after', 1)->get();
            $data = [];
            break;
          case 'result':
            $sessions = $sessions->get();
            $data = [];
            break;
        }

        // $beforeCalculation = ($reactionBefore && $learningBefore) ? ($reactionBefore->response + $learningBefore->response) / 2 : 0;

        // $duringCalculation = ($reactionDuring && $learningDuring) ? ($reactionDuring->response + $learningDuring->response) / 2 : 0; 

        // $afterCalculation = ($reactionAfter && $learningAfter) ? ($reactionAfter->response + $learningAfter->response) / 2 : 0;

        return view('trainning.result', compact('participant', 'trainningID', 'sessions', 'trainning', 'data'));
    }

    public function getModules($id)
    {
        $modules = Module::where('session_id', $id)->get();
        echo json_encode($modules);
    }

    public function getSubmits()
    {
        $trainningID = \Request::get('trainningID');
        $sessionID = \Request::get('sessionID');
        $moduleID = \Request::get('moduleID');

        $trainning = Trainning::find($trainningID);
        $participant_id = explode(",", $trainning->participant_id);
        $users = User::whereIn('id', $participant_id)->get();

        $isBehaviourExist = false;
        foreach ($users as $user) {
            $stories = $user->storyByTrainning($trainningID, $sessionID, $moduleID);
            if (count($stories)) {
                $isBehaviourExist = true;
                break;
            }
        }

        $reaction = Submit::filterCalculation($trainningID, $sessionID, $moduleID, null, null, 'reaction')->first();
        $learning = Submit::filterCalculation($trainningID, $sessionID, $moduleID, null, null, 'learning')->first();        

        $wordCategories = WordCategory::where('id', '!=', 1)->where('code', '!=', 'SC01')->get();
        $formulas = Formulation::all();

        echo view('trainning.submits', compact('learning', 'reaction', 'users', 'wordCategories', 'formulas', 'isBehaviourExist'));
    }

    public function getSubmit()
    {
        $id = \Request::get('userID');
        $trainningID = \Request::get('trainningID');
        $sessionID = \Request::get('sessionID');
        $moduleID = \Request::get('moduleID');
        $active = \Request::get('active');

        $participant    = User::find($id);
        $trainning      = Trainning::find($trainningID);
        $checkmodule = Module::where('module_id',$moduleID)->get();
        $wordCategories = $formulas = null;

        $isBehaviourExist = false;
        $formulationGroups = [];

        switch ($active) {
            case 'before':
            default:
                $submit_periode = 0;
                $story = $participant->storyByTrainning($trainningID, $sessionID, $moduleID, 0, ['id', 'desc']);
                break;
            case 'during':
                $submit_periode = 1;
                $story = $participant->storyByTrainning($trainningID, $sessionID, $moduleID, 1, ['id', 'desc']);
                break;
            case 'after':
                $submit_periode = 2;
                $story = $participant->storyByTrainning($trainningID, $sessionID, $moduleID, 2, ['id', 'desc']);
                break;
            case 'result':
                // Behaviour
                $submit_periode = null;
                $wordCategories = WordCategory::where('id', '!=', 1)->where('code', '!=', 'SC01')->get();
                $formulas = Formulation::all();
                $story = $participant->storyByTrainning($trainningID);
                break;
        }

        $reaction = Submit::filterCalculation($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'reaction')->first();
        $learning = Submit::filterCalculation($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'learning')->first();
        $behaviour = Submit::filterCalculation($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'behaviour')->first();
        $reaction_group = Submit::filterDimension($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'reaction')->get();
        $learning_group = Submit::filterDimension($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'learning')->get();
        $behaviour_group = Submit::filterDimension($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'behaviour')->get();
         $performances = Submit::filterDimension($trainningID, null, null, null, 0, 'performance')->get();

        $behaviour_bosses = Submit::filterBossesPartners($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'bosses')->get();
        $behaviour_partners = Submit::filterBossesPartners($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'partners')->get();
        $behaviour_subordinates = Submit::filterBossesPartners($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'subordinates')->get();
        $submit = Submit::where('trainning_id', '=', $trainningID)->where('user_id', '=', $id)->where('session_id', '=', $sessionID)->where('module_id','=',$moduleID)->where(['submit_periode' => $submit_periode])->get(); 

        if ($active == 'result') {
            $reaction = Submit::filterCalculation($trainningID, null, null, $id, null, 'reaction')->first();
            $learning = Submit::filterCalculation($trainningID, null, null, $id, null, 'learning')->first();
            $behaviour = Submit::filterCalculation($trainningID, null, null, $id, null, 'behaviour')->first();
            $reaction_group = Submit::filterDimension($trainningID, null,  null, $id, null, 'reaction')->get();
            $learning_group = Submit::filterDimension($trainningID, null,  null, $id, null, 'learning')->get();
            $behaviour_group = Submit::filterDimension($trainningID, null,  null, $id, null, 'behaviour')->get();
            $submit = null;
        }

        if (count($story)) {
            $isBehaviourExist = true;
            $formulationGroups = FormulationGroup::orderBy('name', 'asc');
            $formulationGroups->whereHas('formulation', function ($query) {
                $query->where('status', 1);
            });

            $formulationGroups = $formulationGroups->get();
        }

        $data = [
            'reaction' => $reaction,
            'learning' => $learning,
            'behaviour' => $behaviour,            
            'reaction_group' => $reaction_group,
            'learning_group' => $learning_group,
            'behaviour_group' => $behaviour_group,
            'stories' => $story
        ];

        echo view('trainning.submit', compact('data', 'participant', 'trainningID', 'sessionID', 'moduleID', 'trainning', 'submit', 'wordCategories', 'formulas', 'isBehaviourExist', 'behaviour_bosses', 'behaviour_partners', 'behaviour_subordinates', 'formulationGroups','performances','checkmodule'));
    }

    public function download_pdf($id, $trainningID, $sessionID, $moduleID, $active){
        $participant    = User::find($id);
        $trainning      = Trainning::find($trainningID);
        $sessions       = Session::find($sessionID);
        $modules        = Module::find($moduleID);

        $wordCategories = $formulas = null;

        $isBehaviourExist = false;
        $formulationGroups = [];

        switch ($active) {
            case 'before':
            default:
                $submit_periode = 0;
                break;
            case 'during':
                $submit_periode = 1;
                break;
            case 'after':
                $submit_periode = 2;
                break;
        }

        $reaction = Submit::filterCalculation($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'reaction')->first();
        $learning = Submit::filterCalculation($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'learning')->first();
        $behaviour = Submit::filterCalculation($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'behaviour')->first();
        $reaction_group = Submit::filterDimension($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'reaction')->get();
        $learning_group = Submit::filterDimension($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'learning')->get();
        $behaviour_group = Submit::filterDimension($trainningID, $sessionID, $moduleID, $id, $submit_periode, 'behaviour')->get();

        if ($active == 'result') {
            $reaction = Submit::filterCalculation($trainningID, null, null, $id, null, 'reaction')->first();
            $learning = Submit::filterCalculation($trainningID, null, null, $id, null, 'learning')->first();
            $behaviour = Submit::filterCalculation($trainningID, null, null, $id, null, 'behaviour')->first();
            $reaction_group = Submit::filterDimension($trainningID, null,  null, $id, null, 'reaction')->get();
            $learning_group = Submit::filterDimension($trainningID, null,  null, $id, null, 'learning')->get();
            $behaviour_group = Submit::filterDimension($trainningID, null,  null, $id, null, 'behaviour')->get();
        }

        $data = [
            'reaction' => $reaction,
            'learning' => $learning,
            'behaviour' => $behaviour,
            'reaction_group' => $reaction_group,
            'learning_group' => $learning_group,
            'behaviour_group' => $behaviour_group
        ];        

        $pdf = PDF::loadView('email.resultBefore',['data'=>$data,'participant'=>$participant,'trainning'=>$trainning,'session'=>$sessions,'module'=>$modules]);

        return $pdf->download('Result Training.pdf');
    }

    public function delete(Request $request, $id)
    {
        $model  = Trainning::findOrFail($id);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Trainning has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Trainning');
        }

        return redirect('trainning');
    }

    public function export($id, $trainningID, $sessionID, $moduleID, $active, $section)
    {
        $trainning = Trainning::find($trainningID);
        $participant = User::find($id);
        if($section == 'reaction'){
            return Excel::download(new SubmitUsersExport($id, $trainningID, $sessionID, $moduleID, $active, $section), $participant->name . '-' . $trainning->trainning_title . '-Reaction.xlsx');
        }elseif($section == 'learning'){
            return Excel::download(new SubmitUsersExport($id, $trainningID, $sessionID, $moduleID, $active, $section), $participant->name . '-' . $trainning->trainning_title . '-Learning.xlsx');
        }elseif($section == 'behaviour'){
            return Excel::download(new SubmitUsersExport($id, $trainningID, $sessionID, $moduleID, $active, $section), $participant->name . '-' . $trainning->trainning_title . '-Behaviour.xlsx');
        }elseif($section == 'boss'){
            return Excel::download(new SubmitIntermediaryExport($id, $trainningID, $sessionID, $moduleID, $active, $section), $participant->name . '-' . $trainning->trainning_title . '-Boss Submit.xlsx');
        }elseif($section == 'partner'){
            return Excel::download(new SubmitIntermediaryExport($id, $trainningID, $sessionID, $moduleID, $active, $section), $participant->name . '-' . $trainning->trainning_title . '-Partner Submit.xlsx');
        }else{
            return Excel::download(new SubmitIntermediaryExport($id, $trainningID, $sessionID, $moduleID, $active, $section), $participant->name . '-' . $trainning->trainning_title . '-Subordinate Submit.xlsx');
        };
    }

    public function export_qna($id, $trainningID, $sessionID, $moduleID, $active, $level)
    {
        $trainning = Trainning::find($trainningID);
        $participant = User::find($id);
        if($level == 'participant'){
            return Excel::download(new QuestionAnswerExport($id, $trainningID, $sessionID, $moduleID, $active, $level), $participant->name . '-' . $trainning->trainning_title . '-Question And Answer.xlsx');
        }elseif($level == 'boss'){
            return Excel::download(new QuestionAnswerExport($id, $trainningID, $sessionID, $moduleID, $active, $level), $participant->name . '-' . $trainning->trainning_title . '-Question And Answer Boss.xlsx');
        }elseif($level == 'partner'){
            return Excel::download(new QuestionAnswerExport($id, $trainningID, $sessionID, $moduleID, $active, $level), $participant->name . '-' . $trainning->trainning_title . '-Question And Answer Partner.xlsx');
        }else{
            return Excel::download(new QuestionAnswerExport($id, $trainningID, $sessionID, $moduleID, $active, $level), $participant->name . '-' . $trainning->trainning_title . '-Question And Answer Subordinate.xlsx');
        }
    }
    public function import(Request $request) 
    {
        try{$import = Excel::Import(new TrainningImport, $request->file('import'));

  
        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'trainning has been imported');

        return redirect('/trainning');
        } catch (\Illuminate\Database\QueryException $e) {
          return redirect()->back()->with('msg', 'roti has been imported');
        
      }
    }
    public function ResultAll($userID, $trainningID) {
        $trainning = Trainning::find($trainningID);
        $participant = User::find($userID);
        return Excel::download(new MultipleSheet($userID, $trainningID),$participant->name . '-' . $trainning->trainning_title . '-Question And Answer Subordinate.xlsx');
    }
    // public function Result_trainning($trainingID, $sessionID, $moduleID, $periode){
    //     $training = Trainning::find($trainingID);        
    //     Excel::queue(new MultipleSheetAll($trainingID, $sessionID, $moduleID, $periode), '/public/exports/Pelatihan '.$training->trainning_title.'/Hasil Pelatihan Seluruh Peserta-'.$training->trainning_title.'.xlsx');
    //     return back()->with('start', 'mulai');
    // }
    public function Result_trainning($trainingID, $sessionID, $moduleID, $periode){
        $training = Trainning::find($trainingID);        
        
        return Excel::download(new MultipleSheetAll($trainingID, $sessionID, $moduleID, $periode), 'Hasil Pelatihan Seluruh Peserta-'.$training->trainning_title.'.xlsx');
    }
    public function checkDownload(){
        $training = Trainning::find(\Request::get('trainningID'));
        if(Storage::disk('public')->has('/exports/Pelatihan '.$training->trainning_title.'/Hasil Pelatihan Seluruh Peserta-'.$training->trainning_title.'.xlsx')){
            // $files = glob(storage_path('app/public/exports/Pelatihan '.$training->trainning_title.'/*'));
            // $zip   = Zipper::make(storage_path('app/public/exports/Hasil Pelatihan Seluruh Peserta-'.$training->trainning_title.'.zip'))->add($files)->close();
            
            // $path = storage_path('app/public/exports/Hasil Pelatihan Seluruh Peserta-'.$training->trainning_title.'.zip');
            // File::cleanDirectory(storage_path('app\public\exports\Pelatihan '.$title.''));

            return response()->json([
                'status' => 1
            ]);
        }
    }
    public function downloadAll($title){
        $path = storage_path('app/public/exports/Pelatihan '.$title.'/Hasil Pelatihan Seluruh Peserta-'.$title.'.xlsx');        
        return response()->download($path)->deleteFileAfterSend(true);
    }
    public function downloadRoti($trainningID)
    {
        $training = Trainning::find($trainningID);        
        
        return Excel::download(new ALL_Performance($trainningID), 'Hasil Pelatihan Performance Dan Roti Seluruh Peserta-'.$training->trainning_title.'.xlsx');
    }
}
