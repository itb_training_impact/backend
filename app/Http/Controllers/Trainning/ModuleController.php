<?php

namespace App\Http\Controllers\Trainning;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Model\Learning\Learning;
use App\Model\Reaction\Reaction;
use App\Model\Behaviour\Behaviour;
use DB;
use Auth;
use App\Model\Trainning\Module;
use App\Model\Trainning\ModuleQuestion as Questions;
use App\Model\Trainning\Session;
use App\Model\Master\Formulation;
use App\Model\Trainning\ModuleBehaviourFormulations as Formulas;
class ModuleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->limit = 25;
        $this->middleware('auth');
    }    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->level_id != 1)            
          $modules = Module::join("sessions", "sessions.session_id", "=", "modules.session_id")->join("trainnings", "trainnings.trainning_id", "=", "sessions.trainning_id")->where("trainnings.user_id", Auth::User()->id)->orderBy('modules.updated_at', 'desc');
        else{            
          $modules = Module::orderBy('updated_at', 'desc');
        }          

        if($request->has('name') && !empty($request->name))
            $modules   = $modules->where('name', 'like', '%' . $request->name . '%');

        $total = $modules->count();

        $modules = $modules->paginate($this->limit);
        $modules->appends($request->all());
       
        return view('module.index', compact('modules', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $sessions = Session::join('trainnings', 'trainnings.trainning_id', '=', 'sessions.trainning_id')->where('trainnings.user_id', Auth::User()->id)->get();
        $formulas = Formulation::where('status',1)->get();
        return view('module.create',compact('sessions','formulas'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules());
        $session = Session::find($request->session_id);
        $periodeArray = array();
        if($session->before == "1"){
          array_push($periodeArray, 0);
        }elseif ($session->during == "1"){
          array_push($periodeArray, 1);
        }elseif($session->after == "1"){
          array_push($periodeArray, 2);
        }                
        $code = [
            'code_module'=>str_random(7),
        ];
        DB::beginTransaction();
        $model = Module::create(
          array_merge(
            $request->except('_token'),
            $code
          )
        );

          $moduleId = $model->module_id;
        //$periode = $request->trainning_detail_periode;
          

          $module = [
            'module_id' =>$moduleId 
          ];  
         $formulation_ids = $request->formulation_id ?: []; 

        foreach ($formulation_ids as $value) {
         Formulas::create(array_merge($module,['formulation_id'=>$value]));
        }

      

        foreach ($periodeArray as $value) {          
          switch ($value) {
              case 0:
                  $module = 0;
                  break;
              case 1:
                  $module = 1;
                  break;
              case 2:
                  $module = 2;
                  break;
              default:
                  $module = "";
                  break;
          }

            $moduleDetail = [
                'module_id' => $moduleId,
                'trainning_detail_periode' => $module
            ];         

            $isModule = $request->module;

            if (isset($isModule) || !empty($isModule)) {
                //if set reaction              
                if (in_array("is_reaction", $isModule)) {
                    $reactions = Reaction::whereRaw('(reaction_is_default = 1 OR module_id = '.$moduleId.')')->get();
                    foreach ($reactions as $reaction) {
                        Questions::create(
                             array_merge(
                                $moduleDetail,
                                [
                                     'reaction_id' => $reaction->reaction_id
                                ]
                            )
                        );
                    }
                }

                //if set learning
                if (in_array("is_learning", $isModule)) {
                    $learnings = Learning::whereRaw('(learning_is_default = 1 OR module_id = '.$moduleId.')')->get();
                    foreach ($learnings as $learning) {
                        Questions::create(
                            array_merge(
                                $moduleDetail,
                                [
                                    'learning_id' => $learning->learning_id
                                ]
                            )
                        );
                    }
                }

                //if set behaviour
                if (in_array("is_behaviour", $isModule)) {
                    $behaviours = Behaviour::whereRaw('(behaviour_is_default = 1 OR module_id = '.$moduleId.')');
                    if(!empty($formulation_ids)){
                      $behaviours = $behaviours->whereIn("formulation_id", $formulation_ids);
                    }                    
                    //if($behaviours->isEmpty()){
                        // Questions::create(
                        //     array_merge(
                        //         $moduleDetail,
                        //         [                             
                        //             'behaviour_id' => 1
                        //         ]
                        //     )
                        // );
                    // }else{
                        foreach ($behaviours->get() as $behaviour) {
                          Questions::create(
                            array_merge(
                              $moduleDetail,
                              [                             
                                'behaviour_id' => $behaviour->behaviour_id
                              ]
                            )
                          ); 
                        }  
                    // }                    
                }
            }
        }

        DB::commit();
        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Trainning has been added');
        return redirect('/module');
   //   return $request;exit();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    public function edit($id)
    {
    	$module = Module::find($id);
      $sessions = Session::join('trainnings', 'trainnings.trainning_id', '=', 'sessions.trainning_id')->where('trainnings.user_id', Auth::User()->id)->get();
      $reaction = ($module->question->whereNotIn('reaction_id', ''));
      $learning = ($module->question->whereNotIn('learning_id', ''));
      $behaviour = ($module->question->whereNotIn('behaviour_id', ''));
       $formulas = Formulation::where('status',1)->get();
      return view('/module/edit', compact('module','sessions', 'reaction', 'learning', 'behaviour','formulas'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$this->validate($request, $this->rules());
        
    	$model = Module::find($id);

        if ($model) {            
            $session = Session::find($model->session_id);            
            $periodeArray = array();
            if($session->before == "1"){
              array_push($periodeArray, 0);
            }elseif ($session->during == "1"){
              array_push($periodeArray, 1);
            }elseif($session->after == "1"){
              array_push($periodeArray, 2);
            }                
            DB::beginTransaction();
            $model->update($request->except('_token'));
            // var_dump($model);           
            // exit();
            $moduleId = $model->module_id;
            $module = [
              'module_id' =>$moduleId 
            ];  
            $formulation_ids = $request->formulation_id ?: []; 
            foreach ($formulation_ids as $value) {
              Formulas::create(array_merge($module,['formulation_id'=>$value]));
            }
            //$periode = $request->trainning_detail_periode;

            foreach ($periodeArray as $value) {          
              switch ($value) {
                  case 0:
                      $module = 0;
                      break;
                  case 1:
                      $module = 1;
                      break;
                  case 2:
                      $module = 2;
                      break;
                  default:
                      $module = "";
                      break;
              }          
                $moduleDetail = [
                    'module_id' => $moduleId,
                    'trainning_detail_periode' => $module
                ];
                
                $isModule = $request->module;

                if (isset($isModule) || !empty($isModule)) {
                    //if set reaction              
                    if (in_array("is_reaction", $isModule)) {
                        $reactions = Reaction::whereRaw('(reaction_is_default = 1 OR module_id = '.$moduleId.')')->get();                      
                        foreach ($reactions as $reaction) {
                          
                            Questions::create(
                                 array_merge(
                                    $moduleDetail,
                                    [
                                         'reaction_id' => $reaction->reaction_id
                                    ]
                                )
                            );
                        }
                    }

                    //if set learning
                    if (in_array("is_learning", $isModule)) {
                        $learnings = Learning::whereRaw('(learning_is_default = 1 OR module_id = '.$moduleId.')')->get();
                        foreach ($learnings as $learning) {
                            Questions::create(
                                array_merge(
                                    $moduleDetail,
                                    [
                                        'learning_id' => $learning->learning_id
                                    ]
                                )
                            );
                        }
                    }
                    //if set behaviour
                    if (in_array("is_behaviour", $isModule)) {
                      $behaviours = Behaviour::whereRaw('(behaviour_is_default = 1 OR module_id = '.$moduleId.')');
                      if(!empty($formulation_ids)){
                       $behaviours = $behaviours->whereIn("formulation_id", $formulation_ids);
                      }
                      // if($behaviours->isEmpty()){
                      //     Questions::create(
                      //         array_merge(
                      //             $moduleDetail,
                      //             [                             
                      //                 'behaviour_id' => 1
                      //             ]
                      //         )
                      //     );
                      // }else{
                          foreach ($behaviours->get() as $behaviour) {
                            Questions::create(
                              array_merge(
                                $moduleDetail,
                                [                             
                                  'behaviour_id' => $behaviour->behaviour_id
                                ]
                              )
                            ); 
                          }  
                      //}                    
                    }
                }
            }        
            DB::commit();
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Module has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Module');
        }

    	return redirect('/module');
    }

    public function delete(Request $request, $id)
    {
        $model	= Module::findOrFail($id);

        if ($model) {
            $module_questions = Questions::where('module_id', $model->module_id)->delete();            
            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Module has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Module');
        }

    	return redirect('/module');
    }

    public function delete_questions(Request $request, $id, $type){
      $module = Module::findOrFail($id);
      if($module){
        switch ($type) {
          case 'reaction':
            $module_questions = Questions::where('module_id', $module->module_id)->whereNotIn('reaction_id', [''])->delete();
          break;
          case 'learning':
            $module_questions = Questions::where('module_id', $module->module_id)->whereNotIn('learning_id', [''])->delete();
          break;
          case 'behaviour':
            $module_questions = Questions::where('module_id', $module->module_id)->whereNotIn('behaviour_id', [''])->delete();
          break;
        }
        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Module question has been deleted');
      } else {
        $request->session()->flash('status', 'err');
        $request->session()->flash('msg', 'Failed to delete Module question');
      }
      return redirect('/module/edit/'.$module->module_id);
    }

    private function rules()
    {
        return [
            "name" => ['required'],
            "description" => ['required'],
        ];
    }

    public function show($id)
    {
        
        $modules = Module::find($id);
        $session_id = explode(",",$modules->session_id);
        $sessions          = Session::whereIn('session_id', $session_id)->get();

        
        return view('/module/detail', compact('modules','sessions'));
    }  

   
}