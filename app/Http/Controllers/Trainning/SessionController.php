<?php

namespace App\Http\Controllers\Trainning;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

use App\Model\Trainning\Session;
use App\Model\Trainning\Trainning;
use App\Model\Trainning\Speaker;
use DB;
use Auth;
class SessionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->limit = 25;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->level_id != 1)
            $sessions  = Session::join("trainnings", "trainnings.trainning_id", "=", "sessions.trainning_id")->where("trainnings.user_id", Auth::User()->id)->orderBy('sessions.created_at', 'DESC')->get();
        else{            
            $sessions  = Session::orderBy('created_at', 'DESC')->get();
        }       
       
        return view('session.index', compact('sessions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $speakers = Speaker::all();
        $trainnings      = Trainning::where('user_id', Auth::User()->id)->get();
        return view('session.create',compact('speakers','trainnings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // $this->validate($request, $this->rules());
         DB::beginTransaction();
           $session = $request->only(['name', 'description','trainning_id','before','during','after']);
          $merge = [
                'speaker_id' => Speaker::all()->get('speaker_id'),
                'speaker_id' =>implode(",",$request->speaker_id)
            ];

            
        $trainning = Session::create(
                array_merge(
                    $session,
                    $merge
                )
        );
        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Trainning has been added');

        return redirect('/session');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    public function edit($id)
    {
    	$sessions = Session::find($id);
        $speakers = Speaker::all();
        $trainnings      = Trainning::where('user_id', Auth::User()->id)->get();
        return view('/session/edit', compact('sessions','speakers','trainnings'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$this->validate($request, $this->rules());
        
    	$model = Session::find($id);
        $model = [
            'name' => $request->name,
            'description' =>$request->description,
            'trainning_id' => $request->trainning_id
        ];
         $speaker = [
            'speaker_id' => Speaker::all()->get('speaker_id'),
                'speaker_id' =>implode(",",$request->speaker_id)
         ];
        if ($model) {

           $update = Session::where('session_id',$id)->update(array_merge($model,$speaker));

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Session has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Session');
        }

    	return redirect('/session');
    }

    public function delete(Request $request, $id)
    {
        $model	= Session::findOrFail($id);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Session has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Session');
        }

    	return redirect('/session');
    }

    

    private function rules()
    {
        return [
            "name" => ['required'],
            "description" => ['required'],
        ];
    }

    public function show($id)
    {   
        
        $sessions = Session::find($id);
         $speaker_id = explode(",",$sessions->speaker_id);
        $speakers          = Speaker::whereIn('speaker_id', $speaker_id)->get();
        return view('/session/detail', compact('sessions','speakers'));
    }  

   
}