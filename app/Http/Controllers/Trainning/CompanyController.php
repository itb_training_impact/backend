<?php

namespace App\Http\Controllers\Trainning;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Trainning\Company;
use Illuminate\Validation\Rule;
use Auth;

class CompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->limit = 25;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->level_id != 1)
            $companys = Company::where('user_id', Auth::user()->id)->orderBy('updated_at', 'desc');
        else{
            $companys = Company::orderBy('updated_at', 'desc');
        }

        if($request->has('name') && !empty($request->name))
            $companys   = $companys->where('name', 'like', '%' . $request->name . '%');

        $total = $companys->count();

        $companys = $companys->paginate($this->limit);
        $companys->appends($request->all());
       
        return view('company.index', compact('companys', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // $this->validate($request, $this->rules());

       $company                         = new Company;

      
        $company->name              = $request->name;
        $company->description       = $request->description;
        $company->user_id           = Auth::user()->id;
      
           if ($request->HasFile('images')) {
                $destination_path = "/public/company/";
                $image = $request->file('images');
                $image_name = $image->getClientOriginalName();
                $path = $request->file('images')->storeAs($destination_path,$image_name);
                $company['images'] = $image_name;
            }

        $insert = $company->save();
        
        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Company has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Company');
        }

        return redirect('/company');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    public function edit($id)
    {
    	$companys = Company::find($id);

        return view('/company/edit', compact('companys'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$this->validate($request, $this->rules());
        
    	$model = Company::find($id);
        $model = [
            'name' => $request->name,
            'description' =>$request->description,
            'user_id' => Auth::user()->id
        ];
          if ($request->HasFile('images')) {
                $destination_path = "/public/company/";
                $image = $request->file('images');
                $image_name = $image->getClientOriginalName();
                $path = $request->file('images')->storeAs($destination_path,$image_name);
                $model['images'] = $image_name;
            }
        if ($model) {

          $update = Company::where('company_id',$id)->update($model);

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Company has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Company');
        }

    	return redirect('/company');
    }

    public function delete(Request $request, $id)
    {
        $model	= Company::findOrFail($id);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Company has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Company');
        }

    	return redirect('/company');
    }

    

    private function rules()
    {
        return [
            "code" => ['required'],
            "name" => ['required'],
            "status" => ['required', Rule::in([0, 1])],
            "description" => ['required'],
        ];
    }

    public function show($id)
    {
        $company = Company::find($id);

        return view('company.detail', compact('company'));
    }  

   
}