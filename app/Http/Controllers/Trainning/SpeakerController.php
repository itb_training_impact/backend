<?php

namespace App\Http\Controllers\Trainning;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

use App\Model\Trainning\Speaker;
use Auth;

class SpeakerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->limit = 25;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {        

        if(Auth::user()->level_id != 1)            
            $speakers = Speaker::where('user_id', Auth::user()->id)->orderBy('updated_at', 'desc');
        else{            
           $speakers = Speaker::orderBy('updated_at', 'desc');
        }

        if($request->has('name') && !empty($request->name))
            $speakers   = $speakers->where('name', 'like', '%' . $request->name . '%');

        $total = $speakers->count();

        $speakers = $speakers->paginate($this->limit);
        $speakers->appends($request->all());
       
        return view('speaker.index', compact('speakers', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('speaker.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // $this->validate($request, $this->rules());

        $speaker                         = new Speaker;

      
        $speaker->name              = $request->name;
        $speaker->description       = $request->description;
        $speaker->user_id           = Auth::user()->id;
      
           if ($request->HasFile('images')) {
                $destination_path = "/public/speaker/";
                $image = $request->file('images');
                $image_name = $image->getClientOriginalName();
                $path = $request->file('images')->storeAs($destination_path,$image_name);
                $speaker['images'] = $image_name;
            }

        $insert = $speaker->save();
    //return$speaker;exit();
        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Speaker has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Speaker');
        }

        return redirect('/speaker');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 
    public function edit($id)
    {
    	$speakers = Speaker::find($id);

        return view('/speaker/edit', compact('speakers'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$this->validate($request, $this->rules());
        
    	$model = Speaker::find($id);
        $model = [
            'name' => $request->name,
            'description' =>$request->description,
            'user_id' => Auth::user()->id
        ];
          if ($request->HasFile('images')) {
                $destination_path = "/public/speaker/";
                $image = $request->file('images');
                $image_name = $image->getClientOriginalName();
                $path = $request->file('images')->storeAs($destination_path,$image_name);
                $model['images'] = $image_name;
            }
        if ($model) {

           $update = Speaker::where('speaker_id',$id)->update($model);

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Speaker has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Speaker');
        }

    	return redirect('/speaker');
    }

    public function delete(Request $request, $id)
    {
        $model	= Speaker::findOrFail($id);

        if ($model) {

            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Speaker has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Speaker');
        }

    	return redirect('/speaker');
    }

    

    private function rules()
    {
        return [
            "name" => ['required'],
            "description" => ['required'],
        ];
    }

    public function show($id)
    {
        $speaker = Speaker::find($id);

        return view('/speaker/detail', compact('speaker'));
    }  

   
}