<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect('login'); });

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::group(['namespace' => 'User'], function(){
    // Routing Group Menu Users
    Route::group(['prefix' => 'user'], function(){
        Route::get('/', 'UserController@index');
        Route::post('/', 'UserController@store');
        Route::get('/create', 'UserController@create');
        Route::get('/edit/{id}', 'UserController@edit');
        Route::put('/update/{id}', 'UserController@update');
        Route::any('/delete/{id}', 'UserController@delete');
        Route::post('/approved/{id}', 'UserController@approved');
    });

    // Routing Group Menu trainer management
    Route::group(['prefix' => 'trainer'], function(){
        Route::get('/', 'TrainerController@index');
        Route::post('/', 'TrainerController@store');
        Route::get('/create', 'TrainerController@create');
        Route::get('/edit/{id}', 'TrainerController@edit');
        Route::put('/update/{id}', 'TrainerController@update');
        Route::any('/delete/{id}', 'TrainerController@delete');
    });

    // Routing Group Menu trainer user
    Route::group(['prefix' => 'participant'], function(){
        Route::get('/', 'ParticipantController@index');
        Route::post('/', 'ParticipantController@store');
        Route::get('/create', 'ParticipantController@create');
        Route::get('/edit/{id}', 'ParticipantController@edit');
        Route::put('/update/{id}', 'ParticipantController@update');
        Route::any('/delete/{id}', 'ParticipantController@delete');
        Route::post('/import', 'ParticipantController@import');
    });

 // Routing Group Menu trainer user
    Route::group(['prefix' => 'guest'], function(){
        Route::get('/', 'GuestController@index');
        Route::post('/', 'GuestController@store');
        Route::get('/create', 'GuestController@create');
        Route::get('/edit/{id}', 'GuestController@edit');
        Route::put('/update/{id}', 'GuestController@update');
        Route::any('/delete/{id}', 'GuestController@delete');
    });

     // Routing Group Level
    Route::group(['prefix' => 'level'], function(){
        Route::get('/', 'LevelController@index');
        Route::post('/', 'LevelController@store');
        Route::get('/create', 'LevelController@create');
        Route::get('/edit/{id}', 'LevelController@edit');
        Route::put('/update/{id}', 'LevelController@update');
        Route::any('/delete/{id}', 'LevelController@delete');
    });
});


// Routing Group Menu System
Route::group(['prefix' => 'system', 'namespace' => 'System'], function(){

    // Routing Group Menu Module
    Route::group(['prefix' => 'module'], function(){
        Route::get('/', 'ModuleController@index');
        Route::post('/', 'ModuleController@store');
        Route::get('/create', 'ModuleController@create');
        Route::get('/edit/{id}', 'ModuleController@edit');
        Route::put('/update/{id}', 'ModuleController@update');
        Route::any('/delete/{id}', 'ModuleController@delete');
    });

    // Routing Group Menu Task
    Route::group(['prefix' => 'task'], function(){
        Route::get('/', 'TaskController@index');
        Route::post('/', 'TaskController@store');
        Route::get('/create', 'TaskController@create');
        Route::get('/edit/{id}', 'TaskController@edit');
        Route::put('/update/{id}', 'TaskController@update');
        Route::any('/delete/{id}', 'TaskController@delete');
    });

    // Routing Group Menu
    Route::group(['prefix' => 'menu'], function(){
        Route::get('/', 'MenuController@index');
        Route::post('/', 'MenuController@store');
        Route::get('/create', 'MenuController@create');
        Route::get('/edit/{id}', 'MenuController@edit');
        Route::put('/update/{id}', 'MenuController@update');
        Route::any('/delete/{id}', 'MenuController@delete');
    });

    // Routing Group Menu Role
    Route::group(['prefix' => 'role'], function(){
        Route::get('/', 'RoleController@index');
        Route::get('/edit/{id}', 'RoleController@edit');
        Route::post('/update', 'RoleController@update');
    });
});


// Routing Group Menu Master
Route::group(['namespace' => 'Master'], function(){

    // Routing Group Menu Module
    Route::group(['prefix' => 'type'], function(){
        Route::get('/', 'TypeController@index');
        Route::post('/', 'TypeController@store');
        Route::get('/create', 'TypeController@create');
        Route::get('/edit/{id}', 'TypeController@edit');
        Route::put('/update/{id}', 'TypeController@update');
        Route::any('/delete/{id}', 'TypeController@delete');
    });

    // Routing Formula
    Route::group(['prefix' => 'formula'], function(){
        Route::get('/', 'FormulaController@index');
        Route::get('/create', 'FormulaController@create');
        Route::post('/', 'FormulaController@store');
        Route::any('/delete/{id}', 'FormulaController@delete');
        Route::get('/detail/{id}', 'FormulaController@show');
        Route::get('/edit/{id}', 'FormulaController@edit');
        Route::put('/update/{id}', 'FormulaController@update');
    });

    // Routing Dimension
    Route::group(['prefix' => 'dimension'], function(){
        Route::get('/', 'DimensionController@index');
        Route::get('/create', 'DimensionController@create');
        Route::post('/', 'DimensionController@store');
        Route::get('/edit/{id}', 'DimensionController@edit');
        Route::put('/update/{id}', 'DimensionController@update');
        Route::any('/delete/{id}', 'DimensionController@delete');
    });

    // Routing Criteria
    Route::group(['prefix' => 'criteria'], function(){
        Route::get('/', 'CriteriaController@index');
        Route::get('/create', 'CriteriaController@create');
        Route::post('/', 'CriteriaController@store');
        Route::get('/edit/{id}', 'CriteriaController@edit');
        Route::put('/update/{id}', 'CriteriaController@update');
        Route::any('/delete/{id}', 'CriteriaController@delete');
    });

    // Formulation Group
    Route::group(['prefix' => 'formulation-group'], function(){
        Route::get('/', 'FormulationGroupController@index');
        Route::get('/create', 'FormulationGroupController@create');
        Route::post('/', 'FormulationGroupController@store');
        Route::get('/edit/{id}', 'FormulationGroupController@edit');
        Route::get('/detail/{id}', 'FormulationGroupController@show');
        Route::put('/update/{id}', 'FormulationGroupController@update');
        Route::any('/delete/{id}', 'FormulationGroupController@delete');
        Route::any('/delete_all', 'FormulationGroupController@multiple_delete');
    });

    // Formulation
    Route::group(['prefix' => 'formulation'], function(){
        Route::get('/', 'FormulationController@index');
        Route::get('/create', 'FormulationController@create');
        Route::post('/', 'FormulationController@store');
        Route::post('/import', 'FormulationController@import');
        Route::get('/edit/{id}', 'FormulationController@edit');
        Route::get('/detail/{id}', 'FormulationController@show');
        Route::put('/update/{id}', 'FormulationController@update');
        Route::any('/delete/{id}', 'FormulationController@delete');
        Route::any('/delete_all', 'FormulationController@multiple_delete');
        Route::any('/switch_formulation', 'FormulationController@switch_formulation');
    });

    // Dictionary Group
    Route::group(['prefix' => 'dictionary'], function(){
        Route::get('/', 'DictionaryController@index');
        Route::get('/create', 'DictionaryController@create');
        Route::post('/', 'DictionaryController@store');
        Route::post('/import', 'DictionaryController@import');
        Route::get('/edit/{id}', 'DictionaryController@edit');
        Route::get('/detail/{id}', 'DictionaryController@show');
        Route::put('/update/{id}', 'DictionaryController@update');
        Route::any('/delete/{id}', 'DictionaryController@delete');
        Route::any('/delete_all', 'DictionaryController@multiple_delete');
        Route::any('/switch_dict', 'DictionaryController@switch_dict');
    });

    // Word Category Group
    Route::group(['prefix' => 'word_category'], function(){
        Route::get('/', 'WordCategoryController@index');
        Route::get('/create', 'WordCategoryController@create');
        Route::post('/', 'WordCategoryController@store');
        Route::post('/import', 'WordCategoryController@import');
        Route::get('/edit/{id}', 'WordCategoryController@edit');
        Route::get('/edit_sentence/{id}', 'WordCategoryController@edit_sentence');
        Route::get('/detail/{id}', 'WordCategoryController@show');
        Route::get('/detail_sentence/{id}', 'WordCategoryController@show_sentence');
        Route::put('/update/{id}', 'WordCategoryController@update');
        Route::put('/update_sentence/{id}', 'WordCategoryController@update_sentence');
        Route::any('/delete/{id}', 'WordCategoryController@delete');
        Route::any('/delete_all', 'WordCategoryController@multiple_delete');
    });

    // Word Category Group
    Route::group(['prefix' => 'word'], function(){
        Route::get('/', 'WordController@index');
        Route::get('/create', 'WordController@create');
        Route::post('/', 'WordController@store');
        Route::post('/import', 'WordController@import');
        Route::get('/edit/{id}', 'WordController@edit');
        Route::get('/detail/{id}', 'WordController@show');
        Route::put('/update/{id}', 'WordController@update');
        Route::any('/delete/{id}', 'WordController@delete');
        Route::any('/delete_all', 'WordController@multiple_delete');
    });

    Route::group(['prefix' => 'registration'], function(){
        Route::get('/', 'RegistrationController@index');
        Route::any('/switch_status', 'RegistrationController@switch_status');
    });

    // Routing Position Level
    Route::group(['prefix' => 'position_level'], function(){
        Route::get('/', 'PositionLevelController@index');
        Route::get('/create', 'PositionLevelController@create');
        Route::post('/', 'PositionLevelController@store');
        Route::get('/detail/{id}', 'PositionLevelController@show');
        Route::get('/edit/{id}', 'PositionLevelController@edit');
        Route::put('/update/{id}', 'PositionLevelController@update');
        Route::any('/delete/{id}', 'PositionLevelController@delete');
    });

    // Routing Devision
    Route::group(['prefix' => 'devision'], function(){
        Route::get('/', 'DevisionController@index');
        Route::get('/create', 'DevisionController@create');
        Route::post('/', 'DevisionController@store');
        Route::get('/detail/{id}', 'DevisionController@show');
        Route::get('/edit/{id}', 'DevisionController@edit');
        Route::put('/update/{id}', 'DevisionController@update');
        Route::any('/delete/{id}', 'DevisionController@delete');
    });

    // Routing UnidentifiedWord
    Route::group(['prefix' => 'unidentified_word'], function(){
        Route::get('/', 'UnidentifiedWordController@index');
        Route::put('/update/{id}', 'UnidentifiedWordController@update');
        Route::put('/update_all', 'UnidentifiedWordController@multiple_update');
        Route::any('/delete/{id}', 'UnidentifiedWordController@delete');
        Route::any('/delete_all', 'UnidentifiedWordController@multiple_delete');    
    });
});

// Routing Group Reaction
Route::group(['prefix' => 'reaction', 'namespace' => 'Reaction'], function(){
    Route::get('/', 'ReactionController@index');
    Route::post('/', 'ReactionController@store');
    Route::post('/import', 'ReactionController@import');
    Route::get('/create', 'ReactionController@create');
    Route::get('/edit/{id}', 'ReactionController@edit');
    Route::get('/detail/{id}', 'ReactionController@show');
    Route::put('/update/{id}', 'ReactionController@update');
    Route::any('/delete/{id}', 'ReactionController@delete');
    Route::get('/trainning/{id}', 'ReactionController@trainning');
    Route::get('/session/{id}', 'ReactionController@get_session');
    Route::get('/module/{id}', 'ReactionController@get_module');
    Route::any('/delete_questions/{id}/{answer}', 'ReactionController@delete_questions');

    Route::get('/createCustom/{trainningID}/{sessionID}/{moduleID}', 'ReactionController@createCustom');
});

// Routing Group Learning
Route::group(['prefix' => 'learning', 'namespace' => 'Learning'], function(){
    Route::get('/', 'LearningController@index');
    Route::post('/', 'LearningController@store');
    Route::post('/import', 'LearningController@import');
    Route::get('/create', 'LearningController@create');
    Route::get('/edit/{id}', 'LearningController@edit');
    Route::get('/detail/{id}', 'LearningController@show');
    Route::put('/update/{id}', 'LearningController@update');
    Route::any('/delete/{id}', 'LearningController@delete');
    Route::get('/trainning/{id}', 'LearningController@trainning');
    Route::get('/session/{id}', 'LearningController@get_session');
    Route::get('/module/{id}', 'LearningController@get_module');
    Route::any('/delete_questions/{id}/{answer}', 'LearningController@delete_questions');

    Route::get('/createCustomLearning/{trainningID}/{sessionID}/{moduleID}', 'LearningController@createCustom');
});

// Routing Group Learning
Route::group(['prefix' => 'behaviour', 'namespace' => 'Behaviour'], function(){
    Route::get('/', 'BehaviourController@index');
    Route::post('/', 'BehaviourController@store');
    Route::post('/import', 'BehaviourController@import');
    Route::get('/create', 'BehaviourController@create');
    Route::get('/edit/{id}', 'BehaviourController@edit');
    Route::get('/detail/{id}', 'BehaviourController@show');
    Route::put('/update/{id}', 'BehaviourController@update');
    Route::any('/delete/{id}', 'BehaviourController@delete');
    Route::get('/trainning/{id}', 'BehaviourController@trainning');
    Route::get('/session/{id}', 'BehaviourController@get_session');
    Route::get('/module/{id}', 'BehaviourController@get_module');
    Route::any('/delete_questions/{id}/{answer}', 'BehaviourController@delete_questions');
    Route::any('/delete_all', 'BehaviourController@multiple_delete');

    Route::get('/createCustomBehaviour/{trainningID}/{sessionID}/{moduleID}', 'BehaviourController@createCustom');

     Route::any('/download/report/{id}', 'ReportController@export_result');
    Route::any('/download/story/users/{trainningID}/{sessionID}/{moduleID}/{userID}', 'ReportController@export_story_users_to_txt');
    Route::any('/download/story/user/{trainningID}/{userID}', 'ReportController@export_story_user_to_txt');
    Route::any('/download/report/users/{type}/{trainningID}/{sessionID}/{moduleID}/{userID}', 'ReportController@export_behaviour_users_to_excel');
    Route::any('/download/report/user/{type}/{trainningID}/{userID}', 'ReportController@export_behaviour_user_to_excel'); 
});

// Routing Group Performance
Route::group(['prefix' => 'performance', 'namespace' => 'Performance'], function(){
    Route::get('/', 'PerformanceController@index');
    Route::post('/', 'PerformanceController@store');
    Route::post('/import', 'PerformanceController@import');
    Route::get('/create', 'PerformanceController@create');
    Route::get('/edit/{id}', 'PerformanceController@edit');
    Route::get('/detail/{id}', 'PerformanceController@show');
    Route::put('/update/{id}', 'PerformanceController@update');
    Route::any('/delete/{id}', 'PerformanceController@delete');
    Route::get('/trainning/{id}', 'PerformanceController@trainning');
    Route::get('/session/{id}', 'PerformanceController@get_session');
    Route::get('/module/{id}', 'PerformanceController@get_module');
    Route::any('/delete_questions/{id}/{answer}', 'PerformanceController@delete_questions');
    Route::get('/createCustomPerformance/{trainningID}/{sessionID}/{moduleID}', 'PerformanceController@createCustom');
});

// Routing Group Performance
Route::group(['prefix' => 'roti', 'namespace' => 'Roti'], function(){
    Route::get('/', 'RotiController@index');
    Route::post('/', 'RotiController@store');
    Route::post('/import', 'RotiController@import');
    Route::get('/create', 'RotiController@create');
    Route::get('/edit/{id}', 'RotiController@edit');
    Route::get('/detail/{id}', 'RotiController@show');
    Route::put('/update/{id}', 'RotiController@update');
    Route::any('/delete/{id}', 'RotiController@delete');
    Route::get('/trainning/{id}', 'RotiController@trainning');
    Route::get('/session/{id}', 'RotiController@get_session');
    Route::get('/module/{id}', 'RotiController@get_module');
    Route::any('/delete_questions/{id}/{answer}', 'RotiController@delete_questions');
    Route::get('/createCustomRoti/{trainningID}/{sessionID}/{moduleID}', 'RotiController@createCustom');
});

   

    Route::get('/test', 'TestController@index');


// Routing Group Trainning
Route::group(['namespace' => 'Trainning'], function(){

 Route::group(['prefix' => 'trainning'], function(){
    Route::get('/', 'TrainningController@index');
    Route::post('/', 'TrainningController@store');
    Route::post('/import', 'TrainningController@import');
    Route::get('/create', 'TrainningController@create');
    Route::get('/result/{id}/{trainningID}','TrainningController@result');
    Route::get('/modules/{id}','TrainningController@getModules');
    Route::get('/submits','TrainningController@getSubmits');
    Route::get('/submit','TrainningController@getSubmit');
    Route::get('/edit/{id}', 'TrainningController@edit');
    Route::get('/detail/{id}', 'TrainningController@show');
    Route::put('/update/{id}', 'TrainningController@update');
    Route::any('/delete/{id}', 'TrainningController@delete');
    Route::any('/config/{id}', 'TrainningController@config');
    Route::any('/change_config/{id}/{field}/{status}', 'TrainningController@update_config');
    Route::get('/download_pdf/{id}/{trainningID}/{sessionID}/{moduleID}/{active}', 'TrainningController@download_pdf');

    Route::get('/export/{id}/{trainningID}/{sessionID}/{moduleID}', 'TrainningController@export');
    Route::get('/activeModule/{id}/{field}', 'TrainningController@activeModule');
    Route::get('/activeModuleBehaviour/{id}/{field}', 'TrainningController@activeModuleBehaviour');
    Route::get('/activeModulePerformance/{id}/{field}', 'TrainningController@activeModulePerformance');
    Route::get('/nonActive/{id}', 'TrainningController@nonActive');
    Route::get('/ActiveBefore/{sessionID}', 'TrainningController@ActiveBefore');
    Route::get('/ActiveDuring/{sessionID}', 'TrainningController@ActiveDuring');
    Route::get('/ActiveAfter/{sessionID}', 'TrainningController@ActiveAfter');
    Route::get('/export/{id}/{trainningID}/{sessionID}/{moduleID}/{active}/{section}', 'TrainningController@export');
    Route::get('/export_qna/{id}/{trainningID}/{sessionID}/{moduleID}/{active}/{level}', 'TrainningController@export_qna');
    Route::get('/downloadALL/{userID}/{trainningID}', 'TrainningController@ResultAll');
    Route::get('/downloadALL_Trainning/{trainingID}/{sessionID}/{moduleID}/{periode}', 'TrainningController@Result_trainning');

    Route::get('/getbehaviour/{trainningID}/{sessionID}/{moduleID}', 'TrainningController@getBehaviour');
    Route::get('/getlearning/{trainningID}/{sessionID}/{moduleID}', 'TrainningController@getLearning');
    Route::get('/getreaction/{trainningID}/{sessionID}/{moduleID}', 'TrainningController@getReaction');
    Route::get('/getPerformance/{trainningID}/{sessionID}/{moduleID}', 'TrainningController@getPerformance');
    Route::get('/getRoti/{trainningID}/{sessionID}/{moduleID}', 'TrainningController@getRoti');


    Route::get('/check/{trainningID}','TrainningController@checkPerformance');
    Route::get('/checkDefault/{trainningID}','TrainningController@checkDefault');
    Route::get('/check','TrainningController@checkStatus');
    Route::get('/checkDownload','TrainningController@checkDownload');
    Route::get('/downloadAll/{id}','TrainningController@downloadAll');
    Route::get('/download_roti/{trainningID}','TrainningController@downloadRoti');

    Route::get('/getForm/{moduleID}','TrainningController@getForm');
    Route::post('/jadwal/{moduleID}','TrainningController@setSchadule');
    Route::get('/submitTrainning', 'TrainningController@submitAll');

    Route::any('/delete_question_reaction/{id}', 'TrainningController@delete_question_reaction');
    Route::any('/delete_question_learning/{id}', 'TrainningController@delete_question_learning');
    Route::any('/delete_question_behaviour/{id}', 'TrainningController@delete_question_behaviour');
    Route::any('/delete_question_roti/{id}', 'TrainningController@delete_question_roti');
    Route::any('/delete_question_performance/{id}', 'TrainningController@delete_question_performance');

    Route::any('/delete_questions/{ids}/{id}/{answer}', 'TrainningController@delete_questions');
});
//company
 Route::group(['prefix' => 'company'], function(){
        Route::get('/', 'CompanyController@index');
        Route::post('/', 'CompanyController@store');
        Route::get('/create', 'CompanyController@create');
         Route::get('/detail/{id}', 'CompanyController@show');
        Route::get('/edit/{id}', 'CompanyController@edit');
        Route::put('/update/{id}', 'CompanyController@update');
        Route::any('/delete/{id}', 'CompanyController@delete');
    });
 // Routing Speaker
    Route::group(['prefix' => 'speaker'], function(){
        Route::get('/', 'SpeakerController@index');
        Route::get('/create', 'SpeakerController@create');
        Route::post('/', 'SpeakerController@store');
        Route::get('/detail/{id}', 'SpeakerController@show');
        Route::get('/edit/{id}', 'SpeakerController@edit');
        Route::put('/update/{id}', 'SpeakerController@update');
        Route::any('/delete/{id}', 'SpeakerController@delete');
    });
    // Routing Speaker
    Route::group(['prefix' => 'session'], function(){
        Route::get('/', 'SessionController@index');
        Route::get('/create', 'SessionController@create');
        Route::post('/', 'SessionController@store');
        Route::get('/detail/{id}', 'SessionController@show');
        Route::get('/edit/{id}', 'SessionController@edit');
        Route::put('/update/{id}', 'SessionController@update');
        Route::any('/delete/{id}', 'SessionController@delete');
    });
    // Routing Speaker
    Route::group(['prefix' => 'module'], function(){
        Route::get('/', 'ModuleController@index');
        Route::get('/create', 'ModuleController@create');
        Route::post('/', 'ModuleController@store');
        Route::get('/detail/{id}', 'ModuleController@show');
        Route::get('/edit/{id}', 'ModuleController@edit');
        Route::put('/update/{id}', 'ModuleController@update');
        Route::any('/delete/{id}', 'ModuleController@delete');
        Route::any('/delete_questions/{id}/{type}', 'ModuleController@delete_questions');
    });
});