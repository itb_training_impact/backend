@extends('layouts.root')

@section('title','Formulation Group')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Formulation Group</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
    		@if(Session::get('status') == '200')
    	        <div class="alert alert-success alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @elseif(Session::get('status') == 'err')
    			<div class="alert alert-danger alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @endif
        @endif

        <h2 class="content-heading">
            Formulation Group
        </h2>

        {{-- search --}}

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Search Forms</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <form method="GET" class="form form-inline">
                    @csrf
                    <div>
                        <input type="text" class="form-control" name="name" value="{{ Request::has('name') ? Request::get('name') : '' }}" placeholder="Enter Name ...">
                        <button type="submit" class="btn btn-alt-primary">Search</button>
                        <a href="{{ url('formulation-group') }}" class="btn btn-secondary">Refresh</a>
                    </div>
                </form>
            </div>
        </div>


        <a class="btn btn-success mb-3" href="{{ url('formulation-group/create') }}"><i class="fa fa-plus"></i> &nbsp; Create New</a>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">List of Formulation Group</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <table id="datatable" class="table table-vcenter table-stripped">
                    <thead>
                        <tr>
                            <th style="width: 50px;">No</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th style="width: 120px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($groups) > 0)
                            @foreach($groups as $result)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $result->code }}</td>
                                    <td>{{ $result->name }}</td>
                                    <td>
                                        <a href="{{ url('formulation-group/detail/'. $result->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Lihat" style="height: 19px">
                                            <i class="fa fa-search"></i>
                                        </a>
                                        <a href="{{ url('formulation-group/edit/'. $result->id) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Ubah" style="height: 19px">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('formulation-group/delete/'. $result->id) }}" class="btn btn-sm btn-danger btn-xs btn-delete" style="height: 19px">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
						@else
							<tr>
								<td colspan='7' class="text-center">Data not available.</td>
							</tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Table -->
        <div class="block">
            <div class="block-content" style="padding: 18px;">
                <div class="row">
                    <div class="col align-self-center">
                        Showing {{ count($groups) == 0 ? 0 : count($groups) }} out of {{ $total }} entries
                    </div>
                    <div class="col d-flex justify-content-end">
                        <div class="align-self-center" style="height: 34px;">
                            {{ $groups->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

@include('modal_delete')
@endsection

@push('script')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.css') }}">
    <script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js') }} "></script>
    <script src="{{ asset('assets/js/pages/be_tables_datatables.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                "pageLength": 25,
                "lengthChange": true,
                "searching": true
            });
        } );

        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
