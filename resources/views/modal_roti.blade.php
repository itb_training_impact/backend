
 <div class="modal fade" id="modalRoti" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="block-header bg-primary-dark">
            <h3 class="block-title" style="color: #ffff;">List pertanyaan roti</h3>
            <div class="block-options">
               @if(sizeof($sessionRotis) > 0)
                 <a href="{{url('trainning/getRoti/'.$trainning->trainning_id.'/'.$sessionRoti->session_id.'/'.$value->module_id)}}" class="roti-restart btn btn-primary">Restart list</a>
                 @endif
                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                    <i class="si si-close"></i>
                </button>
            </div>
        </div>   
        <div class="modal-header">
         <button class="btn btn-success roti-import mb-3" type="button"> <i class="fa fa-upload"></i> &nbsp; Import Question</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          
           
        </div>
        
        <!-- Modal footer -->
       
        
      </div>
    </div>
  </div>
