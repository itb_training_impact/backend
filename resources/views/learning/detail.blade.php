@extends('layouts.root')

@section('title','Detail Learning')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="{{ url('home') }}">Dashboard</a>
            <a class="breadcrumb-item" href="{{ url('learning') }}">Learning</a>
            <span class="breadcrumb-item active">Detail</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            Detail Learning Question
        </h2>

        @if($learning->type->type_label == "rating")
            @php $explode = explode(",", $learning->learning_answer) @endphp
            <!-- Table -->
            <div class="block">
                <div class="block-content" style="padding: 18px;">
                    <div class="form-group">
                        <label for="" class="control-label">
                            {{ $learning->learning_question }} <span class="text-danger">*</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <table class="table text-center">
                            <tr>
                                <td></td>
                                @for($i = 0; $i < $learning->learning_rating_finish; $i++)
                                    <td>{{ $i+1 }}</td>
                                @endfor
                                <td></td>
                            </tr>
                            <tr>
                                <td>{{ $explode[0] }}</td>
                                @for($i = 0; $i < $learning->learning_rating_finish; $i++)
                                    <td>
                                        <label class="css-control css-control-primary css-radio">
                                            <input type="radio" class="css-control-input" name="">
                                            <span class="css-control-indicator"></span>
                                        </label>
                                    </td>
                                @endfor
                                <td>{{ $explode[1] }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END Table -->
        @endif

        @if($learning->type->type_label == "short_answer")
            <!-- Table -->
            <div class="block">
                <div class="block-content" style="padding: 18px;">

                    <div class="form-group">
                        <label for="" class="control-label">
                            {{ $learning->learning_question }}
                        </label>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Short-answer text">
                    </div>
                </div>
            </div>
            <!-- END Table -->
        @endif

        @if($learning->type->type_label == "multiple_choice")                    
                <!-- Learning Short Answer -->
                  <div class="block">
                    <div class="block-content">
                      <div class="form-group">
                        <label for="" class="control-label">
                          {{ $learning->learning_question }} <span class="text-danger">*</span>
                        </label>
                      </div>
                      <div class="form-group" style="overflow-x: auto;">
                        @foreach (explode(",",$learning->learning_answer) as $question)
                <div class="form-check">
                    <input type="radio" class="css-control-input" name="" style="z-index: 38;opacity: 1;margin-top: 6px;" {{$learning->learning_correct_answer == $question ? 'checked' : ''}}>
                        <span class="css-control-indicator"></span>
                        <label class="form-check-label" for="materialUnchecked">{{ $question }}</label>
                </div>                                                              
                        @endforeach
                      </div>
                    </div>
                  </div>
                @endif 
        <a href="{{ url('learning') }}" class="btn btn-secondary" style="float:right;">Back</a> 
    </div>
</div>

@endsection
