<div class="content-side content-side-full">
    <ul class="nav-main">
        <li>
            <a href="{{ url('home') }}" class="{{ Request::is('home') || Request::is('home/*') ? 'active' : '' }}"><i class="si si-cup"></i><span class="sidebar-mini-hide">Dashboard</span></a>
        </li>

        {{-- System Section --}}
        <li class="nav-main-heading"><span class="sidebar-mini-visible">SS</span><span class="sidebar-mini-hidden">SYSTEM SECTION</span></li>
        <li class="{{ Request::is('system') || Request::is('system/*') || Request::is('level') || Request::is('level/*') ? 'open' : '' }}">
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">System</span></a>
            <ul>
                <li>
                    <a href="{{ url('system/module') }}" class="{{ Request::is('system/module') || Request::is('system/module/*') ? 'active' : '' }}">Module</a>
                </li>
                <li>
                    <a href="{{ url('system/task') }}" class="{{ Request::is('system/task') || Request::is('system/task/*') ? 'active' : '' }}">Task</a>
                </li>
                <li>
                    <a href="{{ url('system/role') }}" class="{{ Request::is('system/role') || Request::is('system/role/*') ? 'active' : '' }}">Role</a>
                </li>
                <li>
                    <a href="{{ url('level') }}" class="{{ Request::is('level') || Request::is('level/*') ? 'active' : '' }}">Level</a>
                </li>
            </ul>
        </li>

        {{-- User Section --}}
        <li class="nav-main-heading"><span class="sidebar-mini-visible">US</span><span class="sidebar-mini-hidden">USER SECTION</span></li>
        <li class="{{ Request::is('user') || Request::is('user/*') || Request::is('trainer') || Request::is('trainer/*') || Request::is('participant') || Request::is('participant/*') || Request::is('guest') || Request::is('guest/*') ? 'open' : '' }}">
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-user"></i><span class="sidebar-mini-hide">User Type</span></a>
            <ul>
                <li>
                    <a href="{{ url('user') }}" class="{{ Request::is('user') || Request::is('user/*') ? 'active' : '' }}">Trainer Administrator</a>
                </li>
                <!-- <li>
                    <a href="{{ url('trainer') }}" class="{{ Request::is('trainer') || Request::is('trainer/*') ? 'active' : '' }}">Trainer Management</a>
                </li> -->
                <li>
                    <a href="{{ url('participant') }}" class="{{ Request::is('participant') || Request::is('participant/*') ? 'active' : '' }}">Participant</a>
                </li>
                <!-- <li>
                    <a href="{{ url('guest') }}" class="{{ Request::is('guest') || Request::is('guest/*') ? 'active' : '' }}">Guest</a>
                </li> -->
            </ul>
        </li>

        {{-- Master Data Section --}}
        <li class="nav-main-heading"><span class="sidebar-mini-visible">MD</span><span class="sidebar-mini-hidden">MASTER DATA SECTION</span></li>
        <li class="{{ 
            Request::is('position_level') || Request::is('position_level/*') ||
            Request::is('devision') || Request::is('devision/*') ||
            Request::is('registration') || Request::is('registration/*') || 
            Request::is('dictionary') || Request::is('dictionary/*') || 
            Request::is('dimension') || Request::is('dimension/*') || 
            Request::is('criteria') || Request::is('criteria/*') || 
            Request::is('formulation-group') || Request::is('formulation-group/*') || 
            Request::is('formulation') || Request::is('formulation/*') || 
            Request::is('unidentified_word') || Request::is('unidentified_word/*') || 
            Request::is('word_category') || Request::is('word_category/*') || 
            Request::is('word') || Request::is('word/*')
             ? 'open' : '' }}">
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-folder"></i><span class="sidebar-mini-hide">Master Data</span></a>
             <ul>
                <li>
                    <a href="{{ url('position_level') }}" class="{{ Request::is('position_level') || Request::is('position_level/*') ? 'active' : '' }}">Position Level</a>
                </li>
                <li>
                    <a href="{{ url('devision') }}" class="{{ Request::is('devision') || Request::is('devision/*') ? 'active' : '' }}">Devision</a>
                </li>
                <li>
                    <a href="{{ url('registration') }}" class="{{ Request::is('registration') || Request::is('registration/*') ? 'active' : '' }}">Registration</a>
                </li>
                <!-- <li>
                    <a href="{{ url('formula') }}" class="{{ Request::is('formula') || Request::is('formula/*') ? 'active' : '' }}">Formula</a>
                </li> -->
                <li>
                    <a href="{{ url('dictionary') }}" class="{{ Request::is('dictionary') || Request::is('dictionary/*') ? 'active' : '' }}">Dictionary</a>
                </li>
                <li>
                    <a href="{{ url('dimension') }}" class="{{ Request::is('dimension') || Request::is('dimension/*') ? 'active' : '' }}">Dimension</a>
                </li>
                <li>
                    <a href="{{ url('criteria') }}" class="{{ Request::is('criteria') || Request::is('criteria/*') ? 'active' : '' }}">Criteria</a>
                </li>
                <li>
                    <a href="{{ url('formulation-group') }}" class="{{ Request::is('formulation-group') || Request::is('formulation-group/*') ? 'active' : '' }}">Formulation Group</a>
                </li>
                <li>
                    <a href="{{ url('formulation') }}" class="{{ Request::is('formulation') || Request::is('formulation/*') ? 'active' : '' }}">Formulation</a>
                </li>
                <li>
                    <a href="{{ url('unidentified_word') }}" class="{{ Request::is('unidentified_word') || Request::is('unidentified_word/*') ? 'active' : '' }}">Unidentified Word</a>
                </li>
                <li>
                    <a href="{{ url('word_category') }}" class="{{ Request::is('word_category') || Request::is('word_category/*') ? 'active' : '' }}">Word Category</a>
                </li>
                <li>
                    <a href="{{ url('word') }}" class="{{ Request::is('word') || Request::is('word/*') ? 'active' : '' }}">Word</a>
                </li>
               <!--  <li>
                    <a href="{{ url('type') }}" class="{{ Request::is('type') || Request::is('type/*') ? 'active' : '' }}">Type of Question</a>
                </li> -->
            </ul>
        </li>


        {{-- Module Section --}}
        <li class="nav-main-heading"><span class="sidebar-mini-visible">MS</span><span class="sidebar-mini-hidden">MODULE SECTION</span></li>
        <li class="{{ Request::is('reaction') || Request::is('reaction/*') || Request::is('learning') || Request::is('learning/*') || Request::is('behaviour') || Request::is('behaviour/*') || Request::is('performance') || Request::is('performance/*') || Request::is('roti') || Request::is('roti/*') ? 'open' : '' }}">
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-folder-alt"></i><span class="sidebar-mini-hide">Modules</span></a>
            <ul>
                <li>
                    <a href="{{ url('reaction') }}" class="{{ Request::is('reaction') || Request::is('reaction/*') ? 'active' : '' }}">Reaction</a>
                </li>
                <li>
                    <a href="{{ url('learning') }}" class="{{ Request::is('learning') || Request::is('learning/*') ? 'active' : '' }}">Learning</a>
                </li>
                <li>
                    <a href="{{ url('behaviour') }}" class="{{ Request::is('behaviour') || Request::is('behaviour/*') ? 'active' : '' }}">Behaviour</a>
                </li>
                <li>
                    <a href="{{ url('performance') }}" class="{{ Request::is('performance') || Request::is('performance/*') ? 'active' : '' }}">Performance</a>
                </li>
                <li>
                    <a href="{{ url('roti') }}" class="{{ Request::is('roti') || Request::is('roti/*') ? 'active' : '' }}">R.O.T.I</a>
                </li>
            </ul>
        </li>

        {{-- Trainning Section --}}
        {{-- Trainning --}}
        <li class="nav-main-heading"><span class="sidebar-mini-visible">TR</span><span class="sidebar-mini-hidden">TRAINING</span></li>
        <li class="{{ 
            Request::is('company') || Request::is('company/*') || 
            Request::is('speaker') || Request::is('speaker/*') || 
            Request::is('session') || Request::is('session/*') || 
            Request::is('module') || Request::is('module/*') || 
            Request::is('trainning') || Request::is('trainning/*')
             ? 'open' : '' }}">
            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bar-chart"></i><span class="sidebar-mini-hide">Training</span></a>
             <ul>
               
                <li>
                    <a href="{{ url('company') }}" class="{{ Request::is('company') || Request::is('company/*') ? 'active' : '' }}">Companies</a>
                </li>
                <li>
                    <a href="{{ url('speaker') }}" class="{{ Request::is('speaker') || Request::is('speaker/*') ? 'active' : '' }}">Speakers</a>
                </li> 
                 <li>
                    <a href="{{ url('trainning') }}" class="{{ Request::is('trainning') || Request::is('trainning/*') ? 'active' : '' }}">Trainings</a>
                </li>
                <!-- <li>
                    <a href="{{ url('session') }}" class="{{ Request::is('session') || Request::is('session/*') ? 'active' : '' }}">Sessions</a>
                </li>
                <li>
                    <a href="{{ url('module') }}" class="{{ Request::is('module') || Request::is('module/*') ? 'active' : '' }}">Modules</a>
                </li> -->
               
               
               
               <!--  <li>
                    <a href="{{ url('type') }}" class="{{ Request::is('type') || Request::is('type/*') ? 'active' : '' }}">Type of Question</a>
                </li> -->
            </ul>
        </li>
    </ul>
</div>
