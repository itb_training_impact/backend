@extends('layouts.root')

@section('title','Word Category - Edit')

@section('content')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/plugins/select2/select2-flat-theme.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/plugins/select2/select2.min.css') }}">
<link rel="stylesheet" id="css-main" href="{{ asset('assets/css/codebase.min.css') }}">

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('word_category') }}">Word Category</a>
        <span class="breadcrumb-item active">Edit</span>
    </nav>

    @if ($errors->any())
        <div class="alert alert-danger m-t-20">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Word Category - Edit</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('word_category/update_sentence/' . $sentence->code) }}" method="post">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <label for="material-email">Word Categories <span class="text-danger">*</span></label>
                                <select name="word_categories[]" class="js-select2 word_categories form-control" multiple="multiple" data-placeholder="Word Categories">
                                    <option value="choose_all">Choose All</option>
                                    @foreach($wordCategories as $wordCategory)
                                        <option value="{{ $wordCategory->id }}"
                                            @if(in_array($wordCategory->id, (old('word_categories') ?: [])))
                                                selected
                                            @endif
                                        >{{ $wordCategory->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Save</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <a href="{{ url('word_category') }}" class="btn btn-secondary" style="float:right;">Back</a> 
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <!-- <script src="{{ asset('assets/js/plugins/select2/select2-bootstrap.min.css') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2-bootstrap.min.css') }}"></script> -->
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script>
        $('.js-select2').select2()

        $('.word_categories').change(function() {
            var val = $(this).val()
            var options = $('.word_categories option')
            selectedItems = []

            if (val[0] == 'choose_all') {
                console.log(options)
                $.each(options, function(index, value) {
                    if (index > 0)
                        selectedItems.push(value.value)
                })

                $(this).val(selectedItems).trigger('change')
            }
        })

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'word_categories': {
                    required: true,
                }
            },
            messages: {
                'word_categories': {
                    required: 'Word Categories has required',
                }
            }
        });
    </script>
@endpush
