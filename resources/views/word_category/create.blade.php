@extends('layouts.root')

@section('title','Word Category - New')

@section('content')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/plugins/select2/select2-flat-theme.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/plugins/select2/select2.min.css') }}">
<link rel="stylesheet" id="css-main" href="{{ asset('assets/css/codebase.min.css') }}">

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('word_category') }}">Word Category</a>
        <span class="breadcrumb-item active">New</span>
    </nav>

    @if ($errors->any())
        <div class="alert alert-danger m-t-20">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Word Category - New</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('word_category') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="code" name="code" placeholder="Code" value="{{ old('code') }}">
                                <label for="material-email">Code <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name') }}">
                                <label for="material-email">Name <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <label for="material-email">Dictionaries <span class="text-danger">*</span></label>
                                <select name="dictionaries[]" class="js-select2 form-control" multiple="multiple" data-placeholder="Dictionaries">
                                    @foreach($dictionaries as $value)
                                        @if(old('dictionaries'))
                                            <option value="{{ $value->id }}" {{ in_array($value->id, old('dictionaries')) ? "selected" : "" }} >{{ $value->name }}</option>
                                        @else
                                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <textarea class="form-control" id="description" name="description" placeholder="Description">{{ old('description') }}</textarea>
                                <label for="material-email">Description <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Save</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <a href="{{ url('word_category') }}" class="btn btn-secondary" style="float:right;">Back</a> 
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <!-- <script src="{{ asset('assets/js/plugins/select2/select2-bootstrap.min.css') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2-bootstrap.min.css') }}"></script> -->
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script>
        $('.js-select2').select2()

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'code': {
                    required: true,
                },
                'name': {
                    required: true,
                },
                'description': {
                    required: true,
                },
                'dictionaries': {
                    required: true,
                }
            },
            messages: {
                'code': {
                    required: 'Code has required',
                },
                'name': {
                    required: 'Name has required',
                },
                'description': {
                    required: 'Description has required',
                },
                'dictionaries': {
                    required: 'Dictionaries has required',
                }
            }
        });
    </script>
@endpush
