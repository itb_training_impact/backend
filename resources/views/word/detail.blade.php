@extends('layouts.root')

@section('title','Detail Word')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <a class="breadcrumb-item" href="{{ url('word') }}">Word</a>
            <span class="breadcrumb-item active">Detail</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Detail Word</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <div class="form-group">
                    <label for="">Name</label>
                    <div> {{ $word->name }} </div>
                </div>
                <div class="form-group">
                    <label for="">One Word?</label>
                    <div> {{ !$word->is_combined_words ? 'No' : 'Yes' }} </div>
                </div>
                <div class="form-group">
                    <label for="">Word Categories</label>
                    <div> 
                        @foreach($word->detail as $key => $detail)
                            @if($key > 0)
                                {{ ', ' . $detail->wordCategory->name }}
                            @else
                                {{ $detail->wordCategory->name }}
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <a href="{{ url('word') }}" class="btn btn-secondary" style="float:right;">Back</a> 
    </div>
</div>

@endsection