@extends('layouts.root')

@section('title','Word')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Word</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
    		@if(Session::get('status') == '200')
    	        <div class="alert alert-success alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @elseif(Session::get('status') == 'err')
    			<div class="alert alert-danger alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @endif
        @endif

        <h2 class="content-heading">
            Word
        </h2>

        {{-- search --}}

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Search Forms</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <form method="GET" class="form form-inline">
                    @csrf
                    <div>
                        <input type="text" class="form-control" name="name" value="{{ Request::has('name') ? Request::get('name') : '' }}" placeholder="Enter Name ...">
                        <select name="wordCategory" id="wordCategory" class="form-control">
                            <option value="">Word Category</option>
                            @foreach($wordCategories as $value)
                                <option value="{{ $value->id }}" {{ Request::get('wordCategory') ==  $value->id ? "selected" : "" }} >{{ $value->name }}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-alt-primary">Search</button>
                        <a href="{{ url('word') }}" class="btn btn-secondary">Refresh</a>
                    </div>
                </form>
            </div>
        </div>


        <a class="btn btn-success mb-3" href="{{ url('word/create') }}"><i class="fa fa-plus"></i> &nbsp; Create New</a>
        <button class="btn btn-success btn-import mb-3" type="button"> <i class="fa fa-upload"></i> &nbsp; Import Word</button>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">List of Word</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <table id="datatable" class="table table-vcenter table-stripped">
                    <thead>
                        <tr>
                            <th width="1%"><center><input type="checkbox" id="check-all"></center></th>
                            <th style="width: 50px;">No</th>
                            <th>Name</th>
                            <th>Categories</th>
                            <th style="width: 120px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($words) > 0)
                            @foreach($words as $result)
                                <tr>
                                    <td align="center"><input type="checkbox" class="checkbox" data-id="{{ $result->id }}" name="checkbox[{{ $result->id }}]"></td>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $result->name }}</td>
                                    <td>
                                        @foreach($result->detail as $key => $detail)
                                            @if($key > 0)
                                                {{ ', ' . $detail->wordCategory->name }}
                                            @else
                                                {{ $detail->wordCategory->name }}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        <a href="{{ url('word/detail/'. $result->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Lihat" style="height: 19px">
                                            <i class="fa fa-search"></i>
                                        </a>
                                        <a href="{{ url('word/edit/'. $result->id) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Ubah" style="height: 19px">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('word/delete/'. $result->id) }}" class="btn btn-sm btn-danger btn-xs btn-delete" style="height: 19px">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
						@else
							<tr>
								<td colspan='7' class="text-center">Data not available.</td>
							</tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Table -->
        <div class="block">
            <div class="block-content" style="padding: 18px;">
                <div class="row">
                    <div class="col align-self-center">
                        Showing {{ count($words) == 0 ? 0 : count($words) }} out of {{ $total }} entries
                    </div>
                    <div class="col d-flex justify-content-end">
                        <div class="align-self-center" style="height: 34px;">
                            {{ $words->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <div class="block-content" style="padding: 18px;">
                <form action="{{ url('word/delete_all') }}" method="post" id="multiple_delete_form">
                    {{ csrf_field() }}
                    <span id ="create_input_hidden"></span>
                    <button class="btn btn-danger btn-sm" onclick="return confirm('Delete Word?');">Delete Word</button>
                </form>
            </div>
        </div>
    </div>


</div>

@include('modal_delete')
@include('modal_import_word')
@endsection

@push('script')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.css') }}">
    <script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js') }} "></script>
    <script src="{{ asset('assets/js/pages/be_tables_datatables.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                "pageLength": 25,
                "lengthChange": true,
                "searching": true
            });
        } );
        
        $('.status').change(function(){
            $('#switch_dict-' + $(this).data('id')).submit();
        })

        $("#check-all").change(function(){
            if ($(this).is(':checked')) {
                $('.checkbox').prop('checked', true);
            } else {
                $('.checkbox').prop('checked', false);
            }
        });

        $("#multiple_delete_form").submit(function(){
            var data = [];
            $("#create_input_hidden").html("");
            $('input[name^="checkbox"]:checked').each(function() {
                $("#create_input_hidden").append("<input type='hidden' name='check["+$(this).data("id")+"]' value='"+$(this).val()+"'>");
            });
            return true;
        });

        $('.btn-import').click(function(){
            var action = $(this).data('action');
            $('#modalImportWord').modal('show');
        });

        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
