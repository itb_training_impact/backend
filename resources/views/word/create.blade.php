@extends('layouts.root')

@section('title','Word - New')

@section('content')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/plugins/select2/select2-flat-theme.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/plugins/select2/select2.min.css') }}">
<link rel="stylesheet" id="css-main" href="{{ asset('assets/css/codebase.min.css') }}">

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('word') }}">Word</a>
        <span class="breadcrumb-item active">New</span>
    </nav>

    @if ($errors->any())
        <div class="alert alert-danger m-t-20">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Word - New</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('word') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name') }}">
                                <label for="material-email">Name <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                                <input type="checkbox" name="is_combined_words" class="form-check-inline" value=1>&nbsp;<span style="font-weight: 600">One Word?</span>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <label for="material-email">Categories <span class="text-danger">*</span></label>
                                <select name="wordCategories[]" class="js-select2 form-control" multiple="multiple" data-placeholder="Categories">
                                    @foreach($wordCategories as $value)
                                        @if(old('wordCatgories'))
                                            <option value="{{ $value->id }}" {{ in_array($value->id, old('wordCategories')) ? "selected" : "" }} >{{ $value->name }}</option>
                                        @else
                                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Save</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <a href="{{ url('word') }}" class="btn btn-secondary" style="float:right;">Back</a> 
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <!-- <script src="{{ asset('assets/js/plugins/select2/select2-bootstrap.min.css') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2-bootstrap.min.css') }}"></script> -->
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script>
        $('.js-select2').select2()

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'name': {
                    required: true,
                },
                'wordCategories': {
                    required: true,
                }
            },
            messages: {
                'name': {
                    required: 'Name has required',
                },
                'wordCategories': {
                    required: 'Categories has required',
                }
            }
        });
    </script>
@endpush
