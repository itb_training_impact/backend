@extends('layouts.root')

@section('title','Criteria - Edit')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('criteria') }}">Criteria</a>
        <span class="breadcrumb-item active">Edit</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">criteria - Edit</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('criteria/update/'. $criterias->criteria_id) }}" method="post">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="criteria" name="criteria" placeholder="criteria" value="{{ $criterias->criteria }}">
                                <label for="material-email">Criteria <span class="text-danger">*</span></label>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="description" name="description" placeholder="Description" value="{{ $criterias->description }}">
                                <label for="material-email">Description <span class="text-danger">*</span></label>
                            </div>                            
                        </div>
                        <div class="form-group">
                             <div class="form-material">
                                 <select autocomplete="off" class="form-control" id="level" name="level">
                                    <option value="">Choose type ..</option>
                                    <option value="0" {{ $criterias->level == 0 ? "selected" : "" }} >Performance</option>
                                    <option value="1" {{ $criterias->level == 1 ? "selected" : "" }} >R.O.T.I</option>
                                 </select>
                                <label for="material-email">Type <span class="text-danger">*</span></label>
                            </div> 
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Save</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <a href="{{ url('criteria') }}" class="btn btn-secondary" style="float:right;">Back</a>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'criteria': {
                    required: true,
                },
                'level': {
                    required: true,
                }
            },
            messages: {
                'criteria': {
                    required: 'Criteria has required',
                },
                'level': {
                    required: 'Type has required',
                }
            }
        });
    </script>
@endpush
