@extends('layouts.root')

@section('title','Detail Speaker')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <a class="breadcrumb-item" href="{{ url('speaker') }}">Speaker</a>
            <span class="breadcrumb-item active">Detail</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <!-- Table -->
        <div class="row">
            <div class="col-md-6 col-xl-3">
                <a class="block text-center" >
                    <div class="block-content block-content-full">
                        @if (config('app.env') === 'development')               
                        <img src="{{'http://46.101.96.173:81/storage/speaker/'.$speaker->images}}" style="width: 90%">
                        @else
                        <img src="{{asset('assets/img/avatars/avatar0.jpg')}}" style="width: 100%; ">
                        @endif
                    </div>
                    <div class="block-content block-content-full bg-body-light">
                        <div class="font-w600 mb-5">{{ $speaker->name }}</div>
                    </div>
                </a>
            </div>
            <div class="col-md-9">
                <div class="block">
                    <div class="block-header">
                        <h3 class="block-title">Speaker Detail</h3>
                    </div>
                    <div class="block-content" style="padding-bottom: 20px;">
                        <div class="row">
                            <div class="col-6 pb-3">
                                <label>Name</label>
                                <div> {{ $speaker->name }} </div>
                            </div>
                            <div class="col-6 pb-3">
                                <label>Description</label>
                                <div> {{ $speaker->description }} </div>
                            </div>                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <a href="{{ url('speaker') }}" class="btn btn-secondary" style="float:right;">Back</a>
        </div>
    </div>
</div>

@endsection