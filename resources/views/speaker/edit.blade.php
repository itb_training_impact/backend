@extends('layouts.root')

@section('title','Speaker - Edit')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('speaker') }}">Speaker</a>
        <span class="breadcrumb-item active">Edit</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Speaker - Edit</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('speaker/update/'. $speakers->speaker_id) }}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="name" name="name" placeholder="Speaker" value="{{ $speakers->name }}">
                                <label for="material-email">Name <span class="text-danger">*</span></label>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="description" name="description" placeholder="Description of Speaker" value="{{ $speakers->description }}">
                                <label for="material-email">Description <span class="text-danger">*</span></label>
                            </div>                            
                        </div>
                         <div class="form-group">
                             <div class="col-md-6 col-xl-3">
            <a class="block text-center" >
                <div class="block-content block-content-full">
                    @if (config('app.env') === 'development')               
                    <img src="{{'http://46.101.96.173:81/storage/speaker/'.$speakers->images}}" style="width: 90%">
                    @else
                    <img src="{{'http://127.0.0.1:8000/storage/speaker/'.$speakers->images}}" style="width: 100%; ">
                    @endif
                </div>
               
            </a>
        </div>
                            <div class="form-material">
                                <input type="file" autocomplete="off" class="form-control" id="images" name="images" placeholder="Speaker" value="{{ $speakers->images }}">
                                <label for="material-email">Gambar pembicara <span class="text-danger">*</span></label>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Save</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <a href="{{ url('speaker') }}" class="btn btn-secondary" style="float:right;">Back</a>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'name': {
                    required: true,
                },
                
            },
            messages: {
                'name': {
                    required: 'Name has required',
                }
            }
        });
    </script>
@endpush
