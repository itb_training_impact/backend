<!DOCTYPE html>
<html>
<head>
    <title>Email Verification</title>
    <style>
        body{
            font-family: sans-serif;
        }
        .biru-muda , span{
            color: #0cf;
        }
        .biru-tua{
            color: #163797;
        } 
        a{
            color: #0cf;
            text-decoration: none;            
        }
        a:hover{
            color: #163797;    
        }
    </style>
</head>

<body>
<center>
    <img src="{{asset('assets/img/itb.png')}}" alt="Klorofil Logo" style="width: 25%">
</center>
<h3 class="biru-tua"><span class="biru-muda">Hai </span>{{$user['name']}}</h3>

<p>Diberitahukan bahwa pelatihan <b>{{$trainning['trainning_title']}}</b> telah dimulai, silakan masuk kedalam akun Anda, dan mengisi kuisioner <b>
@if($field == 'before')
Sebelum 
@elseif($field == 'during')
Saat
@else
Setelah
@endif
melaksanakan pelatihan</b>, klik link dibawah ini untuk melanjutkan.</p>

@if($field == 'before')
<a href="http://46.101.96.173/trainning/show/{{$trainning['trainning_id']}}/?sessions={{$session['session_id']}}&modules={{$module['module_id']}}"><b><u>{{$trainning['trainning_title']}} - {{$module['name']}}</u></b></a>
@elseif($field == 'during')
<a href="http://46.101.96.173/trainning/during/{{$trainning['trainning_id']}}/?sessions={{$session['session_id']}}&modules={{$module['module_id']}}"><b><u>{{$trainning['trainning_title']}} - {{$module['name']}}</u></b></a>
@else
<a href="http://46.101.96.173/trainning/after/{{$trainning['trainning_id']}}/?sessions={{$session['session_id']}}&modules={{$module['module_id']}}"><b><u>{{$trainning['trainning_title']}} - {{$module['name']}}</u></b></a>
@endif
<br/>
<p class="biru-tua">Salam Hangat<br>Training Impact Team</p>
<br/>
<div style="border-bottom: 2px solid #0cf; border-top: 2px solid #0cf; text-align: center; padding: 2px;">
    <span>E:</span> contact@training-impact.com <span>| W:</span> www.training-impact.com
</div>
<div style="width: 100%; height: 20px; margin-top: 5px; background-color: #00349a;"></div>
</body>

</html>