<!DOCTYPE html>
<html>
<head>
    <title>Notify Training</title>
    <style>
        body{
            font-family: sans-serif;
        }
        .biru-muda , span{
            color: #0cf;
        }
        .biru-tua{
            color: #163797;
        } 
        a{
            color: #0cf;
            text-decoration: none;            
        }
        a:hover{
            color: #163797;    
        }
    </style>
</head>

<body>
<center>
    <img src="{{asset('assets/img/itb.png')}}" alt="Klorofil Logo" style="width: 25%">
</center>
<h3 class="biru-tua"><span class="biru-muda">Hai </span>{{$boss['boss_name']}}</h3>

<p>Diberitahukan bahwa pelatihan <b>{{$trainning['trainning_title']}}</b> telah dimulai, Anda terpilih menjadi <b>Atasan</b> dari {{$user['name']}} , dimohon kesediaanya untuk mengisi kuisioner<b> Sebelum melaksanakan pelatihan</b> yang bertujuan untuk mengukur Performa terhadap {{$user['name']}}. Klik link dibawah ini untuk melanjutkan.</p>

<a href="http://46.101.96.173/checkToken/{{$boss['code_boss']}}/{{$trainning['trainning_id']}}/1/{{$session['session_id']}}/{{$module['module_id']}}"><b><u>{{$trainning['trainning_title']}} - {{$module['name']}}</u></b></a>

<br/>
<p class="biru-tua">Salam Hangat<br>Training Impact Team</p>
<br/>
<div style="border-bottom: 2px solid #0cf; border-top: 2px solid #0cf; text-align: center; padding: 2px;">
    <span>E:</span> contact@training-impact.com <span>| W:</span> www.training-impact.com
</div>
<div style="width: 100%; height: 20px; margin-top: 5px; background-color: #00349a;"></div>
</body>

</html>