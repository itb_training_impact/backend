<!DOCTYPE html>
<html>
<head>
    <title>RESULT TRAINING</title>
    <style>
        body{
            font-family: sans-serif;
            max-width: 90%;
            margin: 0 auto;
        }
        span{
            font-weight: bold;
        }
        span.kiri{
            display: inline-block; 
            width: 105px;
            font-weight: bold;
        }
        span.kanan{
            display: inline-block; 
            width: 160px;
            font-weight: bold;
        }
        table{
            width: 100%;
            border-collapse: collapse;
        }
        table, th, td{
            padding: 2px;
            border: 2px solid;
        }
        .biru-muda , span.foot{
            color: #00b7ff;
        }
        .biru-tua{
            color: #163797;
        }
    </style>
</head>

<body>
    <center>
        <img src="{{public_path('assets/img/itb.png')}}" alt="Klorofil Logo" style="width: 35%">
        <br/>
    </center>

    <div>
        <div style="max-width: 50%; width: 100%; float: left;">
            <p><span class="kiri">Nama</span>: {{$participant['name']}}</p>
            <p><span class="kiri">Pendidikan</span>: -</p>
            <p><span class="kiri">Alamat</span>: -</p>
        </div>
        <div style="max-width: 50%; width: 100%; float: right;">
            <p><span class="kanan">Instansi Perusahaan</span>: {{$participant->company['name']}}</p>
            <p><span class="kanan">Instansi Pendidikan</span>: -</p>
        </div>    
    </div>

        <div style="clear: both; width: 80%; margin:0 auto; text-align: center; font-weight: bold;">
            <p>SEBELUM MELAKSANAKAN PELATIHAN {{$trainning['trainning_title']}}</p>
        </div>

        <div>
            <div style="max-width: 50%; width: 100%; float: left;">
                <p><span class="kiri">Sesi</span>: {{$session['name']}}</p>
            </div>
            <div style="max-width: 50%; width: 100%; float: right;">
                <p><span class="kanan">Pembicara</span>: {{$session->speaker['name']}}</p>
            </div>
        </div>

        <div>
            <p><span class="kiri">Deksripsi</span>: {{$session['description']}}</p>
        </div>
    
        <div style="width: 80%; margin:0 auto;">
            <p style="margin-left: -15px;"><span class="kiri" style="width: 120px;">1. Module</span> : {{$module['name']}}</p>
            <p><span class="kiri">Deksripsi</span> : {{$module['description']}}</p>
            @if(!empty($data['reaction']->response))
                    @php
                        $data_result_arr = array();
                    @endphp    
                    @foreach ($data['reaction_group'] as $item)                                            
                        @php
                            $devide = App\Helper\Application::get_calculate('reaction', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id)[0]->result;
                            $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                            array_push($data_result_arr,$calculate)
                        @endphp                                                                                    
                    @endforeach
                @if(array_sum($data_result_arr) != 0)
                    <p><span class="kiri">A. reaction </span>: {{round(array_sum($data_result_arr)/(100*count($data_result_arr))*100,2)}}%</p>
                      <table>
                    <tr>
                        <th colspan="3">DIMENSI</th>
                    </tr>
                @endif
              
                
                    @php
                        $data_arr = array();
                    @endphp                           
                    @foreach ($data['reaction_group'] as $item)                                                                                            
                        @php
                            $devide = App\Helper\Application::get_calculate('reaction', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id)[0]->result;
                            $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                            array_push($data_arr,$calculate)
                        @endphp            
                    <tr>
                        <td style="width: 5%">{{$loop->iteration}}</td>
                        <td style="width: 45%">{{ $item->group }}</td>
                        <td style="width: 45%">{{ round($calculate,2) }} %</td>
                    </tr>
                    @endforeach
                </table>
            @endif
            
            @if(!empty($data['learning']->response))
                    @php
                        $data_result_arr = array();
                    @endphp    
                    @foreach ($data['learning_group'] as $item)                                            
                        @php
                            $devide = App\Helper\Application::get_calculate('learning', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id)[0]->result;
                            $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                            array_push($data_result_arr,$calculate)
                        @endphp                                                                                    
                    @endforeach
                @if(array_sum($data_result_arr) != 0)
                    @if(empty($data['reaction']->response))
                        <p><span class="kiri">A. 
                    @else
                        <p><span class="kiri">B. 
                    @endif
                    Learning </span>: {{round(array_sum($data_result_arr)/(100*count($data_result_arr))*100,2)}}%</p>
                     <table>
                    <tr>
                        <th colspan="3">DIMENSI</th>
                    </tr>
                @endif
               
                
                    @php
                        $data_arr = array();
                    @endphp                           
                    @foreach ($data['learning_group'] as $item)                                                                                            
                        @php
                            $devide = App\Helper\Application::get_calculate('learning', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id)[0]->result;
                            $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                            array_push($data_arr,$calculate)
                        @endphp            
                    <tr>
                        <td style="width: 5%">{{$loop->iteration}}</td>
                        <td style="width: 45%">{{ $item->group }}</td>
                        <td style="width: 45%">{{ round($calculate,2) }} %</td>
                    </tr>
                    @endforeach
                </table>
            @endif

            @if(!empty($data['behaviour_group']))
                    @php
                        $data_arr_behaviour = array();
                    @endphp    
                    @foreach ($data['behaviour_group'] as $item)                                            
                        @php
                            $devide = App\Helper\Application::get_calculate('behaviour', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id, $item->formulation_id)[0]->result;
                            $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                            array_push($data_arr_behaviour,$calculate)
                        @endphp                                                                                    
                    @endforeach
                @if(array_sum($data_arr_behaviour) != 0)
                    @if(empty($data['reaction']->response) && empty($data['learning']->response))
                        <p><span class="kiri">A. 
                    @elseif(empty($data['learning']->response) || empty($data['reaction']->response))
                        <p><span class="kiri">B. 
                    @else
                        <p><span class="kiri">C.
                    @endif
                     behaviour </span>: {{round(array_sum($data_arr_behaviour)/(100*count($data_arr_behaviour))*100,2)}}%</p>
                       <table>
                    <tr>
                        <th colspan="3">DIMENSI</th>
                    </tr>
                @endif
              
                
                    @php
                        $data_arr_behaviour = array();
                    @endphp                           

                    @foreach ($data['behaviour_group'] as $item)                                                
                        @php
                            $devide = App\Helper\Application::get_calculate('behaviour', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id, $item->formulation_id)[0]->result;
                            $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                            array_push($data_arr_behaviour,$calculate)
                        @endphp          
                    <tr>
                        <td style="width: 5%">{{$loop->iteration}}</td>
                        <td style="width: 45%">{{ $item->formulation_name }}</td>
                        <td style="width: 45%">{{ round($calculate,2) }} %</td>
                    </tr>
                    @endforeach
                </table>
            @endif
        </div>
    <br/>
    <br/>

    <div style="border-bottom: 2px solid #00b7ff; border-top: 2px solid #00b7ff; text-align: center; padding: 2px;">
        <span>E:</span> contact@training-impact.com <span>| W:</span> www.training-impact.com
    </div>
    <div style="width: 100%; height: 20px; margin-top: 5px; background-color: #00349a;"></div>
</body>

</html>