@extends('layouts.root')

@section('title','session - Edit')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('session') }}">session</a>
        <span class="breadcrumb-item active">Edit</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
             <div class="block">
                    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                        <h3 class="block-title">Training</h3>
                    </div>
                       <form class="form-level" action="{{ url('session/update/'. $sessions->session_id) }}" method="post">
                          <input type="hidden" name="_method" value="PUT">
                        @csrf
                 <div class="block-content">
                        <!-- Dipilih -->
                        <div class="form-group">
                            <div class="form-material">
                               <select autocomplete="off" class="js-select2 form-control" placeholder="Choose trainning .." id="trainning_id" name="trainning_id">
                                 @php
                                        $trainning_id = explode(",",$sessions->trainning_id);
                                        @endphp
                                        <option value="">Training</option>
                                    @foreach($trainnings as $key =>$value)
                                        <option value="{{$value->trainning_id}}" {{ in_array($value->trainning_id,$trainning_id) ? "selected" : "" }} >{{$value->trainning_title}}</option>
                                    @endforeach
                                    
                                    </select>
                            <label for="material-email">Select training<span class="text-danger">*</span></label>
                        </div>
                    </div>
                </div>
            </div>
            

           <div class="block">
                    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                        <h3 class="block-title">Title of session</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" class="form-control" name="name" placeholder="Title ..." autofocus="" value="{{ $sessions->name }}">
                                <label for="">Title <span class="text-danger">*</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-material">
                                <textarea name="description" class="form-control" id="description" cols="30" rows="10" placeholder="Description ...">{{ $sessions->description }}</textarea>
                                <label for="">Description  <span class="text-danger">*</span></label>
                            </div>
                        </div>
                </div>

              <div class="block">
                    <div class="block-header block-header-default" style="background-color: #f5f6f7;>
                        <h3 class="block-title">CHOISE SPEAKER</h3>
                    </div>
                        <div class="block-content">
                            <div class="form-group">
                                <div class="form-material">
                                    <select autocomplete="off" class="js-select2 form-control" placeholder="Choose formula .." id="speaker_id" name="speaker_id[]" multiple="">
                                        <option value="">Choose Speaker ..</option>
                                        @php
                                        $speaker_id = explode(",",$sessions->speaker_id);
                                        @endphp
                                    @foreach($speakers as $key =>$value)
                                        <option value="{{$value->speaker_id}}" {{ in_array($value->speaker_id,$speaker_id) ? "selected" : "" }} >{{$value->name}}</option>
                                    @endforeach
                                    
                                    </select>
                                <label for="material-email">Select speaker <span class="text-danger">*</span></label>
                            </div> 
                        </div>
                    </div>
                </div>

                 <div class="block">
                <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                    <h3 class="block-title">SECTION</h3>
                </div>
                <div class="block-content">
                       <!--  <input type="hidden" name="trainning_detail_periode[]" value="1"> -->
                        <div class="form-group row">                            
                            <div class="col-12">
                                <div class="custom-controls-stacked">
                                      @if($sessions->where(['session_id'=>$sessions['session_id'],'before'=>1])->get()->count()>0)
                                       <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="before" value="1" checked="">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Before</span>
                                    </label>
                                      @else
                                       <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="before" value="1">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Before</span>
                                    </label>
                                      @endif
                                    @if($sessions->where(['session_id'=>$sessions['session_id'],'during'=>1])->get()->count()>0)
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="during" value="1" checked="">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">During</span>
                                    </label>
                                    @else
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="during" value="1">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">During</span>
                                    </label>
                                    @endif
                                     @if($sessions->where(['session_id'=>$sessions['session_id'],'after'=>1])->get()->count()>0)
                                       <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="after" value="1" checked="">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">After</span>
                                    </label>
                                     @else
                                       <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="after" value="1">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">After</span>
                                    </label>
                                     @endif
                                   
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- END Static Labels -->
          
        </div>
            <div class="col-12">
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="reset" class="btn btn-secondary">Reset</button>
                    <a href="{{ url('session') }}" class="btn btn-secondary" style="float:right;">Back</a>
                </div>
            </div>
                    </form>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
          jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
        });
        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'name': {
                    required: true,
                },
                
            },
            messages: {
                'name': {
                    required: 'Name has required',
                }
            }
        });
    </script>
@endpush
