@extends('layouts.root')

@section('title','Session Trainning')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="{{ url('home') }}">Dashboard</a>
            <a class="breadcrumb-item" href="{{ url('session') }}">Session</a>
            <span class="breadcrumb-item active">Session</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Detail Session</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <div class="form-group">
                    <label for="">Title</label>
                    <div> {{ $sessions->name }} </div>
                </div>
                <div class="form-group">
                    <label for="">Description</label>
                    <div> {{ $sessions->description == null ? "-" :  $sessions->description }} </div>
                </div>
                <div class="form-group">
                    <label for="">Trainning</label>
                    <div> {{ $sessions->trainning['trainning_title'] }} </div>
                </div>
            </div>
        </div>

         <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Detail Section</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <div class="form-group">
                    <label for=""><b>Before</b></label>
                    @if($sessions->where(['session_id'=>$sessions['session_id'],'before'=>1])->get()->count()>0)
                    <div>available</div>
                    @else
                    <div>not available</div>
                    @endif
                </div>
                <div class="form-group">
                    <label for=""><b>During</b></label>
                    @if($sessions->where(['session_id'=>$sessions['session_id'],'during'=>1])->get()->count()>0)
                    <div>available</div>
                    @else
                    <div>not available</div>
                    @endif
                </div>
                <div class="form-group">
                    <label for=""><b>After</b></label>
                     @if($sessions->where(['session_id'=>$sessions['session_id'],'after'=>1])->get()->count()>0)
                    <div>available</div>
                    @else
                    <div>not available</div>
                    @endif
                </div>
            </div>
        </div>

       <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Speaker</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <table id="datatable" class="table table-vcenter table-stripped table-bordered">
                    <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th>Name of Speaker</th>
                            <th width="10%"></th>
                        </tr>
                    </thead>
                    <tbody>
                     @if(sizeof($speakers) > 0)
                            @foreach($speakers as $key => $value) 
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>
                                        <a href="{{ url('speaker/detail/'.$value->speaker_id) }}" class="btn btn-sm btn-primary" style="height: 19px"><i class="fa fa-search"></i>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="3">Trainning not available.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-12">
            <a href="{{ url('session') }}" class="btn btn-secondary" style="float:right;">Back</a>
        </div>

       
        <!-- END Table -->
    </div>
</div>

@include('modal_delete')
@endsection

@push('script')


<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.css') }}">
<script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js') }} "></script>
<script src="{{ asset('assets/js/pages/be_tables_datatables.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable({
            "pageLength": 50,
            "lengthChange": true,
            "searching": true
        });
    });

    $('.btn-delete').click(function(){
        var action = $(this).data('action');

        $('#modalDelete').modal('show');
        $('.modal-title').html('Delete Level');

        $('#formDelete').attr('action',action);
        $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
    });
</script>



@endpush
