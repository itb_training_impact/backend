Hello <strong>{{ $main->receiver_name }}</strong>,
<br/><br/>
Pembayaran Anda untuk pesanan :
<br/><br/>
<table border="1" cellspacing="0" cellpadding="10">
	<tr>
		<td>No. Pesanan</td>
		<td>:</td>
		<td>{{ $main->id }}</td>
	</tr>
	<tr>
		<td>Tanggal</td>
		<td>:</td>
		<td>{{ date("d F Y H:i:s", strtotime($main->created)) }}</td>
	</tr>
	<tr>
		<td>Produk</td>
		<td>:</td>
		<td>{{ $main->product_name }}</td>
	</tr>
	<tr>
		<td>Jumlah</td>
		<td>:</td>
		<td>{{ $main->amount_in_currency }}</td>
	</tr>
	<tr>
		<td>Subtotal</td>
		<td>:</td>
		<td>Rp. @convert($main->subtotal)</td>
	</tr>
	<tr>
		<td>Biaya</td>
		<td>:</td>
		<td>Rp. @convert($main->fee)</td>
	</tr>
	<tr>
		<td>Total</td>
		<td>:</td>
		<td>Rp. @convert($main->grand_total)</td>
	</tr>
</table>
<br/>
telah <b>dibatalkan</b>. Pengembalian dana akan dilakukan paling lambat 7 x 24 jam.
