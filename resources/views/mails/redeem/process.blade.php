Hello <strong>{{ $main->receiver_name }}</strong>,
<br/><br/>
Permohonan redeem Anda :
<br/><br/>
<table border="1" cellspacing="0" cellpadding="10">
	<tr>
		<td>Tanggal</td>
		<td>:</td>
		<td>{{ date("d F Y H:i:s", strtotime($main->date)) }}</td>
	</tr>
	<tr>
		<td>Produk</td>
		<td>:</td>
		<td>{{ $main->product_name }}</td>
	</tr>
	<tr>
		<td>Harga</td>
		<td>:</td>
		<td>{{ $main->amount }}</td>
	</tr>
</table>
<br/>
telah kami <b>proses</b>.
<br/><br/>
@if($main->code)
<center>Kode Voucher</center>
<center><h2>{{ $main->code }}</h2></center>
<br/><br/>
@endif
Terima kasih telah menggunakan website kami.