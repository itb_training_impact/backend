@extends('layouts.root')

@section('title','Behaviour')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Behaviour</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            Behaviour Data Table
        </h2>

        {{-- search --}}

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Search Behaviour</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <form action="{{ url('behaviour') }}" method="GET" class="form form-inline">
                    @csrf
                    <div>
                        <select name="group_id" id="group_id" class="form-control">
                            <option value="">Group</option>
                            @foreach($group as $key => $value)
                                <option value="{{ $value->group_id }}" {{ Request::get('group_id') ==  $value->group_id ? "selected" : "" }} >{{ $value->group }}</option>
                            @endforeach
                        </select>
                        <input type="text" class="form-control" name="behaviour_code" value="{{ Request::has('behaviour_code') ? Request::get('behaviour_code') : '' }}" placeholder="Enter Question Code ...">
                        <input type="text" class="form-control" name="behaviour_question" value="{{ Request::has('behaviour_question') ? Request::get('behaviour_question') : '' }}" placeholder="Enter Question ...">
                        <select name="type_id" id="type_id" class="form-control">
                            <option value="">Type Answer</option>
                            @foreach($type as $key => $value)
                                <option value="{{ $value->type_id }}" {{ Request::get('type_id') ==  $value->type_id ? "selected" : "" }}>{{ $value->type }}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-alt-primary">Search</button>
                        <a href="{{ url('behaviour') }}" class="btn btn-secondary">Refresh</a>
                    </div>
                </form>
            </div>
        </div>

        <a href="{{ url('behaviour/create') }}" class="btn btn-success mb-3"> <i class="fa fa-plus"></i> &nbsp; Create Question</a>
        <button class="btn btn-success btn-import mb-3" type="button"> <i class="fa fa-upload"></i> &nbsp; Import Question</button>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">List of Behaviour</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <table id="datatable" class="table table-vcenter table-stripped table-bordered">
                    <thead>
                        <tr>
                            <th width="5%"></th>                            
                            <th>GROUP</th>
                            <th>QUESTION CODE</th>
                            <th>QUESTION</th>
                            <th>TYPE ANSWER</th>
                            <th>TRAINING</th>
                            <th>STATUS</th>
                            <th width="15%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(sizeof($behaviour) > 0)
                            @foreach($behaviour->where('behaviour_is_default',1) as $key => $value)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>                                    
                                    <td>{{ isset($value->group) ? $value->group->group : "" }}</td>
                                    <td>{{ $value->behaviour_code }}</td>
                                    <td>{{ $value->behaviour_question }}</td>
                                    <td>{{ isset($value->type) ? $value->type->type : "" }}</td>
                                    <td class="text-center">
                                        {!! $value->totalTrainning($value->behaviour_id) ? 
                                        "<button type='button' style='height: 19px' class='btn btn-primary btn-sm btn-trainning' data-id='". $value->behaviour_id ."'>" .$value->totalTrainning($value->behaviour_id) ." Trainnings </button>" 
                                        : "-" !!}
                                    </td>                                   
                                    @if($value->behaviour_is_default == 0)
                                        <td>custom</td>
                                    @else
                                        <td>default</td>
                                    @endif
                                    <td>
                                        <a href="{{ url('behaviour/detail/'. $value->behaviour_id) }}" class="btn btn-sm btn-primary" style="height: 19px"><i class="fa fa-search"></i></a>
                                        <a href="{{ url('behaviour/edit/'. $value->behaviour_id) }}" class="btn btn-sm btn-warning" style="height: 19px"><i class="fa fa-pencil"></i></a>
                                        <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('behaviour/delete/'. $value->behaviour_id) }}" class="btn btn-sm btn-danger btn-xs btn-delete" style="height: 19px">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="6">Behaviour not available.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                  {{$behaviour->links()}}
            </div>
        </div>
        <!-- END Table -->
    </div>
</div>

@include('modal_delete')
@include('modal_trainning')
<div class="modal fade" id="modalImportBehaviour" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document" aria-hidden="true">
        <form id="formImportBehaviour" method="POST" action="{{ url('behaviour/import') }}" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title modal-title">Import Behaviour</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="form-group">
                            <label for="import" class="control-label">Import <span class="text-danger">*</span></label> <br>
                            <input type="file" name="import" required> <br>
                            <a href="{{ url('imports/import_behaviours.xlsx') }}">Download format import</a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-modal-primary">Import</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@push('script')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.css') }}">
    <script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js') }} "></script>
    <script src="{{ asset('assets/js/pages/be_tables_datatables.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                "pageLength": 15,
                "lengthChange": false,
                "paging": false,
                "searching": true
            });
        } );

        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });

        $('.btn-import').click(function(){
            var action = $(this).data('action');
            $('#modalImportBehaviour').modal('show');
        });

        $('.btn-trainning').click(function(){
            var id = $(this).data('id');
            var url = "{{ url('learning/trainning') }}" + "/" + id;

            $.get(url, function(data){
                console.log(data);

                $('#trainning-section').html(data);
                $('#modalTrainning').modal('show');
            });
        });
    </script>
@endpush
