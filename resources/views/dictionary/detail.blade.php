@extends('layouts.root')

@section('title','Detail Trainning')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <a class="breadcrumb-item" href="{{ url('dictionary') }}">Dictionary</a>
            <span class="breadcrumb-item active">Detail</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Detail Dictionary</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <div class="form-group">
                    <label for="">Code</label>
                    <div> {{ $dictionary->code }} </div>
                </div>
                <div class="form-group">
                    <label for="">Name</label>
                    <div> {{ $dictionary->name }} </div>
                </div>
                <div class="form-group">
                    <label for="">Status</label>
                    <div> {{ $dictionary->status ? 'Active' : 'Inactive' }} </div>
                </div>
                <div class="form-group">
                    <label for="">Description</label>
                    <div> {{ $dictionary->description }} </div>
                </div>
            </div>
        </div>
        <a href="{{ url('dictionary') }}" class="btn btn-secondary" style="float:right;">Back</a>
    </div>
</div>

@endsection