@extends('layouts.root')

@section('title','Dictionary - New')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('dictionary') }}">Dictionary</a>
        <span class="breadcrumb-item active">New</span>
    </nav>

    @if ($errors->any())
        <div class="alert alert-danger m-t-20">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Dictionary - New</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('dictionary') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="code" name="code" placeholder="Code" value="{{ old('code') }}">
                                <label for="material-email">Code <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name') }}">
                                <label for="material-email">Name <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="material-status">Status <span class="text-danger">*</span></label>
                            <div class="form-check">
                                <input class="" type="radio" name="status" value=1>
                                <label class="form-check-label">Active</label>
                            </div>
                            <div class="form-check">
                                <input class="" type="radio" name="status" value=0 checked>
                                <label class="form-check-label">Inactive</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <textarea class="form-control" id="description" name="description" placeholder="Description">{{ old('description') }}</textarea>
                                <label for="material-email">Description <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Save</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <a href="{{ url('dictionary') }}" class="btn btn-secondary" style="float:right;">Back</a>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'code': {
                    required: true,
                },
                'name': {
                    required: true,
                },
                'description': {
                    required: true,
                }
            },
            messages: {
                'code': {
                    required: 'Code has required',
                },
                'name': {
                    required: 'Name has required',
                },
                'description': {
                    required: 'Description has required',
                },
            }
        });
    </script>
@endpush
