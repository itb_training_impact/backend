@extends('layouts.root')

@section('title','Registration Form')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Registration Form</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
    		@if(Session::get('status') == '200')
    	        <div class="alert alert-success alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @elseif(Session::get('status') == 'err')
    			<div class="alert alert-danger alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @endif
        @endif

        <h2 class="content-heading">
            Registration Form Data Table
        </h2>

        {{-- search --}}

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Search Forms</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <form method="GET" class="form form-inline">
                    @csrf
                    <div>
                        <input type="text" class="form-control" name="name" value="{{ Request::has('name') ? Request::get('name') : '' }}" placeholder="Enter Name ...">
                        <input type="text" class="form-control" name="label" value="{{ Request::has('label') ? Request::get('label') : '' }}" placeholder="Enter Label ...">
                        <button type="submit" class="btn btn-alt-primary">Search</button>
                        <a href="{{ url('registration') }}" class="btn btn-secondary">Refresh</a>
                    </div>
                </form>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">List of Forms</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <table class="table table-vcenter table-stripped">
                    <thead>
                        <tr role="row">
                            <th>NO</th>
                            <th>NAME</th>
                            <th>LABEL</th>
                            <th>DESCRIPTION</th>
                            <th>STATUS</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(sizeof($registrationForms) > 0)
                            @foreach($registrationForms as $key => $value)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->label }}</td>
                                    <td>{{ $value->description }}</td>
                                    <td>
                                        <form id="switch_status-{{ $value->id }}" action="{{ url('registration/switch_status') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{ $value->id }}">
                                            <input type="checkbox" data-id="{{ $value->id }}" class="status" name="status" {{ $value->status == 1 ? 'checked' : '' }}>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="6">Reaction not available.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Table -->
    </div>
</div>

@endsection

@push('script')
    <script type="text/javascript">
        $('.status').change(function(){
            $('#switch_status-' + $(this).data('id')).submit();
        })
    </script>
@endpush
