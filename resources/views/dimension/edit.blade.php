@extends('layouts.root')

@section('title','Dimension - Edit')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('dimension') }}">Dimension</a>
        <span class="breadcrumb-item active">Edit</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Dimension - Edit</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('dimension/update/'. $groups->group_id) }}" method="post">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="group" name="group" placeholder="Dimension" value="{{ $groups->group }}">
                                <label for="material-email">Dimension <span class="text-danger">*</span></label>
                            </div>                            
                        </div>
                        <div class="form-group">
                             <div class="form-material">
                                 <select autocomplete="off" class="form-control" placeholder="Choose formula .." id="group" name="formula_id">
                                    <option value="">Choose Formula ..</option>
                                    @foreach($formulas as $result)
                                        <option value="{{$result->formula_id}}" {{ $groups->formula_id == $result->formula_id ? "selected" : "" }} >{{$result->formula}}</option>
                                    @endforeach
                                 </select>
                                <label for="material-email">Formula <span class="text-danger">*</span></label>
                            </div> 
                        </div>

                        <div class="form-group">
                             <div class="form-material">
                                 <select autocomplete="off" class="form-control" id="is_reaction" name="is_reaction">
                                    <option value="">Choose type ..</option>
                                    <option value="0" {{ $groups->is_reaction == 0 ? "selected" : "" }} >Reaction</option>
                                    <option value="1" {{ $groups->is_reaction == 1 ? "selected" : "" }} >Learning</option>
                                    <option value="2" {{ $groups->is_reaction == 2 ? "selected" : "" }} >Behaviour</option>
                                 </select>
                                <label for="material-email">Type <span class="text-danger">*</span></label>
                            </div> 
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Save</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <a href="{{ url('dimension') }}" class="btn btn-secondary" style="float:right;">Back</a>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'group': {
                    required: true,
                },
                'formula_id': {
                    required: true,
                },
                'is_reaction': {
                    required: true,
                }
            },
            messages: {
                'group': {
                    required: 'Dimension has required',
                },
                'formula_id': {
                    required: 'Formula has required',
                },
                'is_reaction': {
                    required: 'Type has required',
                }
            }
        });
    </script>
@endpush
