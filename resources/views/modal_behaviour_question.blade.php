<div class="modal fade" id="modalBehaviourBoss" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" aria-hidden="true">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-content">
                    <div class="row">
                        <div class="col-12">
                            <a href="{{ url('trainning/export/'. $participant->id . '/' . $trainningID . '/' . $sessionID . '/' . $moduleID . '/' . Request::get('active') . '/' . 'boss') }}" class="btn btn-sm btn-success"  style="float: right;"><i class="fa fa-download"></i> Download Excel</a>
                        </div>
                        <div class="col-sm-12 modal-text" id="trainning-section"> 
                          @if (!empty($submit) && !empty($boss))
                            <div class="row">
                              <div class="col-sm-6">
                                <h4 class="font-w400">Kompetensi Result</h4>    
                                @php
                                  $data_arr_behaviour = array();
                                @endphp                           
                                @foreach ($behaviour_bosses as $item)                                                                                            
                                    @php
                                      $devide = App\Helper\Application::get_calculate('behaviour', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id, $item->formulation_id)[0]->result;
                                      $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                                      array_push($data_arr_behaviour,$calculate)
                                    @endphp                                                
                                    <h5>{{ $item->formulation_name }}</h5>
                                    <div class="progress push">
                                        <div class="progress-bar progress-bar-striped" role="progressbar" style="width: {{ round($calculate,2) }}%;" aria-valuenow="{{ round($calculate,2) }}" aria-valuemin="0" aria-valuemax="100">
                                            <span class="progress-bar-label">{{ round($calculate,2) }}%</span>
                                        </div>
                                    </div>
                                @endforeach   
                              </div>
                              <div class="col-sm-6">
                                <!-- Lines Chart -->                                            
                                <div class="block">
                                  @if(array_sum($data_arr_behaviour) != 0)
                                    <canvas id="js-chartjs-bars-behaviour-bosses" data-calculation="{{round(array_sum($data_arr_behaviour)/(100*count($data_arr_behaviour))*100,2)}}"></canvas>
                                  @else
                                    <canvas id="js-chartjs-bars-behaviour-bosses" data-calculation="0"></canvas>
                                  @endif
                                </div>
                                <!-- END Lines Chart -->                 
                              </div>
                            </div>
                                <div class="pull-right">
                                    <a href="{{ url('trainning/export_qna/'. $participant->id . '/' . $trainningID . '/' . $sessionID . '/' . $moduleID . '/' . Request::get('active') . '/boss') }}" class="btn btn-sm btn-success"  style="float: right;"><i class="fa fa-download"></i>Download Excel question and answer</a>
                                </div>
                                <h4 class="font-w400">Question and Answer from  {{ $boss->boss_name }}</h4>                          
                                @foreach($submit->where('level_intermediary',1) as $sub)
                                    <p>{{$loop->iteration}} . {{$sub->behaviour['behaviour_question']}}</p>
                                    <p><strong>							
                                        @if(!empty($sub->behaviour['behaviour_answer']) && $sub->behaviour['type_id'] == 1)
                                            {{ explode(",", $sub->behaviour['behaviour_answer'])[0] }}
                                                @for($i = 0; $i < $sub->
                                                    behaviour['behaviour_rating_finish']; $i++)
                                                    <td>
                                                        <label
                                                            class="css-control css-control-primary css-radio">
                                                            <input type="radio"
                                                                class="css-control-input" disabled
                                                                value="{{ $i+1 }}" {{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer == $i+1 ? 'checked' : '' : $sub->submit_response == $i+1 ? 'checked' : ''}} > 
                                                            <span class="css-control-indicator"></span>
                                                        </label>
                                                    </td>
                                                @endfor	
                                            {{ explode(",", $sub->behaviour['behaviour_answer'])[1] }}
                                        @else
                                            {{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer : $sub->submit_response}}
                                        @endif
                                        </strong></p>
                                @endforeach  
                            @endif                          
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalBehaviourPartner" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" aria-hidden="true">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-content">
                    <div class="row">
                        <div class="col-12">
                            <a href="{{ url('trainning/export/'. $participant->id . '/' . $trainningID . '/' . $sessionID . '/' . $moduleID . '/' . Request::get('active') . '/' . 'partner') }}" class="btn btn-sm btn-success"  style="float: right;"><i class="fa fa-download"></i> Download Excel</a>
                        </div>
                        <div class="col-sm-12 modal-text" id="trainning-section">                        
                          @if (!empty($submit) && !empty($partner))
                          <div class="row">
                            <div class="col-sm-6">
                              <h4 class="font-w400">Kompetensi Result</h4>    
                              @php
                                $data_arr_behaviour_partners = array();
                              @endphp                           
                              @foreach ($behaviour_partners as $item)                                                                                            
                                  @php
                                    $devide = App\Helper\Application::get_calculate('behaviour', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id, $item->formulation_id)[0]->result;
                                    $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                                    array_push($data_arr_behaviour_partners,$calculate)
                                  @endphp                                                
                                  <h5>{{ $item->formulation_name }}</h5>
                                  <div class="progress push">
                                      <div class="progress-bar progress-bar-striped" role="progressbar" style="width: {{ round($calculate,2) }}%;" aria-valuenow="{{ round($calculate,2) }}" aria-valuemin="0" aria-valuemax="100">
                                          <span class="progress-bar-label">{{ round($calculate,2) }}%</span>
                                      </div>
                                  </div>
                              @endforeach   
                            </div>
                            <div class="col-sm-6">
                              <!-- Lines Chart -->                                            
                              <div class="block">
                                @if(array_sum($data_arr_behaviour_partners) != 0)
                                  <canvas id="js-chartjs-bars-behaviour-partners" data-calculation="{{round(array_sum($data_arr_behaviour_partners)/(100*count($data_arr_behaviour_partners))*100,2)}}"></canvas>
                                @else
                                  <canvas id="js-chartjs-bars-behaviour-partners" data-calculation="0"></canvas>
                                @endif
                              </div>
                              <!-- END Lines Chart -->  
                            </div>
                          </div>
                                <div class="pull-right">
                                    <a href="{{ url('trainning/export_qna/'. $participant->id . '/' . $trainningID . '/' . $sessionID . '/' . $moduleID . '/' . Request::get('active') . '/partner') }}" class="btn btn-sm btn-success"  style="float: right;"><i class="fa fa-download"></i>Download Excel question and answer</a>
                                </div>              
                                <h4 class="font-w400">Question and Answer from  {{ $partner->partner_name }}</h4>                          
                                @foreach($submit->where('level_intermediary',2) as $sub)
                                    <p>{{$loop->iteration}} . {{$sub->behaviour['behaviour_question']}}</p>
                                    <p><strong>             
                                        @if(!empty($sub->behaviour['behaviour_answer']) && $sub->behaviour['type_id'] == 1)
                                            {{ explode(",", $sub->behaviour['behaviour_answer'])[0] }}
                                                @for($i = 0; $i < $sub->
                                                    behaviour['behaviour_rating_finish']; $i++)
                                                    <td>
                                                        <label
                                                            class="css-control css-control-primary css-radio">
                                                            <input type="radio"
                                                                class="css-control-input" disabled
                                                                value="{{ $i+1 }}" {{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer == $i+1 ? 'checked' : '' : $sub->submit_response == $i+1 ? 'checked' : ''}} > 
                                                            <span class="css-control-indicator"></span>
                                                        </label>
                                                    </td>
                                                @endfor 
                                            {{ explode(",", $sub->behaviour['behaviour_answer'])[1] }}
                                        @else
                                            {{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer : $sub->submit_response}}
                                        @endif
                                        </strong></p>
                            @endforeach  
                          @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalBehaviourSubordinate" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" aria-hidden="true">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-content">
                    <div class="row">
                        <div class="col-12">
                            <a href="{{ url('trainning/export/'. $participant->id . '/' . $trainningID . '/' . $sessionID . '/' . $moduleID . '/' . Request::get('active') . '/' . 'subordinate') }}" class="btn btn-sm btn-success"  style="float: right;"><i class="fa fa-download"></i> Download Excel</a>
                        </div>
                        <div class="col-sm-12 modal-text" id="trainning-section">                        
                          @if (!empty($submit) && !empty($subordinate)))
                          <div class="row">
                            <div class="col-sm-6">
                              <h4 class="font-w400">Kompetensi Result</h4>    
                              @php
                                $data_arr_behaviour_subordinates = array();
                              @endphp                           
                              @foreach ($behaviour_subordinates as $item)                                                                                            
                                  @php
                                    $devide = App\Helper\Application::get_calculate('behaviour', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id, $item->formulation_id)[0]->result;
                                    $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                                    array_push($data_arr_behaviour_subordinates,$calculate)
                                  @endphp                                                
                                  <h5>{{ $item->formulation_name }}</h5>
                                  <div class="progress push">
                                      <div class="progress-bar progress-bar-striped" role="progressbar" style="width: {{ round($calculate,2) }}%;" aria-valuenow="{{ round($calculate,2) }}" aria-valuemin="0" aria-valuemax="100">
                                          <span class="progress-bar-label">{{ round($calculate,2) }}%</span>
                                      </div>
                                  </div>
                              @endforeach  
                            </div>
                            <div class="col-sm-6">
                              <!-- Lines Chart -->                                            
                              <div class="block">
                                @if(array_sum($data_arr_behaviour_subordinates) != 0)
                                  <canvas id="js-chartjs-bars-behaviour-subordinates" data-calculation="{{round(array_sum($data_arr_behaviour_subordinates)/(100*count($data_arr_behaviour_subordinates))*100,2)}}"></canvas>
                                @else
                                  <canvas id="js-chartjs-bars-behaviour-subordinates" data-calculation="0"></canvas>
                                @endif
                              </div>
                              <!-- END Lines Chart -->                 
                            </div>
                          </div>
                                <div class="pull-right">
                                    <a href="{{ url('trainning/export_qna/'. $participant->id . '/' . $trainningID . '/' . $sessionID . '/' . $moduleID . '/' . Request::get('active') . '/subordinate') }}" class="btn btn-sm btn-success"  style="float: right;"><i class="fa fa-download"></i>Download Excel question and answer</a>
                                </div>
                                <h4 class="font-w400">Question and Answer from  {{ $subordinate->subordinate_name }}</h4>                          
                                @foreach($submit->where('level_intermediary',3) as $sub)
                                    <p>{{$loop->iteration}} . {{$sub->behaviour['behaviour_question']}}</p>
                                    <p><strong>             
                                        @if(!empty($sub->behaviour['behaviour_answer']) && $sub->behaviour['type_id'] == 1)
                                            {{ explode(",", $sub->behaviour['behaviour_answer'])[0] }}
                                                @for($i = 0; $i < $sub->
                                                    behaviour['behaviour_rating_finish']; $i++)
                                                    <td>
                                                        <label
                                                            class="css-control css-control-primary css-radio">
                                                            <input type="radio"
                                                                class="css-control-input" disabled
                                                                value="{{ $i+1 }}" {{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer == $i+1 ? 'checked' : '' : $sub->submit_response == $i+1 ? 'checked' : ''}} > 
                                                            <span class="css-control-indicator"></span>
                                                        </label>
                                                    </td>
                                                @endfor 
                                            {{ explode(",", $sub->behaviour['behaviour_answer'])[1] }}
                                        @else
                                            {{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer : $sub->submit_response}}
                                        @endif
                                        </strong></p>
                            @endforeach    
                          @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  new Chart(document.getElementById("js-chartjs-bars-behaviour-bosses"), {
    type: 'bar',
    data: {
        labels: ["Behaviour"],
        datasets: [{
            label: "Behaviour "+$("#js-chartjs-bars-behaviour-bosses").attr('data-calculation')+" %",
            backgroundColor: "#71ecf8",
            data: [
                $("#js-chartjs-bars-behaviour-bosses").attr('data-calculation')
            ]
        }]
    },
    options: {
        title: {
            display: true
        },
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    suggestedMin: 0, // minimum will be 0, unless there is a lower value.
                    beginAtZero: true // minimum value will be 0.
                }
            }]
        }
    }
});
  new Chart(document.getElementById("js-chartjs-bars-behaviour-partners"), {
    type: 'bar',
    data: {
        labels: ["Behaviour"],
        datasets: [{
            label: "Behaviour "+$("#js-chartjs-bars-behaviour").attr('data-calculation')+" %",
            backgroundColor: "#71ecf8",
            data: [
                $("#js-chartjs-bars-behaviour").attr('data-calculation')
            ]
        }]
    },
    options: {
        title: {
            display: true
        },
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    suggestedMin: 0, // minimum will be 0, unless there is a lower value.
                    beginAtZero: true // minimum value will be 0.
                }
            }]
        }
    }
});
  new Chart(document.getElementById("js-chartjs-bars-behaviour-subordinates"), {
    type: 'bar',
    data: {
        labels: ["Behaviour"],
        datasets: [{
            label: "Behaviour "+$("#js-chartjs-bars-behaviour").attr('data-calculation')+" %",
            backgroundColor: "#71ecf8",
            data: [
                $("#js-chartjs-bars-behaviour").attr('data-calculation')
            ]
        }]
    },
    options: {
        title: {
            display: true
        },
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    suggestedMin: 0, // minimum will be 0, unless there is a lower value.
                    beginAtZero: true // minimum value will be 0.
                }
            }]
        }
    }
});
</script>
