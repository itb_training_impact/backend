@extends('layouts.root')

@section('title','Add Performance')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="{{ url('home') }}">Dashboard</a>
            <a class="breadcrumb-item" href="{{ url('performance') }}">Performance</a>
            <span class="breadcrumb-item active">Add</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
    		@if(Session::get('status') == '200')
    	        <div class="alert alert-success alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @elseif(Session::get('status') == 'err')
    			<div class="alert alert-danger alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @endif
        @endif

        <h2 class="content-heading">
            Add Performance Question
        </h2>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Question field</h3>
            </div>
        </div>

        <form action="{{ url('performance') }}" method="POST">

            @csrf
            
            <div id="block-form">                                
                <div id="block-question">
                    <div class="block block_1">
                        <div class="block-content" style="padding: 18px;">
                            <div class="row">
                                <div class="col-sm-8">  
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" placeholder="Performance code" autofocus="" name="performance_code[]" required>
                                            <label for="" class="control-label">Code</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <textarea name="performance_question[]" required cols="30" rows="3" class="form-control" placeholder="Question"></textarea>
                                            <label for="" class="control-label">Question</label>
                                        </div>
                                    </div>

                                    <div class="block-answer_1" data-id="1">
                                        <div class="row mt-3">
                                            <div class="col-sm-5 form-material">
                                                <select name="performance_rating_start[]" class="form-control">
                                                    @for($i=0; $i <= 1; $i++)
                                                        <option value="{{ $i }}" {{ $i == 1 ? "selected" : "" }}>{{ $i }}</option>
                                                    @endfor
                                                </select> 
                                            </div>
                                            <div class="col-sm-2 form-material text-center">
                                                To
                                            </div>
                                            <div class="col-sm-5 form-material">
                                                <select name="performance_rating_finish[]" class="form-control">
                                                    @for($i=2; $i <= 10; $i++)
                                                        <option value="{{ $i }}" {{ $i == 5 ? "selected" : "" }}>{{ $i }}</option>
                                                    @endfor
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="form-group row mt-3">
                                            <div class="col-sm-5">
                                                <div class="input-group form-material">
                                                    <span class="input-group-addon">1</span>
                                                    <input type="text" class="form-control performance_answer_1" name="performance_answer_1[]" placeholder="Label (optional)" required>
                                                </div>
                                                <div class="input-group mt-3 form-material">
                                                    <span class="input-group-addon">5</span>
                                                    <input type="text" class="form-control performance_answer_1" name="performance_answer_1[]" placeholder="Label (optional)" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <select name="type_id[]" class="form-control" onchange="getAnswer(this);" data-id="1" required>
                                            <option value="">Type Answer</option>
                                            @foreach($type as $key => $value)
                                                <option value="{{ $value->type_label }}" {{ $value->type_label == 
                                                "rating" ? "selected" : "" }}>{{ $value->type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select name="group_id[]" class="form-control" required>
                                            <option value="">-- Select Question Group --</option>
                                            @foreach($group as $key => $value)
                                                <option value="{{ $value->group_id }}" {{ $value->group_id == old('group_id')
                                                 ? "selected" : "" }}>{{ $value->group }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select name="criteria_id[]" class="form-control" required>
                                            <option value="">-- Select Criteria --</option>
                                            @foreach($criteria as $key => $value)
                                                <option value="{{ $value->criteria_id }}" {{ $value->criteria_id == old('criteria_id')
                                                 ? "selected" : "" }}>{{ $value->criteria }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if($trainningID ==null)
                                    <div class="form-group hide" id="trainning_id_1">
                                        <select name="trainning_id[]" class="form-control dynamic">
                                            <option value="">Training</option>
                                            @foreach($trainning as $key => $value)
                                                <option value="{{ $value->trainning_id }}" {{ $value->trainning_id == old('trainning_id')
                                                ? "selected" : "" }}>{{ $value->trainning_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @else
                                    <div class="form-group" id="trainning_id_1">
                                        <select name="trainning_id[]" class="form-control dynamic">
                                           
                                            @foreach($trainningID as $key => $value)
                                                <option value="{{ $value->trainning_id }}" {{ $value->trainning_id == old('trainning_id')
                                                ? "selected" : "" }}>{{ $value->trainning_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif                                    
                                    @if($trainningID ==null)
                                    <div class="form-group hide" id="session_id_1">
                                        <select class="form-control dynamic" id="sessions" name="sessions[]">
                                            <option value="">Session</option>        
                                                <option value="sessions"></option>                                                
                                        </select>
                                    </div>
                                    @else
                                    <div class="form-group" id="session_id_1">
                                        <select class="form-control dynamic" id="sessions" name="sessions[]">
                                             @foreach($sessions as $key => $value)
                                                <option value="{{ $value->session_id }}" {{ $value->session_id == old('session_id')
                                                ? "selected" : "" }}>{{ $value->name }}</option>
                                            @endforeach                                                
                                        </select>
                                    </div>
                                    @endif
                                    @if($trainningID ==null)
                                    <div class="form-group hide" id="module_id_1">
                                        <select class="form-control dynamic" id="modules" name="modules[]">
                                            <option value="">Module</option>        
                                                <option value="modules"></option>                                                
                                        </select>
                                    </div>
                                    @else
                                    <div class="form-group" id="module_id_1">
                                        <select class="form-control dynamic" id="modules" name="modules[]">
                                            @foreach($modules as $key => $value)
                                                <option value="{{ $value->module_id }}" {{ $value->module_id == old('module_id')
                                                ? "selected" : "" }}>{{ $value->name }}</option>
                                            @endforeach                                                  
                                        </select>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-sm-12 text-right">

                                    <hr style="height: 10px">

                                    <!-- <button type="button" class="btn btn-alt-primary" onclick="duplicate(this);" data-id="1"><i class="fa fa-copy"></i></button>
                                    <button type="button" class="btn btn-alt-danger" onclick="removeBlock(this);" data-id="1"><i class="fa fa-trash"></i></button>
                                    | -->
                                    <label class="css-control css-control-sm css-control-primary css-switch">
                                        <input type="checkbox" name="performance_is_required[]" id="default_1" class="css-control-input" checked>
                                        <span class="css-control-indicator"></span> Required
                                    </label>
                                    @if($trainningID ==null)
                                    <label class="css-control css-control-sm css-control-primary css-switch">
                                        <input type="checkbox" name="performance_is_default[]" class="css-control-input" data-id="1" checked onchange="getTrainning(this)">
                                        <span class="css-control-indicator"></span> Default
                                    </label>
                                    @else
                                    <label class="css-control css-control-sm css-control-primary css-switch">
                                        <input type="checkbox" name="performance_is_default[]" class="css-control-input" data-id="1">
                                        <span class="css-control-indicator"></span> Default
                                    </label>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>   
            <div class="col-12">
                <button type="submit" class="btn btn-success">Save</button>
                <button class="btn btn-primary" type="submit" value="save_and_new" name="save_and_new"> <i class="fa fa-plus"></i> Save and Create new</button>                
                <a href="{{ url('performance') }}" class="btn btn-secondary" style="float:right;">Back</a>  
            </div>
        </form>
        <!-- END Table -->
    </div>
</div>

@include('modal_delete')
@endsection

@push('script')    
    <script type="text/javascript">
        var group = "{!! $optGroup !!}";
        var type = "{!! $optType !!}";
        var trainning = "{!! $optTrainning !!}";
    </script>

    <script src="{{ asset('js/performance.js') }}" type="text/javascript"></script>

    <script>        
        $(document).ready(function(){
            $('select[name="trainning_id[]"]').on('change', function () {
                let trainningId = $(this).val();
                if (trainningId) {
                    jQuery.ajax({
                        url: '/reaction/session/'+trainningId,
                        type: "GET",
                        dataType: "json",
                        success: function (response) {
                                $('select[name="sessions[]"]').empty();
                                $('select[name="sessions[]"]').append('<option value="">Session</option>');
                                $.each(response, function (key, value) {
                                    $('select[name="sessions[]"]').append('<option value="' + key + '">' + value + '</option>');
                                });
                            },
                        });
                    } else {
                        $('select[name="sessions[]"]').append('<option value="">Session</option>');
                    }
            });
            $('select[name="sessions[]"]').on('change', function () {
                let sessionId = $(this).val();
                if (sessionId) {
                    jQuery.ajax({
                        url: '/reaction/module/'+sessionId,
                        type: "GET",
                        dataType: "json",
                        success: function (response) {
                                $('select[name="modules[]"]').empty();
                                $('select[name="modules[]"]').append('<option value="">Module</option>');
                                $.each(response, function (key, value) {
                                    $('select[name="modules[]"]').append('<option value="' + key + '">' + value + '</option>');
                                });
                            },
                        });
                    } else {
                        $('select[name="modules[]"]').append('<option value="">Module</option>');
                    }
            });
        });
    </script>
@endpush
