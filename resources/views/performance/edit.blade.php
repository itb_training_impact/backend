@extends('layouts.root')

@section('title','Edit Performance')

@section('content')

@php $answer = explode(",", $performance->performance_answer) @endphp

<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="{{ url('home') }}">Dashboard</a>
            <a class="breadcrumb-item" href="{{ url('performance') }}">Performance</a>
            <span class="breadcrumb-item active">Edit</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
    		@if(Session::get('status') == '200')
    	        <div class="alert alert-success alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @elseif(Session::get('status') == 'err')
    			<div class="alert alert-danger alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @endif
        @endif

        <h2 class="content-heading">
            Edit Performance Question
        </h2>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Question field</h3>
            </div>
        </div>

        <form action="{{ url('performance/update/'. $performance->performance_id) }}" method="POST">

            @csrf
            <input type="hidden" value="PUT" name="_method">
            
            <div id="block-form">
                
                <div id="block-question">
                    <div class="block block_1">
                        <div class="block-content" style="padding: 18px;">
                            <div class="row">
                                <div class="col-sm-8">  
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" placeholder="Performance code" autofocus="" name="performance_code" required value="{{ $performance->performance_code }}">
                                            <label for="" class="control-label">Code</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <textarea name="performance_question" required cols="30" rows="3" class="form-control" placeholder="Question">{{ $performance->performance_question }}</textarea>
                                            <label for="" class="control-label">Question</label>
                                        </div>
                                    </div>

                                    <div class="block-answer" data-id="1">
                                        @if($performance->type->type_label == "rating")
                                            <div class="row mt-3">
                                                <div class="col-sm-5 form-material">
                                                    <select name="performance_rating_start" class="form-control">
                                                        @for($i=0; $i <= 1; $i++)
                                                            <option value="{{ $i }}" {{ $i == $performance->performance_rating_start ? "selected" : "" }}>{{ $i }}</option>
                                                        @endfor
                                                    </select> 
                                                </div>
                                                <div class="col-sm-2 form-material text-center">
                                                    To
                                                </div>
                                                <div class="col-sm-5 form-material">
                                                    <select name="performance_rating_finish" class="form-control">
                                                        @for($i=2; $i <= 10; $i++)
                                                            <option value="{{ $i }}" {{ $i == $performance->performance_rating_finish ? "selected" : "" }}>{{ $i }}</option>
                                                        @endfor
                                                    </select> 
                                                </div>
                                            </div>
                                            <div class="form-group row mt-3">
                                                <div class="col-sm-5">
                                                    <div class="input-group form-material">
                                                        <span class="input-group-addon">1</span>
                                                        <input type="text" class="form-control performance_answer_1" name="performance_answer[]" placeholder="Label (optional)" required value="{{ $answer[0] }}">
                                                    </div>
                                                    <div class="input-group mt-3 form-material">
                                                        <span class="input-group-addon">5</span>
                                                        <input type="text" class="form-control performance_answer_1" name="performance_answer[]" placeholder="Label (optional)" required value="{{ $answer[1] }}">
                                                    </div>
                                                </div>
                                            </div>
                                        @elseif($performance->type->type_label == "short_answer")
                                            <div class="row mt-3">
                                                <div class="col-sm-12">
                                                    <div class="form-material">
                                                        <input type="text" class="form-control performance_answer_'+ inc +'" name="performance_answer_'+ inc +'[]" placeholder="Short-answer text">
                                                        <label class="control-label">Short Answer</label>
                                                    </div>
                                                </div>
                                            </div>
                                        @elseif($performance->type->type_label == "multiple_choice")
                                            <div class="">
                                              <div class="col-sm-3">
                                                <button type="button" class="btn btn-sm btn-success" onclick="addField();"><i class="fa fa-plus"></i> Add Answer</button>
                                              </div>            
                                              @if($performance->performance_answer != "")
                                                @foreach(explode(",",$performance->performance_answer) as $key => $answer)
                                                  <div class="col-lg-8">
                                                    <div class="row block_field_{{$key + 1}}">    
                                                      <div class="col-sm-10">
                                                          <div class="form-material" style="padding-top:0px">
                                                              <input type="text" class="form-control performance_answer_{{$key + 1}}" name="performance_answer[]" placeholder="Answer option" value="{{$answer}}" readonly>                            
                                                          </div>
                                                      </div>
                                                       <input type="radio" style="margin-top: 15px;" name="performance_correct_answer[]" placeholder="Answer option" value="{{$answer}}" {{$performance->performance_correct_answer == $answer ? 'checked' : ''}}>
                                                      <div class="col-sm-1"><a href="javascript:void(0)" data-href="{{ url('performance/delete_questions/'.$performance->performance_id.'/'. $answer) }}" class="btn btn-sm btn-danger" onclick="removeField(this);" data-id="{{$key + 1}}" data-remote="true"><i class="fa fa-minus"></i></a></div>
                                                    </div>
                                                    <div class='clearfix'></div>                                                
                                                  </div>
                                                @endforeach
                                              @endif
                                              <div id="block-question-field" class="col-lg-8">        
                                              </div>      
                                            </div>  
                                          <!--   <div class="row mt-3">
                                                <div class="col-sm-12">
                                                    <div class="form-material" style="padding-top:5px">
                                                        <input type="text" class="form-control performance_correct_answer_'+ inc +'" name="performance_correct_answer[]" placeholder="Kunci jawaban (Tulis salah satu jawaban yang ada di list)" value="{{$performance->performance_correct_answer}}">              
                                                    </div>
                                                </div>
                                            </div> -->
                                        @endif
                                    </div>                                    
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <select name="type_id" class="form-control" data-id="1" required disabled="">
                                            <option value="">Type Answer</option>
                                            @foreach($type as $key => $value)
                                                <option value="{{ $value->type_id }}" {{ $value->type_id == $performance->type_id ? "selected" : "" }}>{{ $value->type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select name="group_id" class="form-control" required disabled="">
                                            <option value="">Question Group</option>
                                            @foreach($group as $key => $value)
                                                <option value="{{ $value->group_id }}" {{ $value->group_id == $performance->group_id ? "selected" : "" }}>{{ $value->group }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select name="criteria_id" class="form-control" required disabled="">
                                            <option value="">Criteria</option>
                                            @foreach($criteria as $key => $value)
                                                <option value="{{ $value->criteria_id }}" {{ $value->criteria_id == $performance->criteria_id ? "selected" : "" }}>{{ $value->criteria }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group {{ $performance->performance_is_default == 1 ? "hide" : "" }}" id="trainning_id_1">
                                        <select name="trainning_id" class="form-control">
                                            <option value="">Trainning</option>
                                            @foreach($trainning as $key => $value)
                                                <option value="{{ $value->trainning_id }}" {{ $value->trainning_id == $performance->trainning_id ? "selected" : "" }}>{{ $value->trainning_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group {{ $performance->performance_is_default == 1 ? "hide" : "" }}" id="session_id_1">
                                        <select class="form-control dynamic" id="sessions" name="sessions">                                                
                                                <option value="{{ $performance->session_id }}" {{ $performance->session_id ? "selected" : "" }}>{{$session->name ?? "Session"}}</option>
                                                <option value="sessions"></option>
                                        </select>
                                    </div>
                                    <div class="form-group {{ $performance->performance_is_default == 1 ? "hide" : "" }}" id="module_id_1">
                                        <select class="form-control dynamic" id="modules" name="modules">                                            
                                            <option value="{{ $performance->module_id }}" {{ $performance->module_id ? "selected" : "" }}>{{$module->name ?? "Module"}}</option>                                              
                                            <option value="modules"></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-right">

                                    <hr style="height: 10px">

                                    <label class="css-control css-control-sm css-control-primary css-switch">
                                        <input type="checkbox" name="performance_is_required" class="css-control-input" {{ $performance->performance_is_required == 1 ? "checked" : "" }}>
                                        <span class="css-control-indicator"></span> Required
                                    </label>
                                    |
                                    <label class="css-control css-control-sm css-control-primary css-switch">
                                        <input type="checkbox" name="performance_is_default" class="css-control-input" {{ $performance->performance_is_default == 1 ? "checked" : "" }} data-id="1" onchange="getTrainning(this)">
                                        <span class="css-control-indicator"></span> Default
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>   
  
            <button type="submit" class="btn btn-alt-primary">Submit</button>    
            <button class="btn btn-primary" type="submit" value="save_and_new" name="save_and_new"> <i class="fa fa-plus"></i> Save and Create new</button>
            <a href="{{ url('performance') }}" class="btn btn-secondary" style="float:right;">Back</a> 
        </form>
        <!-- END Table -->
    </div>
</div>
@endsection

@push('script')
    <script>
        function getTrainning(item) {
            var _this   = $(item);
            var id      = _this.data('id');

            if(!_this.is(":checked")){
                $('#trainning_id_'+id).removeClass('hide');
                $('#session_id_'+id).removeClass('hide');
                $('#module_id_'+id).removeClass('hide');
            } else {
                $('#trainning_id_'+id).addClass('hide');
                $('#session_id_'+id).addClass('hide');
                $('#module_id_'+id).addClass('hide');
            }
        }
        $(document).on("click", "a[data-remote=true]", function(e){
            //e.preventDefault();            
            $.ajax({url: $(this).attr("data-href"), success: function(result){
              //$("#div1").html(result);
            }});
        });
          

    </script>

    <script>        
        $(document).ready(function(){
            $('select[name="trainning_id"]').on('change', function () {
                let trainningId = $(this).val();
                if (trainningId) {
                    jQuery.ajax({
                        url: '/reaction/session/'+trainningId,
                        type: "GET",
                        dataType: "json",
                        success: function (response) {
                                $('select[name="sessions"]').empty();
                                $('select[name="sessions"]').append('<option value="">Session</option>');
                                $.each(response, function (key, value) {
                                    $('select[name="sessions"]').append('<option value="' + key + '">' + value + '</option>');
                                });
                            },
                        });
                    } else {
                        $('select[name="sessions"]').append('<option value="">Session</option>');
                    }
            });
            $('select[name="sessions"]').on('change', function () {
                let sessionId = $(this).val();
                if (sessionId) {
                    jQuery.ajax({
                        url: '/reaction/module/'+sessionId,
                        type: "GET",
                        dataType: "json",
                        success: function (response) {
                                $('select[name="modules"]').empty();
                                $('select[name="modules"]').append('<option value="">Module</option>');
                                $.each(response, function (key, value) {
                                    $('select[name="modules"]').append('<option value="' + key + '">' + value + '</option>');
                                });
                            },
                        });
                    } else {
                        $('select[name="modules"]').append('<option value="">Module</option>');
                    }
            });
        });
    </script>
    
    <script src="{{ asset('js/reaction.js') }}" type="text/javascript"></script>
@endpush