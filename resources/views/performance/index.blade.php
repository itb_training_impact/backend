@extends('layouts.root')

@section('title','Performance')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Performance</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
    		@if(Session::get('status') == '200')
    	        <div class="alert alert-success alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @elseif(Session::get('status') == 'err')
    			<div class="alert alert-danger alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @endif
        @endif
          @if (Session::has('error'))
        <div class="alert alert-danger">
            <ul>
                <li>{!! \Session::get('error') !!}</li>
            </ul>
        </div>
        @endif
        <h2 class="content-heading">
            Performance Data Table
        </h2>

        {{-- search --}}

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Search Performance</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <form action="{{ url('performance') }}" method="GET" class="form form-inline">
                    @csrf
                    <div>
                        <select name="criteria_id" id="criteria_id" class="form-control">
                            <option value="">Criteria</option>
                            @foreach($criteria as $key => $value)
                                <option value="{{ $value->criteria_id }}" {{ Request::get('criteria_id') ==  $value->criteria_id ? "selected" : "" }} >{{ $value->criteria }}</option>
                            @endforeach
                        </select>
                        <input type="text" class="form-control" name="performance_code" value="{{ Request::has('performance_code') ? Request::get('performance_code') : '' }}" placeholder="Enter Question Code ...">
                        <input type="text" class="form-control" name="performance_question" value="{{ Request::has('performance_question') ? Request::get('performance_question') : '' }}" placeholder="Enter Question ...">
                        <select name="type_id" id="type_id" class="form-control">
                            <option value="">Type Answer</option>
                            @foreach($type as $key => $value)
                                <option value="{{ $value->type_id }}" {{ Request::get('type_id') ==  $value->type_id ? "selected" : "" }}>{{ $value->type }}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-alt-primary">Search</button>
                        <a href="{{ url('performance') }}" class="btn btn-secondary">Refresh</a>
                    </div>
                </form>
            </div>
        </div>

        <a href="{{ url('performance/create') }}" class="btn btn-success mb-3"> <i class="fa fa-plus"></i> &nbsp; Create Question</a>
        <button class="btn btn-success btn-import mb-3" type="button"> <i class="fa fa-upload"></i> &nbsp; Import Question</button>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">List of Performance</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <table id="datatable" class="table table-vcenter table-stripped table-bordered">
                    <thead>
                        <tr>
                            <th width="5%"></th>
                            <!-- <th>ID</th> -->
                            <th>GROUP</th>
                            <th>QUESTION CODE</th>
                            <th>QUESTION</th>
                            <th>TYPE ANSWER</th>
                            <th>TRAININGS</th>
                            <th>STATUS</th>
                            <th width="15%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(sizeof($performance) > 0)
                            @foreach($performance->where('performance_is_default',1) as $key => $value)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <!-- <td>{{ $value->performance_id }}</td> -->
                                    <td>{{ isset($value->criteria) ? $value->criteria->criteria : "" }}</td>
                                    <td>{{ $value->performance_code }}</td>
                                    <td>{{ $value->performance_question }}</td>
                                    <td>{{ isset($value->type) ? $value->type->type : "" }}</td>
                                    <td class="text-center">
                                        {!! $value->totalTrainning($value->performance_id) ? 
                                        "<button type='button' style='height: 19px' class='btn btn-primary btn-sm btn-trainning' data-id='". $value->performance_id ."'>" .$value->totalTrainning($value->performance_id) ." Trainnings </button>" 
                                        : "-" !!}
                                    </td>
                                    @if($value->performance_is_default == 0)
                                        <td>custom</td>
                                    @else
                                        <td>default</td>
                                    @endif
                                    <td>
                                        <a href="{{ url('performance/detail/'. $value->performance_id) }}" class="btn btn-sm btn-primary" style="height: 19px"><i class="fa fa-search"></i></a>
                                        <a href="{{ url('performance/edit/'. $value->performance_id) }}" class="btn btn-sm btn-warning" style="height: 19px"><i class="fa fa-pencil"></i></a>
                                        <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('performance/delete/'. $value->performance_id) }}" class="btn btn-sm btn-danger btn-xs btn-delete" style="height: 19px">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="8">Performance not available.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                {{$performance->links()}}
            </div>
        </div>
        <!-- END Table -->
    </div>
</div>

@include('modal_delete')
@include('modal_trainning')
@include('modal_import_performance')
@endsection

@push('script')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.css') }}">
    <script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js') }} "></script>
    <script src="{{ asset('assets/js/pages/be_tables_datatables.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
            	 "pageLength": 20,
                "lengthChange": false,
                "paging": false,
                "searching": true
            });
        } );

        $('.btn-delete').click(function(){
        	var action = $(this).data('action');

        	$('#modalDelete').modal('show');
        	$('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });

        $('.btn-import').click(function(){
        	var action = $(this).data('action');
        	$('#modalImportPerformance').modal('show');
        });

        $('.btn-trainning').click(function(){
            var id = $(this).data('id');
            var url = "{{ url('performance/trainning') }}" + "/" + id;

            $.get(url, function(data){
                console.log(data);

                $('#trainning-section').html(data);
                $('#modalTrainning').modal('show');
            });
        });
    </script>
@endpush
