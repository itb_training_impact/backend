@extends('layouts.root')

@section('title','Ubah Menu Baru')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('system/menu') }}">Menu</a>
        <span class="breadcrumb-item active">Ubah</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Ubah Menu Baru</h3>
                </div>
                <div class="block-content">
                    <form class="form-level row" action="{{ url('system/menu/update/'.$menu->menu_id) }}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Menu</label>
                                <input type="text" class="form-control"  id="menu" name="menu" placeholder="Masukan nama menu" value="{{ $menu->menu }}">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Module</label>
                                <select class="form-control" id="module_id" name="module_id">
                                    <option value="">Pilih Module</option>
                                    @foreach ($module as $result)
                                        <option value="{{ $result->modules_id }}" {{ $menu->module_id == $result->module_id ? "selected" : "" }}>{{ $result->module }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>URL</label>
                                <input type="text" class="form-control" id="menu_url" name="menu_url" placeholder="Masukan url" value="{{ $menu->menu_url }}">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Icon</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Masukan kode icon" id="menu_icon" name="menu_icon" value="{{ $menu->menu_icon }}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-secondary" type="button" data-toggle="modal" data-target=".modal-icon">Lihat Icon</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Is Sub</label>
                                <div class="radio-list">
                                    <label class="radio-inline p-0">
                                        <div class="radio radio-info">
                                            <input type="radio" name="menu_is_sub" value="1" {{ $menu->menu_is_sub == 1 ? "checked=checked" : "" }}>
                                            <label>Ya</label>
                                        </div>
                                    </label>
                                    <label class="radio-inline p-0">
                                        <div class="radio radio-info">
                                            <input type="radio" name="menu_is_sub" value="0" {{ $menu->menu_is_sub == 0 ? "checked=checked" : "" }}>
                                            <label>Bukan</label>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Parent</label>
                                <select class="form-control" disabled id="menu_parent" name="menu_parent">
                                    <option value="">Pilih Menu</option>
                                    @foreach ($parent as $result)
                                        <option value="{{ $result->menu_id }}" {{ $menu->menu_parent == $result->menu_id ? "selected" : "" }}>{{ $result->menu }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Posisi</label>
                                <input type="text" class="form-control" id="menu_position" name="menu_position" placeholder="Masukan posisi. note : 0 - n" value="{{ $menu->menu_position }}">
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary">Simpan</button>
                            <a href="{{ url('system/menu') }}" class="btn btn-danger">Batal</a>
                        </div>
                    </form>
                    <br>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@include('modal_icon');
@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        /*
        *  Document   : be_ui_icons.js
        *  Author     : pixelcave
        *  Description: Custom JS code used in Icons Page
        */

        var BeUIIcons = function() {
            // Icon search functionality
            var initIconSearch = function(){
                // Set variables
                var searchItems = jQuery('.js-icon-list > div');
                var searchValue = '';

                // When user types
                jQuery('.js-icon-search').on('keyup', function(){

                    searchValue = jQuery(this).val().toLowerCase();

                    if (searchValue.length > 2) { // If more than 2 characters, search the icons
                        searchItems.hide();

                        jQuery('code', searchItems)
                            .each(function(){
                                if (jQuery(this).text().match(searchValue)) {
                                    jQuery(this).parent('div').fadeIn(300);
                                }
                            });
                    } else if (searchValue.length === 0) { // If text deleted show all icons
                        searchItems.show();
                    }
                });
            };

            return {
                init: function() {
                    // Init icon search
                    initIconSearch();
                }
            };
        }();

        // Initialize when page loads
        jQuery(function(){ BeUIIcons.init(); });

        $(document).ready(function(){

            var is_sub = "{{ $menu->menu_is_sub }}";

            if(is_sub == 1){
                $('#menu_parent').attr('disabled',false);
            }
            else {
                $('#menu_parent').attr('disabled',true);
            }

            $('input[name="menu_is_sub"]').bind('change', function(){
                if($(this).val() == 1){
                    $('#menu_parent').attr('disabled',false);
                }
                else {
                    $('#menu_parent').attr('disabled',true);
                }
            });
        });

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'module': {
                    required: true,
                    minlength: 3
                }
            },
            messages: {
                'module': {
                    required: 'Inputan module harus di isi',
                    minlength: 'Minimal terdiri dari 3 karakter'
                }
            }
        });
    </script>
@endpush
