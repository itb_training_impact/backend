@extends('layouts.root')

@section('title','Tambah Role Baru')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('system/role') }}">Role</a>
        <span class="breadcrumb-item active">Add</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Tambah Role Baru</h3>
                    <div class="pull-right">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" id="taskAll" class="form-check-input" value="all"> Pilih Semua
                            </label>
                        </div>
                    </div>
                </div>
                <div class="block-content">
                    <div class="table-responsive">
                        <table class="table table-vcenter table-stripped">
                            <form class="form-horizontal row" method="POST" action="{{ url('system/role/update') }}">
                                @csrf
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Module</th>
                                        <th>List</th>
                                        <th>Add</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                        <th>View</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($module as $key => $result)
                                        @php 
                                            $task = App\Model\System\Task::where('module_id', $result->modules_id)->pluck('task','task_id')->toArray(); 
                                            $role = App\Model\System\Role::where('level_id', $level->level_id)->pluck('task_id')->toArray();
                                        @endphp
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $result->module }}</td>
                                            <td>
                                                @if (in_array("list", $task))
                                                    @if(in_array(array_search('list',$task), $role))
                                                        <input type="checkbox" name="task_id[]" value="{{ array_search('list', $task) }}" checked>
                                                    @else
                                                        <input type="checkbox" name="task_id[]" value="{{ array_search('list', $task) }}">
                                                    @endif
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if (in_array("create", $task))
                                                    @if(in_array(array_search('create',$task), $role))
                                                        <input type="checkbox" name="task_id[]" value="{{ array_search('create', $task) }}" checked>
                                                    @else
                                                        <input type="checkbox" name="task_id[]" value="{{ array_search('create', $task) }}">
                                                    @endif
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if (in_array("edit", $task))
                                                    @if(in_array(array_search('edit',$task), $role))
                                                        <input type="checkbox" name="task_id[]" value="{{ array_search('edit', $task) }}" checked>
                                                    @else
                                                        <input type="checkbox" name="task_id[]" value="{{ array_search('edit', $task) }}">
                                                    @endif
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if (in_array("delete", $task))
                                                    @if(in_array(array_search('delete',$task), $role))
                                                        <input type="checkbox" name="task_id[]" value="{{ array_search('delete', $task) }}" checked>
                                                    @else
                                                        <input type="checkbox" name="task_id[]" value="{{ array_search('delete', $task) }}">
                                                    @endif
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if (in_array("view", $task))
                                                    @if(in_array(array_search('view',$task), $role))
                                                        <input type="checkbox" name="task_id[]" value="{{ array_search('view', $task) }}" checked>
                                                    @else
                                                        <input type="checkbox" name="task_id[]" value="{{ array_search('view', $task) }}">
                                                    @endif
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="11">
                                            <button class="btn btn-primary">Simpan</button>
                                            <a href="{{ url('system/role') }}" class="btn btn-danger">Batal</a>
                                        </td>
                                    </tr>
                                </tbody>
                                <input type="hidden" name="level_id" value="{{ $level->level_id }}">
                            </form>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>

        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');

            $("#taskAll").click(function(){
                $('input:checkbox').not(this).prop('checked', this.checked);
            });
        });

        $('#modules_id').bind('change', function(){
            var url         = '{{ url("system/role/task") }}';
            var modules_id  = $(this).val();

            $.get(url, {modules_id : modules_id}, function(){
                console.log('success');
            }).done(function(data){
                $('.list-task').html(data);
            }).fail(function(){
                alert('Something Wrong');
            });
        });

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'users_id': {
                    required: true
                },
                'modules_id': {
                    required: true
                }
            },
            messages: {
                'users_id': 'User level haru dipilih',
                'modules_id': 'Module harus dipilih'
            }
        });
    </script>
@endpush
