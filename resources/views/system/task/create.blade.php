@extends('layouts.root')

@section('title','Tambah Task Baru')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('system/task') }}">Task</a>
        <span class="breadcrumb-item active">Tambah</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Tambah Task Baru</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('system/task') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="modules_id" name="modules_id" data-placeholder="Pilih Module ...">
                                    <option></option>
                                    @foreach($modules as $module)
                                        <option value="{{ $module->modules_id }}">{{ $module->module }}</option>
                                    @endforeach
                                </select>
                                <label for="material-module">Module</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <label for="material-task">Pilih Task</label>
                            </div>
                            <div class="form-material">
                                <label class="css-control css-control-primary css-checkbox">
                                    <input type="checkbox" class="css-control-input" name="tasks[]" value="list">
                                    <span class="css-control-indicator"></span> List
                                </label>
                            </div>
                            <div class="form-material">
                                <label class="css-control css-control-primary css-checkbox">
                                    <input type="checkbox" class="css-control-input" name="tasks[]" value="create">
                                    <span class="css-control-indicator"></span> Create
                                </label>
                            </div>
                            <div class="form-material">
                                <label class="css-control css-control-primary css-checkbox">
                                    <input type="checkbox" class="css-control-input" name="tasks[]" value="edit">
                                    <span class="css-control-indicator"></span> Edit
                                </label>
                            </div>
                            <div class="form-material">
                                <label class="css-control css-control-primary css-checkbox">
                                    <input type="checkbox" class="css-control-input" name="tasks[]" value="view">
                                    <span class="css-control-indicator"></span> View
                                </label>
                            </div>
                            <div class="form-material">
                                <label class="css-control css-control-primary css-checkbox">
                                    <input type="checkbox" class="css-control-input" name="tasks[]" value="delete">
                                    <span class="css-control-indicator"></span> Delete
                                </label>
                            </div>
                            <br>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Simpan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <a href="{{ url('system/task') }}" class="btn btn-secondary" style="float:right;">Back</a>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>

        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
        });

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'modules_id': {
                    required: true
                }
            },
            messages: {
                'modules_id': {
                    required: 'Module harus dipilih'
                }
            }
        });
    </script>
@endpush
