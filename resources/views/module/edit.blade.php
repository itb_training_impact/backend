@extends('layouts.root')

@section('title','Module - Edit')

@section('content')

<!-- Page Content -->
<div class="content">

  <nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
    <a class="breadcrumb-item" href="{{ url('module') }}">module</a>
    <span class="breadcrumb-item active">Edit</span>
  </nav>
    <!-- Material Design -->
  <div class="row">
    <div class="col-md-12">
      <!-- Static Labels -->
      <div class="block">
        <div class="block-header block-header-default" style="background-color: #f5f6f7;">
            <h3 class="block-title">MODULE</h3>
        </div>
        <form class="form-level" action="{{ url('module/update/'. $module->module_id) }}" method="post">
          <input type="hidden" name="_method" value="PUT">
          @include('module.form')                            
        </form>
      </div>
    </div>    
  </div>    
</div>
@endsection

@push('script')
 <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
          jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
        });
        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'name': {
                    required: true,
                },
                
            },
            messages: {
                'name': {
                    required: 'Name has required',
                }
            }
        });
    </script>
@endpush
