@extends('layouts.root')

@section('title','Module - New')
 <style>
    .hidden{display: none;}
  </style>
@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('dimension') }}">Module</a>
        <span class="breadcrumb-item active">New</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <form class="form-level" action="{{ url('module') }}" method="post">
                @include('module.form')
            </form>
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection
