@csrf
<input type="hidden" name="trainning_id">
  <div class="block">
    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
        <h3 class="block-title">MODULE</h3>
    </div>
    <div class="block-content">
        <!-- Dipilih -->
      <div class="form-group">
        <div class="form-material">
          <select autocomplete="off" class="js-select2 form-control" placeholder="" id="session_id" name="session_id" required>            
            <option value="">Session ..</option>
            @if(!empty($module))
              @php
                $session_id = explode(",",$module->session_id);
              @endphp
              @foreach($sessions as $key =>$value)
                <option value="{{$value->session_id}}" {{ in_array($value->session_id,$session_id) ? "selected" : "" }} >{{$value->name}} - {{$value->trainning_title}}</option>
              @endforeach
            @else
              @foreach($sessions as $key =>$value)
                <option value="{{$value->session_id}}">{{$value->name}} - {{$value->trainning_title}}</option>
              @endforeach
            @endif            
          </select>
          <label for="material-email">Select session<span class="text-danger">*</span></label>
        </div>
      </div>
    </div>
  </div>

  <div class="block">
    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
      <h3 class="block-title">Title of module</h3>
    </div>
    <div class="block-content">
      <div class="form-group">
          <div class="form-material">
              <input type="text" class="form-control" name="name" placeholder="Title ..." autofocus="" value="{{ !empty($module) ? $module->name : '' }}">
              <label for="">Title <span class="text-danger">*</span></label>
          </div>
      </div>

      <div class="form-group">
          <div class="form-material">
              <textarea name="description" class="form-control" id="description" cols="30" rows="10" placeholder="Description ...">{{ !empty($module) ? $module->description : '' }}</textarea>
              <label for="">Description  <span class="text-danger">*</span></label>
          </div>
      </div>
    </div>
  </div>

<div class="block">
  <div class="block-header block-header-default" style="background-color: #f5f6f7;">
    <h3 class="block-title">Questions</h3>
  </div>
  <div class="block-content">
    <!-- <input type="hidden" name="trainning_detail_periode[]" value="0">  -->
      <div class="form-group row">
        <!-- <label class="col-12">Modules</label> -->
        <div class="col-12">        
          <div class="custom-controls-stacked">
            @if (!empty($reaction) && strval($reaction) != '[]')   
              <div class="col-2">
                <a href="{{ url('module/delete_questions/'. $module->module_id. '/reaction') }}" class="btn btn-danger btn-sm">Remove question Reaction</a>
              </div>
              <br>
            @else
              <label class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" name="module[]" value="is_reaction" {{ !empty($reaction) && strval($reaction) != '[]' ? 'checked disabled' : '' }}>
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Reaction</span>
              </label>
            @endif                                
            @if (!empty($learning) && strval($learning) != '[]')     
              <div class="col-2">           
                <a href="{{ url('module/delete_questions/'. $module->module_id. '/learning') }}" class="btn btn-danger btn-sm">Remove question Learning</a>
              </div>
              <br>
            @else
              <label class="custom-control custom-checkbox">    
                <input type="checkbox" class="custom-control-input" name="module[]" value="is_learning" {{ !empty($learning) && strval($learning) != '[]' ? 'checked disabled' : '' }}>
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Learning</span>
              </label>
            @endif                        
            @if (!empty($behaviour) && strval($behaviour) != '[]')  
              <div class="col-2">              
                <a href="{{ url('module/delete_questions/'. $module->module_id. '/behaviour') }}" class="btn btn-danger btn-sm">Remove question Behaviour</a>                
              </div>
            @else
              <label class="custom-control custom-checkbox">    
                <input type="checkbox" class="custom-control-input" name="module[]" value="is_behaviour" {{ !empty($behaviour) && strval($behaviour) != '[]' ? 'checked disabled' : '' }}>
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Behaviour</span>
              </label>
            @endif                
        </div>
      </div>
    </div>
  </div>
</div>
  
              <div class="block" style="display: none;">
                    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                        <h3 class="block-title">Pilih Kompetensi</h3>
                    </div>
                 <div class="block-content">
                        <!-- Dipilih -->
                        <div class="form-group">
                            <div class="form-material">
                               <select autocomplete="off" class="js-select2 form-control" placeholder="Choose formula .." id="formulation_id" name="formulation_id[]" multiple="" style="width: 100%"> 
                                <option>Pilih Kompetensi</option>
                                @foreach ($formulas as $formula)
                                         <option value="{{$formula->id}}">{{$formula->name}}</option>
                                         @endforeach
                                    </select>
                            <label for="material-email">Select Kompetensi<span class="text-danger">*</span></label>
                        </div>
                    </div>
                </div>
            </div>  

</div>


<!-- END Static Labels -->
<!-- <div class="block">
    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
        <h3 class="block-title">DURING</h3>
    </div>
    <div class="block-content">
          <input type="hidden" name="trainning_detail_periode[]" value="1"> 
            <div class="form-group row">
                <label class="col-12">Modules</label>
                <div class="col-12">
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="module_during[]" value="is_reaction">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Reaction</span>
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="module_during[]" value="is_learning">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Learning</span>
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="module_during[]" value="is_behaviour">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Behaviour</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
</div> -->
<!-- END Static Labels -->
<!-- <div class="block">
    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
        <h3 class="block-title">After</h3>
    </div>
    <div class="block-content">
          <input type="hidden" name="trainning_detail_periode[]" value="2"> 
            <div class="form-group row">
                <label class="col-12">Modules</label>
                <div class="col-12">
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="module_after[]" value="is_reaction">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Reaction</span>
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="module_after[]" value="is_learning">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Learning</span>
                        </label>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="module_after[]" value="is_behaviour">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Behaviour</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
</div> -->
<!-- END Static Labels -->
  <div class="col-12">
    <div class="form-group">
        <button type="submit" class="btn btn-success">Save</button>
        <button type="reset" class="btn btn-secondary">Reset</button>
        <a href="{{ url('module') }}" class="btn btn-secondary" style="float:right;">Back</a>
    </div>
  </div>
@push('script')
<script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
<script>
     jQuery(function () {
        // Init page helpers (Select2 plugin)
        Codebase.helpers('select2');
    });
    $('.form-level').validate({
        ignore: [],
        errorClass: 'invalid-feedback animated fadeInDown',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            jQuery(e).parents('.form-group').append(error);
        },
        highlight: function(e) {
            jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
        },
        success: function(e) {
            jQuery(e).closest('.form-group').removeClass('is-invalid');
            jQuery(e).remove();
        },
        rules: {
            'name': {
                required: true,
            },
            
        },
        messages: {
            'name': {
                required: 'name has required',
            },
           
        }
    });
    $("input[value=is_behaviour]").on( "change", function(evt) {
     if($(this).prop("checked")) {
        $("div[class=block]").show();
      } else{
        $("div[class=block]").hide();
      }
    });
</script>
  
@endpush  