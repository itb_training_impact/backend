<div class="modal fade" id="modalImportParticipant" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document" aria-hidden="true">
        <form id="formImportReaction" method="POST" action="{{ url('participant/import') }}" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title modal-title">Import Participnt</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="form-group">
                            <label for="import" class="control-label">Import <span class="text-danger">*</span></label> <br>
                            <input type="file" name="import" required> <br>
                            <a href="{{ url('imports/import_participants.xlsx') }}">Download format import</a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-modal-primary">Import</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>
