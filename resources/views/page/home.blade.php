@extends('layouts.root')

@section('title','Dashboard')

@section('content')

<div class="content">
    @if(Session::has('status'))
        @if(Session::get('status') == '200')
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
                <p class="mb-0">{{ Session::get('msg') }}</p>
            </div>
        @elseif(Session::get('status') == 'err')
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
                <p class="mb-0">{{ Session::get('msg') }}</p>
            </div>
        @endif
    @endif
    <div class="row gutters-tiny invisible" data-toggle="appear">        
        <!-- Row #1 -->        
        <div class="col-6 col-xl-4">        
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-bag fa-3x text-body-bg-dark"></i>
                    </div>
                    <div class="font-size-h3 font-w600" data-toggle="countTo" data-speed="1000" data-to="{{($trainning->count())}}"></div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">TRAININGS COUNT</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-xl-4">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-wallet fa-3x text-body-bg-dark"></i>
                    </div>
                    <div class="font-size-h3 font-w600"><span data-toggle="countTo" data-speed="1000" data-to="{{$usertrainner}}">0</span></div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">TRAINER {{$in_company}}</div>
                </div>
            </a>
        </div>        
        <!-- <div class="col-6 col-xl-3">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-envelope-open fa-3x text-body-bg-dark"></i>
                    </div>                    
                    <div class="font-size-h3 font-w600" data-toggle="countTo" data-speed="1000" data-to="{{($trainee->count())}}">0</div>                    
                    <div class="font-size-sm font-w600 text-uppercase text-muted">USER TRAINEE</div>
                </div>
            </a>
        </div> -->
        <div class="col-6 col-xl-4">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-users fa-3x text-body-bg-dark"></i>
                    </div>
                    <div class="font-size-h3 font-w600" data-toggle="countTo" data-speed="1000" data-to="{{($participant->count())}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">PARTICIPANTS {{$in_company}}</div>
                </div>
            </a>
        </div>
        <!-- END Row #1 -->
    </div>
    <!-- <div class="row gutters-tiny invisible" data-toggle="appear">    
        <div class="col-md-6">
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">
                        Reaction Chart <small>This week</small>
                    </h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                            <i class="si si-refresh"></i>
                        </button>
                        <button type="button" class="btn-block-option">
                            <i class="si si-wrench"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <div class="pull-all">                        
                        <canvas id="js-chartjs-bars-reaction">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">
                        Learning Chart <small>This week</small>
                    </h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                            <i class="si si-refresh"></i>
                        </button>
                        <button type="button" class="btn-block-option">
                            <i class="si si-wrench"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <div class="pull-all">                        
                        <canvas id="js-chartjs-bars-learning">
                    </div>
                </div>
            </div>
        </div>        
    </div> -->
    <div class="row gutters-tiny invisible" data-toggle="appear">
        <!-- Row #4 -->
        @foreach ($trainning as $trainning)
        @endforeach
        <div class="col-md-6">
            <a class="block block-link-shadow overflow-hidden" href="javascript:void(0)">
                <div class="block-content block-content-full">
                    <i class="si si-briefcase fa-2x text-body-bg-dark"></i>
                    <div class="row py-20">
                        <div class="col-6 text-right border-r">
                            <div class="invisible" data-toggle="appear" data-class="animated fadeInLeft">
                                <div class="font-size-h3 font-w600">{{($trainning->count())}}</div>
                                <div class="font-size-sm font-w600 text-uppercase text-muted">TRAININGS</div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="invisible" data-toggle="appear" data-class="animated fadeInRight">
                                <div class="font-size-h3 font-w600">{{($trainning->where('before',1)->count())}}</div>
                                <div class="font-size-sm font-w600 text-uppercase text-muted">ON GOING</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-6">
            <a class="block block-link-shadow overflow-hidden" href="javascript:void(0)">
                <div class="block-content block-content-full">
                    <div class="text-right">
                        <i class="si si-users fa-2x text-body-bg-dark"></i>
                    </div>
                    <div class="row py-20">
                        <div class="col-6 text-right border-r">
                            <div class="invisible" data-toggle="appear" data-class="animated fadeInLeft">
                                <div class="font-size-h3 font-w600 text-info">{{$my_companies}}</div>
                                <div class="font-size-sm font-w600 text-uppercase text-muted">MY COMPANIES</div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="invisible" data-toggle="appear" data-class="animated fadeInRight">
                                <div class="font-size-h3 font-w600 text-success">{{($companies)}}</div>
                                <div class="font-size-sm font-w600 text-uppercase text-muted">ALL COMPANIES</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Row #4 -->
    </div>
    <div class="row gutters-tiny invisible" data-toggle="appear">
        <!-- Row #5 -->
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="{{ url('reaction') }}">
                <div class="block-content ribbon ribbon-bookmark ribbon-success ribbon-left">
                    <!-- <div class="ribbon-box">{{ \App\Model\Reaction\Reaction::count() }}</div> -->
                    <p class="mt-5">
                        <i class="si si-envelope-letter fa-3x"></i>
                    </p>
                    <p class="font-w600">CREATE <br> REACTION</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="{{ url('learning') }}">
                <div class="block-content ribbon ribbon-bookmark ribbon-primary ribbon-left">
                   <!--  <div class="ribbon-box">{{ \App\Model\Learning\Learning::count() }}</div> -->
                    <p class="mt-5">
                        <i class="si si-user fa-3x"></i>
                    </p>
                    <p class="font-w600">CREATE <br> LEARNING</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="javascript:void(0)">
                <div class="block-content ribbon ribbon-bookmark ribbon-primary ribbon-left">
                    {{--  <div class="ribbon-box">3</div>  --}}
                    <p class="mt-5">
                        <i class="si si-bubbles fa-3x"></i>
                    </p>
                    <p class="font-w600">CREATE <br> BEHAVIOR</p>
                </div>
            </a>
        </div>
         <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="javascript:void(0)">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-bar-chart fa-3x"></i>
                    </p>
                    <p class="font-w600">PERFORMANCE <br> REPORT</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="javascript:void(0)">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-magnifier fa-3x"></i>
                    </p>
                    <p class="font-w600">CREATE <br> ROTI</p>
                </div>
            </a>
        </div>
       
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="{{url('user/edit/'.Auth::user()->id)}}">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-settings fa-3x"></i>
                    </p>
                    <p class="font-w600">SETTINGS <br> {{ \Auth::user()->name }}</p>
                </div>
            </a>
        </div>
        <!-- END Row #5 -->
    </div>
</div>

@endsection

@push('script')
    <!-- Page JS Plugins -->
    <script src="{{ asset('assets/js/plugins/chartjs/Chart.bundle.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('assets/js/pages/be_pages_dashboard.js') }}"></script>


    <script src="{{asset('assets/js/plugins/chartjs/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/chartjs/be_comp_charts.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/chartjs/jquery.flot.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/chartjs/jquery.flot.pie.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/chartjs/jquery.flot.stack.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/chartjs/jquery.flot.resize.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/chartjs/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/chartjs/jquery.easypiechart.min.js')}}"></script>
    <script>
new Chart(document.getElementById("js-chartjs-bars-learning"), {
    type: 'bar',
    data: {
      labels: ["Learning"],
      datasets: [
        {
          label: "Learning",
          backgroundColor: "#71bcf8",
          data: [35]
        }, 
      ]
    },
    options: {
      title: {
        display: true,
        
      }
    }
});
</script>
<script>
new Chart(document.getElementById("js-chartjs-bars-reaction"), {
    type: 'bar',
    data: {
      labels: ["Reaction"],
      datasets: [
        {
          label: "Reaction",
          backgroundColor: "#d0e8fc",
          data: [70]
        }, 
      ]
    },
    options: {
      title: {
        display: true,
        
      }
    }
});
</script>
<!-- <script type="text/javascript">
     $(document).ready(function(){
setInterval(function(){
    $.ajax({
      url: "{{ url('trainning/check') }}",
       type: 'GET',
      success: function( response ) {
        // update div
      }
    });
},5000);
console.log(response);
});
</script> -->
@endpush

