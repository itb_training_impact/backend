@extends('layouts.root')

@section('title','Unidentified Word')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Unidentified Word</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
    		@if(Session::get('status') == '200')
    	        <div class="alert alert-success alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @elseif(Session::get('status') == 'err')
    			<div class="alert alert-danger alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @endif
        @endif

        <h2 class="content-heading">
            Unidentified Word
        </h2>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">List of Unidentified Word</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <table class="table table-vcenter table-stripped">
                    <thead>
                        <tr>
                            <th width="1%"><center><input type="checkbox" id="check-all"></center></th>
                            <th style="width: 50px;">No</th>
                            <th>Name</th>
                            <th>Rasio</th>
                            <th style="width:520px">Suggestion</th>
                            <th style="width: 120px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($words) > 0)
                            @foreach($words as $result)
                            	<form action="{{ url('unidentified_word/update/' . $result->id) }}" method="post">
                            		<input type="hidden" name="_method" value="PUT">
	                            	@csrf
	                                <tr>
	                                    <td align="center"><input type="checkbox" class="checkbox" data-id="{{ $result->id }}" name="checkbox[{{ $result->id }}]"></td>
	                                    <td>{{ $loop->iteration }}</td>
	                                    <td>{{ $result->name }}</td>
	                                    <td>{{ $result->count }}</td>
	                                    <td>
	                                        <select name="wordCategories[]" class="js-select2 form-control" multiple="multiple" data-placeholder="Categories" id="select-{{ $result->id }}">
					                            <option value="">Word Category</option>
					                            @foreach($wordCategories as $value)
					                                <option value="{{ $value->id }}" {{ Request::get('wordCategory') ==  $value->id ? "selected" : "" }} >{{ $value->name }}</option>
					                            @endforeach
					                        </select>
	                                    </td>
	                                    <td>
	                                        <button class="btn btn-sm btn-success" data-toggle="tooltip" title="Simpan">
	                                            <i class="fa fa-check"></i></button>
	                                        </a>
	                                        <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('unidentified_word/delete/'. $result->id) }}" class="btn btn-sm btn-danger btn-xs btn-delete" >
	                                            <i class="fa fa-trash"></i>
	                                        </button>
	                                    </td>
	                                </tr>
                                </form>
                            @endforeach
						@else
							<tr>
								<td colspan='7' class="text-center">Data not available.</td>
							</tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Table -->
        <div class="block">
            <div class="block-content" style="padding: 18px;">
                <div class="row">
                    <div class="col align-self-center">
                        Showing {{ count($words) == 0 ? 0 : count($words) }} out of {{ $total }} entries
                    </div>
                    <div class="col d-flex justify-content-end">
                        <div class="align-self-center" style="height: 34px;">
                            {{ $words->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <div class="block-content d-flex flex-row" style="padding: 18px;">
            	<form action="{{ url('unidentified_word/update_all') }}" method="post" id="multiple_save_form">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                    <span id ="create_input_hidden_save"></span>
            		<button class="btn btn-success btn-sm" onclick="return confirm('Save Unidentified Word?');">Save Unidentified Word</button>
                </form>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <form action="{{ url('unidentified_word/delete_all') }}" method="post" id="multiple_delete_form">
                    {{ csrf_field() }}
                    <span id ="create_input_hidden_delete"></span>
                    <button class="btn btn-danger btn-sm" onclick="return confirm('Delete Unidentified Word?');">Delete Unidentified Word</button>
                </form>
            </div>
        </div>
    </div>


</div>

@include('modal_delete')
@include('modal_import_word')
@endsection

@push('script')
    <!-- DataTables -->
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>

    <script type="text/javascript">
    	$('.js-select2').select2()

        $("#check-all").change(function(){
            if ($(this).is(':checked')) {
                $('.checkbox').prop('checked', true);
            } else {
                $('.checkbox').prop('checked', false);
            }
        });

        $("#multiple_save_form").submit(function(){
			var data = [];
			$("#create_input_hidden_save").html("");
			$('input[name^="checkbox"]:checked').each(function() {
				$("#create_input_hidden_save").append("<input type='hidden' name='check["+$(this).data("id")+"]' value='"+$(this).val()+"'>");
				$("#create_input_hidden_save").append("<input type='hidden' name='select["+$(this).data("id")+"]' value='"+$("#select-"+$(this).data("id")).select2("val")+"'>");
			});
			return true;
		});

        $("#multiple_delete_form").submit(function(){
            var data = [];
            $("#create_input_hidden_delete").html("");
            $('input[name^="checkbox"]:checked').each(function() {
                $("#create_input_hidden_delete").append("<input type='hidden' name='check["+$(this).data("id")+"]' value='"+$(this).val()+"'>");
            });
            return true;
        });

        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush