<table class="table">
    <tbody>      
      <tr>
        <td>Data Pertanyaan dan Jawaban</td>
      </tr>
        <tr>  
          <td></td>        
          @php
            $label_arr = array();
            $reaction_id_arr = array();
            $learning_id_arr = array();
            $behaviour_id_arr = array();
          @endphp                           
          @foreach($submit->get() as $sub)  
            @php 
              array_push($label_arr,$sub->reaction['reaction_question']);
              array_push($reaction_id_arr,$sub->reaction_id);

              array_push($label_arr,$sub->learning['learning_question']);
              array_push($learning_id_arr,$sub->learning_id);              
              if($sub->level_intermediary == ""){
                array_push($label_arr,$sub->behaviour['behaviour_question']);
                array_push($behaviour_id_arr,$sub->behaviour_id);              
              }
            @endphp
          @endforeach
          @foreach (array_unique($label_arr) as $key => $item)
            @if(!empty($item))
              <td>{{ $item }}</td>  
            @endif
          @endforeach
        </tr>
        @php $name_id_arr = array(); @endphp
        @foreach($submit->get() as $sub)
          @if ($sub->user_id != "")            
            @php $name_participant = App\Helper\Application::get_user($sub->user_id) @endphp
            @php array_push($name_id_arr,$name_participant[0]->id);  @endphp
          @endif
        @endforeach
        @foreach (array_unique($name_id_arr) as $key => $user_id)
          @php $name_participant = App\Helper\Application::get_user($user_id) @endphp
          <tr>
            <td>{{ $name_participant[0]->name }}</td>     
            <!-- REACTION -->                    
            @foreach(array_unique($reaction_id_arr) as $item)                
              @php $result = App\Model\Trainning\TrainningSubmit::where("module_id", $submit->get()[0]->module_id)->where("user_id", "=", $user_id)->where("reaction_id", $item)->get() @endphp
              <!-- <td>{{!empty($result[0]) ? $result[0]->submit_response_real_answer : ""}}</td> -->
              <td>{{!empty($result[0]) ? ($result[0]->submit_response * ($result[0]->reaction['type_id'] == 3 ? 100 : 1)) : ""}}</td>
            @endforeach 

            <!-- LEARNING -->                    
            @foreach(array_unique($learning_id_arr) as $item)                
              @php $result = App\Model\Trainning\TrainningSubmit::where("module_id", $submit->get()[0]->module_id)->where("user_id", "=", $user_id)->where("learning_id", $item)->get() @endphp
              <!-- <td>{{!empty($result[0]) ? $result[0]->submit_response_real_answer : ""}}</td>-->
              <td>{{!empty($result[0]) ? ($result[0]->submit_response * ($result[0]->learning['type_id'] == 3 ? 100 : 1)) : ""}}</td>
            @endforeach 

            <!-- BEHAVIOR -->
            @foreach(array_unique($behaviour_id_arr) as $item)     
              @if(!empty($item))           
                @php $result = App\Model\Trainning\TrainningSubmit::where("module_id", $submit->get()[0]->module_id)->where("user_id", "=", $user_id)->where("behaviour_id", $item)->where("level_intermediary", null)->get() @endphp
                <!-- <td>{{!empty($result[0]) ? $result[0]->submit_response_real_answer : ""}}</td>-->
                <td>{{!empty($result[0]) ? ($result[0]->submit_response * ($result[0]->behaviour['type_id'] == 3 ? 100 : 1)) : ""}}</td>
              @endif
            @endforeach            
          </tr>
        @endforeach        
    </tbody>
</table>
