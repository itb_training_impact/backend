<table class="table">
  <tbody>
    <tr>
      <td>Data Learning</td>
    </tr>
    <tr>
      <td></td>
      @php
        $label_arr = array();
      @endphp                           
      @foreach (App\Model\Trainning\TrainningSubmit::filterDimension($trainingID, $sessionID, $moduleID, null, $periode, 'learning')->get() as $item)   
        @php array_push($label_arr,$item->group) @endphp
      @endforeach
      @foreach (array_unique($label_arr) as $item)
        <td>{{ $item }} (%)</td>  
      @endforeach
      <td>Rata-rata</td>
    </tr>
    @foreach (App\Model\Trainning\TrainningSubmit::filterDimension($trainingID, $sessionID, $moduleID, null, $periode, 'learning', null, true)->get() as $itemPrimary)
      @if ($itemPrimary->participant_id != "")
        @php $name_participant = App\Helper\Application::get_user($itemPrimary->participant_id) @endphp
        <tr>
          <td>{{$name_participant[0]->name}}</td>
          @php
            $data_arr = array();
          @endphp                           
          @foreach (App\Model\Trainning\TrainningSubmit::filterDimension($trainingID, $sessionID, $moduleID, $itemPrimary->participant_id, $periode, 'learning')->get() as $item)    
            @php
              $devide = App\Helper\Application::get_calculate('learning', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id)[0]->result;
              $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
              array_push($data_arr,$calculate)
            @endphp                                                
            <td>{{ round($calculate,2) }}</td>                      
          @endforeach    
          <td>{{round(array_sum($data_arr)/(100*count($data_arr))*100,2)}}</td>
        </tr>
      @endif
    @endforeach
  </tbody>
</table>

