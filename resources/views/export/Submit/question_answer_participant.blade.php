<table class="table">
    <thead>      
        <tr>
            <th>Nama</th>
            @foreach($submit as $sub)
            <th>{{$sub->reaction['reaction_question']}}{{$sub->learning['learning_question']}}{{$sub->behaviour['behaviour_question']}}</th>
            @endforeach
        </tr>
        <tr>
            <th>{{$participant->name}}</th>
            @foreach($submit as $sub)
            <!-- @if(!empty($sub->reaction['reaction_answer']) && $sub->reaction['type_id'] == 1)
                <th>{{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer : $sub->submit_response}}</th>
            @elseif(!empty($sub->learning['learning_answer']) && $sub->learning['type_id'] == 1)
                <th>{{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer : $sub->submit_response}} </th>
            @elseif(!empty($sub->behaviour['behaviour_answer']) && $sub->behaviour['type_id'] == 1)
                <th>{{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer : $sub->submit_response}} </th>
            @else
                <th>{{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer : $sub->submit_response}}</th>
            @endif  -->
                @if(!empty($sub->reaction['reaction_answer']) && $sub->reaction['type_id'] == 1)
                    <th>{{$sub->submit_response * ($result[0]->reaction['type_id'] == 3 ? 100 : 1) }}</th>
                @elseif(!empty($sub->learning['learning_answer']) && $sub->learning['type_id'] == 1)
                    <th>{{$sub->submit_response * ($result[0]->reaction['type_id'] == 3 ? 100 : 1) }} </th>
                @elseif(!empty($sub->behaviour['behaviour_answer']) && $sub->behaviour['type_id'] == 1)
                    <th>{{$sub->submit_response * ($result[0]->reaction['type_id'] == 3 ? 100 : 1) }} </th>
                @else
                    <th>{{$sub->submit_response }}</th>
                @endif 
            @endforeach   
        </tr>
    </thead>
</table>
