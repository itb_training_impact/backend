<table class="table">
    <thead>
        <tr>
            <th colspan="3" align="center">PERTANYAAN DAN JAWABAN {{ strtoupper($trainning->trainning_title) }}</th>
        </tr>
        <tr>
            <th colspan="3" align="center"><b>{{ ucfirst($participant->name) }}</b></th>
        </tr>        
        <tr>
            <th width="7">No</th>
            <th>Pertanyaan</th>
            <th>Jawaban</th>
        </tr>
    </thead>
    <tbody>
    @if($level == 'participant')
        <?php $level = null ?>
    @elseif($level == 'boss')
        <?php $level = 1 ?>
    @elseif($level == 'partner')
        <?php $level = 2 ?>
    @else
        <?php $level = 3 ?>
    @endif
    @foreach($submit->where('level_intermediary', $level) as $sub)
        <tr>
            <td>{{$loop->iteration}}</td>

            <td>{{$sub->reaction['reaction_question']}}{{$sub->learning['learning_question']}}{{$sub->behaviour['behaviour_question']}}</td>

            @if(!empty($sub->reaction['reaction_answer']) && $sub->reaction['type_id'] == 1)
                <td>{{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer : $sub->submit_response}}</td>
            @elseif(!empty($sub->learning['learning_answer']) && $sub->learning['type_id'] == 1)
                <td>{{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer : $sub->submit_response}}</td>
            @elseif(!empty($sub->behaviour['behaviour_answer']) && $sub->behaviour['type_id'] == 1)
                <td>{{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer : $sub->submit_response}}</td>
            @else
                <td>{{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer : $sub->submit_response}}</td>
            @endif    
             
        </tr>
    @endforeach
    
    </tbody>
</table>
