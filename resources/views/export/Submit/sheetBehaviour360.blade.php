<table class="table">
    <thead>
        <tr>
            <th colspan="3"><b>BEHAVIOUR {{$trainning->trainning_title}}</b></th>
        </tr>
        <tr>
            <th>Dimensi</th>
            <th>Persentase (%)</th>
        </tr>
    </thead>
    <tbody>

         @php
            $data_arr_behaviour = array();
        @endphp                           
        @foreach ($data['behaviour_group'] as $item)                                                                                            
            @php
                $type = 'behaviour';
                 $devide = App\Helper\Application::get_all_behaviour($item->group_id, $item->trainning_id, $item->formulation_id)[0]->result;
                $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                array_push($data_arr_behaviour,$calculate)
            @endphp            
            <tr>
                <td><b>{{ $item->formulation_name }}</b></td>
                <td align="right">{{ round($calculate,2) }}</td>
            </tr>
            
        @endforeach
         <tr>
            <td colspan="2" align="center">RATA - RATA</td>
            <td>{{ round(array_sum($data_arr_behaviour)/(100*count($data_arr_behaviour))*100,2) }}</td>
        </tr>
    </tbody>
</table>
