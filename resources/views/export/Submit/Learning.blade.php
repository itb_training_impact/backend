<table class="table">
    <thead>
        <tr>
            <th colspan="3" align="center">PERTANYAAN DAN JAWABAN {{ strtoupper($trainning->trainning_title) }}</th>
        </tr>      
        <tr>
            <th width="7">No</th>
            <th>Pertanyaan</th>
            @foreach($users as $user)
            <th>{{$user->name}}</th>
           
        </tr>
    </thead>
    <tbody>
    @foreach($submit as $sub)
        <tr>
            <td>{{$loop->iteration}}</td>

            <td>{{$sub->learning['learning_question']}}</td>

            @if(!empty($sub->reaction['reaction_answer']) && $sub->reaction['type_id'] == 1)
                <td>{{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer : $sub->submit_response}} Point Untuk {{ explode(",", $sub->reaction['reaction_answer'])[1] }}</td>
            @else
                <td>{{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer : $sub->submit_response}}</td>
            @endif    
        </tr>
    @endforeach
     @endforeach
    </tbody>
</table>

<table class="table">
    <thead>
        <tr>
            <th colspan="3"><b>LEARNING {{$trainning->trainning_title}}</b></th>
        </tr>
        <tr>
            <th>Dimensi</th>
            <th>Persentase (%)</th>
        </tr>
    </thead>
    <tbody>

         @php
            $data_arr = array();
        @endphp                           
        @foreach ($data['learning_group'] as $item)                                                                                            
            @php
            $type = 'learning';
                $devide = App\Helper\Application::get_all_learning($item->group_id, $item->trainning_id)[0]->result;
                $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                array_push($data_arr,$calculate)
            @endphp            
            <tr>
                <td><b>{{ $item->group }}</b></td>
                <td align="right">{{ round($calculate,2) }}</td>
            </tr>
        @endforeach
         <tr>
            <td colspan="2" align="center">RATA - RATA</td>
            <td>{{ round(array_sum($data_arr)/(100*count($data_arr))*100,2) }}</td>
        </tr>
    </tbody>
</table>

