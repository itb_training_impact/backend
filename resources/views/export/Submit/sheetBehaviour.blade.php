<table class="table table-bordered table-striped">
                    <tr>
                        <th style="vertical-align: middle; text-align:center">Participant</th>
                        @foreach ($wordCategories as $wc)
                            <th style="vertical-align: middle; text-align:center">{!! ucwords($wc->name) !!}</th>
                        @endforeach

                        <th style="vertical-align: middle; text-align:center">Kategori yang Terisi</th>
                        <th style="vertical-align: middle; text-align:center">Huruf / Kata</th>
                        <th style="vertical-align: middle; text-align:center">Jumlah Kata</th>
                        <th style="vertical-align: middle; text-align:center">Kalimat</th>
                        <th style="vertical-align: middle; text-align:center">Total Koherensi</th>
                        <th style="vertical-align: middle; text-align:center">Derajat Koherensi</th>
                        <th style="vertical-align: middle; text-align:center">Koherensi Kalimat</th>

                        @foreach ($formulas as $f)
                            <th style="vertical-align: middle; text-align:center">{!! ucwords($f->name) !!}</th>
                        @endforeach

                        <th style="vertical-align: middle; text-align:center">Kata tak Teridentifikasi</th>
                        <th style="vertical-align: middle; text-align:center; min-width: 100px;">Aksi</th>
                    </tr>

                    @if (count($stories))
                    <tr style="border: 1px solid #eaecee">
                        <td rowspan="{{ count($stories) }}" style="vertical-align: middle; border: 1px solid #eaecee;">{{ $users->name }}</td>
                        @foreach ($stories as $key => $story)
                            @if($key)
                                <tr style="border: 1px solid #eaecee">
                            @endif

                            @php

                            $countCategories = 0;
                            $percentageCategories = $story->countCategoryPercentage();

                            $storyDetail = $story->detail;

                            // Categories
                            foreach ($wordCategories as $cat) {
                                if ($cat->code != 'SC01') {
                                    $percentage = !empty($percentageCategories[$cat->id]) ? $percentageCategories[$cat->id] : '-';
                                    if ($percentage)
                                        $countCategories++;

                                    if ($percentage != '-')
                                        echo '<td style="vertical-align: middle;" align="center">'.number_format($percentage, 2).'%</td>';
                                    else
                                        echo '<td style="vertical-align: middle;" align="center">' . $percentage . '</td>';
                                }
                            }

                            echo '<td style="vertical-align: middle;" align="center">'.$countCategories.'</td>';

                            // Huruf
                            $char = $storyDetail->countCharPerWord();
                            $conjuct = $storyDetail->countConjuction();
                            echo '<td style="vertical-align: middle;" align="center">'.$char['char_per_word'].'</td>';

                            // Kata
                            echo '<td style="vertical-align: middle;" align="center">'.$char['total_words'].'</td>';


                            // Kalimat
                            $countSentence = $storyDetail->countSentence();
                            echo '<td style="vertical-align: middle;" align="center">'.$countSentence.'</td>';

                            // Koherensi
                            $countCoherence = $storyDetail->countCoherenceOfSentences($countSentence);
                            $percentageCoherence = (($countCoherence + $conjuct) / (empty($char['total_words']) ? 1 : $char['total_words']) * 100) . '%';
                            echo '<td style="vertical-align: middle;" align="center">';
                            echo $countCoherence;
                            echo'</td>';

                            // Derajat Koherensi
                            echo '<td style="vertical-align: middle;" align="center">';
                            echo $countSentence / (empty($char['total_words']) ? 1 : $char['total_words']) * 100 . '%';
                            echo '</td>';

                            echo '<td style="vertical-align: middle;" align="center">';
                            echo $percentageCoherence;
                            echo'</td>';

                            // Formula
                            foreach ($formulas as $formula) {
                                $value = '';
                                $results = $storyDetail->results;

                                foreach ($results as $result) {
                                    if ($result->formula_id == $formula->id) {
                                        $value = number_format($result->score, 2).'%';
                                    } else {
                                        $value = '-';
                                    }
                                }
                                echo '<td style="vertical-align: middle;" align="center">'.$value.'</td>';
                            }
                            @endphp

                            <td style="vertical-align: middle;" align="center">{{ ($percentageCategories['total_unidentified_words'] ? $percentageCategories['total_unidentified_words'] : 0) }}</td>

                            @if (!$key)
                            @endif

                            @if ($key)
                                </tr>
                            @endif
                        @endforeach
                    </tr>
                    @endif
                    </table>
