<table class="table">
    <thead>
        <tr>
            <th colspan="3" align="center">
                @if($section == 'reaction')
                    REACTION {{ strtoupper($trainning->trainning_title) }}
                @elseif($section == 'learning')
                    LEARNING {{ strtoupper($trainning->trainning_title) }}
                @else
                    BEHAVIOUR {{ strtoupper($trainning->trainning_title) }}
                @endif
            </th>
        </tr>
        <tr>
            <th></th>
            <th align="center"><b>Sesi Pelatihan</b></th>
            <th align="center"><b>Module Pelatihan</b></th>
        </tr>
        <tr>
            <th></th>
            <th align="center">{{ ucfirst($sessions->name) }}</th>
            <th align="center">{{ ucfirst($modules->name) }}</th>
        </tr>
        <tr>
            <th width="7">No</th>
            <th>Dimensi</th>
            <th>Persentase (%)</th>
        </tr>
    </thead>
    <tbody>
    @if($section == 'reaction')
        @php
            $data_arr = array();
        @endphp                           
        @foreach ($data['reaction_group'] as $item)                                                                                            
            @php
                $devide = App\Helper\Application::get_calculate('reaction', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id)[0]->result;
                $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                array_push($data_arr,$calculate)
            @endphp            
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><b>{{ $item->group }}</b></td>
                <td align="right">{{ round($calculate,2) }}</td>
            </tr>
        @endforeach
    @elseif($section == 'learning')
        @php
            $data_arr = array();
        @endphp                           
        @foreach ($data['learning_group'] as $item)                                                                                            
            @php
                $devide = App\Helper\Application::get_calculate('learning', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id)[0]->result;
                $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                array_push($data_arr,$calculate)
            @endphp            
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><b>{{ $item->group }}</b></td>
                <td align="right">{{ round($calculate,2) }}</td>
            </tr>
        @endforeach
    @else
        @php
            $data_arr = array();
        @endphp                           
        @foreach ($data['behaviour_group'] as $item)                                                                                            
            @php
                $devide = App\Helper\Application::get_calculate('behaviour', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id, $item->formulation_id)[0]->result;
                $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                array_push($data_arr,$calculate)
            @endphp            
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><b>{{ $item->formulation_name }}</b></td>
                <td align="right">{{ round($calculate,2) }}</td>
            </tr>
        @endforeach
    @endif
        <tr>
            <td colspan="2" align="center">RATA - RATA</td>
            <td>{{ round(array_sum($data_arr)/(100*count($data_arr))*100,2) }}</td>
        </tr>
    
    </tbody>
</table>
