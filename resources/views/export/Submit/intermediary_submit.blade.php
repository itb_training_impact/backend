<table class="table">
    <thead>
        <tr>
            <th colspan="3" align="center">BEHAVIOUR {{ strtoupper($trainning->trainning_title) }}</th>
        </tr>
        <tr>
            <th width="6">No</th>
            <th>Dimensi</th>
            <th width="17">Persentase (%)</th>
        </tr>
    </thead>
    <tbody>
    @if($section == 'boss')
        @php
            $data_arr = array();
        @endphp                           
        @foreach ($data['boss'] as $item)                                                                                            
            @php
                $devide = App\Helper\Application::get_calculate('behaviour', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id, $item->formulation_id)[0]->result;
                $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                array_push($data_arr,$calculate)
            @endphp
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><b>{{ $item->formulation_name }}</b></td>
                <td align="right">{{ round($calculate,2) }}</td>
            </tr>
        @endforeach
    @elseif($section == 'partner')
        @php
            $data_arr = array();
        @endphp                           
        @foreach ($data['partner'] as $item)                                                                                            
            @php
                $devide = App\Helper\Application::get_calculate('behaviour', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id, $item->formulation_id)[0]->result;
                $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                array_push($data_arr,$calculate)
            @endphp
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><b>{{ $item->formulation_name }}</b></td>
                <td align="right">{{ round($calculate,2) }}</td>
            </tr>
        @endforeach
    @else
        @php
            $data_arr = array();
        @endphp                           
        @foreach ($data['subordinate'] as $item)                                                                                            
            @php
                $devide = App\Helper\Application::get_calculate('behaviour', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id, $item->formulation_id)[0]->result;
                $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                array_push($data_arr,$calculate)
            @endphp
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><b>{{ $item->formulation_name }}</b></td>
                <td align="right">{{ round($calculate,2) }}</td>
            </tr>
        @endforeach
    @endif
        <tr>
            <td colspan="2" align="center">RATA - RATA</td>
            <td>{{ round(array_sum($data_arr)/(100*count($data_arr))*100,2) }}</td>
        </tr>
    
    </tbody>
</table>
