
<table class="table">
    <thead>
        <tr>
            <th colspan="3"><b>LEARNING {{$trainning->trainning_title}}</b></th>
        </tr>
        @foreach ($data['training_submit_learnings'] as $learning)  
            <tr>
                @php
                    $data_arr = array();
                     $learning_group     = App\Model\Trainning\TrainningSubmit::filterDimension($trainningID, $learning->session_id,  $learning->module_id, $userID, null, 'learning')->get();
                @endphp 
                @foreach ($learning_group as $item) 
                    @php
                        $type = 'learning';
                        $devide = App\Helper\Application::get_calculate($item->group_id, $item->trainning_id)[0]->result;
                        $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                        array_push($data_arr,$calculate)
                    @endphp             
                    <th><b>{{ $item->group }}</b></th>
                @endforeach
                <th align="center">RATA - RATA</th>
            </tr>
            <tr>
                @php
                    $data_arr = array();
                    $learning_group     = App\Model\Trainning\TrainningSubmit::filterDimension($trainningID, $learning->session_id,  $learning->module_id, $userID, null, 'learning')->get();
                @endphp 
                @foreach ($learning_group as $item)  
                    @php
                        $type = 'learning';
                        $devide = App\Helper\Application::get_calculate($item->group_id, $item->trainning_id)[0]->result;
                        $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
                        array_push($data_arr,$calculate)
                    @endphp             
                    <th>{{ round($calculate,2) }}</th>
                @endforeach
                <th>{{ round(array_sum($data_arr)/(100*count($data_arr))*100,2) }}</th>
            </tr>
        @endforeach
    </thead>
</table>
