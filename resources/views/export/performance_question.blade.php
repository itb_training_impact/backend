<table class="table">
    <tbody>      
      <tr>
        <td>Data Pertanyaan dan Jawaban</td>
      </tr>
        <tr>  
          <td></td>        
          @php
            $label_arr = array();
            $performance_id_arr = array();
            $roti_id_arr = array();
          @endphp                           
          @foreach($submit->get() as $sub)  
            @php 
              array_push($label_arr,$sub->performance['performance_question']);
              array_push($performance_id_arr,$sub->performance_id);

              array_push($label_arr,$sub->roti['roti_question']);
              array_push($roti_id_arr,$sub->roti_id);              
          
            @endphp
          @endforeach
          @foreach (array_unique($label_arr) as $key => $item)
            @if(!empty($item))
              <td>{{ $item }}</td>  
            @endif
          @endforeach
        </tr>
        @php $name_id_arr = array(); @endphp
        @foreach($submit->get() as $sub)
          @if ($sub->user_id != "")            
            @php $name_participant = App\Helper\Application::get_user($sub->user_id) @endphp
            @php array_push($name_id_arr,$name_participant[0]->id);  @endphp
          @endif
        @endforeach
        @foreach (array_unique($name_id_arr) as $key => $user_id)
          @php $name_participant = App\Helper\Application::get_user($user_id) @endphp
          <tr>
            <td>{{ $name_participant[0]->name }}</td>     
            <!-- performance -->                    
            @foreach(array_unique($performance_id_arr) as $item)                
              @php $result = App\Model\Trainning\TrainningSubmit::where("module_id", $submit->get()[0]->module_id)->where("user_id", "=", $user_id)->where("performance_id", $item)->get() @endphp
              <!-- <td>{{!empty($result[0]) ? $result[0]->submit_response_real_answer : ""}}</td> -->
              <td>{{!empty($result[0]) ? ($result[0]->submit_response * ($result[0]->performance['type_id'] == 3 ? 100 : 1)) : ""}}</td>
            @endforeach 

            <!-- roti -->                    
            @foreach(array_unique($roti_id_arr) as $item)                
              @php $result = App\Model\Trainning\TrainningSubmit::where("module_id", $submit->get()[0]->module_id)->where("user_id", "=", $user_id)->where("roti_id", $item)->get() @endphp
              <!-- <td>{{!empty($result[0]) ? $result[0]->submit_response_real_answer : ""}}</td>-->
              <td>{{!empty($result[0]) ? ($result[0]->submit_response * ($result[0]->roti['type_id'] == 3 ? 100 : 1)) : ""}}</td>
            @endforeach      
          </tr>
        @endforeach        
    </tbody>
</table>
