<table border="0" width="100%" cellspacing="0">		
	@foreach ($formulationGroups as $group)
		<tr><td colspan="3" align="center" style="padding:10px;"><h3><b>{{ ucwords($group->name) }}</b></h3></td></tr>
		@foreach ($group->formulation as $formula)
			@foreach ($formula->result($story->detail->id) as $result)
				@php
					$result->score = $result->score > 100 ? 100 : $result->score;
					$half = $result->score - 50 < 0 ? 0 : ($result->score - 50);
				@endphp
				<tr>
					<td colspan="3" style="padding:10px;" align="center">{{ ucwords($result->formula_name) }}</td>
				</tr>
				<tr>
					<td style="padding:10px; font-size:11pt; color:#a94442; text-align: right;">{{ $formula->aspek_terendah }}</td>
					<td width="400px">
						<table width="100%" cellspacing=0 cellpadding=0>
							<tr bgcolor="#f5f5f5">
								<td width="50%" style="border-right:1px solid red; height:30px;">
									<table width="{{ ($result->score > 50 ? 50 * 2 : $result->score * 2) }}%" cellspacing=0>
										<tr>
											<td style="height:30px; {{ ($result->score > 0 ? 'background:#428bca;' : '') }}"></td>
										</tr>
									</table>
								</td>
								<td width="50%" style="border-left:1px solid red; height:30px;">
									<table width="{{ ( round($half, 1) > 50 ? (50 * 2) : round($half * 2, 1)) }}%" cellspacing=0>
										<tr>
											<td style="height:30px; {{ ( round($half * 2, 1) > 0 ? 'background:#428bca;' : '') }}"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td style="padding:10px; font-size:11pt; color:#3c763d; text-align:left;">{{ $formula->aspek_tertinggi }}</td>
				</tr>
			@endforeach
		@endforeach
		<tr><td colspan="2"><br/></td></tr>
	@endforeach
</table>