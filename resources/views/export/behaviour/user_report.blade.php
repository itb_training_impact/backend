<table class="table">
    <thead>
        <tr>
            <th><b>Participant</b></th>
            <th><b>Session</b></th>
            <th><b>Module</b></th>
            <th><b>Periode</b></th>
            @foreach ($wordCategories as $wc)
                <th><b>{!! ucwords($wc->name) !!}</b></th>
            @endforeach

            <th><b>Kategori yang Terisi</b></th>
            <th><b>Huruf / Kata</b></th>
            <th><b>Jumlah Kata</b></th>
            <th><b>Kalimat</b></th>
            <th><b>Total Koherensi</b></th>
            <th><b>Derajat Koherensi</b></th>
            <th><b>Koherensi Kalimat</b></th>

            @foreach ($formulas as $f)
                <th><b>{{ ucwords($f->name) }}</b></th>
            @endforeach

            <th><b>Kata tak Teridentifikasi</b></th>
        </tr>
    </thead>
    <tbody>
    @php
        $stories = $user->storyByTrainning($trainningID);
    @endphp

    @if (count($stories))
    <tr>
        <td rowspan="{{ count($stories) }}">{{ $user->name }}</td>
        @foreach ($stories as $key => $story)
            @if($key)
               <tr>
            @endif

            <td>{{ $story->session->name }}</td>
            <td>{{ $story->module->name }}</td>

            @php
            $countCategories = 0;
            $percentageCategories = $story->countCategoryPercentage();

            $storyDetail = $story->detail;

            echo '<td>';
            switch($story->submit_periode) {
                case 0:
                    echo 'before';
                    break;
                case 1:
                    echo 'during';
                    break;
                case 2:
                    echo 'after';
                    break;
            }
            echo '</td>';

            // Categories
            foreach ($wordCategories as $cat) {
                if ($cat->code != 'SC01') {
                    $percentage = !empty($percentageCategories[$cat->id]) ? $percentageCategories[$cat->id] : 0;
                    if ($percentage)
                        $countCategories++;
                    echo '<td>'.number_format($percentage, 2).'%</td>';
                }
            }

            echo '<td>'.$countCategories.'</td>';

            // Huruf
            $char = $storyDetail->countCharPerWord();
            $conjuct = $storyDetail->countConjuction();
            echo '<td>'.$char['char_per_word'].'</td>';

            // Kata
            echo '<td>'.$char['total_words'].'</td>';


            // Kalimat
            $countSentence = $storyDetail->countSentence();
            echo '<td>'.$countSentence.'</td>';

            // Koherensi
            $countCoherence = $storyDetail->countCoherenceOfSentences($countSentence);
            $percentageCoherence = (($countCoherence + $conjuct) / (empty($char['total_words']) ? 1 : $char['total_words']) * 100) . '%';
            echo '<td>';
            echo $countCoherence;
            echo'</td>';

            // Derajat Koherensi
            echo '<td>';
            echo $countSentence / (empty($char['total_words']) ? 1 : $char['total_words']) * 100 . '%';
            echo '</td>';

            echo '<td>';
            echo $percentageCoherence;
            echo'</td>';

            // Formula
            foreach ($formulas as $formula) {
                $value = '';
                $results = $storyDetail->results;

                foreach ($results as $result) {
                    if ($result->formula_id == $formula->id) {
                        $value = number_format($result->score, 2).'%';
                    }
                }
                echo '<td>'.$value.'</td>';
            }
            @endphp

            <td>{{ ($percentageCategories['total_unidentified_words'] ? $percentageCategories['total_unidentified_words'] : 0) }}</td>
            @if ($key)
                </tr>
            @endif
        @endforeach
    @endif
    </tbody>
</table>