    <table class="table table-sorted table-stripped">
        <tr>
          <th>No</th>
          <th>Participant Name</th>
          <th>Boss Name</th>
          <th>Performance</th>
          <th>Perceived</th>
          <th>Nominal</th>
          <th>Interpretasi</th>
          <th>Submit at</th>
          <!-- <th>Action</th>   -->        
        </tr>        
        @if(count($performances) > 0)
          @foreach($performances as $key => $item)
            <tr>
              <td rowspan="2" style="border: 1px solid #eaeaea">{{$key += 1}}</td>
              <td rowspan="2" style="border: 1px solid #eaeaea">{{App\Helper\Application::get_user($item->participant_id)[0]->name}}</td>
            </tr>
            <tr>
              <td style="border: 1px solid #eaeaea">{{ App\Helper\Application::get_relations($item->group, $item->participant_id)->name }}</td>              
              <td style="border: 1px solid #eaeaea">
                @php
                  $devide = App\Helper\Application::get_calculate('performance', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id)[0]->result;
                  $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;              
                @endphp 
                {{ round($calculate,2) }}%
              </td>
                <td style="border: 1px solid #eaeaea">
                @php
                  $criteria_perceived = App\Helper\Application::get_criteria('perceived','1');
                  $criteria_nominal = App\Helper\Application::get_criteria('nominal','1');
                  $devide_roti = App\Helper\Application::get_calculate('roti', $criteria_perceived->criteria_id, $item->trainning_id, $item->session_id, $item->module_id, null, $item->participant_id);                  
                  $devide_roti_nominal = App\Helper\Application::get_calculate('roti', $criteria_nominal->criteria_id, $item->trainning_id, $item->session_id, $item->module_id, null, $item->participant_id);                  
                  if(!empty($devide_roti[0])){
                    $calculate_roti = round($calculate,2) / ($devide_roti[0]->result == 0 ? 1 : $devide_roti[0]->result);   
                  }else{
                    $calculate_roti = 0;                    
                  }                  
                  if(!empty($devide_roti_nominal[0])){
                    $calculate_roti_nominal = (int)str_replace(".","",$devide_roti_nominal[0]->result_real)/(int)str_replace(".","",$devide_roti_nominal[1]->result_real) * 100;
                  }else{
                    $calculate_roti_nominal = 0;
                  }
                @endphp
                {{ round($calculate_roti, 2) }}%                
              </td>
              <td style="border: 1px solid #eaeaea">{{ round($calculate_roti_nominal, 2) }}%</td>
              <td style="border: 1px solid #eaeaea">
                @php $interpretasi = round((round($calculate_roti, 2) + round($calculate_roti_nominal, 2)) / 2,2); @endphp
                {{ $interpretasi < 100 ? "Pelatihan kurang berpengaruh terhadap karyawan" : $interpretasi == 100 ? "Pelatihan tidak memberikan pengaruh terhadap karyawan" : "Pelatihan memiliki pengaruh terhadap karyawan" }}
              </td>
              <td style="border: 1px solid #eaeaea">{{date_format($item->created_at,"d/M/Y H:i")}}</td>
              <!-- <td></td> -->
            </tr>          
          @endforeach
        @else
          <tr>
            <th colspan="8" style="border: 1px solid #eaeaea"><center>No data found</center></th>
          </tr>
        @endif
      </table>