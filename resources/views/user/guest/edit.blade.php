@extends('layouts.root')

@section('title','Guest - Update')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('guest') }}">Guest</a>
        <span class="breadcrumb-item active">Edit</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Ubah Guest</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('guest/update/'.$guests->id) }}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="name" name="name" placeholder="Masukan nama Anda" value="{{ $guests->name }}">
                                <label for="material-name">Nama <span class="text-danger">*</span></label>
                            </div>
                        </div>
                         <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="username" name="username" placeholder="Masukan nama Anda" value="{{ $guests->username }}">
                                <label for="material-name">Username <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="email" name="email" placeholder="Masukan email Anda"value="{{ $guests->email }}">
                                <label for="material-email">Email <span class="text-danger">*</span></label>
                            </div>
                        </div>
                       
                       
                        <div class="form-group">
                            <div class="form-material">
                                <input type="password" autocomplete="off" class="form-control" id="password" name="password" placeholder="Masukan kata sandi Anda">
                                <label for="material-password">Kata Sandi <span class="text-danger">(kosongkan jika tidak ingin diubah)</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="password" autocomplete="off" class="form-control" id="confirm-password" name="confirm-password" placeholder="Masukan konfirmasi password Anda">
                                <label for="material-conf-password">Konfirmasi Kata Sandi</label>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="level_id" name="level_id" data-placeholder="Pilih Level">
                                    <option></option>
                                    @foreach($levels as $level)
                                        @if($user->level_id == $level->level_id)
                                            <option value="{{ $level->level_id }}" selected>{{ $level->level }}</option>
                                        @else
                                            <option value="{{ $level->level_id }}">{{ $level->level }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <label for="level_id">Level <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Save</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
        });

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'name': {
                    required: true
                },
                'email': {
                    required: true,
                    email: true
                },
                'mobile': {
                    required: true
                },
                'sex': {
                    required: true
                },
                'password': {
                    minlength: 5
                },
                'confirm-password': {
                    equalTo: '#password'
                },
                'pin': {
                    digits: true,
                    rangelength: [6, 6]
                },
                'level_id': {
                    required: true,
                },
            },
            messages: {
                'name': {
                    required: 'Inputan nama harus diisi',
                },
                'email': {
                    required: 'Inputan email harus diisi',
                },
                'mobile': {
                    required: 'Inputan no. telepon harus diisi',
                },
                'sex': 'Inputan jenis kelamin harus diisi',
                'password': {
                    minlength: 'Isian kata sandi minimal terdiri dari 5 karakter atau lebih',
                },
                'confirm-password': {
                    minlength: 'Isian kata sandi minimal terdiri dari 5 karakter atau lebih',   
                    equalTo: 'Kata sandi tidak sama'
                },
                'pin': {
                    digits: 'Inputan pin harus angka',
                    rangelength: 'Isian pin harus 6 digit'
                },
                'level_id': 'Level harus dipilih'
            }
        });
    </script>
@endpush
