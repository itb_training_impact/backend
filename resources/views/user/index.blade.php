@extends('layouts.root')

@section('title','Trainer Adminstrator')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Trainer Adminstrator</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif
@if(Auth::user()->level_id ==1)
        <h2 class="content-heading">
            <a class="btn btn-sm btn-warning float-right" href="{{ url('user/create') }}">
                <i class="fa fa-plus"></i> &nbsp; Create Trainer Adminstrator
            </a>
            Trainer Adminstrator
        </h2>
@endif

        <!-- Filter -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Searching</h3>
            </div>
            <div class="block-content">
                <form action="{{ url('user') }}" class="form-inline" method="GET">
                    <div class="form-group" style="width: 25%">
                        <label for="">Search By</label>
                        <select name="key" id="key" class="form-control" style="width: 100%">
                            <option value="">Search By</option>
                            <option value="name" @if(Request::get("key") == 'name') selected @endif>Name</option>
                            <option value="email" @if(Request::get("key") == 'email') selected @endif>Email</option>
                        </select>
                    </div>
                    <div class="form-group" style="margin-left:10px; width: 51%">
                        <label for="">Search</label>
                        <input type="text" autocomplete="off" class="form-control" id="value" name="value" value="{{ Request::get('value') }}" style="width: 100%" placeholder="Search">
                    </div>
                    <div class="form-group" style="margin:20px 0 0 10px; width: 21.5%">
                        <button class="btn btn-primary" style="width: 100%"> <i class="fa fa-search"></i> Cari</button>
                    </div>
                </form> <br>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content">
                <table class="table table-vcenter table-stripped">
                    <thead>
                        <tr>
                            <th style="width: 50px;">No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Company</th>
                            <th>Level</th>
                            <th>Status</th>
                            <th style="width: 200px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($user) > 0)
                            @foreach($user as $key => $result)
                                <tr>
                                    <td>{{ ($user->currentpage()-1) * $user->perpage() + $key + 1 }}</td>
                                    <td>{{ $result->name }}</td>
                                    <td>{{ $result->email }}</td>
                                    <td>{{ $result->company != '' ? $result->company->name : '-' }}</td>
                                    <td>{{ $result->level->level }}</td>
                                    @if(Auth::user()->level_id ==1)
                                    <td>
                                        @if($result->verified ==0)
                                        <button type="button" data-toggle="tooltip" title="Approved" data-action="{{ url('user/approved/'. $result->id) }}" class="btn btn-sm btn-danger btn-xs btn-approved">Not approve yet</button>
                                        @else 
                                        <button class="btn btn-sm btn-success" disabled>Approved</button>
                                        @endif
                                    </td>
                                    @endif
                                     @if(Auth::user()->level_id ==3)
                                    <td>
                                        @if($result->verified ==0)
                                        <button class="btn btn-sm btn-danger" disabled>Not approve yet</button>
                                        @else 
                                        <button class="btn btn-sm btn-success" disabled>Approved</button>
                                        @endif
                                    </td>
                                    @endif
                                    <td>
                                        @if(Auth::user()->level_id ==1)
                                        <a href="{{ url('user/edit/'. $result->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Ubah">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <button type="button" data-toggle="tooltip" title="Hapus" data-action="{{ url('user/delete/'. $result->id) }}" class="btn btn-sm btn-danger btn-xs btn-delete">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Table -->
        {{ $user->links() }}
    </div>


</div>
@include('modal_delete')
@include('modal_approved')
@endsection

@push('script')

    <script type="text/javascript">

        $('.btn-delete').click(function(){
        	var action = $(this).data('action');

        	$('#modalDelete').modal('show');
        	$('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
        
         $('.btn-approved').click(function(){
            var action = $(this).data('action');

            $('#modalApproved').modal('show');
            $('.modal-title').html('Approved Level');

            $('#formApproved').attr('action',action);
        
        });
    </script>
@endpush
