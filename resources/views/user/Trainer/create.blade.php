@extends('layouts.root')

@section('title','Trainner - create')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('participant') }}">Trainner</a>
        <span class="breadcrumb-item active">Create</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-4">
            <div class="block">
                <form class="form-level" action="{{ url('user') }}" method="post">
                        @csrf
                <input type="hidden" name="_method" value="PUT">

                <div class="block-header" style="padding-bottom: 0px; padding-left: 40px;padding-top: 45px;" >
                    <img src="{{asset('assets/img/avatars/org.jpg')}}" style="width: 90%">
                </div>
                <div class="block-content" style="padding-top: 20px;padding-bottom: 30px; padding-left: 45px;">
                    <div class="form-group row">
                            <div class="col-12">
                                <input type="file" id="example-file-multiple-input" name="avatar" multiple>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">PERSONAL INFORMATION</h3>
                </div>
                <div class="block-content">
                    <form action="" style="padding: 2%">
          <div class="row">
              <div class="col-sm-6">
              <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" class="form-control" placeholder="Number Phone..." value="">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Email</label>
                <input type="text" id="email" name="email" class="form-control" placeholder="Postal Code..." value="">
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-sm-6">
              <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" class="form-control" placeholder="Number Phone..." name="username" id="username" value="">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control" placeholder="Postal Code...">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>No phone</label>
                <input type="text" name="no_telpon" class="form-control" placeholder="Number Phone...">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Instansi Perusahaan</label>
                <input type="text" name="instansi_perusahaan" class="form-control" placeholder="Instansi Perusahaan...">
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-6">
                <label>pendidikan</label>
                                <select class="form-control" id="example-select" name="pendidikan_terakhir">
                                    <option value="0">Please select</option>
                                    <option value="1">SD</option>
                                    <option value="2">SMP</option>
                                    <option value="3">SMP</option>
                                    <option value="4">SMA</option>
                                    <option value="5">S1</option>
                                    <option value="6">S2</option>
                                    <option value="7">S3</option>
                                </select>
                            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Instansi Pendidikan</label>
                <input type="text" name="instansi_pendidikan" class="form-control" placeholder="Postal Code..." value="">
              </div>
            </div>
          </div>
          <div class="form-group row">
                            <label class="col-12" for="example-textarea-input">Alamat</label>
                            <div class="col-12">
                                <textarea class="form-control" id="example-textarea-input" name="alamat" rows="6" placeholder="Content.." value=""></textarea>
                            </div>
                        </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="level_id" name="level_id" data-placeholder="Pilih Level">
                                    <option></option>
                                    @foreach($levels as $level)
                                        @if($level->level_id == $level->level_id)
                                            <option value="{{ $level->level_id }}" selected>{{ $level->level }}</option>
                                        @else
                                            <option value="{{ $level->level_id }}">{{ $level->level }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <label for="level_id">Level <span class="text-danger">*</span></label>
                            </div>
                        </div>
                    </div>
          </div>
           <div class="form-group">
                            <button type="submit" class="btn btn-warning">Save</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
        </form>
                </div>
            </div>
        </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
        });

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'name': {
                    required: true
                },
                'email': {
                    required: true,
                    email: true
                },
                'mobile': {
                    required: true
                },
                'sex': {
                    required: true
                },
                'password': {
                    minlength: 5
                },
                'confirm-password': {
                    equalTo: '#password'
                },
                'pin': {
                    digits: true,
                    rangelength: [6, 6]
                },
                'level_id': {
                    required: true,
                },
            },
            messages: {
                'name': {
                    required: 'Inputan nama harus diisi',
                },
                'email': {
                    required: 'Inputan email harus diisi',
                },
                'mobile': {
                    required: 'Inputan no. telepon harus diisi',
                },
                'sex': 'Inputan jenis kelamin harus diisi',
                'password': {
                    minlength: 'Isian kata sandi minimal terdiri dari 5 karakter atau lebih',
                },
                'confirm-password': {
                    minlength: 'Isian kata sandi minimal terdiri dari 5 karakter atau lebih',   
                    equalTo: 'Kata sandi tidak sama'
                },
                'pin': {
                    digits: 'Inputan pin harus angka',
                    rangelength: 'Isian pin harus 6 digit'
                },
                'level_id': 'Level harus dipilih'
            }
        });
    </script>
@endpush
