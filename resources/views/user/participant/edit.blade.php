@extends('layouts.root')

@section('title','Participant - Edit')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('participant') }}">Participant</a>
        <span class="breadcrumb-item active">Ubah</span>
    </nav>
    <!-- Material Design -->
   
<form class="form-level" action="{{ url('participant/update/'.$participant->id) }}" method="post" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="level_id" value="4">
    <div class="row">
        <div class="col-md-3 col-sm-12">
            <div class="block">
                <div class="block-header" style="padding-bottom: 0px; padding-left: 40px;padding-top: 45px;" >
                    @if (config('app.env') === 'development')               
                        <img src="{{'http://46.101.96.173:81/storage/avatar/'.$participant->avatar}}" style="width: 90%">
                    @else
                        <img src="{{'http://127.0.0.1:8000/storage/avatar/'.$participant->avatar}}" style="width: 100%; ">
                    @endif
                </div>
                <div class="block-content" style="padding-top: 20px;padding-bottom: 30px; padding-left: 45px;">
                    <div class="form-group row">
                        <div class="col-12">
                            <input type="file" name="avatar" value="{{ $participant->avatar }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-sm-12">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">PERSONAL INFORMATION - EDIT</h3>
                </div>
                <div class="block-content">
                    <form action="" style="padding: 2%">
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                    <label>Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="Name" id="name" value="{{$participant->name}}">
                                </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                    <label>Username</label>
                                    <input type="text" name="username" class="form-control" placeholder="Username" name="username" id="username" value="{{$participant->username}}">
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                    <label>Email</label>
                                    <input type="text" id="email" name="email" class="form-control" placeholder="Email" id="email" value="{{$participant->email}}">
                                </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control" placeholder="Password" value="" id="password"> 
                                </div>
                                </div>
                            </div>
                        </div>          
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Tanggal lahir</label>
                                        <input type="date" name="dob" class="form-control" id="dob" value="{{$participant->dob}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Umur</label>
                                        <input type="text" name="age" class="form-control" placeholder="Umur" id="age" value="{{$participant->age}}">
                                    </div>
                                </div>
                            </div>
                        </div>   
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Agama</label>
                                        <select class="js-select2 form-control" name="religion" id="religion" data-placeholder="Pilih Agama">
                                            <option value="{{$participant->religion}}">{{$participant->religion}}</option>
                                            <option value="Islam">Islam</option>
                                            <option value="Katolik">Katolik</option>
                                            <option value="Hindu">Hindu</option>
                                            <option value="Budha">Budha</option>
                                            <option value="Kristen">Kristen</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Etnis</label>
                                        <input type="text" name="etnis" class="form-control" placeholder="Etnis" id="etnis" value="{{$participant->etnis}}">
                                    </div>
                                </div>
                            </div>
                        </div>   
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Jenis kelamin</label>
                                        <select class="js-select2 form-control" name="gender" id="gender" data-placeholder="Pilih Jenis Kelamin">
                                            <option value="{{$participant->gender}}">{{$participant->gender}}</option>
                                            <option value="Pria">Pria</option>
                                            <option value="Wanita">Wanita</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Status</label>
                                        <select class="js-select2 form-control"  name="marriage" id="marriage" data-placeholder="Pilih Status">
                                            <option value="{{$participant->marriage}}">{{$participant->marriage}}</option>
                                            <option value="Sudah kawin">Sudah kawin</option>
                                            <option value="Belum Kawin">Belum kawin</option>
                                        </select>
                                    </div>
                                </div>
                            </div>                        
                        </div>    
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Pekerjaan</label>
                                        <input type="text" class="form-control" placeholder="Pekerjaan" name="job" id="job" value="{{$participant->job}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Masa Kerja</label>
                                        <input type="text" class="form-control" placeholder="Masa Kerja" name="years_of_service" id="years_of_service" value="{{$participant->years_of_service}}">
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Perusahaan / Agensi</label>
                                        <select class="js-select2 form-control" id="companies" name="company_id" data-placeholder="Pilih Perusahaan">
                                            <option value="{{ $participant->company_id }}">{{ $participant->company['name'] }}</option>
                                            @foreach($companies as $company)
                                                <option {{ old('company_id') == $company->company_id ? 'selected' : '' }} value="{{ $company->company_id }}">{{ $company->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Jenjang Jabatan</label>
                                        <select class="js-select2 form-control" id="position_level" name="position_level" data-placeholder="Pilih Jenjang Jebatan">
                                            <option value="{{ $participant->position_level_id }}">{{ $participant->position_level['name'] }}</option>
                                            @foreach($position_level as $position)
                                                <option {{ old('position_level_id') == $position->position_level_id ? 'selected' : '' }} value="{{ $position->position_level_id }}">{{ $position->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                    <label>Divisi / Departemen</label>
                                        <select class="js-select2 form-control" id="devision" name="devision" data-placeholder="Pilih Divisi">
                                            <option value="{{ $participant->devision_id }}">{{ $participant->devision['name'] }}</option>
                                            @foreach($devision as $devision)
                                                <option {{ old('devision_id') == $devision->devision_id ? 'selected' : '' }} value="{{ $devision->devision_id }}">{{ $devision->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @foreach($boss as $boss)
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Nama Bos</label>
                                         <input type="text" class="form-control" placeholder="Nama Bos" name="boss_name[]" id="boss_name" value="{{!empty($boss->boss_name) ? $boss->boss_name : ''}}">
                                      
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Email Bos</label>
                                        <input type="text" class="form-control" placeholder="Email Bos" name="boss_email[]" id="boss_email" value="{{!empty($boss->boss_email) ? $boss->boss_email : ''}}">
                                        <input type="hidden" class="form-control" placeholder="{{!empty($boss->code_boss) ? $boss->code_boss : ''}}" name="code_boss" id="code_boss" value="{{!empty($boss->code_boss) ? $boss->code_boss : ''}}">
                                    </div>
                                </div>
                            </div>
                              @endforeach
                               <div class="col-lg-2 col-md-12">
                                 <a class="btn btn-warning sm tambah">Tambah boss</a>
                             </div>
                        </div>
                         <span id="boss"></span>

                        <div class="row">
                             @foreach($partner as $partner)
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Nama Kolega</label>
                                       
                                       
                                        <input type="text" class="form-control" placeholder="Nama Kolega" name="partner_name[]" id="partner_name" value="{{!empty($partner->partner_name) ? $partner->partner_name : ''}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Email Kolega</label>
                                        <input type="text" class="form-control" placeholder="Email Kolega" name="partner_email[]" id="partner_email" value="{{!empty($partner->partner_email) ? $partner->partner_email : ''}}">
                                        <input type="hidden" class="form-control" placeholder="{{!empty($partner->code_partner) ? $partner->code_partner : ''}}" name="code_partner" id="code_partner" value="{{!empty($partner->code_partner) ? $partner->code_partner : ''}}">
                                    </div>
                                </div>
                            </div>
                             @endforeach
                              <div class="col-lg-2 col-md-12">
                                 <a class="btn btn-warning sm tambah_kolega">Tambah kolega</a>
                             </div>
                        </div>
                        <span id="kolega"></span>
                        <div class="row">
                               @foreach($subordinate as $subordinate)
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Nama Bawahan</label>
                                     
                                      
                                        <input type="text" class="form-control" placeholder="Nama Subordinate" name="subordinate_name[]" id="subordinate_name" value="{{!empty($subordinate->subordinate_name) ? $subordinate->subordinate_name : ''}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Email Bawahan</label>
                                        <input type="text" class="form-control" placeholder="Email Subordinate" name="subordinate_email[]" id="subordinate_email" value="{{!empty($subordinate->subordinate_email) ? $subordinate->subordinate_email : ''}}">
                                        <input type="hidden" class="form-control" placeholder="{{!empty($subordinate->code_subordinate) ? $subordinate->code_subordinate : ''}}" name="code_subordinate" id="code_subordinate" value="{{!empty($subordinate->code_subordinate) ? $subordinate->code_subordinate : ''}}">
                                    </div>
                                </div>
                            </div>
                              @endforeach
                               <div class="col-lg-2 col-md-12">
                                 <a class="btn btn-warning sm tambah_bawahan">Tambah bawahan</a>
                             </div>
                        </div>
                        <span id="bawahan"></span>
                      <div class="form-group">
                            <button type="submit" class="btn btn-warning">Save</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <a href="{{ url('participant') }}" class="btn btn-secondary" style="float:right;">Back</a>
                      </div>
                    </form>
                </div>
            </div>
        </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
        </div>
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
         jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
        });
        $('.form-participant').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'name': {
                    required: true,
                },
                'email': {
                    required: true,
                },
                'username': {
                    required: true,
                },
                 'password': {
                    minlength: 5
                },
                  'dob': {
                     required: true,
                },
                 'marriage': {
                     required: true,
                },
                 'job': {
                     required: true,
                },
                 'religion': {
                     required: true,
                },
                'etnis': {
                     required: true,
                },
                'companies': {
                     required: true,
                },
            },
            messages: {
                'name': {
                    required: 'Name harus diisi',
                },
                'email': {
                    required: 'Email harus diisi',
                },
                'username': {
                    required: 'Username harus diisi'
                },
                 'password': {
                    required: 'Password harus diisi',
                },
                 'dob': {
                    required: 'Tanggal Lahir harus diisi',
                },
                 'marriage': {
                     required: 'Status kawin harus diisi',
                },
                'job': {
                     required: 'Pekerjaan  harus diisi',
                },
                 'religion': {
                     required: 'Agama harus diisi',
                },
                 'etnis': {
                        required: 'Etnis harus diisi',
                },
                 'companies': {
                        required: 'Perushaan harus diisi',
                },
            }
        });
        var i = 1;
        var a = 1;

        console.log($("div[id^=box]").length);
        //when the Add Field button is clicked
        $(".tambah").click(function (e) {            
            //Append a new row of code to the "#items" div
            $("#boss").append(
                '<div class="row">'+
                            '<div class="col-lg-6 col-md-12">'+
                                '<div class="form-group">'+
                                    '<div class="form-material">'+
                                        '<label>Nama Bos</label>'+
                                        '<input type="text" class="form-control" placeholder="Nama Bos" name="boss_name[]" id="boss_name">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-lg-4 col-md-12">'+
                                '<div class="form-group">'+
                                    '<div class="form-material">'+
                                        '<label>Email Bos</label>'+
                                        '<input type="text" class="form-control" placeholder="Email Bos" name="boss_email[]" id="boss_email">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                             // '<div class="col-lg-2 col-md-12">'+
                             //     '<button type="button" id="'+i+'" class="btn btn-danger sm deleteSession">Delete boss</button>'+
                             // '</div>'+
                        '</div>'+
                    '</div>'
            ); 
            i++;
        });

           var i = 1;
        var a = 1;

        console.log($("div[id^=box]").length);
        //when the Add Field button is clicked
        $(".tambah_kolega").click(function (e) {            
            //Append a new row of code to the "#items" div
            $("#kolega").append(
                '<div class="row">'+
                            '<div class="col-lg-6 col-md-12">'+
                                '<div class="form-group">'+
                                    '<div class="form-material">'+
                                        '<label>Nama Kolega</label>'+
                                        '<input type="text" class="form-control" placeholder="Nama Kolega" name="partner_name[]" id="partner_name">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-lg-4 col-md-12">'+
                                '<div class="form-group">'+
                                    '<div class="form-material">'+
                                        '<label>Email Kolega</label>'+
                                        '<input type="text" class="form-control" placeholder="Email Kolega" name="partner_email[]" id="partner_email">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                             // '<div class="col-lg-2 col-md-12">'+
                             //     '<button type="button" id="'+i+'" class="btn btn-danger sm deleteSession">Delete boss</button>'+
                             // '</div>'+
                        '</div>'+
                    '</div>'
            ); 
            i++;
        });

         var i = 1;
        var a = 1;

        console.log($("div[id^=box]").length);
        //when the Add Field button is clicked
        $(".tambah_bawahan").click(function (e) {            
            //Append a new row of code to the "#items" div
            $("#bawahan").append(
                '<div class="row">'+
                            '<div class="col-lg-6 col-md-12">'+
                                '<div class="form-group">'+
                                    '<div class="form-material">'+
                                        '<label>Nama Bawahan</label>'+
                                        '<input type="text" class="form-control" placeholder="Nama Bawahan" name="subordinate_name[]" id="subordinate_name">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-lg-4 col-md-12">'+
                                '<div class="form-group">'+
                                    '<div class="form-material">'+
                                        '<label>Email Bawahan</label>'+
                                        '<input type="text" class="form-control" placeholder="Email Bawahan" name="subordinate_email[]" id="subordinate_email">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                             // '<div class="col-lg-2 col-md-12">'+
                             //     '<button type="button" id="'+i+'" class="btn btn-danger sm deleteSession">Delete boss</button>'+
                             // '</div>'+
                        '</div>'+
                    '</div>'
            ); 
            i++;
        });
    </script>
@endpush
