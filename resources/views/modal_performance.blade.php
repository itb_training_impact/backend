 <div class="modal fade" id="modalPerformance" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="block-header bg-primary-dark">
            <h3 class="block-title" style="color: #ffff;">List pertanyaan performance</h3>
            <div class="block-options">
              @if(sizeof($sessionRotis) > 0)
                 <a href="{{url('trainning/getPerformance/'.$trainning->trainning_id.'/'.$sessionRoti->session_id.'/'.$value->module_id)}}" class="performance-restart btn btn-primary">Restart list</a>
                 @endif
                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                    <i class="si si-close"></i>
                </button>
            </div>
        </div>   
        <div class="modal-header">
         <button class="btn btn-success performance-import mb-3" type="button"> <i class="fa fa-upload"></i> &nbsp; Import Question</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          
           
        </div>
        
        <!-- Modal footer -->
       
        
      </div>
    </div>
  </div>