@foreach($sessions as $key=>$session)
@foreach($moduleBefore->where('session_id',$session->session_id) as $key => $value) 
<div class="modal fade" id="modalBehaviour" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="block-header bg-primary-dark">
            <h3 class="block-title" style="color: #ffff;">List pertanyaan behaviour</h3>
            <div class="block-options">
                 <a href="{{url('trainning/getbehaviour/'.$trainning->trainning_id.'/'.$session->session_id.'/'.$value->module_id)}}" class="behaviour-restart-before btn btn-primary">Refresh list</a>
                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                    <i class="si si-close"></i>
                </button>
            </div>
        </div>   
         <div class="modal-header">
         <button class="btn btn-success behaviour-import mb-3" type="button"> <i class="fa fa-upload"></i> &nbsp; Import Question</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          
           
        </div>
        
        <!-- Modal footer -->
       
        
      </div>
    </div>
  </div>
@endforeach
 @foreach($moduleDuring->where('session_id',$session->session_id) as $key => $value) 
  <div class="modal fade" id="modalBehaviourDuring" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="block-header bg-primary-dark">
            <h3 class="block-title" style="color: #ffff;">List pertanyaan behaviour</h3>
            <div class="block-options">
                 <a href="{{url('trainning/getbehaviour/'.$trainning->trainning_id.'/'.$session->session_id.'/'.$value->module_id)}}" class="behaviour-restart-during btn btn-primary">Restart</a>
                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                    <i class="si si-close"></i>
                </button>
            </div>
        </div>
          <div class="modal-header">
         <button class="btn btn-success behaviour-import mb-3" type="button"> <i class="fa fa-upload"></i> &nbsp; Import Question</button>
        </div>   
        <!-- Modal body -->
        <div class="modal-body">
          
           
        </div>
        
        <!-- Modal footer -->
       
        
      </div>
    </div>
  </div>
   @endforeach
   @foreach($moduleAfter->where('session_id',$session->session_id) as $key => $value) 
  <div class="modal fade" id="modalBehaviourAfter" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="block-header bg-primary-dark">
            <h3 class="block-title" style="color: #ffff;">List pertanyaan behaviour</h3>
            <div class="block-options">
                 <a href="{{url('trainning/getbehaviour/'.$trainning->trainning_id.'/'.$session->session_id.'/'.$value->module_id)}}" class="behaviour-restart-after btn btn-primary">Restart</a>
                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                    <i class="si si-close"></i>
                </button>
            </div>
        </div>   
          <div class="modal-header">
         <button class="btn btn-success behaviour-import mb-3" type="button"> <i class="fa fa-upload"></i> &nbsp; Import Question</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          
           
        </div>
        
        <!-- Modal footer -->
       
        
      </div>
    </div>
  </div>
   @endforeach
@endforeach
