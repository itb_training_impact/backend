@extends('layouts.root')

@section('title','Trainning - Edit')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('trainning') }}">Trainning</a>
        <span class="breadcrumb-item active">Edit</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <form class="form-level" action="{{ url('trainning/update/'. $trainning->trainning_id) }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                @csrf
                
                 <div class="block">
                    <div class="block-header block-header-default" style="background-color: #f5f6f7;>
                        <h3 class="block-title">COMPANY</h3>
                    </div>
                        <div class="block-content">
                            <div class="form-group">
                                <div class="form-material">
                                    <select autocomplete="off" class="js-select2 form-control" placeholder="Choose Companies .." id="company_id" name="company_id" multiple="" required>
                                        <option value="">Company ..</option>
                                        @php
                                        $company_id = explode(",",$trainning->company_id);
                                        @endphp
                                    @foreach($companys as $key =>$value)
                                        <option value="{{$value->company_id}}" {{ in_array($value->company_id,$company_id) ? "selected" : "" }} >{{$value->name}}</option>
                                    @endforeach
                                    
                                    </select>
                                <label for="material-email">Company <span class="text-danger">*</span></label>
                            </div> 
                        </div>
                    </div>
                </div>

                <div class="block">
                    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                        <h3 class="block-title">TRAINING</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" class="form-control" name="trainning_title" placeholder="Title of trainning" autofocus="" value="{{$trainning->trainning_title}}" required>
                                <label for="">Title <span class="text-danger">*</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-material">
                                <textarea name="trainning_description" class="form-control" id="trainning_description" cols="30" rows="10" placeholder="Description of trainning" required>{{$trainning->trainning_description}}</textarea>
                                <label for="">Description  <span class="text-danger">*</span></label>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="block">
                    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                        <h3 class="block-title">TRAINING DATE</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group">
                            <div class="form-material">
                                <input type="date" class="form-control" name="start_date" autofocus="" value="{{$trainning->start_date}}" required>
                                <label for="">Start date <span class="text-danger">*</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-material">
                                 <input type="date" class="form-control" name="end_date"  autofocus="" value="{{$trainning->end_date}}" required>
                                <label for="">End date <span class="text-danger">*</span></label>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block">
                    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                        <h3 class="block-title">SPEAKER</h3>
                    </div>
                        <div class="block-content">
                            <div class="form-group">
                                <div class="form-material">
                                    <select autocomplete="off" class="js-select2 form-control" placeholder="Choose Speaker .." id="speaker_id" name="speaker_id[]" multiple="" required>
                                        <option value="">Speaker ..</option>
                                        @php
                                        $speaker_id = explode(",",$trainning->speaker_id);
                                        @endphp
                                    @foreach($speakers as $key =>$value)
                                        <option value="{{$value->speaker_id}}" {{ in_array($value->speaker_id,$speaker_id) ? "selected" : "" }} >{{$value->name}}</option>
                                    @endforeach
                                    
                                    </select>
                                    <label for="material-email">Speaker <span class="text-danger">*</span></label>
                                </div> 
                         </div>
                        </div>
                </div>

                <div class="block">
                    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                        <h3 class="block-title">CHOOSE PARTICIPANT</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group">
                            <div class="form-material">
                                <select autocomplete="off" class="js-select2 form-control" placeholder="Choose formula .." id="participant_id" name="participant_id[]" multiple="" required>
                                    <option value="">Choose Participant ..</option>
                                    @php
                                    $participant_id = explode(",",$trainning->participant_id);
                                    @endphp
                                @foreach($users as $key =>$value)
                                    <option value="{{$value->id}}" {{ in_array($value->id,$participant_id) ? "selected" : "" }} >{{$value->name}}</option>
                                @endforeach
                                
                                </select>
                                <label for="material-email">Participant <span class="text-danger">*</span></label>
                            </div> 
                        </div>
                    </div>
                </div>

                <div class="block additional">
                    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                        <h3 class="block-title">Tambah Session dan Module</h3>
                    </div>
                    @for($s = 0; $s < count($sessions); $s++)
                        <div class="block-content row" id="box{{$s}}">
                            <input type="hidden" name="session_id[{{$s}}]" value="{{$sessions[$s]->session_id}}">
                            <div class="col-8" style="padding-left: 0">
                                <div class="form-group">
                                    <div class="form-material">
                                        <input type="text" class="form-control" name="session_title[{{$s}}]" placeholder="Judul Session" autofocus="" value="{{ $sessions[$s]->name }}" required>
                                        <label for="">Judul Session <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-material">
                                        <textarea name="session_description[{{$s}}]" class="form-control" id="session_description" cols="30" rows="5" placeholder="Deksripsi Session" required>{{ $sessions[$s]->description }}</textarea>
                                        <label for="">Deksripsi Session  <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="col-2">
                                <div class="custom-controls-stacked">
                                <label>Pilih Section</label>
                                    <label class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" name="section[{{$s}}]" value="before" {{ !empty($sessions[$s]->before) != '' ? 'checked' : ''}}>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Before</span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" name="section[{{$s}}]" value="during" {{ !empty($sessions[$s]->during) != '' ? 'checked' : ''}}>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">During</span>
                                    </label>
                                        <label class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" name="section[{{$s}}]" value="after" {{ !empty($sessions[$s]->after) != '' ? 'checked' : ''}}>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">After</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn btn-success addSession" style="width: 100%;">Tambah Session</button>
                            </div>
                            
                            @for($m = 0; $m < count($modules[$s]); $m++)
                            <div class="moduletab{{$s}}{{$m}} col-12">                                
                                <div id="modulebox{{$s.$m}}" style="display: flex; flex-wrap: wrap;">
                                <input type="hidden" name="module_id[{{$s}}][{{$m}}]" value="{{ $modules[$s][$m]->module_id }}">
                                    <div class="col-8">
                                        <input type="hidden" name="modulebox[{{$s}}][{{$m}}]" value="modulebox{{$s.$m}}">
                                        <div class="form-group" style="padding-left: 0">
                                            <div class="form-material">
                                                <input type="text" class="form-control" name="module_title[{{$s}}][{{$m}}]" placeholder="Judul Module" autofocus="" value="{{ $modules[$s][$m]->name }}" required>
                                                <label for="">Judul Module <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                        <div class="form-group" style="padding-left: 15px">
                                            <div class="form-material">
                                                <textarea name="module_description[{{$s}}][{{$m}}]" class="form-control" id="module_description" cols="30" rows="5" placeholder="Deksripsi Module" required>{{ $modules[$s][$m]->description  }}</textarea>
                                                <label for="">Deksripsi Module  <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-4">
                                    @if($m == 0)
                                        <button type="button" class="btn btn-primary addModule" id="{{$s}}">Tambah Module</button> 
                                    @endif                                           
                                    </div>

                                    <div class="col-12">
                                        <div class="custom-controls-stacked modules{{$s.$m}}">
                                             @if( !empty($reaction[$s][$m]) && strval($reaction[$s][$m]) != '[]' )
                                                <div class="col-2">              
                                                    <a href="{{ url('trainning/delete_questions/'.$trainning->trainning_id.'/'.$modules[$s][$m]->module_id. '/reaction') }}" class="btn btn-danger btn-sm">Remove question Reaction</a>
                                                    <input type="hidden" name="module[{{$s}}][{{$m}}][]" value="not">
                                                </div>
                                            @else
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="module[{{$s}}][{{$m}}][]" value="is_reaction" {{ !empty($reaction[$s][$m]) && strval($reaction[$s][$m]) != '[]' ? 'checked disabled '  : '' }}>
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">Reaction</span>
                                                </label>
                                            @endif
                                            @if( !empty($learning[$s][$m]) && strval($learning[$s][$m]) != '[]' )
                                                <div class="col-2">              
                                                    <a href="{{ url('trainning/delete_questions/'.$trainning->trainning_id.'/'.$modules[$s][$m]->module_id. '/learning') }}" class="btn btn-danger btn-sm">Remove question Learning</a>
                                                    <input type="hidden" name="module[{{$s}}][{{$m}}][]" value="not">
                                                </div>
                                            @else
                                                <label class="custom-control custom-checkbox">    
                                                    <input type="checkbox" class="custom-control-input" name="module[{{$s}}][{{$m}}][]" value="is_learning" {{ !empty($learning[$s][$m]) && strval($learning[$s][$m]) != '[]' ? 'checked disabled ' : '' }}>
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">Learning</span>
                                                </label>
                                            @endif
                                            @if( !empty($behaviour[$s][$m]) && strval($behaviour[$s][$m]) != '[]' )
                                                <div class="col-2">              
                                                    <a href="{{ url('trainning/delete_questions/'.$trainning->trainning_id.'/'.$modules[$s][$m]->module_id. '/behaviour') }}" class="btn btn-danger btn-sm">Remove question Behaviour</a>
                                                    <input type="hidden" name="module[{{$s}}][{{$m}}][]" value="not">
                                                </div>
                                            @else
                                                <label class="custom-control custom-checkbox">    
                                                    <input type="checkbox" class="custom-control-input" name="module[{{$s}}][{{$m}}][]" value="is_behaviour" id="{{$s.$m}}" {{ !empty($behaviour[$s][$m]) && strval($behaviour[$s][$m]) != '[]' ? 'checked disabled '  : '' }}>
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">Behaviour</span>
                                                </label>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="formula{{$s.$m}}" style="display: none; width: 65%">
                                        <div class="form-group" style="padding-left: 15px;">
                                            <div class="form-material">
                                            <select autocomplete="off" class="js-select2 form-control" placeholder="Choose formula .." id="formulation_id" name="formulation_id[{{$s}}][{{$m}}][]" multiple="" style="width: 100%">
                                                    <option>Pilih Kompetensi</option>
                                                    @foreach ($formulas as $formula)
                                                        <option value="{{$formula->id}}">{{$formula->name}}</option>
                                                    @endforeach
                                                </select>
                                                <label for="material-email">Pilih Kompetensi<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            @endfor
                        </div>
                    @endfor
                </div>
                <!-- END Static Labels -->


                <div class="form-group">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="reset" class="btn btn-secondary">Reset</button>
                    <a href="{{ url('trainning') }}" class="btn btn-secondary" style="float:right;">Back</a>
                </div>
            </form>
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
        });
        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'trainning_title': {
                    required: true,
                },
                'trainning_description': {
                    required: true,
                }
                
            },
            messages: {
                'trainning_title': {
                    required: 'Title has required',
                },
                'trainning_description': {
                    required: 'Description has required',
                },
                
            }
        });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
        console.log($('input[value^=modulebox0]').length - 1);
        var i = {{count($sessions)}};

        $(".addSession").click(function (e) {            
            //Append a new row of code to the "#items" div
            $(".additional").append(
                ' <div class="block-content row" id="box'+i+'">' +
                    ' <input type="hidden" name="session_id['+i+']" value="">' +
                    ' <div class="col-8" style="padding-left: 0">' +
                        '<div class="form-group">' +
                            '<div class="form-material">' +
                                '<input type="text" class="form-control" name="session_title['+i+']" placeholder="Judul Session" autofocus="" value="{{ old('session_title') }}" required>' +
                                '<label for="">Judul Session <span class="text-danger">*</span></label>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<div class="form-material">' +
                                '<textarea name="session_description['+i+']" class="form-control" id="session_description" cols="30" rows="5" placeholder="Deksripsi Session" required>{{ old('session_description') }}</textarea>' +
                                '<label for="">Deksripsi Session  <span class="text-danger">*</span></label>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +

                        '<div class="col-2">' +
                            '<div class="custom-controls-stacked">' +
                            '<label>Pilih Section</label>' +
                                '<label class="custom-control custom-radio">' +
                                    '<input type="radio" class="custom-control-input" name="section['+i+']" value="before">' +
                                    '<span class="custom-control-indicator"></span>' +
                                    '<span class="custom-control-description">Before</span>' +
                                '</label>' +
                                '<label class="custom-control custom-radio">' +
                                    '<input type="radio" class="custom-control-input" name="section['+i+']" value="during">' +
                                    '<span class="custom-control-indicator"></span>' +
                                    '<span class="custom-control-description">During</span>' +
                                '</label>' +
                                    '<label class="custom-control custom-radio">' +
                                    '<input type="radio" class="custom-control-input" name="section['+i+']" value="after">' +
                                    '<span class="custom-control-indicator"></span>' +
                                    '<span class="custom-control-description">After</span>' +
                                '</label>' +
                            '</div>' +
                        '</div>' +
                        '<div class="col-2">' +
                            '<button type="button" id="'+i+'" class="btn btn-danger deleteSession" style="margin-top: 5px; width: 100%;">Hapus Session</button>' +
                        '</div>' +
                        '<div class="moduletab'+i+'0 col-12">' +
                            '<div id="modulebox'+i+'0" style="display: flex; flex-wrap: wrap; padding: 0;"">' +
                            '<input type="hidden" name="module_id['+i+'][0]" value="">' +
                                '<div class="col-8">' +
                                    '<input type="hidden" name="modulebox['+i+'][0]" value="modulebox'+i+'0">' +
                                    '<div class="form-group" style="padding-left: 0">' +
                                        '<div class="form-material">' +
                                            '<input type="text" class="form-control" name="module_title['+i+'][0]" placeholder="Judul Module" autofocus="" value="{{ old('module_title') }}" required>' +
                                            '<label for="">Judul Module <span class="text-danger">*</span></label>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="form-group" style="padding-left: 15px">' +
                                        '<div class="form-material">' +
                                            '<textarea name="module_description['+i+'][0]" class="form-control" id="module_description" cols="30" rows="5" placeholder="Deksripsi Module" required>{{ old('module_description') }}</textarea>' +
                                            '<label for="">Deksripsi Module  <span class="text-danger">*</span></label>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +

                                '<div class="col-4">' +
                                    '<button type="button" class="btn btn-primary addModule" id="'+i+'">Tambah Module</button>' +
                                '</div>' +

                                    '<div class="col-12">'+ 
                                    '<div class="custom-controls-stacked modules'+i+'0">' +
                                            '<label class="custom-control custom-checkbox">' +
                                                '<input type="checkbox" class="custom-control-input" name="module['+i+'][0][]" value="is_reaction">' +
                                                '<span class="custom-control-indicator"></span>' +
                                                '<span class="custom-control-description">Reaction</span>' +
                                            '</label>' +
                                            '<label class="custom-control custom-checkbox">' +    
                                                '<input type="checkbox" class="custom-control-input" name="module['+i+'][0][]" value="is_learning">' +
                                                '<span class="custom-control-indicator"></span>' +
                                                '<span class="custom-control-description">Learning</span>' +
                                            '</label>' +
                                            '<label class="custom-control custom-checkbox">' +    
                                                '<input type="checkbox" class="custom-control-input" name="module['+i+'][0][]" value="is_behaviour" id='+i+'0>' +
                                                '<span class="custom-control-indicator"></span>' +
                                                '<span class="custom-control-description">Behaviour</span>' +
                                            '</label>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="formula'+i+'0" style="display: none;  width: 65%">' +
                                        '<div class="form-group" style="padding-left: 15px;">' +
                                            '<div class="form-material">' +
                                            '<select autocomplete="off" class="js-select2 form-control" placeholder="Choose formula .." id="formulation_id" name="formulation_id['+i+'][0][]" multiple="" style="width: 100%"> ' +
                                                    '<option>Pilih Kompetensi</option>' +
                                                    '@foreach ($formulas as $formula)' +
                                                        '<option value="{{$formula->id}}">{{$formula->name}}</option>' +
                                                    '@endforeach' +
                                                '</select>' +
                                                '<label for="material-email">Pilih Kompetensi<span class="text-danger">*</span></label>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>'  +
                                '</div>' +        
                            '</div>' +                    
                        '</div>' +
                '</div>'
            ); 
            i++;
            Codebase.helpers('select2');
        });

        $(document).on("click", ".deleteSession", function (e) {
           var button_id = $(this).attr("id");   
           $('#box'+button_id+'').remove();
           i--;
           console.log(i);
        });

        $(document).on("click", ".addModule", function (e) {
            var button_id = $(this).attr("id");
            var a = $('input[value^=modulebox'+button_id+']').length;
            var b = a - 1;
            //Append a new row of code to the "#items" div
            $(".moduletab"+button_id+b).append(                 
                '<div id="modulebox'+button_id+a+'" style="display: flex; flex-wrap: wrap; padding: 0;">' +
                    '<div class="col-8">' +
                        '<input type="hidden" name="module_id['+button_id+']['+a+']" value="">' +
                        '<input type="hidden" name="modulebox['+button_id+']['+a+']" value="modulebox'+button_id+a+'">' +
                        '<div class="form-group" style="padding-left: 0">' +
                            '<div class="form-material">' +
                                '<input type="text" class="form-control" name="module_title['+button_id+']['+a+']" placeholder="Judul Module" autofocus="" value="{{ old('module_title') }}" required>' +
                                '<label for="">Judul Module <span class="text-danger">*</span></label>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group" style="padding-left: 15px">' +
                            '<div class="form-material">' +
                                '<textarea name="module_description['+button_id+']['+a+']" class="form-control" id="module_description" cols="30" rows="5" placeholder="Deksripsi Module" required>{{ old('module_description') }}</textarea>' +
                                '<label for="">Deksripsi Module  <span class="text-danger">*</span></label>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +

                    '<div class="col-4">' +
                        '<button type="button" class="btn btn-danger deleteModule" id="'+button_id+a+'"">Hapus Module</button>' +
                    '</div>' +

                        '<div class="col-12">'+ 
                            '<div class="custom-controls-stacked modules'+button_id+a+'">' +
                                '<label class="custom-control custom-checkbox">' +
                                    '<input type="checkbox" class="custom-control-input" name="module['+button_id+']['+a+'][]" value="is_reaction">' +
                                    '<span class="custom-control-indicator"></span>' +
                                    '<span class="custom-control-description">Reaction</span>' +
                                '</label>' +
                                '<label class="custom-control custom-checkbox">' +    
                                    '<input type="checkbox" class="custom-control-input" name="module['+button_id+']['+a+'][]" value="is_learning">' +
                                    '<span class="custom-control-indicator"></span>' +
                                    '<span class="custom-control-description">Learning</span>' +
                                '</label>' +
                                '<label class="custom-control custom-checkbox modules'+button_id+a+'">' +   
                                    '<input type="checkbox" class="custom-control-input" name="module['+button_id+']['+a+'][]" value="is_behaviour"  id="'+button_id+a+'">' +
                                    '<span class="custom-control-indicator"></span>' +
                                    '<span class="custom-control-description">Behaviour</span>' +
                                '</label>' +
                            '</div>' +
                        '</div>' +
                        '<div class="formula'+button_id+a+'" style="display: none;  width: 65%">' +
                            '<div class="form-group" style="padding-left: 15px;">' +
                                '<div class="form-material">' +
                                '<select autocomplete="off" class="js-select2 form-control" placeholder="Choose formula .." id="formulation_id" name="formulation_id['+button_id+']['+a+'][]" multiple="" style="width: 100%"> ' +
                                        '<option>Pilih Kompetensi</option>' +
                                        '@foreach ($formulas as $formula)' +
                                            '<option value="{{$formula->id}}">{{$formula->name}}</option>' +
                                        '@endforeach' +
                                    '</select>' +
                                    '<label for="material-email">Pilih Kompetensi<span class="text-danger">*</span></label>' +
                                '</div>' +
                            '</div>' +
                        '</div>'  +
                    '</div>' +
                '</div>'
            );
            Codebase.helpers('select2');
        });
        
        $(document).on("click", ".deleteModule", function (e) {
           var button_id = $(this).attr("id");   
           console.log(button_id);
           $('#modulebox'+button_id+'').remove();
           a--;
        });

        $(document).on( "change", 'input[value^=is_behaviour]', function(evt) {
            var formid = $(this).attr("id");
            if($(this).prop("checked")) {
                console.log($(this).attr("id"));
                $('div[class=formula'+formid+']').show();
            } else{
                $('div[class=formula'+formid+']').hide();
            }
        });
    });
    </script>
@endpush
