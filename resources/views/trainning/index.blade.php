@extends('layouts.root')

@section('title','Training')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Training</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            Training Data Table
        </h2>

        {{-- search --}}

        <a href="{{ url('trainning/create') }}" class="btn btn-success mb-3"> <i class="fa fa-plus"></i> &nbsp; Create Training</a>
        <button class="btn btn-success btn-import mb-3" type="button"> <i class="fa fa-upload"></i> &nbsp; Import Training</button>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">List of Learning</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <table id="datatable" class="table table-vcenter table-stripped table-bordered">
                    <thead>
                        <tr>
                            <th width="5%"></th>
                            <th>TITLE</th>
                            <th>NAMA PERUSAHAAN</th>
                            <th width="20%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(sizeof($trainning) > 0)
                            @foreach($trainning as $key => $value)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $value->trainning_title }}</td>
                                     <td>{{ $value->company['name'] }}</td>
                                    <td>
                                       <!--  <a href="{{ url('trainning/config/'. $value->trainning_id) }}" class="btn btn-sm btn-success" style="height: 19px"><i class="fa fa-gear"></i></a> -->
                                        <a href="{{ url('trainning/detail/'. $value->trainning_id) }}" class="btn btn-sm btn-primary" style="height: 19px"><i class="fa fa-search"></i></a>
                                        <a href="{{ url('trainning/edit/'. $value->trainning_id) }}" class="btn btn-sm btn-warning" style="height: 19px"><i class="fa fa-pencil"></i></a>
                                        <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('trainning/delete/'. $value->trainning_id) }}" class="btn btn-sm btn-danger btn-xs btn-delete" style="height: 19px">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="4">Trainning not available.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Table -->
    </div>
</div>

@include('modal_delete')
@include('modal_import_trainning')
@endsection

@push('script')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.css') }}">
    <script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js') }} "></script>
    <script src="{{ asset('assets/js/pages/be_tables_datatables.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                "pageLength": 50,
                "lengthChange": true,
                "searching": true
            });
        } );

        $('.btn-import').click(function(){
            var action = $(this).data('action');
            $('#modalImportTrainning').modal('show');
        });

        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
//          $(document).ready(function(){
// setInterval(function(){
//     $.ajax({
//       url: "{{ url('trainning/check') }}",
//        type: 'GET',
//       success: function( response ) {
//         // update div
//       }
//     });
// },5000);
// console.log(response);
// });
    </script>
@endpush
