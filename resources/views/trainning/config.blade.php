@extends('layouts.root')

@section('title','Trainning Configuration')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Training</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            Training Configuration Data Table
        </h2>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">List of Configuration</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <table id="datatable" class="table table-vcenter table-stripped table-bordered">
                    <thead>
                        <tr>
                            <th>POSITION</th>
                            <th>PARTICIPANTS</th>
                            <th>STATUS</th>
                            <th width="20%">ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($position as $_position)
                            @php $position = $_position['position'] @endphp
                            @switch ($trainning->$position)
                                @case(0)
                                    @php
                                    $status = 'pending';
                                    $button = '<a href="' . url('trainning/change_config/' . $trainning->trainning_id . '/' . $position . '/1') . '" class="btn btn-success">activate</a>';
                                    @endphp

                                    @break
                                @case(1)
                                    @php
                                    $status = 'active';
                                    $button = '<a href="' . url('trainning/change_config/' . $trainning->trainning_id . '/' . $position . '/2') . '" class="btn btn-danger">closing</a>';
                                    @endphp

                                    @break
                                @case(2)
                                    @php
                                    $status = 'close';
                                    $button = '';
                                    @endphp

                                    @break
                            @endswitch
                            <tr>
                                <td>{{ ucwords($position) }} Trainning</td>
                                <td>{{ $_position['participants'] }}</td>
                                <td>{{ $status }}</td>
                                <td>{!! $button !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>            
        </div>
        <!-- END Table -->
        <div class="col-lg-12">
            <a href="{{ url('trainning') }}" class="btn btn-secondary" style="float:right;">Back</a>
        </div>
    </div>
</div>

@endsection
