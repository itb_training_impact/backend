@extends('layouts.root')

@section('title','Trainning')

@section('content')

<!-- Page Content -->

<div class="content">
	<nav class="breadcrumb bg-white push">
		<a class="breadcrumb-item" href="{{ url('home') }}">Dashboard</a>
		<a class="breadcrumb-item" href="{{ url('trainning') }}">Trainning</a>
		<span class="breadcrumb-item active">Result</span>
	</nav>

	<!-- Block Tabs -->
	<h2 class="content-heading">Participant Result</h2>

	<div class="row">
		<div class="col-md-6 col-xl-3">
			<a class="block text-center" >
				<div class="block-content block-content-full">
					@if (config('app.env') === 'development')               
					<img src="{{'http://46.101.96.173/storage/avatar/'.$participant->avatar}}" style="width: 90%">
					@else
					<img src="{{asset('assets/img/avatars/avatar0.jpg')}}" style="width: 100%; ">
					@endif
				</div>
				<div class="block-content block-content-full bg-body-light">
					<div class="font-w600 mb-5">{{ $participant->name }}</div>
				</div>
			</a>
		</div>
		<div class="col-md-9">
			<div class="block">
				<div class="block-header">
					<h3 class="block-title">Participant Detail</h3>
				</div>
				<div class="block-content" style="padding-bottom: 20px;">
					<div class="row">
						<div class="col-6 pb-3">
							<label>Name</label>
							<div> {{ $participant->name }} </div>
						</div>
						<div class="col-6 pb-3">
							<label>Username</label>
							<div> {{ $participant->username }} </div>
						</div>
						<div class="col-6 pb-3">
							<label>Email</label>
							<div> {{ $participant->email }} </div>
						</div>
						<div class="col-6 pb-3">
							<label>Date of Birth</label>
							<div> {{ $participant->dob == '0000-00-00' ? '-' : date($participant->dob) }} </div>
						</div>
						<div class="col-6 pb-3">
							<label>Gender</label>
							<div> {{ $participant->gender ?: '-' }} </div>
						</div>
						<div class="col-6 pb-3">
							<label>Ethnicity</label>
							<div> {{ $participant->etnis ?: '-' }} </div>
						</div>
						<div class="col-6 pb-3">
							<label>Religion</label>
							<div> {{ $participant->religion ?: '-' }} </div>
						</div>
						<div class="col-6 pb-3">
							<label>Occupation</label>
							<div> {{ $participant->job ?: '-' }} </div>
						</div>
						<div class="col-6 pb-3">
							<label>Marriage</label>
							<div> {{ $participant->marriage ?: '-' }} </div>
						</div>
						<div class="col-6 pb-3">
							<label>Instansi Perusahaan</label>
							<div> {{ $participant->company['name'] }} </div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- <div class="col-lg-12">
			<div class="block">
				<div class="block-content" style="padding: 18px;">
					<div class="row">
						<div class="col-3">
							<select name="session" class="form-control js-select2" data-placeholder="Choose session">
								<option></option>
								@foreach($sessions as $session)
								<option value="{{ $session->session_id }}">{{ $session->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-3">
							<select name="module" class="form-control js-select2"  data-placeholder="Choose module">
								<option></option>
							</select>
						</div>
						<div class="col-1">
							<a href="{{ url('trainning/result/' . $participant->id . '/' . $trainningID) }}" class="btn btn-danger">Clear</a>
						</div>
						<div class="col-5">
							<a href="{{ url('trainning/detail/'. $trainningID) }}" class="btn btn-secondary" style="float:right;">Back</a>
						</div>
					</div>
				</div>
			</div>
		</div>		 -->
		<!-- <div class="col-lg-12" class="result" id="submit-result"> -->
		<div class="col-lg-12">
			<!-- Block Tabs Default Style -->
			<div class="block">
				<ul class="nav nav-tabs nav-tabs-block">
					<li class="nav-item">
						<a class="nav-link link-before {{ Request::get('active') == 'before' || !Request::get('active') ? 'active' : '' }}" href="{{ url('trainning/result/' . $participant->id . '/' . $trainningID . '?active=before') }}">BEFORE</a>
					</li>
					<li class="nav-item">
						<a class="nav-link link-during {{ Request::get('active') == 'during' ? 'active' : '' }}" href="{{ url('trainning/result/' . $participant->id . '/' . $trainningID . '?active=during') }}">DURING</a>
					</li>
					<li class="nav-item">
						<a class="nav-link link-after {{ Request::get('active') == 'after' ? 'active' : '' }}" href="{{ url('trainning/result/' . $participant->id . '/' . $trainningID . '?active=after') }}">AFTER</a>
					</li>
					<li class="nav-item">
						<a class="nav-link link-result {{ Request::get('active') == 'result' ? 'active' : '' }}" href="{{ url('trainning/result/' . $participant->id . '/' . $trainningID . '?active=result') }}">RESULT</a>
					</li>
				</ul>

				@if(Request::get('active') != 'result')
				<div class="block-content">
					<div class="row">
						<div class="col-3">
							<select name="session" class="form-control js-select2" data-placeholder="Choose session">
								<option></option>
								@foreach($sessions as $session)
								<option value="{{ $session->session_id }}">{{ $session->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-3">
							<select name="module" class="form-control js-select2"  data-placeholder="Choose module">
								<option></option>
							</select>
						</div>
						<div class="col-1">
							<a href="{{ url('trainning/result/' . $participant->id . '/' . $trainningID) }}?active=before" class="btn btn-danger">Clear</a>
						</div>
						<div class="col-5">
							<a href="{{ url('trainning/detail/'. $trainningID) }}" class="btn btn-secondary" style="float:right;">Back</a>
						</div>
					</div>
				</div>
				@endif

				<div class="col-lg-12" class="result" id="submit-result" style="padding: 18px;">
					@if(Request::get('active') != 'result')
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">x</button> 
						<strong>Silahkan Pilih Sesi dan Modul Terlebih Dahulu</strong>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('script')
<script src="{{asset('assets/js/plugins/chartjs/Chart.bundle.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/chartjs/be_comp_charts.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/chartjs/jquery.flot.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/chartjs/jquery.flot.pie.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/chartjs/jquery.flot.stack.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/chartjs/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/chartjs/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/chartjs/jquery.easypiechart.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>

<script>
	var sessionID = '{{ Request::get("sessionID") }}'
	var moduleID = '{{ Request::get("moduleID") }}'
	var active = '{{ Request::get("active") }}'

	$('.js-select2').select2()

	$('select[name=session]').change(function() {
		var val = $(this).val()
		$.get('{!! url("trainning/modules") !!}/' + val, function(data) {
			var data = $.parseJSON(data)
			$('select[name=module]').empty()
			$('select[name=module]').append('<option></option>')
			$.each(data, function (index, value) {
				$('select[name=module]').append('<option value="' + value.module_id + '">' + value.name + '</option>')
			})
			$('select[name=module]').select2()

			if (moduleID)
				$('select[name=module').val(moduleID).trigger('change')
		})
	})

	$('select[name=module]').change(function() {
		$.ajax({
			url: '{!! url("trainning/submit") !!}',
			data: {
				userID: '{{ $participant->id }}',
				trainningID: '{{ $trainningID }}',
				sessionID: $('select[name=session]').val(),
				moduleID: $('select[name=module]').val(),
				active: active //'{!! Request::get("active") !!}'
			},
			beforeSend: function (data) {
                $('#submit-result').html(`<div class="block"><center><img src="{{ asset('assets/img/preloader.gif') }}"></center></div>`);
            },
			success: function(data) {
				$('#submit-result').html(data)

				var linkBefore = $('.link-before').attr('href')
				var linkDuring = $('.link-during').attr('href')
				var linkAfter = $('.link-after').attr('href')
				var linkResult = $('.link-result').attr('href')

				$('.link-before').attr('href', linkBefore + '&sessionID=' + $('select[name=session]').val() + '&moduleID=' + $('select[name=module]').val())
				$('.link-during').attr('href', linkDuring + '&sessionID=' + $('select[name=session]').val() + '&moduleID=' + $('select[name=module]').val())
				$('.link-after').attr('href', linkAfter + '&sessionID=' + $('select[name=session]').val() + '&moduleID=' + $('select[name=module]').val())
				$('.link-result').attr('href', linkResult + '&sessionID=' + $('select[name=session]').val() + '&moduleID=' + $('select[name=module]').val())
			}
		})
	})

	jQuery(function () {
		Codebase.helpers('easy-pie-chart');
	});
	
	if (sessionID)
		$('select[name=session]').val(sessionID).trigger('change')

	if (active == 'result') {
		$.ajax({
			url: '{!! url("trainning/submit") !!}',
			data: {
				userID: '{{ $participant->id }}',
				trainningID: '{{ $trainningID }}',
				sessionID: null,
				moduleID: null,
				active: active //'{!! Request::get("active") !!}'
			},
			beforeSend: function (data) {
                $('#submit-result').html(`<div class="block" style="padding: 18px"><center><img src="{{ asset('assets/img/preloader.gif') }}"></center></div>`);
            },
			success: function(data) {
				$('#submit-result').html(data)
			}
		})
	}
</script>

@endpush