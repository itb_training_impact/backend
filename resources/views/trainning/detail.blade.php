@extends('layouts.root')

@section('title','Detail Trainning')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="{{ url('home') }}">Dashboard</a>
            <a class="breadcrumb-item" href="{{ url('trainning') }}">Training</a>
            <span class="breadcrumb-item active">Detail</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('start'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>Proses Zipping Dimulai, tunggu beberapa saat agar dapat mengunduh hasil pelatihan !</strong>
            </div>
        @endif
        @if (session('progress'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>Proses Download Belum Selesai !</strong>
            </div>
        @endif
        <div id="success_export"></div>
        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
            
        @endif

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Detail Training</a></h3><a class="font-w600" data-toggle="collapse" data-parent="#accordion" href="#accordion_q1000" aria-expanded="true" aria-controls="accordion_q1000"><i class="fa fa-arrow-circle-down fa-2x"></i></a>
            </div>
             <div id="accordion_q1000" class="collapse show" role="tabpanel" aria-labelledby="accordion_h1" data-parent="#accordion">
            <div class="block-content" style="padding: 18px;">
                <div class="form-group">
                    <label for="">Title</label>
                    <div> {{ $trainning->trainning_title }} </div>
                </div>
                <div class="form-group">
                    <label for="">Description</label>
                    <div> {{ $trainning->trainning_description == null ? "-" :  $trainning->trainning_description }} </div>
                </div>
                <div class="form-group">
                    <label for="">Code trainning</label>
                    <div> {{$trainning->code }} </div>
                </div>
           </div>
            </div>
        </div>


       <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Speaker</h3></h3><a class="font-w600" data-toggle="collapse" data-parent="#accordion" href="#accordion_q1001" aria-expanded="true" aria-controls="accordion_q1001"><i class="fa fa-arrow-circle-down fa-2x"></i></a>
            </div>
             <div id="accordion_q1001" class="collapse" role="tabpanel" aria-labelledby="accordion_h1" data-parent="#accordion">
            <div class="block-content" style="padding: 18px;">
                <table id="datatable" class="table table-vcenter table-stripped table-bordered">
                    <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th>Name of Speaker</th>
                            <th width="10%"></th>
                        </tr>
                    </thead>
                    <tbody>
                     @if(sizeof($speakers) > 0)
                            @foreach($speakers as $key => $value) 
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>
                                        <a href="{{ url('speaker/detail/'.$value->speaker_id) }}" class="btn btn-sm btn-primary" style="height: 25px;"><i class="fa fa-search"></i>

                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="3">Trainning not available.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Participant</h3><a class="font-w600" data-toggle="collapse" data-parent="#accordion" href="#accordion_q1002" aria-expanded="true" aria-controls="accordion_q1002"><i class="fa fa-arrow-circle-down fa-2x"></i></a>
            </div>
             <div id="accordion_q1002" class="collapse" role="tabpanel" aria-labelledby="accordion_h1" data-parent="#accordion">
            <div class="block-content" style="padding: 18px;">
                <table id="datatable1" class="table table-vcenter table-stripped table-bordered">
                    <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th>Name of Participant</th>
                            <th width="10%"></th>
                        </tr>
                    </thead>
                    <tbody>
                     @if(sizeof($users) > 0)
                            @foreach($users as $key => $value) 
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>
                                        <a href="{{ url('trainning/result/'.$value->id.'/'.$trainning->trainning_id) }}?active=before" class="btn btn-sm btn-primary" style="height: 25px;"><i class="fa fa-search"></i>
                                        <!-- <a href="{{ url('trainning/downloadALL/'.$value->id.'/'.$trainning->trainning_id) }}" class="btn btn-sm btn-success" style="height: 25px;"><i class="fa fa-download"></i> -->
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="3">Trainning not available.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
        <!-- END Table -->
                 @if(sizeof($sessions) > 0)
                    @foreach($sessions as $key=>$session)
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">{{$session->name}}</h3><a href="{{ url('trainning/ActiveBefore/'.$session->session_id) }}" class="btn btn-success" style="margin-right: 10px;"><i class="fa fa-gear"></i>Aktikan semua</a><a class="font-w600" data-toggle="collapse" data-parent="#accordion" href="#accordion_q{{ $key + 1}}" aria-expanded="true" aria-controls="accordion_q{{ $key + 1}}"><i class="fa fa-arrow-circle-down fa-2x"></i></a>
                        </div>
                         <div id="accordion_q{{ $key + 1}}" class="collapse" role="tabpanel" aria-labelledby="accordion_h1" data-parent="#accordion">
                        <div class="block-content" style="padding: 18px;">
                        
                        
                            @if($session->before ==1)
                            <table id="datatable1" class="table table-vcenter table-stripped table-bordered">
                                <thead>
                                     <tr class="text-center">
                                            <th colspan="9">
                                                @if($session->before ==1)Sebelum melaksanakan pelatihan @endif 
                                                @if($session->during ==1)Saat melaksanakan pelatihan @endif 
                                                @if($session->after ==1)Sesudah melaksanakan pelatihan @endif</th>
                                        </tr>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="10%">Kode</th>
                                        <th width="20%">Nama Module</th>
                                        <th width="10%">Desc</th>
                                        <th width="10%">Question</th>
                                         <!-- <th width="10%">Jadwalkan</th> -->
                                        <!-- <th width="20%">Jumlah Question</th> -->
                                        <th width="10%">Pembicara</th>
                                        <th width="10%" colspan="2"><center>Action</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                 @if(sizeof($moduleBefore) > 0)
                                    @foreach($moduleBefore->where('session_id',$session->session_id) as $key => $value) 
                                      @php 
                                        $moduleID = $value->module_id;
                                        $periode = 0;
                                        $getBehaviour = $value->questions->behaviourTrue($moduleID,$periode);
                                        $getLearning  = $value->questions->learningTrue($moduleID,$periode);
                                        $getReaction  = $value->questions->ReactionTrue($moduleID,$periode);
                                      @endphp                                      
                                      <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $value->code_module }}</td>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->description }}</td>
                                       
                                        <td>    
                                          @if(count($getLearning)>0) 
                                            <a href="{{url('trainning/getlearning/'.$trainning->trainning_id.'/'.$session->session_id.'/'.$value->module_id)}}" class="learning btn btn-warning btn-sm" style="height: auto"> learning</a> 
                                          @endif
                                          @if(count($getBehaviour)>0) 
                                            <a href="{{url('trainning/getbehaviour/'.$trainning->trainning_id.'/'.$session->session_id.'/'.$value->module_id)}}" class="behaviour btn btn-warning btn-sm" style="height: auto">behaviour</a> 
                                          @endif
                                          @if(count($getReaction)>0) 
                                            <a href="{{url('trainning/getreaction/'.$trainning->trainning_id.'/'.$session->session_id.'/'.$value->module_id)}}" class="reaction btn btn-warning btn-sm" style="height: auto">reaction</a> 
                                          @endif 
                                        </td>                                                                                 
                                        <!-- <td>{{ $value->question->count() }}</td> -->
                                        <td>{{ $value->session->speaker->name }}</td>                                            
                                        <td>
                                          @if($value->start_date ==true)
                                            {{ $value->start_date }}
                                          @else
                                            @if($value->status ==0)
                                            <a href="{{ url('trainning/getForm/'.$value->module_id) }}"  class="btn btn-success btn-sm" id="getForm" data-toggle="tooltip" data-placement="top" title="Jadwalkan aktivasi untuk module ini">Jadwalkan</a>
                                         @endif
                                          @if($value->status ==0)
                                            @if(count($getBehaviour)>0)
                                                <a href="{{ url('trainning/activeModuleBehaviour/'.$value->module_id.'/before') }}" class="btn btn-sm btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Aktifkan module ini">Aktifkan</a>
                                            @else
                                                <a href="{{ url('trainning/activeModule/'.$value->module_id.'/before') }}" class="btn btn-sm btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Aktifkan module ini">Aktifkan</a>
                                            @endif
                                          @else
                                            <a href="{{ url('trainning/nonActive/'.$value->module_id) }}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Non Aktifkan module ini">Non aktifkan</a>
                                          @endif
                                          @endif
                                        </td>
                                        <td>
                                          <a href="{{ url('trainning/downloadALL_Trainning/'.$trainning->trainning_id.'/'.$session->session_id.'/'.$moduleID).'/'.$periode }}" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Download hasil pelatihan seluruh peserta untuk module ini dalam excel" style="height: auto">
                                            Download
                                          </a>
                                        </td>
                                      </tr>
                                      @endforeach
                                    @else
                                        <tr class="text-center">
                                            <td colspan="7">Module not available.</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                           @endif

                            @if($session->during ==1)
                            <table id="datatable1" class="table table-vcenter table-stripped table-bordered">
                                <thead>
                                     <tr class="text-center">
                                            <th colspan="9">
                                            @if($session->before ==1)Sebelum melaksanakan pelatihan @endif
                                            @if($session->during ==1)Saat melaksanakan pelatihan @endif 
                                            @if($session->after ==1)Sesudah melaksanakan pelatihan @endif</th>
                                        </tr>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="10%">Kode</th>
                                        <th width="20%">Nama Module</th>
                                        <th width="10%">Desc</th>
                                        <th width="10%">Question</th>
                                        <!-- <th width="10%">Jadwalkan</th> -->
                                        <!-- <th width="20%">Jumlah Question</th> -->
                                        <th width="10%">Pembicara</th>
                                        <th width="10%" colspan="2"><center>Action</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                 @if(sizeof($moduleDuring) > 0)
                                        @foreach($moduleDuring->where('session_id',$session->session_id) as $key => $value) 
                                            @php 
                                            $moduleID = $value->module_id;
                                            $periode = 1;
                                            $getBehaviour = $value->questions->behaviourTrue($moduleID,$periode);
                                            $getLearning  = $value->questions->learningTrue($moduleID,$periode);
                                            $getReaction  = $value->questions->ReactionTrue($moduleID,$periode);
                                            @endphp
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $value->code_module }}</td>
                                                <td>{{ $value->name }}</td>
                                              <!--   <td>@if($session->before ==1)Sebelum melaksanakan pelatihan @endif @if($session->during ==1)Saat melaksanakan pelatihan @endif @if($session->after ==1)Sesudah melaksanakan pelatihan @endif</td> -->
                                                <td>{{ $value->description }}</td>
                                               
                                                <td>    
                                                  @if(count($getLearning)>0) 
                                                    <a href="{{url('trainning/getlearning/'.$trainning->trainning_id.'/'.$session->session_id.'/'.$value->module_id)}}" class="learningDuring btn btn-warning btn-sm" style="height: auto">learning</a> 
                                                  @endif,
                                                  @if(count($getBehaviour)>0) 
                                                    <a href="{{url('trainning/getbehaviour/'.$trainning->trainning_id.'/'.$session->session_id.'/'.$value->module_id)}}" class="behaviourDuring btn btn-warning btn-sm" style="height: auto">behaviour</a> 
                                                  @endif,
                                                  @if(count($getReaction)>0) 
                                                    <a href="{{url('trainning/getreaction/'.$trainning->trainning_id.'/'.$session->session_id.'/'.$value->module_id)}}" class="reactionDuring btn btn-warning btn-sm" style="height: auto">reaction</a> 
                                                  @endif 
                                                </td>                                                
                                                <!-- <td>{{ $value->question->count() }}</td> -->
                                                <td>{{ $value->session->speaker->name }}</td>
                                                <td>
                                                  @if($value->start_date ==true)
                                                    {{ $value->start_date }}
                                                  @else
                                                   @if($value->status ==0)     
                                                    <a href="{{ url('trainning/getForm/'.$value->module_id) }}" class="btn btn-success btn-sm" id="getForm" data-toggle="tooltip" data-placement="top" title="Jadwalkan aktivasi untuk module ini">Jadwalkan</a>
                                                    @endif
                                                  @if($value->status ==0)                                                
                                                    @if(count($getBehaviour)>0)
                                                        <a href="{{ url('trainning/activeModuleBehaviour/'.$value->module_id.'/during') }}" class="btn btn-sm btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Aktifkan module ini">Aktifkan</a>
                                                    @else
                                                        <a href="{{ url('trainning/activeModule/'.$value->module_id.'/during') }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Aktifkan module ini">Aktifkan</a>
                                                    @endif    
                                                  @else
                                                    <a href="{{ url('trainning/nonActive/'.$value->module_id) }}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Non Aktifkan module ini">Non aktifkan</a>
                                                  @endif
                                                  @endif
                                                </td>
                                                <td>
                                                  <a href="{{ url('trainning/downloadALL_Trainning/'.$trainning->trainning_id.'/'.$session->session_id.'/'.$value->module_id).'/'.$periode }}" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Download hasil pelatihan seluruh peserta untuk module ini dalam excel" style="height: auto">
                                                    Download
                                                  </a>
                                                </td>
                                            </tr>
                                          @endforeach
                                    @else
                                        <tr class="text-center">
                                            <td colspan="7">Module not available.</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                           @endif

                           @if($session->after ==1)
                            <table id="datatable1" class="table table-vcenter table-stripped table-bordered">
                                <thead>
                                     <tr class="text-center">
                                            <th colspan="9">
                                                @if($session->before ==1)Sebelum melaksanakan pelatihan @endif 
                                                @if($session->during ==1)Saat melaksanakan pelatihan @endif 
                                                @if($session->after ==1)Sesudah melaksanakan pelatihan @endif</th>
                                        </tr>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="10%">Kode</th>
                                        <th width="20%">Nama Module</th>
                                        <th width="10%">Desc</th>
                                        <th width="10%">Question</th>
                                        <!-- <th width="10%">Jadwalkan</th> -->
                                        <!-- <th width="20%">Jumlah Question</th> -->
                                        <th width="10%">Pembicara</th>
                                        <th width="10%" colspan="2"><center>Action</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                 @if(sizeof($moduleAfter) > 0)
                                        @foreach($moduleAfter->where('session_id',$session->session_id) as $key => $value) 
                                            @php 
                                            $moduleID = $value->module_id;
                                            $periode = 2;
                                            $getBehaviour = $value->questions->behaviourTrue($moduleID,$periode);
                                            $getLearning  = $value->questions->learningTrue($moduleID,$periode);
                                            $getReaction  = $value->questions->ReactionTrue($moduleID,$periode);
                                            @endphp
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $value->code_module }}</td>
                                                <td>{{ $value->name }}</td>
                                                <!-- <td>@if($session->before ==1)Sebelum melaksanakan pelatihan @endif, @if($session->during ==1)Saat melaksanakan pelatihan @endif @if($session->after ==1)Sesudah melaksanakan pelatihan @endif</td> -->
                                                <td>{{ $value->description }}</td>
                                               
                                                <td>    
                                                  @if(count($getLearning)>0) 
                                                    <a href="{{url('trainning/getlearning/'.$trainning->trainning_id.'/'.$session->session_id.'/'.$value->module_id)}}" class="learningAfter btn btn-warning btn-sm" style="height: auto">learning</a> 
                                                  @endif
                                                  @if(count($getBehaviour)>0) 
                                                    <a href="{{url('trainning/getbehaviour/'.$trainning->trainning_id.'/'.$session->session_id.'/'.$value->module_id)}}" class="behaviourAfter btn btn-warning btn-sm" style="height: auto">behaviour</a> 
                                                  @endif
                                                  @if(count($getReaction)>0) 
                                                    <a href="{{url('trainning/getreaction/'.$value->module_id)}}" class="reactionAfter btn btn-warning btn-sm" style="height: auto">reaction</a> 
                                                  @endif 
                                                </td>                                                
                                                <!-- <td>{{ $value->question->count() }}</td> -->
                                                <td>{{ $value->session->speaker->name }}</td>
                                                <td>
                                                  @if($value->start_date ==true)
                                                    {{ $value->start_date }}
                                                  @else
                                                   @if($value->status ==0)
                                                    <a href="{{ url('trainning/getForm/'.$value->module_id) }}" class="btn btn-success btn-sm" id="getForm" data-toggle="tooltip" data-placement="top" title="Jadwalkan aktivasi untuk module ini">Jadwalkan</a>
                                                    @endif
                                                  @if($value->status ==0)
                                                    @if(count($getBehaviour)>0)
                                                        <a href="{{ url('trainning/activeModuleBehaviour/'.$value->module_id.'/after') }}" class="btn btn-sm btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Aktifkan module ini">Aktifkan</a>
                                                    @else
                                                        <a href="{{ url('trainning/activeModule/'.$value->module_id.'/after') }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Aktifkan module ini">Aktifkan</a>
                                                    @endif
                                                  @else
                                                    <a href="{{ url('trainning/nonActive/'.$value->module_id) }}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Non Aktifkan module ini">Non aktifkan</a>
                                                  @endif
                                                  @endif
                                                </td>
                                                <td>
                                                  <a href="{{ url('trainning/downloadALL_Trainning/'.$trainning->trainning_id.'/'.$session->session_id.'/'.$value->module_id).'/'.$periode }}" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Download hasil pelatihan seluruh peserta untuk module ini dalam excel" style="height: auto">
                                                    Download
                                                  </a>
                                                </td>
                                            </tr>
                                          @endforeach
                                    @else
                                        <tr class="text-center">
                                            <td colspan="7">Module not available.</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                           @endif
                       </div>
                        </div>
                    </div>
                     @endforeach        

      @foreach($sessionRotis as $key => $sessionRoti)
          <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">{{ $sessionRoti->name }}</h3><a href="{{ url('trainning/ActiveBefore/'.$sessionRoti->session_id) }}" class="btn btn-success" style="margin-right: 10px;"><i class="fa fa-gear"></i>Aktikan semua</a><a class="font-w600" data-toggle="collapse" data-parent="#accordion" href="#accordion_q2000" aria-expanded="true" aria-controls="accordion_q2000"><i class="fa fa-arrow-circle-down fa-2x"></i></a>
            </div>
             <div id="accordion_q2000" class="collapse" role="tabpanel" aria-labelledby="accordion_h1" data-parent="#accordion">
            <div class="block-content" style="padding: 18px;">
                <table id="datatable1" class="table table-vcenter table-stripped table-bordered">
                    <thead>
                        <tr>
                           <th width="10%">No</th>
                            <th>Kode module</th>
                            <th>Name module</th>
                            <th>Description</th>
                            <th>Question</th>
                            <th colspan="2"><center>Action</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rotis as $key => $value)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $value->code_module }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->description }}</td>
                                    <td>
                                    <a href="{{url('trainning/getRoti/'.$trainning->trainning_id.'/'.$sessionRoti->session_id.'/'.$value->module_id)}}" class="roti btn btn-warning btn-sm" style="height: auto">roti</a>
                                     <a href="{{url('trainning/getPerformance/'.$trainning->trainning_id.'/'.$sessionRoti->session_id.'/'.$value->module_id)}}" class="performance btn btn-warning btn-sm" style="height: auto">Performance</a>
                                    </td>
                                    <td>
                                    @if($value->start_date ==true)
                                    {{ $value->start_date }}
                                    @else
                                    @if($value->status ==0)
                                    <a href="{{ url('trainning/getForm/'.$value->module_id) }}" class="btn btn-success btn-sm" id="getForms" data-toggle="tooltip" data-placement="top" title="Jadwalkan aktivasi untuk module ini">Jadwalkan</a>
                                     @endif   
                                    @if($value->status ==0)
                                    <a href="{{ url('trainning/activeModulePerformance/'.$value->module_id.'/before') }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Aktifkan module ini">Aktifkan</a></td>
                                    @else
                                    <a href="{{ url('trainning/nonActive/'.$value->module_id) }}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Non Aktifkan module ini">Non aktifkan</a>
                                    @endif
                                    @endif
                                    <td>
                                      <a href="{{ url('trainning/download_roti/'.$trainning->trainning_id)}}" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Download hasil pelatihan seluruh peserta untuk module ini dalam excel" style="height: auto">
                                        Download
                                      </a>
                                    </td>
                                </tr>
                            @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>    
      @endforeach
            @else
                <tr class="text-center">
                    <td colspan="7">Session not available.</td>
                </tr>
            @endif
            <!-- </div> -->
      <!--   </div>
    </div> -->
      
    <div class="block">
  <div class="block-header block-header-default" style="background-color: #f5f6f7;">
    <h3 class="block-title">Hasil Pelatihan</h3>
  </div>
  <div class="block-content">
    <div class="custom-controls-stacked">
      <label class="custom-control custom-radio">    
        <input type="radio" class="custom-control-input radio-input-all" name="trainning" value="{{$trainning->trainning_id}}">
        <span class="custom-control-indicator"></span>
        <span class="custom-control-description">Hasil keseluruhan</span>
      </label>
      <label class="custom-control custom-radio">    
        <input type="radio" class="custom-control-input" name="trainning" value="module">
        <span class="custom-control-indicator"></span>
        <span class="custom-control-description">Hasil per session</span>
      </label>                
    </div>
  </div>
</div>

<div class="show" style="display: none;">
        <div class="block">
            <div class="block-content" style="padding: 18px;">
                <div class="row">

                    <div class="col-3">
                        <select name="session" class="form-control js-select2" data-placeholder="Choose session" style="width: 100%;">
                            <option></option>
                            @foreach($sessions as $session)
                            <option value="{{ $session->session_id }}">{{ $session->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-3">
                        <select name="module" class="form-control js-select2"  data-placeholder="Choose module" style="width: 100%;">
                            <option></option>
                        </select>
                    </div>
                    <div class="col-1">
                        <a href="{{ url('trainning/detail/' . $trainning->trainning_id) }}" class="btn btn-danger">Clear filter</a>
                    </div>                    
                    <div class="col-5">
                        <a href="{{ url('trainning') }}" class="btn btn-secondary" style="float:right;">Back</a>
                    </div>
                </div>
            </div>
        </div>
</div>
        <span class="submit-result"></span>
        <!-- <span id="submit-all"></span> -->

    @include('modal_question_detail_learning')
    @include('modal_question_detail_reaction')
    @include('modal_question_detail_behaviour')
    @include('modal_performance')
    @include('modal_roti')

    @include('modal_import_performance')
    @include('modal_import_roti')

    @include('modal_import_reaction')
    @include('modal_import_behaviour')
    @include('modal_import_learning')

<div class="modal fade" id="moduleForm" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document" aria-hidden="true">
       
    </div>
</div>

<div class="modal fade" id="moduleForms" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document" aria-hidden="true">
       
    </div>
</div>
    

@endsection

@push('script')
<script src="{{asset('assets/js/plugins/chartjs/Chart.bundle.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/chartjs/be_comp_charts.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/chartjs/jquery.flot.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/chartjs/jquery.flot.pie.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/chartjs/jquery.flot.stack.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/chartjs/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/chartjs/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/chartjs/jquery.easypiechart.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>

<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.css') }}">
<script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js') }} "></script>
<script src="{{ asset('assets/js/pages/be_tables_datatables.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable, #datatable1,#datatableReaction').dataTable({
            "pageLength": 50,
            "lengthChange": true,
            "searching": true
        });
    });

    $('.js-select2').select2()

    $('select[name=session]').change(function() {
        var val = $(this).val()
        $.get('{!! url("trainning/modules") !!}/' + val, function(data) {
            var data = $.parseJSON(data)
            $('select[name=module]').empty()
            $('select[name=module]').append('<option></option>')
            $.each(data, function (index, value) {
                $('select[name=module]').append('<option value="' + value.module_id + '">' + value.name + '</option>')
            })
            $('select[name=module]').select2()
        })
    })

    $('select[name=module]').change(function() {
      $('.submit-result').show();
        $.ajax({
            url: '{!! url("trainning/submits") !!}',
            data: {
                trainningID: '{{ $trainning->trainning_id }}',
                sessionID: $('select[name=session]').val(),
                moduleID: $('select[name=module]').val(),
            },
            beforeSend: function (data) {
                $('.submit-result').html(`<div class="block" style="padding: 18px"><center><img src="{{ asset('assets/img/preloader.gif') }}"></center></div>`);
            },
            success: function(data) {
                $('.submit-result').html(data)
            }
        })
    })
   $('.performance').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalPerformance").modal('show');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalPerformance").find('.modal-body').html(response);
        });

    });
    $('.roti').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalRoti").modal('show');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalRoti").find('.modal-body').html(response);
        });

    });
     $('.performance').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalPerformance").modal('show');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalPerformance").find('.modal-body').html(response);
        });

    });
//get question
    $('.behaviour').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalBehaviour").modal('show');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalBehaviour").find('.modal-body').html(response);
        });

    });
     $('.behaviourDuring').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalBehaviourDuring").modal('show');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalBehaviourDuring").find('.modal-body').html(response);
        });

    });
      $('.behaviourAfter').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalBehaviourAfter").modal('show');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalBehaviourAfter").find('.modal-body').html(response);
        });

    });
//restart behaviour
 $('.behaviour-restart-before').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalBehaviour").modal('hide');
        setTimeout(function () {
        $("#modalBehaviour").modal("show");
        }, 1000);

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalBehaviour").find('.modal-body').html(response);
        });

    });
  $('.behaviour-restart-during').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalBehaviourDuring").modal('hide');
        setTimeout(function () {
        $("#modalBehaviourDuring").modal("show");
        }, 1000);

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalBehaviourDuring").find('.modal-body').html(response);
        });

    });
   $('.behaviour-restart-after').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalBehaviourAfter").modal('hide');
        setTimeout(function () {
        $("#modalBehaviourAfter").modal("show");
        }, 1000);

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalBehaviourAfter").find('.modal-body').html(response);
        });

    });
//end restart
//reaction
     $('.reaction').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalReaction").modal('show');

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            $("#modalReaction").find('.modal-body').html(response);
        });

    });
      $('.reactionDuring').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalReactionDuring").modal('show');

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            $("#modalReactionDuring").find('.modal-body').html(response);
        });

    });
       $('.reactionAfter').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalReactionAfter").modal('show');

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            $("#modalReactionAfter").find('.modal-body').html(response);
        });

    });

//restart reaction
      $('.reaction-restart-before').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalReaction").modal('hide');
        setTimeout(function () {
        $("#modalReaction").modal("show");
        }, 1000);

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalReaction").find('.modal-body').html(response);
        });

    });
        $('.reaction-restart-during').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalReactionDuring").modal('hide');
        setTimeout(function () {
        $("#modalReactionDuring").modal("show");
        }, 1000);

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalReactionDuring").find('.modal-body').html(response);
        });

    });
         $('.reaction-restart-after').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalReactionAfter").modal('hide');
        setTimeout(function () {
        $("#modalReactionAfter").modal("show");
        }, 1000);

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalReactionAfter").find('.modal-body').html(response);
        });

    });
//end
//endreaction
//learning
     $('.learning').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalLearning").modal('show');

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })

        .done(function(response) {
            $("#modalLearning").find('.modal-body').html(response);
        });

    });
      $('.learningDuring').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalLearningDuring").modal('show');

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })

        .done(function(response) {
            $("#modalLearningDuring").find('.modal-body').html(response);
        });

    });
       $('.learningAfter').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalLearningAfter").modal('show');

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })

        .done(function(response) {
            $("#modalLearningAfter").find('.modal-body').html(response);
        });

    });
      $('.learning-restart-before').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalLearning").modal('hide');
        setTimeout(function () {
        $("#modalLearning").modal("show");
        }, 1000);

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalLearning").find('.modal-body').html(response);
        });

    });

    $('.learning-restart-during').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalLearningDuring").modal('hide');
        setTimeout(function () {
        $("#modalLearningDuring").modal("show");
        }, 1000);

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalLearningDuring").find('.modal-body').html(response);
        });

    });
     $('.learning-restart-after').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalLearningAfter").modal('hide');
        setTimeout(function () {
        $("#modalLearningAfter").modal("show");
        }, 1000);

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalLearningAfter").find('.modal-body').html(response);
        });

    });
//learning end
 $('#getForm').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#moduleForm").modal('show');

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#moduleForm").find('.modal-dialog').html(response);
        });

    });
 $('#getForms').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#moduleForms").modal('show');

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#moduleForms").find('.modal-dialog').html(response);
        });

    });
 $(document).ready(function(){
setInterval(function(){
    $.ajax({
      url: "{{ url('trainning/checkDefault/'.$trainning->trainning_id) }}",
       type: 'GET',
      success: function( response ) {
        // update div
      }
    });
},5000);
console.log(response);
});
    $('.roti-restart').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalRoti").modal('hide');
        setTimeout(function () {
        $("#modalRoti").modal("show");
        }, 1000);

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalRoti").find('.modal-body').html(response);
        });

    });
     $('.performance-restart').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modalPerformance").modal('hide');
        setTimeout(function () {
        $("#modalPerformance").modal("show");
        }, 1000);

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            console.log(response);
            $("#modalPerformance").find('.modal-body').html(response);
        });

    });
  $("input[value=module]").on( "change", function(evt) {
    
     if($(this).prop("checked")) {
        $("div[class=show]").show();
        $('.submit-result').hide();
      } else{
        $("div[class=show]").hide();
      }
    });

   $(".radio-input-all").on( "change", function(evt) {
      $("div[class=show]").hide();
      $('.submit-result').show();
        $.ajax({
            url: '{!! url("trainning/submitTrainning") !!}',
            data: {
                trainningID: '{{ $trainning->trainning_id }}',
            },
            beforeSend: function (data) {
                $('.submit-result').html(`<div class="block" style="padding: 18px"><center><img src="{{ asset('assets/img/preloader.gif') }}"></center></div>`);
            },
            success: function(data) {
                $('.submit-result').html(data)
            }
        })
    })

    setInterval(function(){ 
	$.ajax({
        url: '{{ url("trainning/checkDownload") }}',
        data: {
            trainningID: '{{ $trainning->trainning_id }}'
        },
		dataType: 'json',
		success: function(data) {
			if (data.status == 1) {
				$('#success_export').attr('class', 'alert alert-success');
				$('#success_export').html('Laporan siap diunduh. Klik <a href="{{ url('trainning/downloadAll/'.$trainning->trainning_title) }}" target="_blank">disini</a> untuk mengunduh');
			}
		}
        })
    }, 3000);
     $('.roti-import').click(function(){
            var action = $(this).data('action');
            $('#modalImportRoti').modal('show');
        });
      $('.performance-import').click(function(){
            var action = $(this).data('action');
            $('#modalImportPerformance').modal('show');
        });
        $('.reaction-import').click(function(){
            var action = $(this).data('action');
            $('#modalImportReaction').modal('show');
        });
         $('.behaviour-import').click(function(){
            var action = $(this).data('action');
            $('#modalImportBehaviour').modal('show');
        });
         $('.learning-import').click(function(){
            var action = $(this).data('action');
            $('#modalImportLearning').modal('show');
        });
</script>
<script>jQuery(function(){ Codebase.helpers('easy-pie-chart'); });</script>

@endpush
