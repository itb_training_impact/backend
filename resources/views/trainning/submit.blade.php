<br/>
<div class="tab-pane active" id="btabs-static-before" role="tabpanel">
	<h4 class="font-w400">{{ !Request::get('active') ? 'BEFORE' : (Request::get('active') == 'result' ? 'FINAL RESULT' : strtoupper(Request::get('active'))) }} {{ strtoupper($trainning->trainning_title) }}</h4>
	<div class="row">
		<div class="col-3">
			<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				<a class="nav-link active" id="v-pills-reaction-tab" data-toggle="pill" href="#reaction" role="tab" aria-controls="reaction" aria-selected="true">REACTION</a>
				<a class="nav-link" id="v-pills-learning-tab" data-toggle="pill" href="#learning" role="tab" aria-controls="learning" aria-selected="false">LEARNING</a>
				<a class="nav-link" id="v-pills-behaviour-tab" data-toggle="pill" href="#behaviour" role="tab" aria-controls="behaviour" aria-selected="false">BEHAVIOUR</a>
				<a class="nav-link" id="v-pills-performance-tab" data-toggle="pill" href="#performance" role="tab" aria-controls="performance" aria-selected="false">PERFORMANCE</a>
			</div>
		</div>		
		<div class="col-9 tab-content" id="v-pills-tabContent">
			<div class="tab-pane active" id="reaction" role="tabpanel" aria-labelledby="v-pills-reaction-tab">
				@if (Request::get('active') == 'result')
					<div class="row">
						<div class="col-lg-12">
					@php
						$data_result_arr = array();
					@endphp    
					@foreach ($data['reaction_group'] as $item)
						@php
						$devide = App\Helper\Application::get_calculate('reaction', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id)[0]->result;                  

						$calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
						array_push($data_result_arr,$calculate)
						@endphp
					@endforeach
						<!-- Lines Chart -->
						<div class="block">
						@if(array_sum($data_result_arr) != 0)
							<canvas id="js-chartjs-bars-reaction" data-calculation="{{round(array_sum($data_result_arr)/(100*count($data_result_arr))*100,2)}}"></canvas>
						@else
							<canvas id="js-chartjs-bars-reaction" data-calculation="0"></canvas>
						@endif
						</div>
						<!-- END Lines Chart -->
					</div>
					@else
					<div class="row">
						<div class="col-12 mb-15">
							<a href= "{{url('trainning/download_pdf/'. $participant->id . '/' . $trainningID . '/' . $sessionID . '/' . $moduleID . '/' . Request::get('active'))}}" class="btn btn-sm btn-danger" style="float: right; margin-left: 1px;"><i class="fa fa-download"></i> Download PDF</a>
							<a href="{{ url('trainning/export/'. $participant->id . '/' . $trainningID . '/' . $sessionID . '/' . $moduleID . '/' . Request::get('active') . '/' . 'reaction') }}" class="btn btn-sm btn-success"  style="float: right;"><i class="fa fa-download"></i> Download Excel</a>
						</div>
						<div class="col-6">
							<h3>Dimensi</h3>
							@php
							$data_arr = array();
						@endphp                           
						@foreach ($data['reaction_group'] as $item)                                                                                            
							@php
								$devide = App\Helper\Application::get_calculate('reaction', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id)[0]->result;
								$calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
								array_push($data_arr,$calculate)
							@endphp                                                
							<h5>{{ $item->group }}</h5>
							<div class="progress push">
								<div class="progress-bar progress-bar-striped" role="progressbar" style="width: {{ round($calculate,2) }}%;" aria-valuenow="{{ round($calculate,2) }}" aria-valuemin="0" aria-valuemax="100">
									<span class="progress-bar-label">{{ round($calculate,2) }}%</span>
								</div>
							</div>
						@endforeach
						</div>
						<div class="col-lg-6">
                <!-- Lines Chart -->                                            
                <div class="block">
                  @if(array_sum($data_arr) != 0)
                    <canvas id="js-chartjs-bars-reaction" data-calculation="{{round(array_sum($data_arr)/(100*count($data_arr))*100,2)}}"></canvas>
                  @else
                    <canvas id="js-chartjs-bars-reaction" data-calculation="0"></canvas>
                  @endif
                </div>
                <!-- END Lines Chart -->
            </div>
          @endif
				</div>
			</div>
			<div class="tab-pane" id="learning" role="tabpanel" aria-labelledby="v-pills-learning-tab">
				@if (Request::get('active') == 'result')
					<div class="row">
						<div class="col-lg-12">
							@php
								$data_result_arr = array();
							@endphp    
							@foreach ($data['learning_group'] as $item)
								@php
									$devide = App\Helper\Application::get_calculate('learning', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id)[0]->result;
									$calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
									array_push($data_result_arr,$calculate)
								@endphp
							@endforeach

							<!-- Lines Chart -->
							<div class="block">
								@if(array_sum($data_result_arr) != 0)
								<canvas id="js-chartjs-bars-learning" data-calculation="{{round(array_sum($data_result_arr)/(100*count($data_result_arr))*100,2)}}"></canvas>
								@else
								<canvas id="js-chartjs-bars-learning" data-calculation="0"></canvas>
								@endif
							</div>
							<!-- END Lines Chart -->
						</div>
					@else
					<div class="row">
						<div class="col-12 mb-15">
							<a href= "{{url('trainning/download_pdf/'. $participant->id . '/' . $trainningID . '/' . $sessionID . '/' . $moduleID . '/' . Request::get('active'))}}" class="btn btn-sm btn-danger" style="float: right; margin-left: 1px;"><i class="fa fa-download"></i> Download PDF</a>
							<a href="{{ url('trainning/export/'. $participant->id . '/' . $trainningID . '/' . $sessionID . '/' . $moduleID . '/' . Request::get('active') . '/' . 'learning') }}" class="btn btn-sm btn-success"  style="float: right;"><i class="fa fa-download"></i> Download Excel</a>
						</div>
						<div class="col-6">
							<h3>Dimensi</h3>
							@php
								$data_arr = array();
							@endphp                           
							@foreach ($data['learning_group'] as $item)                                                                                            
								@php
									$devide = App\Helper\Application::get_calculate('learning', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id)[0]->result;
									$calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
									array_push($data_arr,$calculate)
								@endphp                                                
								<h5>{{ $item->group }}</h5>
								<div class="progress push">
									<div class="progress-bar progress-bar-striped" role="progressbar" style="width: {{ round($calculate,2) }}%;" aria-valuenow="{{ round($calculate,2) }}" aria-valuemin="0" aria-valuemax="100">
										<span class="progress-bar-label">{{ round($calculate,2) }}%</span>
									</div>
								</div>
							@endforeach
						</div>
						<div class="col-lg-6">
							<!-- Lines Chart -->                                            
							<div class="block">
								@if(array_sum($data_arr) != 0)
									<canvas id="js-chartjs-bars-learning" data-calculation="{{round(array_sum($data_arr)/(100*count($data_arr))*100,2)}}"></canvas>
								@else
									<canvas id="js-chartjs-bars-learning" data-calculation="0"></canvas>
								@endif
							</div>
							<!-- END Lines Chart -->
						</div>
					@endif
				</div>
			</div>
			<div class="tab-pane" id="behaviour" role="tabpanel" aria-labelledby="v-pills-behaviour-tab" {{ Request::get('active') == 'result' && $isBehaviourExist ? 'style=overflow-x:scroll' : '' }}>
				@if (Request::get('active') == 'result' && $isBehaviourExist)
				<table class="table table-bordered table-striped">
					<tr>
						<th style="vertical-align: middle; text-align:center">Participant</th>
						<th style="vertical-align: middle; text-align:center">Session</th>
						<th style="vertical-align: middle; text-align:center">Module</th>
						<th style="vertical-align: middle; text-align:center">Periode</th>
						@foreach ($wordCategories as $wc)
							<th style="vertical-align: middle; text-align:center">{!! ucwords($wc->name) !!}</th>
						@endforeach

						<th style="vertical-align: middle; text-align:center">Kategori yang Terisi</th>
						<th style="vertical-align: middle; text-align:center">Huruf / Kata</th>
						<th style="vertical-align: middle; text-align:center">Jumlah Kata</th>
						<th style="vertical-align: middle; text-align:center">Kalimat</th>
						<th style="vertical-align: middle; text-align:center">Total Koherensi</th>
						<th style="vertical-align: middle; text-align:center">Derajat Koherensi</th>
						<th style="vertical-align: middle; text-align:center">Koherensi Kalimat</th>

						@foreach ($formulas as $f)
						    <th style="vertical-align: middle; text-align:center">{!! ucwords($f->name) !!}</th>
						@endforeach

						<th style="vertical-align: middle; text-align:center">Kata tak Teridentifikasi</th>
						<th style="vertical-align: middle; text-align:center; min-width: 100px;">Aksi</th>
					</tr>
					@php
						$trainningID = Request::get('trainningID');

						$stories = $participant->storyByTrainning($trainningID);
					@endphp

					@if (count($stories))
					<tr style="border: 1px solid #eaecee">
						<td rowspan="{{ count($stories) }}" style="vertical-align: middle; border: 1px solid #eaecee;">{{ $participant->name }}</td>
						@foreach ($stories as $key => $story)
							@if($key)
								<tr style="border: 1px solid #eaecee">
							@endif

							<td style="vertical-align: middle;">{{ $story->session->name }}</td>
							<td style="vertical-align: middle;">{{ $story->module->name }}</td>

							@php

							$countCategories = 0;
							$percentageCategories = $story->countCategoryPercentage();

							$storyDetail = $story->detail;

							echo '<td style="vertical-align: middle;">';
							switch($story->submit_periode) {
								case 0:
									echo 'before';
									break;
								case 1:
									echo 'during';
									break;
								case 2:
									echo 'after';
									break;
							}
							echo '</td>';

							// Categories
							foreach ($wordCategories as $cat) {
								if ($cat->code != 'SC01') {
									$percentage = !empty($percentageCategories[$cat->id]) ? $percentageCategories[$cat->id] : '-';
									if ($percentage)
										$countCategories++;

									if ($percentage != '-')
	                                    echo '<td style="vertical-align: middle;" align="center">'.number_format($percentage, 2).'%</td>';
	                                else
	                                    echo '<td style="vertical-align: middle;" align="center">' . $percentage . '</td>';
								}
							}

							echo '<td style="vertical-align: middle;" align="center">'.$countCategories.'</td>';

							// Huruf
							$char = $storyDetail->countCharPerWord();
							$conjuct = $storyDetail->countConjuction();
							echo '<td style="vertical-align: middle;" align="center">'.$char['char_per_word'].'</td>';

							// Kata
							echo '<td style="vertical-align: middle;" align="center">'.$char['total_words'].'</td>';


							// Kalimat
							$countSentence = $storyDetail->countSentence();
							echo '<td style="vertical-align: middle;" align="center">'.$countSentence.'</td>';

							// Koherensi
							$countCoherence = $storyDetail->countCoherenceOfSentences($countSentence);
							$percentageCoherence = (($countCoherence + $conjuct) / (empty($char['total_words']) ? 1 : $char['total_words']) * 100) . '%';
							echo '<td style="vertical-align: middle;" align="center">';
							echo $countCoherence;
							echo'</td>';

							// Derajat Koherensi
							echo '<td style="vertical-align: middle;" align="center">';
							echo $countSentence / (empty($char['total_words']) ? 1 : $char['total_words']) * 100 . '%';
							echo '</td>';

							echo '<td style="vertical-align: middle;" align="center">';
							echo $percentageCoherence;
							echo'</td>';

							// Formula
							foreach ($formulas as $formula) {
								$value = '';
								$results = $storyDetail->results;

								foreach ($results as $result) {
									if ($result->formula_id == $formula->id) {
										$value = number_format($result->score, 2).'%';
									} else {
	                                    $value = '-';
	                                }
								}
								echo '<td style="vertical-align: middle;" align="center">'.$value.'</td>';
							}
							@endphp

							<td style="vertical-align: middle;" align="center">{{ ($percentageCategories['total_unidentified_words'] ? $percentageCategories['total_unidentified_words'] : 0) }}</td>

							@if (!$key)
								<td align="center" rowspan="{{ count($stories) }}" style="vertical-align: middle; border: 1px solid #eaecee;">
									<a href="{{ url('behaviour/download/story/user/' . $trainningID . '/' . $participant->id) }}" class="btn btn-success btn-sm" title="Download Story"><i class="fa fa-file"></i></a>
									<a href="{{ url('behaviour/download/report/user/excel/' . $trainningID . '/' . $participant->id) }}" class="btn btn-success btn-sm" title="Download Excel"><i class="fa fa-file-excel-o"></i></a>
								</td>
							@endif

							@if ($key)
								</tr>
							@endif
						@endforeach
					</tr>
					@endif
					</table>
				@else
					
				<div class="row">
					<div class="col-12 mb-15">
						<a href= "{{url('trainning/download_pdf/'. $participant->id . '/' . $trainningID . '/' . $sessionID . '/' . $moduleID . '/' . Request::get('active'))}}" class="btn btn-sm btn-danger" style="float: right; margin-left: 1px;"><i class="fa fa-download"></i> Download PDF</a>
						<a href="{{ url('trainning/export/'. $participant->id . '/' . $trainningID . '/' . $sessionID . '/' . $moduleID . '/' . Request::get('active') . '/' . 'behaviour') }}" class="btn btn-sm btn-success"  style="float: right;"><i class="fa fa-download"></i> Download Excel</a>
					</div>
					<br>
					<table class="table table-bordered table-striped" style="margin-bottom: 40px;">

						@if(!empty($behaviour_bosses[0]))
							<tr>
								<th colspan="4">Penilaian atasan</th>
							</tr>
							<tr>								
								<th>Nama Atasan</th>
								<th width="20%">Lihat Hasil Identifikasi</th>
							</tr>

							@foreach($behaviour_bosses as $boss)
							@endforeach
							<tr>								
								<td>{{ $boss->boss_name }}</td>
								<td><a type="button" class="btn btn-sm btn-warning btn-behaviour1" href="javascript:void(0)"><i class="fa fa-eye"></i></a></td>
							</tr>						
						@endif
						
						@if(!empty($behaviour_partners[0]))
							<tr>
								<th colspan="4">Penilaian kolega</th>
							</tr>
							<tr>									
								<th>Nama kolega</th>
								<th width="20%">Lihat Hasil Identifikasi</th>							
							</tr>
							
							@foreach($behaviour_partners as $partner)
							@endforeach
							<tr>										
								<td>{{ $partner->partner_name }}</td>								
								<td><a type="button" class="btn btn-sm btn-warning btn-behaviour2"  href="javascript:void(0)"><i class="fa fa-eye"></i></a></td>
							</tr>
						@endif
						
						@if(!empty($behaviour_subordinates[0]))
							<tr>
								<th colspan="4">Penilaian bawahan</th>
							</tr>
							<tr>									
								<th>Nama bawahan</th>
								<th width="20%">Lihat Hasil Identifikasi</th>							
							</tr>

							@foreach($behaviour_subordinates as $subordinate)
							@endforeach
							<tr>										
								<td>{{ $subordinate->subordinate_name }}</td>
								<td><a type="button" class="btn btn-sm btn-warning btn-behaviour3" href="javascript:void(0)"><i class="fa fa-eye"></i></a></td>
							</tr>						
						@endif
					</table>
					<div class="col-lg-6">
						<h3>Dimensi</h3>                 
						@php
							$data_arr_behaviour = array();
						@endphp                           
						@foreach ($data['behaviour_group'] as $item)                                                                                            
							@php
								$devide = App\Helper\Application::get_calculate('behaviour', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id, $item->formulation_id)[0]->result;
								$calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;
								array_push($data_arr_behaviour,$calculate)
							@endphp                                                
							<h5>{{ $item->formulation_name }}</h5>
							<div class="progress push">
								<div class="progress-bar progress-bar-striped" role="progressbar" style="width: {{ round($calculate,2) }}%;" aria-valuenow="{{ round($calculate,2) }}" aria-valuemin="0" aria-valuemax="100">
									<span class="progress-bar-label">{{ round($calculate,2) }}%</span>
								</div>
							</div>
						@endforeach
					</div>
					<div class="col-lg-6">
						<!-- Lines Chart -->                                            
						<div class="block">
							@if(array_sum($data_arr_behaviour) != 0)
							<canvas id="js-chartjs-bars-behaviour" data-calculation="{{round(array_sum($data_arr_behaviour)/(100*count($data_arr_behaviour))*100,2)}}"></canvas>
							@else
							<canvas id="js-chartjs-bars-behaviour" data-calculation="0"></canvas>
							@endif
						</div>
						<!-- END Lines Chart -->
					</div>
	       			@if ($isBehaviourExist)
						@php $story = $data['stories'][0]; @endphp
						@php $storyDetail = $story->detail; @endphp
							<div class="col-12 text-right mb-15">
								<a href="{{ url('behaviour/download/report/' . $story->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-download"></i> Download PDF</a>
							</div>
						@foreach ($formulationGroups as $key => $group)
							<div class="col-12 {{ ($key) ? 'mt-20' : '' }}">
								<center><h3 style="margin-top:10px; margin-bottom:10px;"><b>{{ $group->name }}</b></h3></center>
							</div>
							@foreach ($group->formulation as $formula)
								@foreach ($formula->result($story->detail->id) as $result)
									<div class="col-12" style="padding-top: 5px; text-align:center; margin-bottom:10px">
										{{ ucwords($formula->name) }}
									</div>
									<div class="col-lg-3 col-sm-1 col-2" style="text-align:right;">
										<span class="text-danger">{{ $formula->aspek_terendah }} </span>
									</div>
									<div class="col-lg-6 col-sm-6 col-8">
										<div class="progress" style="position:relative;">
										<div style="text-align:center; position:absolute; left:50%; border: 1px solid #e74c3c; height:100%;">
										</div>
										<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="{{ $result->score }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $result->score }}%">
											</div>
										</div>
									</div>
									<div class="col-lg-3 col-sm-1 col-2">	
										<span class="text-success">{{ $formula->aspek_tertinggi }}</span>
									</div>
									<div class="clearfix"></div>
								@endforeach
							@endforeach
						@endforeach

						@php
							$percentageCategories = $story->countCategoryPercentage();

							$char = $storyDetail->countCharPerWord();
							$countSentence = $storyDetail->countSentence();
							$conjuct = $storyDetail->countConjuction();
							$countCoherence = $storyDetail->countCoherenceOfSentences($countSentence);
							$percentageCoherence = (($countCoherence + $conjuct) / (empty($char['total_words']) ? 1 : $char['total_words']) * 100) . '%';

							// Unidentified Words
							$unwords = ($percentageCategories['total_unidentified_words'] ? $percentageCategories['total_unidentified_words'] : 0);

							$content = trim($storyDetail->story_after);

							// Split Story to Words
							$storyWords = explode(' ', $content);
							$totalStoryWord = count($storyWords);
						@endphp

							<div class="col-3 mt-20" style="text-align:right;">
								Tingkat Kepercayaan Profil
							</div>
							<div class="col-2" style="padding-top:25px;">
								<div class="btn btn-info form-control">{{ number_format(($countSentence / (empty($char['total_words']) ? 1 : $char['total_words']) * 100), 2) }}%</div>
							</div>
							<div class="col-4" style="padding-top:25px;">
								<div class="progress">
								<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="{{ ($countSentence / (empty($char['total_words']) ? 1 : $char['total_words']) * 100) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $countSentence / (empty($char['total_words']) ? 1 : $char['total_words']) * 100 }}%"></div>
								</div>
							</div>
							<div class="col-3"></div>
							<div class="clearfix"><br/><br/></div>
							<div class="col-3 mt-15" style="text-align:right;">
								Kata Teridentifikasi
							</div>
							<div class="col-2 mt-20">
								<div class="btn btn-info form-control">{{ number_format(100 - ($unwords / $totalStoryWord * 100), 2) }}%</div>
							</div>
							<div class="col-4 mt-20">
								<div class="progress">
								<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="{{ number_format(100 - ($unwords / $totalStoryWord * 100), 2) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ number_format(100 - ($unwords / $totalStoryWord * 100), 2) }}%"></div>
								</div>
							</div>
							<div class="col-3"></div>
							<div class="clearfix"><br/><br/></div>
							<div class="col-3 mt-15" style="text-align:right;">
								Kata tak Teridentifikasi
							</div>
							<div class="col-2 mt-20">
								<div class="btn btn-info form-control">{{ number_format(($unwords / $totalStoryWord * 100), 2) }}%</div>
							</div>
							<div class="col-4 mt-20">
								<div class="progress">
								<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="{{ ($unwords / $totalStoryWord * 100) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ ($unwords / $totalStoryWord * 100) }}%"></div>
								</div>
							</div>
						</div>
					@endif
				@endif
			</div>
		</div>	
		<div class="tab-pane" id="performance" role="tabpanel" aria-labelledby="v-pills-performance-tab">
	@if (Request::get('active') == 'before')
	<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Performance</h3>
    </div>
    <div class="block-content" style="padding: 18px;">
      <table class="table table-sorted table-stripped">
        <tr>
          <th style="border: 1px solid #eaeaea" width="1%">No</th>
          <th style="border: 1px solid #eaeaea" width="10%">Participant Name</th>
          <th style="border: 1px solid #eaeaea" width="10%">Boss Name</th>
          <th style="border: 1px solid #eaeaea" width="5%">Performance</th>
          <th style="border: 1px solid #eaeaea" width="5%">Perceived</th>
          <th style="border: 1px solid #eaeaea" width="5%">Nominal</th>
          <th style="border: 1px solid #eaeaea" width="5%">Interpretasi</th>
          <th style="border: 1px solid #eaeaea" width="9%">Submit at</th>
          <!-- <th style="border: 1px solid #eaeaea" width="5%">Action</th>   -->        
        </tr>        
        @if(count($performances) > 0)
          @foreach($performances as $key => $item)
            <tr>
              <td rowspan="2" style="border: 1px solid #eaeaea">{{$key += 1}}</td>
              <td rowspan="2" style="border: 1px solid #eaeaea">{{App\Helper\Application::get_user($item->participant_id)[0]->name}}</td>
            </tr>
            <tr>
              <td style="border: 1px solid #eaeaea">{{ App\Helper\Application::get_relations($item->group, $item->participant_id)->name }}</td>              
              <td style="border: 1px solid #eaeaea">
                @php
                  $devide = App\Helper\Application::get_calculate('performance', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id)[0]->result;
                  $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;              
                @endphp 
                {{ round($calculate,2) }}%
              </td>
              <td style="border: 1px solid #eaeaea">
                @php
                  $criteria_perceived = App\Helper\Application::get_criteria('perceived','1');
                  $criteria_nominal = App\Helper\Application::get_criteria('nominal','1');
                  $devide_roti = App\Helper\Application::get_calculate('roti', $criteria_perceived->criteria_id, $item->trainning_id, $item->session_id, $item->module_id, null, $item->participant_id);                  
                  $devide_roti_nominal = App\Helper\Application::get_calculate('roti', $criteria_nominal->criteria_id, $item->trainning_id, $item->session_id, $item->module_id, null, $item->participant_id);                  
                  if(!empty($devide_roti[0])){
                    $calculate_roti = round($calculate,2) / (($devide_roti[0]->result == 0 ? 1 : $devide_roti[0]->result) * 10) *100;   
                  }else{
                    $calculate_roti = 0;                    
                  }                  
                  if(!empty($devide_roti_nominal[0])){
                    $calculate_roti_nominal = ((int)str_replace(".","",$devide_roti_nominal[1]->result_real)/(int)str_replace(".","",$devide_roti_nominal[0]->result_real)) * 100;
                  }else{
                    $calculate_roti_nominal = 0;
                  }
                @endphp
                {{ round($calculate_roti, 2) }}%                
              </td>
              <td style="border: 1px solid #eaeaea">{{ round($calculate_roti_nominal, 2) > 100 ? 100 : round($calculate_roti_nominal, 2) }}%</td>
              <td style="border: 1px solid #eaeaea">
                @php $interpretasi = round((round($calculate_roti, 2) + round($calculate_roti_nominal, 2)) / 2,2); @endphp
                {{ $interpretasi < 100 ? "Pelatihan kurang berpengaruh terhadap karyawan" : $interpretasi == 100 ? "Pelatihan tidak memberikan pengaruh terhadap karyawan" : "Pelatihan memiliki pengaruh terhadap karyawan" }}
              </td>
              <td style="border: 1px solid #eaeaea">{{date_format($item->created_at,"d/M/Y H:i")}}</td>
              <!-- <td></td> -->
            </tr>          
          @endforeach
        @else
          <tr>
            <th colspan="8" style="border: 1px solid #eaeaea"><center>No data found</center></th>
          </tr>
        @endif
      </table>
    </div>
    @endif
  </div>
			</div>			
	</div>
</div>			
<hr>
<div class="col-12 mt-3" id="question_and_answer">
	@if (!empty($submit))
		<div class="pull-right">
			<a href="{{ url('trainning/export_qna/'. $participant->id . '/' . $trainningID . '/' . $sessionID . '/' . $moduleID . '/' . Request::get('active') . '/participant') }}" class="btn btn-sm btn-success"  style="float: right;"><i class="fa fa-download"></i>Download Excel question and answer</a>
		</div>
		<div>
			<h4 class="font-w400">QUESTION AND ANSWER</h4>
			@foreach($submit->where('level_intermediary', null) as $sub)
				<p>{{$loop->iteration}}. {{$sub->reaction['reaction_question']}}{{$sub->learning['learning_question']}}{{$sub->behaviour['behaviour_question']}}{{$sub->performance['performance_question']}}</p>
				<p><strong>
					@if(!empty($sub->reaction['reaction_answer']) && $sub->reaction['type_id'] == 1)
						{{ explode(",", $sub->reaction['reaction_answer'])[0] }}
							@for($i = 0; $i < $sub->
								reaction['reaction_rating_finish']; $i++)
								<td>
									<label
										class="css-control css-control-primary css-radio">
										<input type="radio"
											class="css-control-input" disabled
											value="{{ $i+1 }}" {{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer == $i+1 ? 'checked' : '' : $sub->submit_response == $i+1 ? 'checked' : ''}} > 
										<span class="css-control-indicator"></span>
									</label>
								</td>
							@endfor								
						{{ explode(",", $sub->reaction['reaction_answer'])[1] }}
					@elseif(!empty($sub->learning['learning_answer']) && $sub->learning['type_id'] == 1)
						{{ explode(",", $sub->learning['learning_answer'])[0] }}
							@for($i = 0; $i < $sub->
								learning['learning_rating_finish']; $i++)
								<td>
									<label
										class="css-control css-control-primary css-radio">
										<input type="radio"
											class="css-control-input" disabled
											value="{{ $i+1 }}" {{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer == $i+1 ? 'checked' : '' : $sub->submit_response == $i+1 ? 'checked' : ''}} > 
										<span class="css-control-indicator"></span>
									</label>
								</td>
							@endfor	
						{{ explode(",", $sub->learning['learning_answer'])[1] }}
					@elseif(!empty($sub->behaviour['behaviour_answer']) && $sub->behaviour['type_id'] == 1)
						{{ explode(",", $sub->behaviour['behaviour_answer'])[0] }}
							@for($i = 0; $i < $sub->
								behaviour['behaviour_rating_finish']; $i++)
								<td>
									<label
										class="css-control css-control-primary css-radio">
										<input type="radio"
											class="css-control-input" disabled
											value="{{ $i+1 }}" {{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer == $i+1 ? 'checked' : '' : $sub->submit_response == $i+1 ? 'checked' : ''}} > 
										<span class="css-control-indicator"></span>
									</label>
								</td>
							@endfor	
						{{ explode(",", $sub->behaviour['behaviour_answer'])[1] }}
					@else
						{{!empty($sub->submit_response_real_answer) ? $sub->submit_response_real_answer : $sub->submit_response}}
					@endif
					</strong></p>
			@endforeach
		</div>
	@endif
</div>
@include('modal_behaviour_question')

<script>

$(document).ready(function() {
	$('#v-pills-behaviour-tab, #v-pills-performance-tab').click(function() {
		//$('#question_and_answer').css('display', 'none');
	})

	$('#v-pills-learning-tab, #v-pills-reaction-tab').click(function() {
		//$('#question_and_answer').css('display', 'block');
	})
})
$(document).ready(function() {
	$('.btn-behaviour1').click(function(){
		$('#modalBehaviourBoss').modal('show');
	})
	$('.btn-behaviour2').click(function(){
		$('#modalBehaviourPartner').modal('show');
	})
	$('.btn-behaviour3').click(function(){
		$('#modalBehaviourSubordinate').modal('show');
	})
})

new Chart(document.getElementById("js-chartjs-bars-reaction"), {
	type: 'bar',
	data: {
		labels: ["Reaction"],
		datasets: [{
			label: "Reaction {{ $data["reaction"] ? round(($data["reaction"]->response),2) : 0 }} %",
			backgroundColor: "#71bcf8",
			data: [
				'{{ $data["reaction"] ? round(($data["reaction"]->response),2) : 0 }}'
			]
		}]
	},
	options: {
		title: {
			display: true
		},
		scales: {
			yAxes: [{
				display: true,
				ticks: {
					suggestedMin: 0, // minimum will be 0, unless there is a lower value.
					beginAtZero: true // minimum value will be 0.
				}
			}]
		}
	}
});

new Chart(document.getElementById("js-chartjs-bars-learning"), {
    type: 'bar',
    data: {
        labels: ["Learning"],
        datasets: [{
            label: "Learning "+$("#js-chartjs-bars-learning").attr('data-calculation')+" %",
            backgroundColor: "#71ecf8",
            data: [
                $("#js-chartjs-bars-learning").attr('data-calculation')
            ]
        }]
    },
    options: {
        title: {
            display: true
        },
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    suggestedMin: 0, // minimum will be 0, unless there is a lower value.
                    beginAtZero: true // minimum value will be 0.
                }
            }]
        }
    }
});

new Chart(document.getElementById("js-chartjs-bars-behaviour"), {
    type: 'bar',
    data: {
        labels: ["Behaviour"],
        datasets: [{
            label: "Behaviour "+$("#js-chartjs-bars-behaviour").attr('data-calculation')+" %",
            backgroundColor: "#71ecf8",
            data: [
                $("#js-chartjs-bars-behaviour").attr('data-calculation')
            ]
        }]
    },
    options: {
        title: {
            display: true
        },
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    suggestedMin: 0, // minimum will be 0, unless there is a lower value.
                    beginAtZero: true // minimum value will be 0.
                }
            }]
        }
    }
});
</script>