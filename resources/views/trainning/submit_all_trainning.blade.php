@if ($reaction->response)
<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Reaction</h3>
    </div>
    <div class="block-content" style="padding: 18px;">
        <canvas id="js-chartjs-reaction-bars">
    </div>
</div>
<!-- END Table -->
@endif

@if ($learning->response)
<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Learning</h3>
    </div>
    <div class="block-content" style="padding: 18px;">
        <canvas id="js-chartjs-learning-bars">
    </div>
</div>
<!-- END Table -->
@endif

@if ($isBehaviourExist)
<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Behaviour</h3>
    </div>
    <div class="block-content" style="padding: 18px; overflow-x: scroll;">
        <table class="table">
            <tr>
                <th style="vertical-align: middle; text-align:center; border: 1px solid #eaecee">Participant</th>
                <th style="vertical-align: middle; text-align:center; border: 1px solid #eaecee">Periode</th>
                @foreach ($wordCategories as $wc)
                    <th style="vertical-align: middle; text-align:center; border: 1px solid #eaecee">{!! ucwords($wc->name) !!}</th>
                @endforeach

                <th style="vertical-align: middle; text-align:center; border: 1px solid #eaecee">Kategori yang Terisi</th>
                <th style="vertical-align: middle; text-align:center; border: 1px solid #eaecee">Huruf / Kata</th>
                <th style="vertical-align: middle; text-align:center; border: 1px solid #eaecee">Jumlah Kata</th>
                <th style="vertical-align: middle; text-align:center; border: 1px solid #eaecee">Kalimat</th>
                <th style="vertical-align: middle; text-align:center; border: 1px solid #eaecee">Total Koherensi</th>
                <th style="vertical-align: middle; text-align:center; border: 1px solid #eaecee">Derajat Koherensi</th>
                <th style="vertical-align: middle; text-align:center; border: 1px solid #eaecee">Koherensi Kalimat</th>

                @foreach ($formulas as $f)
                    <th style="vertical-align: middle; text-align:center; border: 1px solid #eaecee">{!! ucwords($f->name) !!}</th>
                @endforeach

                <th style="vertical-align: middle; text-align:center; border: 1px solid #eaecee">Kata tak Teridentifikasi</th>
                <th style="vertical-align: middle; text-align:center; border: 1px solid #eaecee; min-width: 100px;">Aksi</th>
            </tr>
            @foreach ($users as $user)
                @php
                    $trainningID = Request::get('trainningID');
                    $stories = $user->storyByTrainning($trainningID);
                @endphp

                @if (count($stories))
                <tr style="border: 1px solid #eaecee">
                    <td rowspan="{{ count($stories) }}" style="vertical-align: middle; border: 1px solid #eaecee;">{{ $user->name }}</td>
                    @foreach ($stories as $key => $story)
                        @php

                        if ($key)
                            echo '<tr style="border: 1px solid #eaecee">';

                        $countCategories = 0;
                        $percentageCategories = $story->countCategoryPercentage();

                        $storyDetail = $story->detail;

                        echo '<td style="vertical-align: middle; border: 1px solid #eaecee">';
                        switch($story->submit_periode) {
                            case 0:
                                echo 'before';
                                break;
                            case 1:
                                echo 'during';
                                break;
                            case 2:
                                echo 'after';
                                break;
                        }
                        echo '</td>';

                        // Categories
                        foreach ($wordCategories as $cat) {
                            if ($cat->code != 'SC01') {
                                $percentage = !empty($percentageCategories[$cat->id]) ? $percentageCategories[$cat->id] : '-';
                                if ($percentage)
                                    $countCategories++;

                                if ($percentage != '-')
                                    echo '<td style="vertical-align: middle; border: 1px solid #eaecee" align="center">'.number_format($percentage, 2).'%</td>';
                                else
                                    echo '<td style="vertical-align: middle; border: 1px solid #eaecee" align="center">' . $percentage . '</td>';
                            }
                        }

                        echo '<td style="vertical-align: middle; border: 1px solid #eaecee" align="center">'.$countCategories.'</td>';

                        // Huruf
                        $char = $storyDetail->countCharPerWord();
                        $conjuct = $storyDetail->countConjuction();
                        echo '<td style="vertical-align: middle; border: 1px solid #eaecee" align="center">'.$char['char_per_word'].'</td>';

                        // Kata
                        echo '<td style="vertical-align: middle; border: 1px solid #eaecee" align="center">'.$char['total_words'].'</td>';


                        // Kalimat
                        $countSentence = $storyDetail->countSentence();
                        echo '<td style="vertical-align: middle; border: 1px solid #eaecee" align="center">'.$countSentence.'</td>';

                        // Koherensi
                        $countCoherence = $storyDetail->countCoherenceOfSentences($countSentence);
                        $percentageCoherence = (($countCoherence + $conjuct) / (empty($char['total_words']) ? 1 : $char['total_words']) * 100) . '%';
                        echo '<td style="vertical-align: middle; border: 1px solid #eaecee" align="center">';
                        echo $countCoherence;
                        echo'</td>';

                        // Derajat Koherensi
                        echo '<td style="vertical-align: middle; border: 1px solid #eaecee" align="center">';
                        echo $countSentence / (empty($char['total_words']) ? 1 : $char['total_words']) * 100 . '%';
                        echo '</td>';

                        echo '<td style="vertical-align: middle; border: 1px solid #eaecee" align="center">';
                        echo $percentageCoherence;
                        echo'</td>';

                        // Formula
                        foreach ($formulas as $formula) {
                            $value = '';
                            $results = $storyDetail->results;

                            foreach ($results as $result) {
                                if ($result->formula_id == $formula->id) {
                                    $value = number_format($result->score, 2).'%';
                                    break;
                                } else {
                                    $value = '-';
                                }
                            }
                            echo '<td style="vertical-align: middle; border: 1px solid #eaecee" align="center">'.$value.'</td>';
                        }
                        @endphp

                        <td style="vertical-align: middle; border: 1px solid #eaecee" align="center">{{ ($percentageCategories['total_unidentified_words'] ? $percentageCategories['total_unidentified_words'] : 0) }}</td>

                        @if (!$key)
                            <td align="center" rowspan="{{ count($stories) }}" style="vertical-align: middle; border: 1px solid #eaecee;">
                                <a href="{{ url('behaviour/download/story/user/' . $trainningID . '/' . $user->id) }}" class="btn btn-success btn-sm" title="Download Story"><i class="fa fa-file"></i></a>
                                <a href="{{ url('behaviour/download/report/user/excel/' . $trainningID . '/' . $user->id) }}" class="btn btn-success btn-sm" title="Download Excel"><i class="fa fa-file-excel-o"></i></a>
                            </td>
                        @endif
                    @endforeach
                </tr>
                @endif
            @endforeach
        </table>
    </div>
</div>
<!-- END Table -->
@endif

  <div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Performance</h3>
    </div>
    <div class="block-content" style="padding: 18px;">
      <table class="table table-sorted table-stripped">
        <tr>
          <th style="border: 1px solid #eaeaea" width="1%">No</th>
          <th style="border: 1px solid #eaeaea" width="10%">Participant Name</th>
          <th style="border: 1px solid #eaeaea" width="10%">Boss Name</th>
          <th style="border: 1px solid #eaeaea" width="5%">Performance</th>
          <th style="border: 1px solid #eaeaea" width="5%">Perceived</th>
          <th style="border: 1px solid #eaeaea" width="5%">Nominal</th>
          <th style="border: 1px solid #eaeaea" width="5%">Interpretasi</th>
          <th style="border: 1px solid #eaeaea" width="9%">Submit at</th>
          <!-- <th style="border: 1px solid #eaeaea" width="5%">Action</th>   -->        
        </tr>        
        @if(count($performances) > 0)
          @foreach($performances as $key => $item)
            <tr>
              <td rowspan="2" style="border: 1px solid #eaeaea">{{$key += 1}}</td>
              <td rowspan="2" style="border: 1px solid #eaeaea">{{App\Helper\Application::get_user($item->participant_id)[0]->name}}</td>
            </tr>
            <tr>
              <td style="border: 1px solid #eaeaea">{{ App\Helper\Application::get_relations($item->group, $item->participant_id)->name }}</td>              
              <td style="border: 1px solid #eaeaea">
                @php
                  $devide = App\Helper\Application::get_calculate('performance', $item->group_id, $item->trainning_id, $item->session_id, $item->module_id)[0]->result;
                  $calculate = $item->sum_response / ($devide == 0 ? 1 : $devide) * 100;              
                @endphp 
                {{ round($calculate,2) }}%
              </td>
              <td style="border: 1px solid #eaeaea">
                @php
                  $criteria_perceived = App\Helper\Application::get_criteria('perceived','1');
                  $criteria_nominal = App\Helper\Application::get_criteria('nominal','1');
                  $devide_roti = App\Helper\Application::get_calculate('roti', $criteria_perceived->criteria_id, $item->trainning_id, $item->session_id, $item->module_id, null, $item->participant_id);                  
                  $devide_roti_nominal = App\Helper\Application::get_calculate('roti', $criteria_nominal->criteria_id, $item->trainning_id, $item->session_id, $item->module_id, null, $item->participant_id);                  
                  if(!empty($devide_roti[0])){
                    $calculate_roti = round($calculate,2) / ($devide_roti[0]->result == 0 ? 1 : $devide_roti[0]->result);   
                  }else{
                    $calculate_roti = 0;                    
                  }                  
                  if(!empty($devide_roti_nominal[0])){
                    $calculate_roti_nominal = ((int)str_replace(".","",$devide_roti_nominal[1]->result_real)/(int)str_replace(".","",$devide_roti_nominal[0]->result_real)) * 100;
                  }else{
                    $calculate_roti_nominal = 0;
                  }
                @endphp
                {{ round($calculate_roti, 2) }}%                
              </td>
              <td style="border: 1px solid #eaeaea">{{ round($calculate_roti_nominal, 2) > 100 ? 100 : round($calculate_roti_nominal, 2) }}%</td>
              <td style="border: 1px solid #eaeaea">
                @php $interpretasi = round((round($calculate_roti, 2) + round($calculate_roti_nominal, 2)) / 2,2); @endphp
                {{ $interpretasi < 100 ? "Pelatihan kurang berpengaruh terhadap karyawan" : $interpretasi == 100 ? "Pelatihan tidak memberikan pengaruh terhadap karyawan" : "Pelatihan memiliki pengaruh terhadap karyawan" }}
              </td>
              <td style="border: 1px solid #eaeaea">{{date_format($item->created_at,"d/M/Y H:i")}}</td>
              <!-- <td></td> -->
            </tr>          
          @endforeach
        @else
          <tr>
            <th colspan="8" style="border: 1px solid #eaeaea"><center>No data found</center></th>
          </tr>
        @endif
      </table>
    </div>
  </div>
<!-- END Table -->

<script>
    new Chart(document.getElementById("js-chartjs-reaction-bars"), {
        type: 'bar',
        data: {
            labels: ["Progress"],
            datasets: [
                {
                    label: "Reaction {{ $reaction ? round(($reaction->response), 2) : 0 }} %",
                    backgroundColor: "#71bcf8",
                    data: ['{{ $reaction ? round(($reaction->response), 2) : 0 }}']
                }
            ]
        },
        options: {
            title: {
                display: true,
            },
            scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                        beginAtZero: true   // minimum value will be 0.
                    }
                }]
            }
        }
    });    
</script>
<script type="text/javascript">
    new Chart(document.getElementById("js-chartjs-learning-bars"), {
        type: 'bar',
        data: {
            labels: ["Progress"],
            datasets: [
                {
                    label: "Learning {{ $learning ? round(($learning->response), 2) : 0 }} %",
                    backgroundColor: "#d0e8fc",
                    data: ['{{ $learning ? round(($learning->response), 2) : 0 }}']
                }
            ]
        },
        options: {
            title: {
                display: true,
            },
            scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                        beginAtZero: true   // minimum value will be 0.
                    }
                }]
            }
        }
    });
</script>