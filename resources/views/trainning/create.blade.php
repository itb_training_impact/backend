@extends('layouts.root')

@section('title','Trainning - New')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('trainning') }}">Training</a>
        <span class="breadcrumb-item active">New</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <form class="form-level" action="{{ url('trainning') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="code">
                <div class="block">
                    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                        <h3 class="block-title">PERUSAHAAN</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group">
                            <div class="form-material">
                                <select autocomplete="off" class="js-select2 form-control" placeholder="Choose formula .." id="company_id" name="company_id" required>
                                    <option value="">Choose Perusahaan ..</option>
                                @foreach($companys as $key =>$value)
                                    <option value="{{$value->company_id}}" {{ old('company_id') == $value->company_id ? "selected" : "" }} >{{$value->name}}</option>
                                @endforeach
                                
                                </select>
                                <label for="material-email">Perusahaan <span class="text-danger">*</span></label>
                            </div> 
                        </div>
                    </div>
                </div>

                <div class="block">
                    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                        <h3 class="block-title">TRAINING</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" class="form-control" name="trainning_title" placeholder="Title of trainning" autofocus="" value="{{ old('trainning_title') }}" required>
                                <label for="">Title <span class="text-danger">*</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-material">
                                <textarea name="trainning_description" class="form-control" id="trainning_description" cols="30" rows="10" placeholder="Description of trainning" required>{{ old('trainning_description') }}</textarea>
                                <label for="">Description  <span class="text-danger">*</span></label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Static Labels -->

                <div class="block">
                    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                        <h3 class="block-title">TRAINING DATE</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group">
                            <div class="form-material">
                                <input type="date" class="form-control" name="start_date" autofocus="" value="{{ old('start_date') }}" required>
                                <label for="">Start date <span class="text-danger">*</span></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-material">
                                 <input type="date" class="form-control" name="end_date"  autofocus="" value="{{ old('end_date') }}" required>
                                <label for="">End date <span class="text-danger">*</span></label>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="block">
                    <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                        <h3 class="block-title">PEMBICARA</h3>
                    </div>
                 <div class="block-content">
                        <!-- Dipilih -->
                        <div class="form-group">
                            <div class="form-material">
                                <select autocomplete="off" class="js-select2 form-control" placeholder="" id="speaker_id" name="speaker_id[]" multiple="" value="{{ old('speakers') }}" required>
                                    <option value="">Choose Speaker ..</option>
                                @foreach($speakers as $key =>$value)
                                    <option value="{{$value->speaker_id}}" {{ old('speaker_id') == $value->speaker_id ? "selected" : "" }} >{{$value->name}}</option>
                                @endforeach
                                </select>
                            <label for="material-email">Nama pembicara<span class="text-danger">*</span></label>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="session_roti_performance" value="Session Performance dan Roti">
                <input type="hidden" name="module_roti_performance" value="Module Performance dan Roti">
                <input type="hidden" name="description_roti_performance" value="Module Roti untuk mengukur ROTI peserta dan Module Performance untuk mengukur Performance peserta">
                

<!-- END Static Labels -->                
        </div>
        <div class="block">
            <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                <h3 class="block-title">PILIH PARTICIPANT</h3>
            </div>
            <div class="block-content">
                <div class="form-group">
                    <div class="form-material">
                        <select autocomplete="off" class="js-select2 form-control" placeholder="Choose formula .." id="participant_id" name="participant_id[]" multiple="" required>
                            <option value="">Choose Participant ..</option>
                        @foreach($users as $key =>$value)
                            <option value="{{$value->id}}" {{ old('id') == $value->id ? "selected" : "" }} >{{$value->name}}</option>
                        @endforeach                                    
                        </select>
                        <label for="material-email">Participant <span class="text-danger">*</span></label>
                    </div> 
                </div>
            </div>
        </div>

        <div class="block additional">
            <div class="block-header block-header-default" style="background-color: #f5f6f7;">
                <h3 class="block-title">Tambah Session dan Module</h3>
            </div>
            <div class="block-content row" id="box0">
                <input type="hidden" name="box[0]" value="0">
                <div class="col-8" style="padding-left: 0">
                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" name="session_title[0]" placeholder="Judul Session" autofocus="" value="{{ old('session_title') }}" required>
                            <label for="">Judul Session <span class="text-danger">*</span></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <textarea name="session_description[0]" class="form-control" id="session_description" cols="30" rows="5" placeholder="Deksripsi Session" required>{{ old('session_description') }}</textarea>
                            <label for="">Deksripsi Session  <span class="text-danger">*</span></label>
                        </div>
                    </div>
                </div>
            
                <div class="col-2">
                    <div class="custom-controls-stacked">
                    <label>Pilih Section</label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="section[0]" value="before">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Before</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="section[0]" value="during">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">During</span>
                        </label>
                            <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="section[0]" value="after">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">After</span>
                        </label>
                    </div>
                </div>
                <div class="col-2">
                    <button type="button" class="btn btn-success addSession" style="width: 100%;">Tambah Session</button>
                </div>

                <div class="moduletab0 col-12">
                    <div id="modulebox00" style="display: flex; flex-wrap: wrap;">
                        <div class="col-8">
                            <input type="hidden" name="modulebox[0][0]" value="modulebox00">
                            <div class="form-group" style="padding-left: 0">
                                <div class="form-material">
                                    <input type="text" class="form-control" name="module_title[0][0]" placeholder="Judul Module" autofocus="" value="{{ old('module_title') }}" required>
                                    <label for="">Judul Module <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="form-group" style="padding-left: 15px">
                                <div class="form-material">
                                    <textarea name="module_description[0][0]" class="form-control" id="module_description" cols="30" rows="5" placeholder="Deksripsi Module" required>{{ old('module_description') }}</textarea>
                                    <label for="">Deksripsi Module  <span class="text-danger">*</span></label>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <button type="button" class="btn btn-primary addModule" id="0">Tambah Module</button>
                        </div>
                        <div class="col-12">        
                            <div class="custom-controls-stacked modules00">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="module[0][0][]" value="is_reaction">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Reaction</span>
                                </label>
                                <label class="custom-control custom-checkbox">    
                                    <input type="checkbox" class="custom-control-input" name="module[0][0][]" value="is_learning">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Learning</span>
                                </label>
                                <label class="custom-control custom-checkbox">    
                                    <input type="checkbox" class="custom-control-input" name="module[0][0][]" value="is_behaviour" id="00">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Behaviour</span>
                                </label>              
                            </div>
                        </div>
                        <div class="formula00" style="display: none; width: 65%">
                            <div class="form-group" style="padding-left: 15px;">
                                <div class="form-material">
                                <select autocomplete="off" class="js-select2 form-control" placeholder="Choose formula .." id="formulation_id" name="formulation_id[0][0][]" multiple="" style="width: 100%">
                                        <option>Pilih Kompetensi</option>
                                        @foreach ($formulas as $formula)
                                            <option value="{{$formula->id}}">{{$formula->name}}</option>
                                        @endforeach
                                    </select>
                                    <label for="material-email">Pilih Kompetensi<span class="text-danger">*</span></label>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <!-- END Static Labels -->
        <!-- END column -->
        <div class="form-group">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="reset" class="btn btn-secondary">Reset</button>
            </div>
        </form>  
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
        });
        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'trainning_title': {
                    required: true,
                },
                'trainning_description': {
                    required: true,
                }
                
            },
            messages: {
                'trainning_title': {
                    required: 'Title has required',
                },
                'trainning_description': {
                    required: 'Description has required',
                },
                
            }
        });
    </script>
    <script type="text/javascript">
    
    $(document).ready(function(){
        var i = 1;
        var a = 1;

        console.log($("div[id^=box]").length);
        //when the Add Field button is clicked
        $(".addSession").click(function (e) {            
            //Append a new row of code to the "#items" div
            $(".additional").append(
                ' <div class="block-content row" id="box'+i+'">' +
                    ' <input type="hidden" name="box['+i+']" value="'+i+'">' +
                    ' <div class="col-8" style="padding-left: 0">' +
                        '<div class="form-group">' +
                            '<div class="form-material">' +
                                '<input type="text" class="form-control" name="session_title['+i+']" placeholder="Judul Session" autofocus="" value="{{ old('session_title') }}" required>' +
                                '<label for="">Judul Session <span class="text-danger">*</span></label>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<div class="form-material">' +
                                '<textarea name="session_description['+i+']" class="form-control" id="session_description" cols="30" rows="5" placeholder="Deksripsi Session" required>{{ old('session_description') }}</textarea>' +
                                '<label for="">Deksripsi Session  <span class="text-danger">*</span></label>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +

                        '<div class="col-2">' +
                            '<div class="custom-controls-stacked">' +
                            '<label>Pilih Section</label>' +
                                '<label class="custom-control custom-radio">' +
                                    '<input type="radio" class="custom-control-input" name="section['+i+']" value="before">' +
                                    '<span class="custom-control-indicator"></span>' +
                                    '<span class="custom-control-description">Before</span>' +
                                '</label>' +
                                '<label class="custom-control custom-radio">' +
                                    '<input type="radio" class="custom-control-input" name="section['+i+']" value="during">' +
                                    '<span class="custom-control-indicator"></span>' +
                                    '<span class="custom-control-description">During</span>' +
                                '</label>' +
                                    '<label class="custom-control custom-radio">' +
                                    '<input type="radio" class="custom-control-input" name="section['+i+']" value="after">' +
                                    '<span class="custom-control-indicator"></span>' +
                                    '<span class="custom-control-description">After</span>' +
                                '</label>' +
                            '</div>' +
                        '</div>' +
                        '<div class="col-2">' +
                            '<button type="button" id="'+i+'" class="btn btn-danger deleteSession" style="margin-top: 5px; width: 100%;">Hapus Session</button>' +
                        '</div>' +
                        '<div class="moduletab'+i+' col-12">' +
                            '<div id="modulebox'+i+'0" style="display: flex; flex-wrap: wrap; padding: 0;"">' +
                                '<div class="col-8">' +
                                    '<input type="hidden" name="modulebox['+i+'][0]" value="modulebox'+i+'0">' +
                                    '<div class="form-group" style="padding-left: 0">' +
                                        '<div class="form-material">' +
                                            '<input type="text" class="form-control" name="module_title['+i+'][0]" placeholder="Judul Module" autofocus="" value="{{ old('module_title') }}" required>' +
                                            '<label for="">Judul Module <span class="text-danger">*</span></label>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="form-group" style="padding-left: 15px">' +
                                        '<div class="form-material">' +
                                            '<textarea name="module_description['+i+'][0]" class="form-control" id="module_description" cols="30" rows="5" placeholder="Deksripsi Module" required>{{ old('module_description') }}</textarea>' +
                                            '<label for="">Deksripsi Module  <span class="text-danger">*</span></label>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +

                                '<div class="col-4">' +
                                    '<button type="button" class="btn btn-primary addModule" id="'+i+'">Tambah Module</button>' +
                                '</div>' +

                                    '<div class="col-12">'+ 
                                    '<div class="custom-controls-stacked modules'+i+'0">' +
                                            '<label class="custom-control custom-checkbox">' +
                                                '<input type="checkbox" class="custom-control-input" name="module['+i+'][0][]" value="is_reaction">' +
                                                '<span class="custom-control-indicator"></span>' +
                                                '<span class="custom-control-description">Reaction</span>' +
                                            '</label>' +
                                            '<label class="custom-control custom-checkbox">' +    
                                                '<input type="checkbox" class="custom-control-input" name="module['+i+'][0][]" value="is_learning">' +
                                                '<span class="custom-control-indicator"></span>' +
                                                '<span class="custom-control-description">Learning</span>' +
                                            '</label>' +
                                            '<label class="custom-control custom-checkbox">' +    
                                                '<input type="checkbox" class="custom-control-input" name="module['+i+'][0][]" value="is_behaviour" id='+i+'0>' +
                                                '<span class="custom-control-indicator"></span>' +
                                                '<span class="custom-control-description">Behaviour</span>' +
                                            '</label>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="formula'+i+'0" style="display: none;  width: 65%">' +
                                        '<div class="form-group" style="padding-left: 15px;">' +
                                            '<div class="form-material">' +
                                            '<select autocomplete="off" class="js-select2 form-control" placeholder="Choose formula .." id="formulation_id" name="formulation_id['+i+'][0][]" multiple="" style="width: 100%"> ' +
                                                    '<option>Pilih Kompetensi</option>' +
                                                    '@foreach ($formulas as $formula)' +
                                                        '<option value="{{$formula->id}}">{{$formula->name}}</option>' +
                                                    '@endforeach' +
                                                '</select>' +
                                                '<label for="material-email">Pilih Kompetensi<span class="text-danger">*</span></label>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>'  +
                                '</div>' +        
                            '</div>' +                    
                        '</div>' +
                '</div>'
            ); 
            i++;
            Codebase.helpers('select2');
        });

        $(document).on("click", ".deleteSession", function (e) {
           var button_id = $(this).attr("id");   
           $('#box'+button_id+'').remove();
           i--;
        });

        //when the Add Field button is clicked
        $(document).on("click", ".addModule", function (e) {
            var button_id = $(this).attr("id");
            var a = $('input[value^=modulebox'+button_id+']').length;
            //Append a new row of code to the "#items" div
            $(".moduletab"+button_id).append(                 
                '<div id="modulebox'+button_id+a+'" style="display: flex; flex-wrap: wrap; padding: 0;">' +
                    '<div class="col-8">' +
                        '<input type="hidden" name="modulebox['+button_id+']['+a+']" value="modulebox'+button_id+a+'">' +
                        '<div class="form-group" style="padding-left: 0">' +
                            '<div class="form-material">' +
                                '<input type="text" class="form-control" name="module_title['+button_id+']['+a+']" placeholder="Judul Module" autofocus="" value="{{ old('module_title') }}" required>' +
                                '<label for="">Judul Module <span class="text-danger">*</span></label>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group" style="padding-left: 15px">' +
                            '<div class="form-material">' +
                                '<textarea name="module_description['+button_id+']['+a+']" class="form-control" id="module_description" cols="30" rows="5" placeholder="Deksripsi Module" required>{{ old('module_description') }}</textarea>' +
                                '<label for="">Deksripsi Module  <span class="text-danger">*</span></label>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +

                    '<div class="col-4">' +
                        '<button type="button" class="btn btn-danger deleteModule" id="'+button_id+a+'"">Hapus Module</button>' +
                    '</div>' +

                        '<div class="col-12">'+ 
                            '<div class="custom-controls-stacked modules'+button_id+a+'">' +
                                '<label class="custom-control custom-checkbox">' +
                                    '<input type="checkbox" class="custom-control-input" name="module['+button_id+']['+a+'][]" value="is_reaction">' +
                                    '<span class="custom-control-indicator"></span>' +
                                    '<span class="custom-control-description">Reaction</span>' +
                                '</label>' +
                                '<label class="custom-control custom-checkbox">' +    
                                    '<input type="checkbox" class="custom-control-input" name="module['+button_id+']['+a+'][]" value="is_learning">' +
                                    '<span class="custom-control-indicator"></span>' +
                                    '<span class="custom-control-description">Learning</span>' +
                                '</label>' +
                                '<label class="custom-control custom-checkbox modules'+button_id+a+'">' +   
                                    '<input type="checkbox" class="custom-control-input" name="module['+button_id+']['+a+'][]" value="is_behaviour"  id="'+button_id+a+'">' +
                                    '<span class="custom-control-indicator"></span>' +
                                    '<span class="custom-control-description">Behaviour</span>' +
                                '</label>' +
                            '</div>' +
                        '</div>' +
                        '<div class="formula'+button_id+a+'" style="display: none;  width: 65%">' +
                            '<div class="form-group" style="padding-left: 15px;">' +
                                '<div class="form-material">' +
                                '<select autocomplete="off" class="js-select2 form-control" placeholder="Choose formula .." id="formulation_id" name="formulation_id['+button_id+']['+a+'][]" multiple="" style="width: 100%"> ' +
                                        '<option>Pilih Kompetensi</option>' +
                                        '@foreach ($formulas as $formula)' +
                                            '<option value="{{$formula->id}}">{{$formula->name}}</option>' +
                                        '@endforeach' +
                                    '</select>' +
                                    '<label for="material-email">Pilih Kompetensi<span class="text-danger">*</span></label>' +
                                '</div>' +
                            '</div>' +
                        '</div>'  +
                    '</div>' +
                '</div>'
            );
            Codebase.helpers('select2');
        });

        $(document).on("click", ".deleteModule", function (e) {
           var button_id = $(this).attr("id");   
           console.log(button_id);
           $('#modulebox'+button_id+'').remove();
           a--;
        });

        $(document).on( "change", 'input[value^=is_behaviour]', function(evt) {
            var formid = $(this).attr("id");
            if($(this).prop("checked")) {
                console.log($(this).attr("id"));
                $('div[class=formula'+formid+']').show();
            } else{
                $('div[class=formula'+formid+']').hide();
            }
        });
    });

    </script>
@endpush
