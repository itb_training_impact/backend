@if ($reaction->response)
<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Reaction</h3>
    </div>
    <div class="block-content" style="padding: 18px;">
        <canvas id="js-chartjs-reaction-bars">
    </div>
</div>
<!-- END Table -->
@endif

@if ($learning->response)
<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Learning</h3>
    </div>
    <div class="block-content" style="padding: 18px;">
        <canvas id="js-chartjs-learning-bars">
    </div>
</div>
<!-- END Table -->
@endif

@if ($isBehaviourExist)
<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Behaviour</h3>
    </div>
    <div class="block-content" style="padding: 18px; overflow-x: scroll;">
        <table class="table">
            <tr>
                <th style="vertical-align: middle; text-align:center">Participant</th>
                <th style="vertical-align: middle; text-align:center">Periode</th>
                @foreach ($wordCategories as $wc)
                    <th style="vertical-align: middle; text-align:center">{!! ucwords($wc->name) !!}</th>
                @endforeach

                <th style="vertical-align: middle; text-align:center">Kategori yang Terisi</th>
                <th style="vertical-align: middle; text-align:center">Huruf / Kata</th>
                <th style="vertical-align: middle; text-align:center">Jumlah Kata</th>
                <th style="vertical-align: middle; text-align:center">Kalimat</th>
                <th style="vertical-align: middle; text-align:center">Total Koherensi</th>
                <th style="vertical-align: middle; text-align:center">Derajat Koherensi</th>
                <th style="vertical-align: middle; text-align:center">Koherensi Kalimat</th>

                @foreach ($formulas as $f)
                    <th style="vertical-align: middle; text-align:center">{!! ucwords($f->name) !!}</th>
                @endforeach

                <th style="vertical-align: middle; text-align:center">Kata tak Teridentifikasi</th>
                <th style="vertical-align: middle; text-align:center; min-width: 100px;">Aksi</th>
            </tr>
            @foreach ($users as $user)
                @php
                    $trainningID = Request::get('trainningID');
                    $sessionID = Request::get('sessionID');
                    $moduleID = Request::get('moduleID');
                    $userID = $user->id;
                    $stories = $user->storyByTrainning($trainningID, $sessionID, $moduleID);
                @endphp

                @if (count($stories))
                <tr style="border: 1px solid #eaecee">
                    <td rowspan="{{ count($stories) }}" style="vertical-align: middle; border: 1px solid #eaecee;">{{ $user->name }}</td>
                    @foreach ($stories as $key => $story)
                        @php

                        if ($key)
                            echo '<tr style="border: 1px solid #eaecee">';

                        $countCategories = 0;
                        $percentageCategories = $story->countCategoryPercentage();

                        $storyDetail = $story->detail;

                        echo '<td style="vertical-align: middle;">';
                        switch($story->submit_periode) {
                            case 0:
                                echo 'before';
                                break;
                            case 1:
                                echo 'during';
                                break;
                            case 2:
                                echo 'after';
                                break;
                        }
                        echo '</td>';

                        // Categories
                        foreach ($wordCategories as $cat) {
                            if ($cat->code != 'SC01') {
                                $percentage = !empty($percentageCategories[$cat->id]) ? $percentageCategories[$cat->id] : '-';
                                if ($percentage)
                                    $countCategories++;

                                if ($percentage != '-')
                                    echo '<td style="vertical-align: middle;" align="center">'.number_format($percentage, 2).'%</td>';
                                else
                                    echo '<td style="vertical-align: middle;" align="center">' . $percentage . '</td>';
                            }
                        }

                        echo '<td style="vertical-align: middle;" align="center">'.$countCategories.'</td>';

                        // Huruf
                        $char = $storyDetail->countCharPerWord();
                        $conjuct = $storyDetail->countConjuction();
                        echo '<td style="vertical-align: middle;" align="center">'.$char['char_per_word'].'</td>';

                        // Kata
                        echo '<td style="vertical-align: middle;" align="center">'.$char['total_words'].'</td>';


                        // Kalimat
                        $countSentence = $storyDetail->countSentence();
                        echo '<td style="vertical-align: middle;" align="center">'.$countSentence.'</td>';

                        // Koherensi
                        $countCoherence = $storyDetail->countCoherenceOfSentences($countSentence);
                        $percentageCoherence = (($countCoherence + $conjuct) / (empty($char['total_words']) ? 1 : $char['total_words']) * 100) . '%';
                        echo '<td style="vertical-align: middle;" align="center">';
                        echo $countCoherence;
                        echo'</td>';

                        // Derajat Koherensi
                        echo '<td style="vertical-align: middle;" align="center">';
                        echo $countSentence / (empty($char['total_words']) ? 1 : $char['total_words']) * 100 . '%';
                        echo '</td>';

                        echo '<td style="vertical-align: middle;" align="center">';
                        echo $percentageCoherence;
                        echo'</td>';

                        // Formula
                        foreach ($formulas as $formula) {
                            $value = '';
                            $results = $storyDetail->results;

                            foreach ($results as $result) {
                                if ($result->formula_id == $formula->id) {
                                    $value = number_format($result->score, 2).'%';
                                    break;
                                } else {
                                    $value = '-';
                                }
                            }
                            echo '<td style="vertical-align: middle;" align="center">'.$value.'</td>';
                        }
                        @endphp

                        <td style="vertical-align: middle;" align="center">{{ ($percentageCategories['total_unidentified_words'] ? $percentageCategories['total_unidentified_words'] : 0) }}</td>

                        @if (!$key)
                            <td align="center" rowspan="{{ count($stories) }}" style="vertical-align: middle; border: 1px solid #eaecee;">
                                <a href="{{ url('behaviour/download/story/users/' . $trainningID . '/' . $sessionID . '/' . $moduleID. '/' .$userID) }}" class="btn btn-success btn-sm" title="Download Story"><i class="fa fa-file"></i></a>
                                <a href="{{ url('behaviour/download/report/users/excel/' . $trainningID . '/' . $sessionID . '/' . $moduleID. '/' .$userID) }}" class="btn btn-success btn-sm" title="Download Excel"><i class="fa fa-file-excel-o"></i></a>
                            </td>
                        @endif

                        @if ($key)
                            </tr>
                        @endif
                    @endforeach
                </tr>
                @endif
            @endforeach
        </table>
    </div>
</div>
<!-- END Table -->
@endif

<!-- <div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Performance</h3>
    </div>
    <div class="block-content" style="padding: 18px;">
        <canvas id="js-chartjs-performance-bars">
    </div>
</div> -->
<!-- END Table -->

<!-- <div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">ROTI</h3>
    </div>
    <div class="block-content" style="padding: 18px;">
        <canvas id="js-chartjs-roti-bars">
    </div>
</div> -->
<!-- END Table -->

<script>
    new Chart(document.getElementById("js-chartjs-reaction-bars"), {
        type: 'bar',
        data: {
            labels: ["Progress"],
            datasets: [
                {
                    label: "Reaction {{ $reaction ? round(($reaction->response), 2) : 0 }} %",
                    backgroundColor: "#71bcf8",
                    data: ['{{ $reaction ? round(($reaction->response), 2) : 0 }}']
                }
            ]
        },
        options: {
            title: {
                display: true,
            },
            scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                        beginAtZero: true   // minimum value will be 0.
                    }
                }]
            }
        }
    });    
</script>
<script type="text/javascript">
    new Chart(document.getElementById("js-chartjs-learning-bars"), {
        type: 'bar',
        data: {
            labels: ["Progress"],
            datasets: [
                {
                    label: "Learning {{ $learning ? round(($learning->response), 2) : 0 }} %",
                    backgroundColor: "#d0e8fc",
                    data: ['{{ $learning ? round(($learning->response), 2) : 0 }}']
                }
            ]
        },
        options: {
            title: {
                display: true,
            },
            scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                        beginAtZero: true   // minimum value will be 0.
                    }
                }]
            }
        }
    });
</script>