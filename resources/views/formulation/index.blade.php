@extends('layouts.root')

@section('title','Formulation')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Formulation</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
    		@if(Session::get('status') == '200')
    	        <div class="alert alert-success alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @elseif(Session::get('status') == 'err')
    			<div class="alert alert-danger alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @endif
        @endif

        <h2 class="content-heading">
            Formulation
        </h2>

        {{-- search --}}

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Search Forms</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <form method="GET" class="form form-inline">
                    @csrf
                    <div>
                        <input type="text" class="form-control" name="name" value="{{ Request::has('name') ? Request::get('name') : '' }}" placeholder="Enter Code or Name ...">
                        <button type="submit" class="btn btn-alt-primary">Search</button>
                        <a href="{{ url('formulation') }}" class="btn btn-secondary">Refresh</a>
                    </div>
                </form>
            </div>
        </div>


        <a class="btn btn-success mb-3" href="{{ url('formulation/create') }}"><i class="fa fa-plus"></i> &nbsp; Create New</a>
        <button class="btn btn-success btn-import mb-3" type="button"> <i class="fa fa-upload"></i> &nbsp; Import Formulation</button>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">List of Formulation</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <table id="datatable" class="table table-vcenter table-stripped">
                    <thead>
                        <tr>
                            <th width="1%"><center><input type="checkbox" id="check-all"></center></th>
                            <th style="width: 50px;">No</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Categories</th>
                            <th>Status</th>
                            <th style="width: 120px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($formulations) > 0)
                            @foreach($formulations as $result)
                                <tr>
                                    <td align="center"><input type="checkbox" class="checkbox" data-id="{{ $result->id }}" name="checkbox[{{ $result->id }}]"></td>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $result->code }}</td>
                                    <td>{{ $result->name }}</td>
                                    <td>
                                        @foreach($result->detail as $key => $detail)
                                            @if($key > 0)
                                                {{ ', ' . $detail->wordCategory->name }}
                                            @else
                                                {{ $detail->wordCategory->name }}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        <form id="switch_formulation-{{ $result->id }}" action="{{ url('formulation/switch_formulation') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{ $result->id }}">
                                            <input type="checkbox" data-id="{{ $result->id }}" class="status" name="status" {{ $result->status == 1 ? 'checked' : '' }}>
                                        </form>
                                    </td>
                                    <td>
                                        <a href="{{ url('formulation/detail/'. $result->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Lihat" style="height: 19px">
                                            <i class="fa fa-search"></i>
                                        </a>
                                        <a href="{{ url('formulation/edit/'. $result->id) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Ubah" style="height: 19px">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('formulation/delete/'. $result->id) }}" class="btn btn-sm btn-danger btn-xs btn-delete" style="height: 19px">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
						@else
							<tr>
								<td colspan='7' class="text-center">Data not available.</td>
							</tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Table -->
        <div class="block">
            <div class="block-content" style="padding: 18px;">
                <div class="row">
                    <div class="col align-self-center">
                        Showing {{ count($formulations) == 0 ? 0 : count($formulations) }} out of {{ $total }} entries
                    </div>
                    <div class="col d-flex justify-content-end">
                        <div class="align-self-center" style="height: 34px;">
                            {{ $formulations->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <div class="block-content" style="padding: 18px;">
                <form action="{{ url('formulation/delete_all') }}" method="post" id="multiple_delete_form">
                    {{ csrf_field() }}
                    <span id ="create_input_hidden"></span>
                    <button class="btn btn-danger btn-sm" onclick="return confirm('Delete Formulation?');">Delete Formulation</button>
                </form>
            </div>
        </div>
    </div>


</div>

@include('modal_delete')
@include('modal_import_formulation')
@endsection

@push('script')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.css') }}">
    <script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js') }} "></script>
    <script src="{{ asset('assets/js/pages/be_tables_datatables.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                "pageLength": 25,
                "lengthChange": true,
                "searching": true
            });
        } );

        $('.status').change(function(){
            $('#switch_formulation-' + $(this).data('id')).submit();
        })

        $("#check-all").change(function(){
            if ($(this).is(':checked')) {
                $('.checkbox').prop('checked', true);
            } else {
                $('.checkbox').prop('checked', false);
            }
        });

        $("#multiple_delete_form").submit(function(){
            var data = [];
            $("#create_input_hidden").html("");
            $('input[name^="checkbox"]:checked').each(function() {
                $("#create_input_hidden").append("<input type='hidden' name='check["+$(this).data("id")+"]' value='"+$(this).val()+"'>");
            });
            return true;
        });

        $('.btn-import').click(function(){
            var action = $(this).data('action');
            $('#modalImportFormulation').modal('show');
        });

        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Delete Formulation');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
