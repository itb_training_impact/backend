@extends('layouts.root')

@section('title','Formulation - Edit')

@section('content')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/plugins/select2/select2-flat-theme.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/plugins/select2/select2.min.css') }}">
<link rel="stylesheet" id="css-main" href="{{ asset('assets/css/codebase.min.css') }}">

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('formulation') }}">Formulation</a>
        <span class="breadcrumb-item active">Edit</span>
    </nav>

    @if ($errors->any())
        <div class="alert alert-danger m-t-20">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Formulation - Edit</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('formulation/update/' . $formulation->id) }}" method="post" id="form">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <select name="group" class="js-select2 form-control" data-placeholder="Formulation Group">
                                    <option></option>
                                    @foreach($groups as $value)
                                        <option value="{{ $value->id }}" {{ $formulation->formulation_group_id == $value->id ? "selected" : "" }} >{{ $value->name }}</option>
                                    @endforeach
                                </select>
                                <label for="material-email">Formulation Group <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="code" name="code" placeholder="Code" value="{{ $formulation->code }}">
                                <label for="material-email">Code <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="name" name="name" placeholder="Name" value="{{ $formulation->name }}">
                                <label for="material-email">Name <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="material-status">Status <span class="text-danger">*</span></label>
                            <div class="form-check">
                                <input class="" type="radio" name="status" value=1 {{ $formulation->status ? 'checked' : '' }}>
                                <label class="form-check-label">Active</label>
                            </div>
                            <div class="form-check">
                                <input class="" type="radio" name="status" value=0 {{ !$formulation->status ? 'checked' : '' }}>
                                <label class="form-check-label">Inactive</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <textarea class="form-control" id="description" name="description" placeholder="Description">{{ $formulation['description'] }}</textarea>
                                <label for="material-email">Description</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="aspek_tertinggi" name="aspek_tertinggi" placeholder="Aspek Tertinggi" value="{{ $formulation->aspek_tertinggi }}">
                                <label for="material-email">Aspek Tertinggi <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="aspek_terendah" name="aspek_terendah" placeholder="Aspek Terendah" value="{{ $formulation->aspek_terendah }}">
                                <label for="material-email">Aspek Terendah <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <label for="material-email">Word Categories<span class="required">*</span></label>
                                <div class="row mt-3">
                                    <div class="col-6">
                                        <select class="form-control js-select2" name="cat[0]" id="cat-0">
                                            @foreach ($wordCategories as $cat)
                                                <option value="{{ $cat->id }}">{{ $cat->code }} - {{ $cat->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-2">
                                        <input type="text" name="konstanta[0]" class="form-control" id="konstanta-0" value=1 placeholder="konstanta">
                                    </div>
                                    <div class="col-2">
                                        <input type="text" name="bobot[0]" class="form-control" id="bobot-0" value=0 placeholder="weight">
                                        <small class="text-muted">satuan %</small>
                                    </div>
                                    <div class="col-2 text-left" id="first-add-button">
                                        <a href="javascript:void(0)" data-id="0" class="add-file btn btn-primary"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                                <span id="generate-html"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Save</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <a href="{{ url('formulation') }}" class="btn btn-secondary" style="float:right;">Back</a> 
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <!-- <script src="{{ asset('assets/js/plugins/select2/select2-bootstrap.min.css') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2-bootstrap.min.css') }}"></script> -->
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script>
        $('.js-select2').select2()

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'group': {
                    required: true,
                },
                'code': {
                    required: true,
                },
                'name': {
                    required: true,
                },
                'status': {
                    required: true,
                },
                'aspek_tertinggi': {
                    required: true,
                },
                'aspek_terendah': {
                    required: true,
                }
            }
        });
    </script>

    <script>
        var data_select = [];
        var current_select = [];
        var selected_value = 0;
        var selected_bobot = 0;
        var selected_konstanta = 0;
        var i = 1;

        $("#form").submit(function(){
            for (var j = 0; j <= i; j++) {
                $('#cat-'+(j)).prop('disabled', false); 
            }
        });

        $("#cat-0 option").each(function() {
            data_select.push({'id' : parseInt($(this).val()), 'label' : $(this).text()});
        });

        $('.content').on('click', '.add-file', function() {
            var html = ''
            current_select.push(parseInt($('#cat-'+(i - 1)).val()));
            $('#cat-' + (i - 1)).prop('disabled', true);

            html += `
                <span class="additional-cat-${i} row mt-3">
                    <div class="col-lg-6">
                        <select class="form-control js-select2" name="cat[${i}]" id="cat-${i}">
            `;

            var bobot = 0;
            var konstanta = 1;
            for (var j = 0; j < data_select.length; j++) {
                if ($.inArray(data_select[j].id, current_select) == -1) {
                    if (selected_value != 0 && selected_value == data_select[j].id) {
                        var selected = 'selected';
                        var bobot = selected_bobot;
                        var konstanta = selected_konstanta;
                    } else {
                        var selected = '';
                    }
                    html += `<option ${selected} value="${data_select[j].id}">${data_select[j].label}</option>`;
                }
            }
            html += `
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <input type="text" name="konstanta[${i}]" class="form-control" id="konstanta-${i}" value=${konstanta} placeholder="konstanta">
                    </div>
                    <div class="col-2">
                        <input type="text" name="bobot[${i}]" id="bobot-${i}" value="${bobot}" class="form-control">
                        <small class="text-muted">satuan %</small>
                    </div>
                    <div class="col-2" style="margin-bottom:3px;" id="delete-button">
                        <a id="minus-${i}" data-id="${i}" href="javascript:void(0)" class="remove-file btn btn-danger"><i class="fa fa-minus"></i></a>
                    </div>
                </span> 
            `;

            if (i >= data_select.length - 1) {
                html += `
                    <div class="row">
                        <div class="col-10"></div>
                        <div class="col-2" id="add-button">
                            <a data-id="${i}" href="javascript:void(0)" class="add-disabled-file btn btn-primary" disabled><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                `;
            } else {
                html += `
                    <div class="row">
                        <div class="col-10"></div>
                        <div class="col-2" id="add-button">
                            <a data-id="${i}" href="javascript:void(0)" class="add-file btn btn-primary"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                `;
            }
            
            $("#add-button").remove();
            $("#delete-button").remove();

            i++;

            $("#generate-html").append(html);
            $(this).remove();

            $('.js-select2').select2()
        });

        $('.content').on('click', '.remove-file', function() {
            i--;
            var delete_button_html = $('#delete-button').html();
            $(".additional-cat-" + i).remove();
            if (i == data_select.length - 1) {
                $("#add-button").remove();
                var html = `
                    <div class="row">
                        <div class="col-10"></div>
                        <div class="col-2" id="add-button">
                            <a data-id="${i}" href="javascript:void(0)" class="add-file btn btn-primary"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                `;
                $("#generate-html").append(html);
            }

            var remove = current_select.indexOf(parseInt($('#cat-' + (i - 1)).val()));
            if (remove > -1) {
                current_select.splice(remove, 1);
            }

            if (i <= 1) {
                $("#first-add-button").html('<a href="javascript:void(0)" class="add-file btn btn-primary"><i class="fa fa-plus"></i></a>');
                $("#add-button").remove();
                $("#delete-button").remove();
            } else {
                console.log($(".additional-cat-" + (i - 1)).html())
                $(".additional-cat-" + (i - 1)).append(`
                    <div class="col-2" style="margin-bottom:3px;" id="delete-button">
                        ${delete_button_html}
                    </div>
                    `)
            }

            $('#cat-' + (i - 1)).prop('disabled', false);
    });

    var current_values = JSON.parse('{!! !empty($formulation->detail) ? $formulation->detail : "[]" !!}');
    $("#cat-0").val(current_values[0]['cat_id']);
    $("#bobot-0").val(current_values[0]['bobot']);
    $("#konstanta-0").val(current_values[0]['konstanta']);

    for (var j = 1; j < current_values.length; j++) {
        selected_value = current_values[j]['cat_id'];
        selected_bobot = current_values[j]['bobot'];
        selected_konstanta = current_values[j]['konstanta'];
        $('.add-file').trigger('click');
        selected_value = 0;
    }
    </script>
@endpush
