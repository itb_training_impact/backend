@extends('layouts.root')

@section('title','Detail Position Level')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="{{ url('home') }}">Dashboard</a>
            <a class="breadcrumb-item" href="{{ url('position_level') }}">Position Level</a>
            <span class="breadcrumb-item active">Detail</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
    		@if(Session::get('status') == '200')
    	        <div class="alert alert-success alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @elseif(Session::get('status') == 'err')
    			<div class="alert alert-danger alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @endif
        @endif

        <h2 class="content-heading">
            Detail Position Level
        </h2>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Detail Position Level</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <div class="form-group">
                    <label for="">Name</label>
                    <div> {{ $positionLevels->name }} </div>
                </div>
                <div class="form-group">
                    <label for="">Description</label>
                    <div> {{ $positionLevels->description }} </div>
                </div>
            </div>
        </div>
        <a href="{{ url('position_level') }}" class="btn btn-secondary" style="float:right;">Back</a>
    </div>
</div>

@include('modal_delete')
@endsection

@push('script')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.css') }}">
    <script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js') }} "></script>
    <script src="{{ asset('assets/js/pages/be_tables_datatables.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
            	"pageLength": 50,
            	"lengthChange": true,
            	"searching": true
            });
        } );

        $('.btn-delete').click(function(){
        	var action = $(this).data('action');

        	$('#modalDelete').modal('show');
        	$('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
