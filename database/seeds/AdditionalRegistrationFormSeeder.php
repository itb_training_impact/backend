<?php

use Illuminate\Database\Seeder;

class AdditionalRegistrationFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('registration_form')->insert([
        	[
				'id' => 9,
				'type' => 'registration_form',
				'label' => 'Usia',
				'description' => 'Field untuk isian usia',
				'name' => 'registration_age',
				'index' => 8,
				'value' => '<p><input type=\"text\" name=\"registration_age\" class=\"form-control input-text\" placeholder=\"Usia\"></p>',
				'additional_data' => '{\"validation\":\"required\"}',
				'total_column' => 6,
				'status' => 0,
				'created_at' => '2018-06-13 10:35:09',
				'updated_at' => '2018-07-28 05:24:25',
			], [
				'id' => 10,
				'type' => 'registration_form',
				'label' => 'Masa Kerja',
				'description' => 'Field untuk isian masa kerja',
				'name' => 'registration_working_time',
				'index' => 9,
				'value' => '<p><input type=\"text\" name=\"registration_working_time\" class=\"form-control input-text\" placeholder=\"Masa Kerja\"></p>',
				'additional_data' => '{\"validation\":\"required\"}',
				'total_column' => 6,
				'status' => 0,
				'created_at' => '2018-06-13 10:35:09',
				'updated_at' => '2018-07-28 05:24:25',
			], [
				'id' => 11,
				'type' => 'registration_form',
				'label' => 'Jenjang Jabatan',
				'description' => 'Field untuk isian jenjang jabatan',
				'name' => 'registration_position_level',
				'index' => 10,
				'value' => '<select class=\"form-control input-text\" name=\"registration_position_level\"><option value=\"\">Pilih Jenjang Jabatan</option></select>',
				'additional_data' => '{\"validation\":\"required\"}',
				'total_column' => 6,
				'status' => 0,
				'created_at' => '2018-06-13 10:35:09',
				'updated_at' => '2018-07-28 05:24:25',
			], [
				'id' => 12,
				'type' => 'registration_form',
				'label' => 'Divisi',
				'description' => 'Field untuk isian divisi',
				'name' => 'registration_devision',
				'index' => 11,
				'value' => '<select class=\"form-control input-text\" name=\"registration_position_level\"><option value=\"\">Pilih Divisi / Departemen</option></select>',
				'additional_data' => '{\"validation\":\"required\"}',
				'total_column' => 6,
				'status' => 0,
				'created_at' => '2018-06-13 10:35:09',
				'updated_at' => '2018-07-28 05:24:25',
			]
		]);
    }
}
