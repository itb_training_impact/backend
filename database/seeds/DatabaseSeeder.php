<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(ArticleCategory::class);
        // $this->call(RegistrationFormSeeder::class);
        // $this->call(AdditionalRegistrationFormSeeder::class);
        // $this->call(MasterGroupsSeeder::class);
        $this->call(CriteriaSeeder::class);
        
        // $this->call(UserLevelSeeder::class);
        // $this->call(UserSeeder::class);
        // $this->call(ModuleSeeder::class);
        // $this->call(TaskSeeder::class);
        // $this->call(RoleSeeder::class);
        // $this->call(ProductTypeSeeder::class);
    }
}
