<?php

use Illuminate\Database\Seeder;

class MasterGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert([
            [
				'group_id' => 0,
				'group' => 'All Position',
				'formula_id' => 5,
				'is_reaction' => 2,
				'created_at' => '2020-10-13 10:35:09',
				'updated_at' => '2020-10-13 10:35:09',
            ], [
				'group_id' => 0,
				'group' => 'Boss Questions',
				'formula_id' => 5,
				'is_reaction' => 2,
				'created_at' => '2020-10-13 10:35:09',
				'updated_at' => '2020-10-13 10:35:09',
            ], [
				'group_id' => 0,
				'group' => 'Partner Questions',
				'formula_id' => 5,
				'is_reaction' => 2,
				'created_at' => '2020-10-13 10:35:09',
				'updated_at' => '2020-10-13 10:35:09',
            ]            
        ]);
    }
}
