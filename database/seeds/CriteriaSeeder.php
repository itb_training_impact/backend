<?php

use Illuminate\Database\Seeder;

class CriteriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('criterias')->insert([
        	[
				'criteria_id' => 1,
				'criteria' => 'Output',
				'criteria_description' => 'Karyawan memenuhi KPI sesuai target.',
				'level' => 0,
				'created_at' => '2020-11-02 14:35:09',
				'updated_at' => '2020-11-02 14:35:09',
			], [
				'criteria_id' => 2,
				'criteria' => 'Quality',
				'criteria_description' => 'Setiap pekerjaan diselesaikan sesuai standard.',
				'level' => 0,
				'created_at' => '2020-11-02 14:35:09',
				'updated_at' => '2020-11-02 14:35:09',
			], [
				'criteria_id' => 3,
				'criteria' => 'Time',
				'criteria_description' => 'Karyawan menyelesaikan pekerjaan tepat waktu.',
				'level' => 0,
				'created_at' => '2020-11-02 14:35:09',
				'updated_at' => '2020-11-02 14:35:09',
			], [
				'criteria_id' => 4,
				'criteria' => 'Cost',
				'criteria_description' => 'Biaya yang ditanggung perusahaan akibat kegagalan karyawan menyelesaikan pekerjaan besar.',
				'level' => 0,
				'created_at' => '2020-11-02 14:35:09',
				'updated_at' => '2020-11-02 14:35:09',
			], [
				'criteria_id' => 5,
				'criteria' => 'Work Habits',
				'criteria_description' => 'Karyawan selalu datang tepat waktu.',
				'level' => 0,
				'created_at' => '2020-11-02 14:35:09',
				'updated_at' => '2020-11-02 14:35:09',
			], [
				'criteria_id' => 6,
				'criteria' => 'Work Climate and Attitudes',
				'criteria_description' => 'Tingkat kepuasaan pelanggan tinggi.',
				'level' => 0,
				'created_at' => '2020-11-02 14:35:09',
				'updated_at' => '2020-11-02 14:35:09',
			], [
				'criteria_id' => 7,
				'criteria' => 'Development',
				'criteria_description' => 'Kinerja karyawan secara keseluruhan tinggi.',
				'level' => 0,
				'created_at' => '2020-11-02 14:35:09',
				'updated_at' => '2020-11-02 14:35:09',
			], [
				'criteria_id' => 8,
				'criteria' => 'Innovation',
				'criteria_description' => 'Karyawan memiliki tingkat inovasi tinggi.',
				'level' => 0,
				'created_at' => '2020-11-02 14:35:09',
				'updated_at' => '2020-11-02 14:35:09',
			]
		]);
    }
}
