<?php

use Illuminate\Database\Seeder;

class RegistrationFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('registration_form')->insert([
        	[
	            'id' => 1,
				'type' => 'registration_form',
				'label' => 'Jenis Kelamin',
				'description' => 'Field untuk isian jenis kelamin',
				'name' => 'registration_gender',
				'index' => 2,
				'value' => '<label class=\"radio-inline\">\n  <input type=\"radio\" name=\"registration_gender\" value=\"L\"> Laki - Laki\n</label>\n<label class=\"radio-inline\">\n  <input type=\"radio\" name=\"registration_gender\" value=\"P\"> Perempuan\n</label>',
				'additional_data' => '{\"validation\":\"required|in_list[L,P]\"}',
				'total_column' => 6,
				'status' => 1,
				'created_at' => '2018-06-13 09:57:32',
				'updated_at' => '2018-07-04 11:53:34'
			], [
				'id' => 3,
				'type' => 'registration_form',
				'label' => 'Agama',
				'description' => 'Field untuk isian agama',
				'name' => 'registration_religion',
				'index' => 4,
				'value' => '<select class=\"form-control input-text\" name=\"registration_religion\"><option value=\"\">Pilih Agama..</option><option value=\"islam\">Islam</option><option value=\"katholik\">Katholik</option><option value=\"protestan\">Protestan</option><option value=\"budha\">Budha</option><option value=\"hindu\">Hindu</option><option value=\"lain-lain\">Lain - lain</option></select>',
				'additional_data' => '{\"validation\":\"required|in_list[islam,katholik,protestan,budha,hindu,lain-lain]\"}',
				'total_column' => 6,
				'status' => 0,
				'created_at' => '2018-06-13 10:35:09',
				'updated_at' => '2018-07-28 05:24:10',
			], [
				'id' => 4,
				'type' => 'registration_form',
				'label' => 'Pekerjaan',
				'description' => 'Field untuk isian pekerjaan',
				'name' => 'registration_job',
				'index' => 5,
				'value' => '<p><input type=\"text\" name=\"registration_job\" class=\"form-control input-text\" placeholder=\"Pekerjaan\"></p>',
				'additional_data' => '{\"validation\" : \"required|trim\"}',
				'total_column' => 6,
				'status' => 0,
				'created_at' => '2018-06-13 10:36:17',
				'updated_at' => '2018-07-04 11:53:25',
			], [
				'id' => 5,
				'type' => 'registration_form',
				'label' => 'Tanggal Lahir',
				'description' => 'Field untuk isian tanggal lahir',
				'name' => 'registration_dob',
				'index' => 1,
				'value' => '<p><input type=\"text\" name=\"registration_dob\" class=\"form-control input-text datepicker\" placeholder=\"Tanggal Lahir\"></p>',
				'additional_data' => '{\"validation\" : \"required|trim\"}',
				'total_column' => 6,
				'status' => 0,
				'created_at' => '2018-06-13 10:36:17',
				'updated_at' => '2018-08-16 02:40:08',
			], [
				'id' => 6,
				'type' => 'registration_form',
				'label' => 'Etnis',
				'description' => 'Field untuk isian etnis',
				'name' => 'registration_etnis',
				'index' => 3,
				'value' => '<p><input type=\"text\" name=\"registration_etnis\" class=\"form-control input-text\" placeholder=\"Etnis\"></p>',
				'additional_data' => '{\"validation\" : \"required|trim\"}',
				'total_column' => 6,
				'status' => 0,
				'created_at' => '2018-06-13 10:36:17',
				'updated_at' => '2018-08-16 02:40:04',
			], [
				'id' => 7,
				'type' => 'registration_form',
				'label' => 'Status Kawin',
				'description' => 'Field untuk isian status pernikahan',
				'name' => 'registration_marriage',
				'index' => 6,
				'value' => '<select class=\"form-control input-text\" name=\"registration_marriage\"><option value=\"\">Pilih Status Pernikahan..</option><option value=\"belum-kawin\">Belum Kawin</option><option value=\"kawin\">Kawin</option></select>',
				'additional_data' => '{\"validation\":\"required|in_list[belum-kawin,kawin]\"}',
				'total_column' => 6,
				'status' => 0,
				'created_at' => '2018-06-13 10:35:09',
				'updated_at' => '2018-07-28 05:24:16',
			], [
				'id' => 8,
				'type' => 'registration_form',
				'label' => 'Pendidikan',
				'description' => 'Field untuk isian pendidikan',
				'name' => 'registration_education',
				'index' => 7,
				'value' => '<select class=\"form-control input-text\" name=\"registration_education\"><option value=\"0\">Please select</option><option value=\"1\">SD</option><option value=\"2\">SMP</option><option value=\"4\">SMA</option><option value=\"5\">S1</option><option value=\"6\">S2</option><option value=\"7\">S3</option></select>',
				'additional_data' => '{\"validation\":\"required\"}',
				'total_column' => 6,
				'status' => 0,
				'created_at' => '2018-06-13 10:35:09',
				'updated_at' => '2018-07-28 05:24:25',
			]
		]);
    }
}
