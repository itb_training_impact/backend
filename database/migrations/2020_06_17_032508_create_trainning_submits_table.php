<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainningSubmitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainning_submits', function (Blueprint $table) {
            $table->bigIncrements('submit_id');
            $table->bigInteger('trainning_id')->nullable()->unsigned();
            $table->bigInteger('reaction_id')->nullable()->unsigned();
            $table->bigInteger('learning_id')->nullable()->unsigned();
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->integer('submit_periode')->default(0)->comment('0: before, 1: during, 2: after');
            $table->string('submit_response')->nullable();
            $table->timestamps();

            $table->foreign('trainning_id')
                    ->references('trainning_id')
                    ->on('trainnings')
                    ->onDelete('set null')
                    ->onUpdate('cascade');

            $table->foreign('reaction_id')
                    ->references('reaction_id')
                    ->on('reactions')
                    ->onDelete('set null')
                    ->onUpdate('cascade');

            $table->foreign('learning_id')
                    ->references('learning_id')
                    ->on('learnings')
                    ->onDelete('set null')
                    ->onUpdate('cascade');

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('set null')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainning_submits');
    }
}
