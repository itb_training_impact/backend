<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewCalculation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create view reaction calculation
        DB::statement("DROP VIEW IF EXISTS v_reaction_calculate");
        DB::statement("
            CREATE VIEW v_reaction_calculate AS
            SELECT 
                groups.`group`,
                trainning_submits.submit_periode as periode,
                trainning_submits.user_id as participant_id,
                trainning_submits.trainning_id as trainning_id,
                SUM(trainning_submits.submit_response) as sum_response,
                COUNT(*) as count_response
            FROM 
                reactions 
            JOIN
                groups
            ON
                reactions.group_id = `groups`.group_id
            JOIN
                trainning_submits
            ON
                reactions.reaction_id = trainning_submits.reaction_id
            WHERE 
                trainning_submits.user_id = 3
            AND
                `groups`.is_reaction = 0
            GROUP BY
                reactions.group_id, trainning_submits.user_id, trainning_submits.submit_periode;
        ");

        //create view learning calculation
        DB::statement("DROP VIEW IF EXISTS v_learning_calculate");
        DB::statement("
            CREATE VIEW v_learning_calculate AS
            SELECT 
                groups.`group`,
                trainning_submits.submit_periode as periode,
                trainning_submits.user_id as participant_id,
                trainning_submits.trainning_id as trainning_id,
                SUM(trainning_submits.submit_response) as sum_response,
                COUNT(*) as count_response
            FROM 
                learnings 
            JOIN
                groups
            ON
                learnings.group_id = `groups`.group_id
            JOIN
                trainning_submits
            ON
                learnings.learning_id = trainning_submits.learning_id
            WHERE 
                trainning_submits.user_id = 3
            AND
                `groups`.is_reaction = 1
            GROUP BY
                learnings.group_id, trainning_submits.user_id, trainning_submits.submit_periode;
        
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS v_reaction_calculate");
        DB::statement("DROP VIEW IF EXISTS v_learning_calculate");
    }
}
