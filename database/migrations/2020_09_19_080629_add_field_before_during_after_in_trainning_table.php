<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldBeforeDuringAfterInTrainningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trainnings', function (Blueprint $table) {
            $table->tinyInteger('before')->default('0')->after('participant_id')->comment('0 : pending, 1 : active, 2 : close');
            $table->tinyInteger('during')->default('0')->after('before')->comment('0 : pending, 1 : active, 2 : close');
            $table->tinyInteger('after')->default('0')->after('during')->comment('0 : pending, 1 : active, 2 : close');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trainning', function (Blueprint $table) {
            //
        });
    }
}
