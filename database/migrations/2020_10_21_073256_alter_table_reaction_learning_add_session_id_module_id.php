<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableReactionLearningAddSessionIdModuleId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reactions', function (Blueprint $table) {
            $table->bigInteger('session_id')->unsigned()->nullable()->after('trainning_id');
            $table->bigInteger('module_id')->unsigned()->nullable()->after('session_id');

            $table->foreign('session_id')
                ->references('session_id')->on('sessions')
                ->onDelete('set null')
                ->onUpdate('cascade');


            $table->foreign('module_id')
                ->references('module_id')->on('modules')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });

        Schema::table('learnings', function (Blueprint $table) {
            $table->bigInteger('session_id')->unsigned()->nullable()->after('trainning_id');
            $table->bigInteger('module_id')->unsigned()->nullable()->after('session_id');

            $table->foreign('session_id')
                ->references('session_id')->on('sessions')
                ->onDelete('set null')
                ->onUpdate('cascade');


            $table->foreign('module_id')
                ->references('module_id')->on('modules')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });

        Schema::table('behaviours', function (Blueprint $table) {
            $table->bigInteger('session_id')->unsigned()->nullable()->after('trainning_id');
            $table->bigInteger('module_id')->unsigned()->nullable()->after('session_id');

            $table->foreign('session_id')
                ->references('session_id')->on('sessions')
                ->onDelete('set null')
                ->onUpdate('cascade');


            $table->foreign('module_id')
                ->references('module_id')->on('modules')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
