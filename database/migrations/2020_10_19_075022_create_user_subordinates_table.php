<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSubordinatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_subordinates', function (Blueprint $table) {
            $table->bigIncrements('user_subordinate_id');
            $table->bigInteger('user_id');
            $table->string('subordinate_name', 100);
            $table->string('subordinate_email', 100);
            $table->string('code_subordinate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_subordinates');
    }
}
