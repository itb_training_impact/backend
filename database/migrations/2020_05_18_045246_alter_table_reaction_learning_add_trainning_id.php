<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableReactionLearningAddTrainningId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reactions', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned()->nullable()->after('type_id');
            $table->bigInteger('trainning_id')->unsigned()->nullable()->after('user_id');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('set null')
                ->onUpdate('cascade');


            $table->foreign('trainning_id')
                ->references('trainning_id')->on('trainnings')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });


        Schema::table('learnings', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned()->nullable()->after('type_id');
            $table->bigInteger('trainning_id')->unsigned()->nullable()->after('user_id');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('set null')
                ->onUpdate('cascade');


            $table->foreign('trainning_id')
                ->references('trainning_id')->on('trainnings')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
