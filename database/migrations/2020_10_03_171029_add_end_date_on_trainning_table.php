<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEndDateOnTrainningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
         Schema::table('trainnings', function (Blueprint $table) {
             $table->date('end_date')->nullable()->after('trainning_description');
        });
}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('trainnings', function (Blueprint $table) {
            $table->dropColumn('end_date');
        });
    }
}
