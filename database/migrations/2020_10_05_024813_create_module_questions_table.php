<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create('module_questions', function (Blueprint $table) {
            $table->bigIncrements('module_question_id');
            $table->bigInteger('module_id')->unsigned()->nullable();
            $table->bigInteger('reaction_id')->nullable()->unsigned();
            $table->bigInteger('learning_id')->nullable()->unsigned();
            $table->bigInteger('behaviour_id')->nullable()->unsigned();
            $table->bigInteger('performance_id')->nullable()->unsigned();
            $table->bigInteger('roti_id')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('module_id')
                ->references('module_id')->on('modules')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('reaction_id')
                ->references('reaction_id')->on('reactions')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('learning_id')
                ->references('learning_id')->on('learnings')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_questions');
    }
}
