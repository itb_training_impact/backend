<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoryResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('story_result', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('story_detail_id')->unsigned();
            $table->integer('formula_id');
            $table->string('formula_name', 50);
            $table->double('score');

            $table->foreign('story_detail_id')
            ->references('id')->on('story_detail')
            ->onDelete('cascade')
            ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
