<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulationDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulation_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('formulation_id')->unsigned();
            $table->integer('cat_id')->unsigned();
            $table->double('bobot');
            $table->double('konstanta')->default(1);
            $table->timestamps();

            $table->foreign('formulation_id')
            ->references('id')->on('formulation')
            ->onDelete('cascade')
            ->onUpdate('restrict');

            $table->foreign('cat_id')
            ->references('id')->on('word_category')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulation_detail');
    }
}
