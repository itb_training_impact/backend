<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableReactionLearning extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reactions', function (Blueprint $table) {
            $table->integer('reaction_rating_start')->nullable()->after('reaction_is_default');
            $table->integer('reaction_rating_finish')->nullable()->after('reaction_rating_start');
        });


        Schema::table('learnings', function (Blueprint $table) {
            $table->integer('learning_rating_start')->nullable()->after('learning_is_default');
            $table->integer('learning_rating_finish')->nullable()->after('learning_rating_start');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reactions', function (Blueprint $table) {
            $table->dropColumn(['reaction_rating_start','reaction_rating_finish']);
        });

        Schema::table('learnings', function (Blueprint $table) {
            $table->dropColumn(['learning_rating_start','learning_rating_finish']);
        });
    }
}
