<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLevelIntermediaryOnTrainningSubmitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trainning_submits', function (Blueprint $table) {
            $table->dropColumn('level_intermediary')->default(null)->comment('1: Boss, 2: Partner');
         });
         Schema::table('trainning_submits', function (Blueprint $table) {
            $table->integer('level_intermediary')->nullable()->unsigned()->comment('1: Boss, 2: Partner')
            ->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
