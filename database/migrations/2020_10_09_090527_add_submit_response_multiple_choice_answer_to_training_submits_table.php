<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmitResponseMultipleChoiceAnswerToTrainingSubmitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trainning_submits', function (Blueprint $table) {
            $table->string('submit_response_multiple_choice_answer')->after('submit_response');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trainning_submits', function (Blueprint $table) {
            $table->dropColumn('submit_response_multiple_choice_answer');
        });
    }
}
