<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reactions', function (Blueprint $table) {
            $table->bigIncrements('reaction_id');
            $table->string('reaction_code', 100);
            $table->text('reaction_question');
            $table->text('reaction_answer')->nullable();
            $table->bigInteger('type_id')->nullable()->unsigned();
            $table->bigInteger('group_id')->nullable()->unsigned();
            $table->integer('reaction_is_required')->default(0);
            $table->integer('reaction_is_default')->default(0);
            $table->timestamps();

            $table->foreign('type_id')
                ->references('type_id')->on('types')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('group_id')
                ->references('group_id')->on('groups')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reactions');
    }
}
