<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTrainningPeriodeOnModuleQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('module_questions', function (Blueprint $table) {
            $table->integer('trainning_detail_periode')->default(0)->comment('0: before, 1: during, 2: after');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            $table->dropColumn('trainning_detail_periode');
    }
}
