<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoryResultWordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('story_result_word', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('story_detail_id')->unsigned();
            $table->integer('word_id');
            $table->double('score');

            $table->foreign('story_detail_id')
            ->references('id')->on('story_detail')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
