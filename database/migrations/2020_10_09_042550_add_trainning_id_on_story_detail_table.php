<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTrainningIdOnStoryDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('story_detail', function (Blueprint $table) {
            $table->bigInteger('trainning_id')->nullable()->unsigned()->after('story_id');
            $table->bigInteger('session_id')->nullable()->unsigned()->after('trainning_id');
            $table->bigInteger('module_id')->nullable()->unsigned()->after('session_id');

             $table->foreign('trainning_id')
                    ->references('trainning_id')
                    ->on('trainnings')
                    ->onDelete('set null')
                    ->onUpdate('cascade');

            $table->foreign('session_id')
                    ->references('session_id')
                    ->on('sessions')
                    ->onDelete('set null')
                    ->onUpdate('cascade');

            $table->foreign('module_id')
                    ->references('module_id')
                    ->on('modules')
                    ->onDelete('set null')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('story_detail', function (Blueprint $table) {
            $table->dropcolumn('trainning_id');
             $table->dropcolumn('session_id');
              $table->dropcolumn('module_id');
        });
    }
}
