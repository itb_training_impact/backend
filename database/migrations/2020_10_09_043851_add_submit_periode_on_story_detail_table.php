<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmitPeriodeOnStoryDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

          Schema::table('story_detail', function (Blueprint $table) {
           $table->integer('submit_periode')->default(0)->comment('0: before, 1: during, 2: after');
           
                 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('story_detail', function (Blueprint $table) {
            $table->dropcolumn('submit_periode');
            
        });
    }
}
