<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBehavioursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('behaviours', function (Blueprint $table) {
            $table->bigIncrements('behaviour_id');
            $table->string('behaviour_code', 100);
            $table->text('behaviour_question');
            $table->text('behaviour_answer')->nullable();
            $table->string('behaviour_correct_answer', 199)->nullable();
            $table->bigInteger('type_id')->nullable()->unsigned();
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->bigInteger('trainning_id')->nullable()->unsigned();
            $table->bigInteger('group_id')->nullable()->unsigned();
            $table->integer('behaviour_is_required')->default(0);
            $table->integer('behaviour_is_default')->default(0);
            $table->integer('behaviour_rating_start')->nullable();
            $table->integer('behaviour_rating_finish')->nullable();
            $table->timestamps();

            $table->foreign('type_id')
                ->references('type_id')->on('types')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('group_id')
                ->references('group_id')->on('groups')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('trainning_id')
                ->references('trainning_id')->on('trainnings')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('behaviours');
    }
}
