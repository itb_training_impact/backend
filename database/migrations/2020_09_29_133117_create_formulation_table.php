<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('formulation_group_id')->unsigned();
            $table->string('code', 30);
            $table->string('name', 200);
            $table->tinyInteger('status');
            $table->string('aspek_tertinggi', 500);
            $table->string('aspek_terendah', 500);
            $table->timestamps();

            $table->foreign('formulation_group_id')
            ->references('id')->on('formulation_group')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulation');
    }
}
