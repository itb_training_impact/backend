<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_roles', function (Blueprint $table) {
            $table->bigIncrements('role_id');
            $table->bigInteger('level_id')->unsigned();
            $table->bigInteger('task_id')->unsigned();
            $table->timestamps();

            $table->foreign('level_id')
                ->references('level_id')->on('levels')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('task_id')
                ->references('task_id')->on('sys_tasks')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
