<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReactionLearningAddAnswerTrue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reactions', function (Blueprint $table) {
            if (!Schema::hasColumn('reactions', 'reaction_correct_answer')) {
                $table->string('reaction_correct_answer',191)->nullable()->after('reaction_answer');
            }
        });

        Schema::table('learnings', function (Blueprint $table) {
            if (!Schema::hasColumn('learnings', 'learning_correct_answer')) {
                $table->string('learning_correct_answer',191)->nullable()->after('learning_answer');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reactions', function (Blueprint $table) {
            if (Schema::hasColumn('reactions', 'reaction_correct_answer')) {
                $table->dropColumn('reaction_correct_answer');
            }
        });

        Schema::table('learnings', function (Blueprint $table) {
            if (Schema::hasColumn('learnings', 'learning_correct_answer')) {
                $table->dropColumn('learning_correct_answer');
            }
        });
    }
}
