<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainningDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainning_details', function (Blueprint $table) {
            $table->bigIncrements('trainning_detail_id');
            $table->bigInteger('trainning_id')->unsigned()->nullable();
            $table->integer('trainning_detail_periode')->default(0)->comment('0: before, 1: during, 2: after');
            $table->bigInteger('reaction_id')->nullable()->unsigned();
            $table->bigInteger('learning_id')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('trainning_id')
                ->references('trainning_id')->on('trainnings')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('reaction_id')
                ->references('reaction_id')->on('reactions')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('learning_id')
                ->references('learning_id')->on('learnings')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainning_details');
    }
}
