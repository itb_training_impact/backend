<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration_form', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 20);
            $table->string('label', 50);
            $table->text('description');
            $table->string('name', 50);
            $table->tinyInteger('index');
            $table->string('value', 500);
            $table->text('additional_data');
            $table->tinyInteger('total_column');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registration_form');
    }
}
