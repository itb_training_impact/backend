<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLearnings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('learnings', function (Blueprint $table) {
            $table->bigIncrements('learning_id');
            $table->string('learning_code', 100);
            $table->text('learning_question');
            $table->text('learning_answer')->nullable();
            $table->bigInteger('type_id')->nullable()->unsigned();
            $table->bigInteger('group_id')->nullable()->unsigned();
            $table->integer('learning_is_required')->default(0);
            $table->integer('learning_is_default')->default(0);
            $table->timestamps();

            $table->foreign('type_id')
                ->references('type_id')->on('types')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('group_id')
                ->references('group_id')->on('groups')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('learnings');
    }
}
