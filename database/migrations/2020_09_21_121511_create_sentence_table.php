<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSentenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sentence', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cat_id', 20);
            $table->integer('cat_child_id')->unsigned();
            $table->timestamps();

            $table->foreign('cat_id')
            ->references('code')->on('word_category')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('cat_child_id')
            ->references('id')->on('word_category')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sentence');
    }
}
