<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_tasks', function (Blueprint $table) {
            $table->bigIncrements('task_id');
            $table->bigInteger('module_id')->unsigned();
            $table->string('task', 100);
            $table->timestamps();

            $table->foreign('module_id')
                ->references('module_id')->on('sys_modules')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_tasks');
    }
}
