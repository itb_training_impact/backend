<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStartDateOnTrainningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
         Schema::table('trainnings', function (Blueprint $table) {
            $table->date('start_date')->nullable()->after('trainning_description');
        });
}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trainnings', function (Blueprint $table) {
            $table->dropColumn('start_date');
        });
    }
}
