<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBehaviourIdOnTrainningSubmits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trainning_submits', function (Blueprint $table) {
            $table->bigInteger('behaviour_id')->nullable()->unsigned()->after('learning_id');

            $table->foreign('behaviour_id')
                    ->references('behaviour_id')
                    ->on('behaviours')
                    ->onDelete('set null')
                    ->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trainning_submits', function (Blueprint $table) {
            $table->dropcolumn('behaviour_id');
        });
    }
}
