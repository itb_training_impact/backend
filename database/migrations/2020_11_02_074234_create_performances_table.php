<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {            
        Schema::create('performances', function (Blueprint $table) {
            $table->bigIncrements('performance_id');
            $table->string('performance_code', 100);
            $table->text('performance_question');
            $table->text('performance_answer')->nullable();
            $table->string('performance_correct_answer', 199)->nullable();
            $table->bigInteger('type_id')->nullable()->unsigned();
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->bigInteger('trainning_id')->nullable()->unsigned();
            $table->bigInteger('session_id')->unsigned()->nullable();
            $table->bigInteger('module_id')->unsigned()->nullable();
            $table->bigInteger('group_id')->nullable()->unsigned();
            $table->bigInteger('criteria_id')->nullable()->unsigned();
            $table->integer('performance_is_required')->default(0);
            $table->integer('performance_is_default')->default(0);
            $table->integer('performance_rating_start')->nullable();
            $table->integer('performance_rating_finish')->nullable();
            $table->timestamps();

            $table->foreign('session_id')
                ->references('session_id')->on('sessions')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('module_id')
                ->references('module_id')->on('modules')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('type_id')
                ->references('type_id')->on('types')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('group_id')
                ->references('group_id')->on('groups')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('trainning_id')
                ->references('trainning_id')->on('trainnings')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->foreign('criteria_id')
                ->references('criteria_id')->on('criterias')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performances');
    }
}
