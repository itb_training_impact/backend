<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRotiIdPerformanceIdOnTrainningSubmitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trainning_submits', function (Blueprint $table) {
       $table->bigInteger('performance_id')->unsigned()->nullable()->after('behaviour_id');
        $table->bigInteger('roti_id')->unsigned()->nullable()->after('performance_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn('performance_id');
         $table->dropColumn('roti_id');
    }

}
