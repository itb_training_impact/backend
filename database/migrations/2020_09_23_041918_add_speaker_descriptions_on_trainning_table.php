<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpeakerDescriptionsOnTrainningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::table('trainnings', function (Blueprint $table) {
            $table->text('speaker_descriptions')->nullable()->after('speakers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('trainnings', function (Blueprint $table) {
            $table->dropColumn('speaker_descriptions');
        });
    }
}
